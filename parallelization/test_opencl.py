"""
Created on 23-Dec-15 11:24

@author: MAL
"""

from __future__ import absolute_import

import pyopencl as cl
import numpy as np



if __name__ == '__main__':

    plat = cl.get_platforms()

    platid = 0
    if len(plat) > 1:
        for i in xrange(len(plat)):
            if plat[i].name.find('NVIDIA') >= 0:
                platid = i
                break

    device = plat[platid].get_devices()[0]

    print plat[platid]
    print device

    ctx = cl.Context([device])

    ####

    queue = cl.CommandQueue(ctx)

    a = np.arange(32).astype(np.float32)
    res = np.empty_like(a)

    mf = cl.mem_flags
    a_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a)
    dest_buf = cl.Buffer(ctx, mf.WRITE_ONLY, res.nbytes)

    prg = cl.Program(ctx, """
        __kernel void sq(__global const float *a,
        __global float *c)
        {
          int gid = get_global_id(0);
          c[gid] = a[gid] * a[gid];
        }
        """).build()

    prg.sq(queue, a.shape, None, a_buf, dest_buf)

    cl.enqueue_copy(queue, res, dest_buf)

    print a, res
