# -*- coding: utf-8 -*-
"""
Created on Tue Feb 24 09:52:43 2015

@author: MAL
"""

import numpy as np
from itertools import combinations, chain

from sklearn.cross_validation import _PartitionIterator
from sklearn.preprocessing import LabelEncoder

import logging

__all__ = ['LeaveOneObservationOut']


def make_cv_iterator(folds):

    n_folds = len(folds)
    if n_folds > 1:
        train_folds = map(lambda p: list(chain(*p)), combinations(folds, n_folds-1))[::-1]
        return zip(train_folds, folds)
    else:
        return [(folds[0], [])]


class LeaveOneAnimalOut(_PartitionIterator):
    """docstring for LeaveOneAnimalOut"""
    def __init__(self, animal_index):
        super(LeaveOneAnimalOut, self).__init__()
        self.animal_index = animal_index
        self.n_animals = len(np.unique(animal_index))

    # TODO: implement
    def _iter_test_masks(self):
        pass


class LeaveOneObservationOut(_PartitionIterator):


    def __init__(self, multilabels, test_subjects=None):
        # We make a copy of labels to avoid side-effects during iteration
        super(LeaveOneObservationOut, self).__init__(len(multilabels))
        self.multilabels = np.array(multilabels, copy=True)

        self.unique_observations = np.unique(self.multilabels[:,0])
        self.n_unique_observations = len(self.unique_observations)

        if test_subjects is None:
            self.test_subjects = np.unique(self.multilabels[:, 1])
        elif np.isscalar(test_subjects):
            self.test_subjects = np.asarray([test_subjects])
        else:
            self.test_subjects = np.asarray(test_subjects)
        self.n_test_subjects = len(self.test_subjects)




    def __iter__(self):
        indices = self._indices
        if indices:
            ind = np.arange(self.n)
        for test_index, train_index in self._iter_test_masks():
            if indices:
                train_index = ind[train_index]
                test_index = ind[test_index]
            yield train_index, test_index


    def _iter_test_masks(self):
        for i in self.unique_observations:
            if self.n_test_subjects == 1:
                yield ( (self.multilabels[:,0] == i) & (self.multilabels[:,1] == self.test_subjects), (self.multilabels[:,0] != i)  )
            else:
                yield ( (self.multilabels[:,0] == i) & (np.in1d(self.multilabels[:,1], self.test_subjects)), (self.multilabels[:,0] != i)  )


    def __repr__(self):
        return '%s.%s(labels=%s)' % (
            self.__class__.__module__,
            self.__class__.__name__,
            self.multilabels,
        )

    def __len__(self):
        return self.n_unique_observations


class SamplingCrossValidation(_PartitionIterator):
    """docstring for SamplingCrossValidation"""
    def __init__(self, sampler, X, y, fix_test_set_mask, k=3, indices=True, ensure_all_classes=False, **kwargs):
        super(SamplingCrossValidation, self).__init__(y.shape[0])
        self._sampler_type = sampler
        self._args = kwargs

        self.k = k
        self.sampler = self._sampler_type(**self._args)
        self._X = X
        self._y = y
        self._test_set_mask = np.asarray(fix_test_set_mask)
        self._len_test = np.sum(self._test_set_mask)
        self.indices = indices
        self.ensure_all_classes = ensure_all_classes

        if y.shape[0] != self._test_set_mask.shape[0] or self._test_set_mask.ndim != 1:
            raise AttributeError('Test set mask must be 1-dimensional with same size as y.')


    def __iter__(self):

        if self.indices:
            ind = np.arange(self.n)

        for i in range(self.k):

            train_index = self.sampler.sample(self._X[~self._test_set_mask, :],
                                              self._y[~self._test_set_mask])

            if self.ensure_all_classes:
                # ensure that the first n samples or of the n classes

                le = LabelEncoder().fit(self._y)

                unique_y = set(le.classes_)
                n_classes = len(unique_y)

                y = le.transform(self._y)


                # first ensure that all classes are in sampled training set

                # how many are there of each class? minlength ensures, we get a count for all classes
                n_y = np.bincount(y[~self._test_set_mask][train_index], minlength=n_classes)

                missing_y = set(np.where(n_y == 0)[0])
                unique_y.difference_update(missing_y) # for later

                new_index = []

                if len(missing_y) > 0:

                    for i, label in enumerate(y[~self._test_set_mask]):

                        if label in missing_y:
                            new_index.append(i)
                            missing_y.remove(label)

                        if len(missing_y) == 0:
                            break

                    if len(missing_y) > 0:
                        raise RuntimeError('Training set is missing samples of labels: {}'.format(missing_y))

                # 2) take

                reorder_index = []
                if len(unique_y) > 0:

                    for i, label in enumerate(y[~self._test_set_mask][train_index]):

                        if label in unique_y:
                            reorder_index.append(i)
                            unique_y.remove(label)

                        if len(unique_y) == 0:
                            break

                reorder_index = np.asarray(reorder_index)

                tempcopy = train_index[:len(reorder_index)].copy()
                train_index[:len(reorder_index)] = train_index[reorder_index]

                reorder_except_first_mask = np.array(reorder_index > len(reorder_index), np.bool)

                train_index[reorder_index[reorder_except_first_mask]] = tempcopy[reorder_except_first_mask]

                train_index = np.concatenate( (np.asarray(new_index, np.int), train_index))


            if self.indices: # return indices
                train_index = ind[~self._test_set_mask][train_index]
                test_index = ind[self._test_set_mask]
            else:
                test_index = self._test_set_mask
                train_index_out = ~self._test_set_mask
                train_index_out[~train_index] = False
                train_index = train_index_out

            yield train_index, test_index


    def __len__(self):
        return self.k


    def __repr__(self):
        return '%s.%s(sampler=%s, k=%s)' % (
            self.__class__.__module__,
            self.__class__.__name__,
            self.sampler,
            self.k
        )



class CVSamplingWrapper(_PartitionIterator):
    """
    Wrapping around another CV iterator and sampling the training set before returning the fold indices.

    I.e., for each training/test fold, apply the sampler to the training fold and return the sampled training fold + the (untouched) test fold.

    TODO: needs more comment in code.

    Attributes
    ----------
    base_cv : _PartitionIterator (cross validation iterator)
        Base fold iterator
    ensure_all_classes : bool
        If True, the sampling of the training fold ensures that at least one sample of every class is contained in the returned fold.
    indices : bool
        If True (default), return indices. Otherwise return boolean mask.
    k : int
        Number of fold (derived from base_cv)
    sampler : SamplerMixin
        Sampler to sample training fold
    store_indices : bool
        If True, stores the sampled indices for later, external access in '_training_samples'
    verbose : int
        Verbosity level
    """
    def __init__(self, sampler, X, y, base_cv, indices=True, ensure_all_classes=False,
                 verbose=0, store_indices=False, **kwargs):
        super(CVSamplingWrapper, self).__init__(y.shape[0])

        self._X = X
        self._y = y
        self.k = len(base_cv)
        self.base_cv = base_cv
        self.indices = indices
        self.ensure_all_classes = ensure_all_classes
        self.verbose = verbose
        self.store_indices = store_indices

        self._args = kwargs

        if type(sampler) == type:
            self.sampler = self._sampler_type(**self._args)
        else:
            self.sampler = sampler
        self._sampler_type = type(sampler)


    def __len__(self):
        return len(self.base_cv)


    def __iter__(self):

        logger = logging.getLogger(__name__)

        self._training_samples = None
        self._test_samples = None

        for i, (train_index, test_index) in enumerate(self.base_cv):

            if hasattr(self.sampler, 'return_indices'):
                self.sampler.return_indices = True
                sampled_train_index = self.sampler.sample(self._X[train_index, :],
                                                          self._y[train_index])

                sampled_train_index = train_index[sampled_train_index]
            else:
                sampled_train_mask = self.sampler.sample(self._X[train_index, :],
                                                          self._y[train_index])

                sampled_train_index = train_index[np.where(sampled_train_mask)[0]]
                del sampled_train_mask


            if self.ensure_all_classes:
                # ensure that the first n samples or of the n classes

                le = LabelEncoder().fit(self._y)

                unique_y = set(le.classes_)
                n_classes = len(unique_y)

                y = le.transform(self._y)

                # first ensure that all classes are in sampled training set

                # how many are there of each class? minlength ensures, we get a count for all classes
                n_y = np.bincount(y[sampled_train_index], minlength=n_classes)

                missing_y = set(np.where(n_y == 0)[0])
                unique_y.difference_update(missing_y) # for later

                new_index = []

                if len(missing_y) > 0:

                    for i, label in enumerate(y[train_index]): # TODO: this can be very slow

                        if label in missing_y:
                            new_index.append(i)
                            missing_y.remove(label)

                        if len(missing_y) == 0:
                            break

                    if len(missing_y) > 0:
                        raise RuntimeError('Training set is missing samples of labels: {}'.format(missing_y))

                # 2) take

                reorder_index = []
                if len(unique_y) > 0:

                    for i, label in enumerate(y[sampled_train_index]): # TODO: this can be very slow

                        if label in unique_y:
                            reorder_index.append(i)
                            unique_y.remove(label)

                        if len(unique_y) == 0:
                            break

                reorder_index = np.asarray(reorder_index)


                tempcopy = sampled_train_index[:len(reorder_index)].copy()
                sampled_train_index[:len(reorder_index)] = sampled_train_index[reorder_index]

                reorder_except_first_mask = np.array(reorder_index > len(reorder_index), np.bool)

                sampled_train_index[reorder_index[reorder_except_first_mask]] = tempcopy[reorder_except_first_mask]


                sampled_train_index = np.concatenate( (np.asarray(new_index, np.int), sampled_train_index))

            ######################
            # Iterator output
            ######################

            if self.store_indices:
                if self._training_samples is None:
                    self._training_samples = []

                if self._test_samples is None:
                    self._test_samples = []

                self._training_samples.append(sampled_train_index)
                self._test_samples.append(test_index)

            if self.indices: # return indices
                train_index_out = sampled_train_index
                test_index_out = test_index

                if self.verbose > 0:
                    print '{}/{} ({:.2f}) training samples sampled.'.format(len(train_index_out), len(train_index), len(train_index_out)/float(len(train_index)))
            else:
                test_index_out = np.zeros_like(y, dtype=np.bool)
                test_index_out[test_index] = True
                #test_index = test_index_out

                train_index_out = np.zeros_like(y, dtype=np.bool)
                train_index_out[sampled_train_index] = True
                #train_index = train_index_out

            yield train_index_out, test_index_out
