#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-02-11 12:06:04
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-10-24 12:38:34


import numpy as np
import pandas as pd
import logging

from sklearn.base import BaseEstimator
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.decomposition.pca import PCA

from MLUtilities.annotation.utils import add_segment_ids
from MLUtilities.models.stats import segment_sampling, shuffle_per_segment
from MLUtilities.models.stats import shuffle_segments_per_class
from MLUtilities.utils import gradual_balanced_subset


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def _get_segment_structure_dataframe(y):
    '''
    create DataFrame containing all frames grouped by action and segments

    '''
    if not isinstance(y, (pd.Series, np.ndarray)):
        raise TypeError('y must be array-like but it of type {}.'.format(type(y)))

    if isinstance(y, pd.Series):
        dfs = pd.DataFrame(data=LabelEncoder().fit_transform(y.values), columns=['label'])
    else:
        dfs = pd.DataFrame(data=LabelEncoder().fit_transform(y), columns=['label'])
    dfs = add_segment_ids(dfs, action_col='label')
    dfs = dfs.reset_index().set_index(['label', 'segid'], drop=True).sortlevel(0)

    dfs['frameid'] = np.arange(dfs.shape[0])

    dfs = dfs.set_index('frameid', append=True)

    return dfs


def _append_segment_prob_dist(dfs, p_dist, col_name='p_dist', normalize=True, interpolation_method='index'):
    """
    For each segment in dfs, fill the column 'col_name' with the discrete probability mass function p_dist.

    The pmf is interpolated of the segment is longer than p_dist, or compressed if it is shorter.

    The resulting pmf is normalized per segment to sum to 1.

    Parameters
    ----------
    dfs : pd.DataFrame
        Input DataFrame containing action event structured by segments (see _get_segment_structure_dataframe(y))
    p_dist : array-like, 1D
        Probability mass function
    col_name : str, optional
        Output column name
    interpolation_method : str, optional, default = 'nearest'
        Method to use for interpolation (see pandas.Series.interpolate)

    Returns
    -------
    pd.DataFrame
        dfs is appended column
    """

    p_dist = np.ravel(np.asarray(p_dist))

    dfs = dfs.copy()
    dfs[col_name] = 0.

    dp = pd.Series(index=np.linspace(0,1,p_dist.shape[0]), data=p_dist)

    def _fill_p_dist(x):
        N = x.shape[0]

        if N == 1:
            # special case of 1-frame long segments: assign mean of p_dist to that sample
            return np.mean(dp)

        dps = dp.copy()
        # new index from 1 to N;
        dps.index = dps.index * (N-1) + 1
        # reindex to add the values in between 1 and N; values are interpolateds:
        dps = dps.reindex(np.arange(1,N+1)).interpolate(method=interpolation_method)
        dps += 1e-3
        if normalize:
            return dps.values / dps.sum()
        else:
            return dps.values

    dfs[col_name] = dfs.groupby(level='segid')[col_name].transform( _fill_p_dist )

    return dfs


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class SamplerMixin(BaseEstimator):
    """
    "Abstract" base class for all samplers.
    """
    def __init__(self):
        super(SamplerMixin, self).__init__()

    def sample(self, X, y=None):
        """
        Samples from input set X with optional label information y.

        Parameters
        ----------
        X : array-like
            Data (n_samples, n_features)
        y : array-like, optional
            Labels (n_samples,)

        Returns
        -------
        np.ndarray
            Binary mask of (n_samples,).
        """
        return np.ones(X.shape[0], dtype=np.bool)


class DummySampler(SamplerMixin):
    """
    Dummy sampler that returns all samples (mask of all 1s)
    """
    def __init__(self):
        super(DummySampler, self).__init__()

    def sample(self, X, y=None):
        return np.ones(X.shape[0], dtype=np.bool)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class SamplerPipeline(SamplerMixin):
    """
    Pipeline concatenating multiple samplers.

    Attributes
    ----------
    return_indices : bool
        Return sample indices instead of boolean mask.
    samplers : list-like
        List of samplers
    """
    def __init__(self, samplers, return_indices=False):
        super(SamplerPipeline, self).__init__()
        self.samplers = samplers
        self.return_indices = return_indices


    def sample(self, X, y=None):

        X = np.asarray(X)
        if y is not None:
            y = np.asarray(y)

        if self.return_indices:

            idx = np.arange(X.shape[0], dtype=np.int)

            for sampler in self.samplers:

                if hasattr(sampler, 'return_indices'):
                    sampler.return_indices = True
                else:
                    raise AttributeError('If return_indices is True all samplers in the Pipeline must support returning indices but {} does not.'.format(sampler))

                ys = None if y is None else y[idx]
                new_idx = sampler.sample(X[idx], ys)
                idx = idx[new_idx]

            return idx

        else:
            idx = np.arange(X.shape[0], dtype=np.int)

            for sampler in self.samplers:
                if hasattr(sampler, 'return_indices'):
                    sampler.return_indices = False

                ys = None if y is None else y[idx]
                new_mask = sampler.sample(X[idx], ys)
                idx = idx[new_mask]

            mask = np.zeros(X.shape[0], dtype=np.bool)
            mask[idx] = True

            return mask

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class RandomSampler(SamplerMixin):
    """
    Uniform sampling of either a ratio of the input set or a discrete number of samples.

    Attributes
    ----------
    n : int, float
        If n is a float between (0,1), the n fraction of the input set is sampled.
        If n is an integer >= 1, exactly n samples are sampled.
    seed : int
        Seed for random generator.
    return_indices : bool (default False)
        If True sample() returns the indices of the samples instead of a boolean mask.
    """
    def __init__(self, n, seed=None, return_indices=False):
        super(RandomSampler, self).__init__()
        self.seed = seed
        self.return_indices = return_indices

        if n <= 0:
            raise ValueError("n must be great than 0 but is {}.".format(n))
        self.n = n

    def sample(self, X, y=None):

        np.random.seed(self.seed)

        N = X.shape[0]
        if isinstance(self.n, float) and self.n <= 1:
            k = min(N, int(np.round(self.n*N)))
        else:
            k = min(N, int(np.round(self.n)))
        m = N-k

        if self.return_indices:
            a = np.random.randint(0, N, k)
        else:
            # return boolean mask
            a = np.hstack( (np.ones(k, dtype=bool), np.zeros(m, dtype=bool)) )
            np.random.shuffle(a)
        return a



class RandomConsecutiveSegmentSampler(SamplerMixin):
    """
    Uniform sampling of either a ratio of the input set or a discrete number of samples.

    Attributes
    ----------
    n : int, float
        If n is a float between (0,1), the n fraction of the input set is sampled.
        If n is an integer >= 1, exactly n samples are sampled.
    seed : int
        Seed for random generator.
    return_indices : bool (default False)
        If True sample() returns the indices of the samples instead of a boolean mask.
    """
    def __init__(self, n, c, seed=None, return_indices=False):
        super(RandomConsecutiveSegmentSampler, self).__init__()
        self.seed = seed
        self.return_indices = return_indices

        if n <= 0:
            raise ValueError("n must be great than 0 but is {}.".format(n))
        self.n = n
        self.c = c

    def sample(self, X, y=None):

        np.random.seed(self.seed)

        ind = np.arange(X.shape[0])[::self.c]

        N = ind.shape[0]
        if isinstance(self.n, float) and self.n <= 1:
            k = min(N, int(np.round(self.n*N)))
        else:
            k = min(N, int(np.round(self.n)))

        np.random.shuffle(ind)

        ind = np.add(np.tile(ind[:k], (self.c, 1)),
                     np.arange(self.c)[:, np.newaxis])\
                .reshape(k*self.c, order='F')

        if self.return_indices:
            return ind
        else:
            a = np.zeros(X.shape[0], dtype=bool)
            a[ind] = True
            return a




# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class EquidistantSampler(SamplerMixin):
    """
    Samples the input set in equidistance steps (take every n'th sample).

    Attributes
    ----------
    n : int
        Step size
    return_indices : bool (default False)
        If True return indices of samples instead of boolean mask.
    seed : int
        Seed for random generator.
    start : int, default 0
        Offset of first item to sample.
    """
    def __init__(self, n, start=0, return_indices=False, seed=None):
        super(EquidistantSampler, self).__init__()

        if not isinstance(n, int):
            raise TypeError("n must be integer but is {}.".format(type(n)))
        if n <= 0:
            raise ValueError("n must be greater than 0 but is {}.".format(n))

        if not isinstance(start, int):
            raise TypeError("start must be integer but is {}.".format(type(start)))
        if start < 0:
            raise ValueError("start must be greater or equal than 0 but is {}.".format(start))

        self.n = n
        self.start = start
        self.return_indices = return_indices
        self.seed = seed

    def sample(self, X, y=None):
        np.random.seed(self.seed)
        N = X.shape[0]
        a = np.zeros(N, dtype=np.bool)
        a[self.start::self.n] = True

        if self.return_indices:
            a = np.where(a)[0]

        return a


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class PPerSegmentSampler(SamplerMixin):
    """
    Sampling such that p percent of each segment are sampled.

    At least one sample per segment is sampled. Samples per segment are chosen either
    randomly (uniform probability) or in equidistant steps starting with the first sample of a segment.

    Note that if indices are returned, indices are in random order.

    Attributes
    ----------
    n : int, float
        If n is a float between (0,1), the n percent of each segment is sampled.
        If n is an integer >= 1, every n'th item of each segment is sampled.
    return_indices : bool (default False)
        If True return indices of samples instead of boolean mask.
    seed : int
        Seed for random generator.
    uniform_random : bool, default True
        Deprecated. Specifying n as either float or int decides whether to return random samples or equidistant samples.

    """
    def __init__(self, n, p_dist=None, return_indices=False, seed=None):
        super(PPerSegmentSampler, self).__init__()
        self.n = n
        self.return_indices = return_indices
        self.seed = seed
        self.p_dist = p_dist

        if not isinstance(self.n, (int, float)) or self.n <= 0:
            raise ValueError('n must be a positive number > 0 but is {}'.format(n))

        if isinstance(self.n, float) and self.n > 1:
            raise ValueError('If n is a float, n represents a fraction and must be between 0 and  1, but n is {}.'.format(self.n))


    def _get_first_n(self, df, n):
        return df.iloc[: max(1, int(np.round(len(df)*n,0))) ]

    def _get_every_nth(self, df, n):
        return df.iloc[ ::n ]


    def sample(self, X, y):

        np.random.seed(self.seed)

        dfs = _get_segment_structure_dataframe(y)

        if isinstance(self.n, float):
            # sample randomly

            if self.p_dist is None: # uniform sampling

                # shuffle samples in every segment, then pick first n sampels of every segment
                dfs = shuffle_per_segment(dfs)
            else:
                # add column to dfs representing the probabilities for picking a particular sample
                dfs = _append_segment_prob_dist(dfs, self.p_dist)

                # shuffle segments according to probability mass p_dist
                dfs = shuffle_per_segment(dfs, uniform=False, w_col='p_dist')
                dfs = dfs.drop('p_dist', axis=1)

                # TODO: if n is larger than p_dist would allow (binary mask), then we also select the very unprobable samples here. I think it would be good to restrict this if p_dist is almost binary.

            idx = dfs.groupby(level='segid').apply(self._get_first_n, n=self.n).values
        else:
            # otherwise pick samples in equidistant steps starting with sample 0 in every segment
            idx = dfs.groupby(level='segid').apply(self._get_every_nth, n=self.n).values

        if self.return_indices:
            np.random.shuffle(idx) # shuffle all
            return idx.ravel()
        else:
            N = X.shape[0]
            a = np.zeros(N, dtype=np.bool)
            a[idx] = True
            return a


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class PerSegmentFilter(SamplerMixin):
    """
    Filters each segment in y given the mask pattern.
    The mask pattern is scaled according to the segment length.

    Attributes
    ----------
    mask : array-like, boolean
        Pattern for selecting samples in segments, e.g., [0,0,1,1,0,0] selects the two thirds of the samples in the segment center.
    return_indices : bool
        If True return indices instead of boolean mask.
    """
    def __init__(self, mask, return_indices=False):
        super(PerSegmentFilter, self).__init__()
        self.mask = mask
        self.return_indices = return_indices

    def sample(self, X, y):

        dfs = _get_segment_structure_dataframe(y)
        dfs = _append_segment_prob_dist(dfs, self.mask,
                                        normalize=False,
                                        interpolation_method='nearest')
        dfs['p_dist'] = np.round(dfs['p_dist'].values, 0).astype(np.bool)

        idx = dfs.loc[dfs['p_dist'], 'index'].values
        if self.return_indices:
            return idx
        else:
            a = np.zeros_like(y, dtype=np.bool)
            a[idx] = True
            return a


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class BalancingSampler(SamplerMixin):
    """
    Sampling such that the class distribution is balanced by factor n.

    If n is 0, no balancing is performed and sample() returns all data.
    If n is 1, sample() returns a fully balanced sample of (X,y) in which all classes are represented by the same amount of data.
    If 0 < n < 1, sample() returns a by factor n gradually balanced sample of (X,y).

    The factor n specifies a linear relationship between the maximum number of items per class (0.0) and the minimum number of items per class (1.0). The minimum is determined by the smallest class in the set.

    Attributes
    ----------
    n : float, default = 1.0
        Linear balancing factor
    """
    def __init__(self, n=1.0, return_indices=False):
        super(BalancingSampler, self).__init__()

        self.return_indices = return_indices

        if not isinstance(n, (float, int)):
            raise TypeError("n must be numeric but is {}.".format(type(n)))
        if n < 0 or n > 1:
            raise ValueError("n must be between 0 and 1 but is {}.".format(n))

        self.n = n

    def sample(self, X, y):
        idx = gradual_balanced_subset(X, y, balance_factor=self.n, return_indices=True)

        if not self.return_indices:
            N = X.shape[0]
            a = np.zeros(N, dtype=np.bool)
            a[idx] = True
            return a

        return idx


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class SegmentRandomSampler(SamplerMixin):
    """
    can't remember what it does...

    Attributes
    ----------
    n : TYPE
        Description
    seed : TYPE
        Description
    """
    def __init__(self, n, seed=None):
        super(SegmentRandomSampler, self).__init__()

        if n <= 0:
            raise ValueError("n must be greater than 0 but is {}.".format(n))

        self.n = n
        self.seed = seed


    def sample(self, X, y):

        N = X.shape[0]
        if self.n < 1:
            J_samples = min(N, int(np.round(self.n*N)))
        else:
            J_samples = min(N, int(np.round(self.n)))

        dfs = _get_segment_structure_dataframe(y)

        # print dfs

        dfs = shuffle_per_segment(dfs, 'segid', 'frameid', uniform=True)
        dfs = shuffle_segments_per_class(dfs, 'segid', 'label')

        # print dfs

        segment_set = dfs.groupby(level='label').apply(segment_sampling, return_values=True).to_dict()

        # print segment_set

        mask = np.zeros(N, dtype=np.bool)

        # total number of samples per label
        # k = np.bincount(y, minlength=len(dfs))
        maxN = max(map(len, segment_set.values()))

        for k, l in segment_set.viewitems():
            if len(l) == maxN:
                continue
            m = maxN - len(l)
            segment_set[k] = np.concatenate( (np.array(l), np.ones(m, dtype=np.int)*-1) )
        segment_array = np.stack(segment_set.values(), axis=1)

        mask[ segment_array[segment_array != -1][:J_samples] ] = True

        return mask


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class FullSegmentSampler(SamplerMixin):
    """
    Sample entire segments at random.

    Attributes
    ----------
    n : int, float
        Absolute number of segments to sample or fraction of total number.
    return_indices : bool
        If True return indices of samples rather than boolean mask.
    seed : int
        Seed for random generator
    """
    def __init__(self, n, return_indices=False, seed=None):
        super(FullSegmentSampler, self).__init__()
        self.n = n
        self.return_indices = return_indices
        self.seed = seed

    def sample(self, X, y):

        if y is None:
            raise ValueError('FullSegmentSampler needs labels but y is None.')

        np.random.seed(self.seed)

        X = np.asarray(X)
        y = np.asarray(y)

        dfs = _get_segment_structure_dataframe(y)

        dfs = dfs.reset_index(drop=False).set_index(['segid', 'frameid']).sort_index(level='segid')

        segments = dfs.index.get_level_values('segid').unique()
        np.random.shuffle(segments)

        N = len(segments)
        if isinstance(self.n, float):
            k = int(N*min(max(0., self.n), 1.))  # make sure between 0 and N
        else:
            k = min(max(0, self.n), N)  # make sure between 0 and N

        if self.return_indices:
            return dfs.loc[(segments[:k], slice(None)), 'index'].values
        else:
            mask = np.zeros_like(y, dtype=bool)
            mask[dfs.loc[(segments[:k], slice(None)), 'index'].values] = True
            return mask

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class BalancedFullSegmentSampler(SamplerMixin):
    """
    Sample n entire segments per class at random.

    Attributes
    ----------
    balancing_behavior : str
        Determines the balancing behavior if there are not enough segments of each class (< n). Choices are {'min', 'max'}.

        If 'min', sample only as many segments per class as are available in the smallest class. Resulting set is balanced.

        If 'max', sample n segments per class with smaller classes being represented by less samples. Resulting set may not be balanced.
    n : int
        Absolute number of segments to sample per class.
    return_indices : bool
        If True return indices of samples rather than boolean mask.
    seed : int
        Seed for random generator
    """
    def __init__(self, n, balancing_behavior='max', return_indices=False,
                 seed=None):
        super(BalancedFullSegmentSampler, self).__init__()
        self.n = n
        self.return_indices = return_indices
        self.seed = seed
        self.balancing_behavior = balancing_behavior

    def sample(self, X, y):

        if y is None:
            raise ValueError('FullSegmentSampler needs labels but y is None.')

        np.random.seed(self.seed)

        X = np.asarray(X)
        y = np.asarray(y)

        dfs = _get_segment_structure_dataframe(y)

        dfs = dfs.reset_index(drop=False).set_index(['segid', 'frameid']).sort_index(level='segid')

        # segments = dfs.index.get_level_values('segid').unique()
        # np.random.shuffle(segments)

        # count the available segments per class
        labels, counts = np.unique(dfs['label'].values, return_counts=True)

        # determine max n to sample depending on chosen behavior
        if self.balancing_behavior == 'min':
            n_max = np.min(counts)  # max is min(classes)
        else:
            n_max = self.n  # max is as much as possible


        # sample per class
        if self.return_indices:

            # matrix for storing selected segment
            #  (default value -1 means: no selection)
            selected_segments = np.ones((n_max, len(labels)),
                                        dtype=np.int) * (-1)

            for i, l in enumerate(labels):
                segments = dfs.loc[dfs['label'] == l].index.get_level_values('segid').unique()

                np.random.shuffle(segments)

                # select first n_max segments, or maximum all
                selected_segments[:min(n_max, len(segments)), i] = segments[:n_max]

            # flatten matrix of selected segments such that order of labels is [0,1,2, ... 0,1,2, ..., 0,1,2, ...]
            return_idx = selected_segments.ravel()

            # remove (-1)'s from the selected index list
            return_idx = return_idx[np.where(return_idx >= 0)[0]]

            # convert segments to frame indices
            #  reindex is necessary because .loc[] sorts by index again but
            #  we want it in the specific order of return_idx
            return dfs.loc[(return_idx, slice(None)), 'index'].reindex(return_idx, level=0).values

        else:
            # if we return a mask, we cannot control the order of selected segments, so it's a bit easier
            mask = np.zeros_like(y, dtype=np.bool)
            for l in labels:
                segments = dfs.loc[dfs['label'] == l].index.get_level_values('segid').unique()

                np.random.shuffle(segments)

                # convert segments to frame mask
                mask[dfs.loc[(segments[:n_max], slice(None)), 'index'].values] = True

            return mask


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class MaxOccurrenceSampler(SamplerMixin):
    """
    Sample a maximum number of samples per label randomly.

    For each label, randomly sample `n` samples.

    If return_indices is True, the returned set is ordered such that any sub-sample of the first k*n_labels is balanced. That is the labels in the returned set are, for example, [0,1,2,3, 0,1,2,3, 0,1,2,3, ...].

    Attributes
    ----------
    n : int, str
        Number of samples per label or 'min'. If n == 'min', take the smallest class as reference. Then the returned sample is guaranteed to be balanced. Note that this requires that there is at least one sample of each class.
    return_indices : bool
        If True return indices of samples rather than boolean mask.
    seed : int
        Seed for random generator
    """
    def __init__(self, n, return_indices=False, seed=None):
        super(MaxOccurrenceSampler, self).__init__()

        if n != 'min':
            if not isinstance(n, int):
                raise TypeError('n must be integer but is {}'.format(type(n)))
            if n <= 0:
                raise ValueError('n must be positive integer but is {}'.format(n))

        self.n = n
        self.return_indices = return_indices
        self.seed = seed

    def sample(self, X, y):
        """
        Sample data X given labels y.

        Parameters
        ----------
        X : array-like (n_samples, n_features)
            Data
        y : array-like (n_samples, )
            Labels

        Returns
        -------
        np.ndarray
            Sample mask (or indices if return_indices == True)
        """

        if y is None:
            raise ValueError('MaxOccurrenceSampler needs labels but y is None.')

        np.random.seed(self.seed)

        X = np.asarray(X)
        y = np.asarray(y)

        labels, counts = np.unique(y, return_counts=True)

        if self.n == 'min':
            n_max = np.min(counts)
        else:
            n_max = self.n

        if self.return_indices:

            selected_indices = np.ones((n_max, len(labels)), dtype=np.int)*(-1)

            for i, l in enumerate(labels):
                label_indices = np.where(y == l)[0]
                np.random.shuffle(label_indices)

                try:
                    selected_indices[:min(n_max, len(label_indices)), i] = label_indices[:n_max]
                except:
                    raise

            return_idx = selected_indices.ravel()
            return return_idx[np.where(return_idx >= 0)[0]]

        else:
            mask = np.zeros_like(y, dtype=np.bool)
            for l in labels:
                label_indices = np.where(y == l)[0]
                np.random.shuffle(label_indices)
                mask[label_indices[:n_max]] = True
            return mask


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class GreedyAntiRedundanceSampler(SamplerMixin):
    """
    Greedily removes neighbouring samples that are closer to each other than a given threshold.

    Iterative removal of samples:
      - pick a random sample
      - compute its distance to all other samples of the same label
      - remove all other samples whose distance < eps

    Attributes
    ----------
    eps : float
        Radius of neighbourhood
    pca_components : int, float, or None
        If int, compute as many PCA components before sampling.
        If float, compute as many PCA components so as to keep p percent of variance.
        If None, do not perform PCA before sampling.
    return_indices : bool, default False
        If True, return the left over indices. Otherwise return boolean mask.
    seed : int
        Seed for random generator.
    verbose : int
        If > 0, print additional information.
    """
    def __init__(self, eps, pca_components=0.95, return_indices=False,
                 verbose=0, seed=None):
        super(GreedyAntiRedundanceSampler, self).__init__()

        if eps < 0:
            raise ValueError('Threshold parameter eps must be positive but is {}.'.format(eps))
        self.eps = eps
        self.return_indices = return_indices
        self.pca_components = pca_components
        self.verbose = verbose
        self.seed = seed


    def sample(self, X, y, return_weights=False):
        """
        Sample data X given labels y.

        Parameters
        ----------
        X : array-like (n_samples, n_features)
            Data
        y : array-like (n_samples, )
            Labels

        Returns
        -------
        np.ndarray
            Sample mask (or indices if return_indices == True)
        """
        logger = logging.getLogger(__name__)

        np.random.seed(self.seed)

        X = np.asarray(X)
        y = np.asarray(y)

        if self.verbose > 0:
            print 'Variance of input data = {:.3f}'.format(np.var(X))


        mask = np.ones_like(y, dtype=np.bool)
        labels = np.unique(y)

        # if self.label_sensitive:
        #     # compute label distribution
        #     label_dist = np.unique(y, return_counts=True)[1]
        #     label_dist = label_dist/float(y.shape[0])
        # else:
        #     label_dist = np.ones(len(labels))

        if self.pca_components != False:
            self._pca = PCA(self.pca_components, whiten=True)
            X = self._pca.fit_transform(X)
            logger.debug('PCA fitted with {} output components.'.format(self._pca.n_components_))
        else:
            X = StandardScaler().fit_transform(X)
            logger.debug('I z-scaled the input data because PCA was disabled.')

        if return_weights:
            weights = np.ones_like(y, dtype=np.float)


        for k, l in enumerate(labels):

            # remember original indices of this label's samples for later
            index_label = np.where(y == l)[0]
            keep_mask = np.ones_like(index_label, dtype=np.bool)
            for i in np.random.permutation(index_label.shape[0]):
                if keep_mask[i] == False: # skip this sample if it's already been removed
                    continue

                # compute distances between current sample and all other samples of the same label
                #  (compute only of the other samples that are still there)
                D = np.squeeze(pairwise_distances(X[index_label[i], np.newaxis],
                X[index_label[keep_mask], :])) # --> (|l|)

                # find neighbours
                # idx = np.where( D < self.eps/(1-label_dist[k]/3.) )[0].tolist() # values in range:
                # [0 .. (|l|-1)]
                idx = np.where( D < self.eps/.6 )[0].tolist() # values in range: [0 .. (|l|-1)]

                # set the removed samples to False (result idx is idx of the currently true elements in keep_mask)
                keep_mask[ np.where(keep_mask)[0][idx] ] = False
                keep_mask[ i ] = True # keep current sample as it will have been set to False in the row above

                if return_weights and len(idx) > 1:
                    weights[index_label[i]] = len(idx)-1 # -1 for oneself

            mask[index_label] = keep_mask

            if return_weights:
                # some arbitrary computation of weight
                weights[index_label] = .5 + 5.*weights[index_label] / np.sum(weights[index_label])

        if self.return_indices:
            # recover indices where mask is True
            ret = np.where(mask)[0]
        else:
            ret = mask

        if return_weights:
            return (ret, weights)

        return ret


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
