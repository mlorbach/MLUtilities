# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-08-17 12:22:38
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 13:02:49

import numpy as np
import pandas as pd
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.utils import column_or_1d
from sklearn.utils.validation import check_is_fitted
from sklearn.preprocessing import LabelEncoder as skLabelEncoder
from ..utils import rolling_mode


class LabelEncoderExtra(skLabelEncoder):
    """
    Sub-class of sklearn's LabelEncoder with the additional ability to
    map special classes to negative integer labels.

    Special classes are not part of the usual class set, so code using
    sklearn's implementation should work without differences when
    using this version.

    There are two pre-defined special classes 'Other', 'Uncertain',
    and 'Unknown', but these can be changed upon initialization.

    Why is this useful?

    Sometimes, e.g. in semi-supervised learning, we have data points
    with labels that are not part of the usual class set. For instance,
    data points can be unlabeled or have a special "uncertain" label.

    To be able to deal with those instances without disrupting the
    normal training/classification workflow (the classifier only cares
    about the actual classes not about unknown labels), this LabelEncoder
    implementation was written.

    Attributes
    ----------
    classes_ : np.ndarray
        Original class names (n_classes)
    special_classes : np.ndarray
        Special class names (n_special_classes)
    """
    def __init__(self,
                 special_classes=['Other', 'Uncertain', 'Unknown']):
        super(LabelEncoderExtra, self).__init__()
        self.special_classes = np.asarray(sorted(special_classes))
        self._le_special = skLabelEncoder().fit(self.special_classes)
        self._special_labels = -(1 +
                                 self._le_special.transform(self.special_classes))

    def fit(self, y):

        y = column_or_1d(y, warn=True)
        self.classes_ = np.asarray([c
                                    for c in np.unique(y)
                                    if c not in self.special_classes])
        return self

    def fit_transform(self, y):
        return self.fit(y).transform(y)

    def transform(self, y):

        check_is_fitted(self, 'classes_')
        classes = [c
                   for c in np.unique(y)
                   if c not in self.special_classes]

        if len(np.intersect1d(classes, self.classes_)) < len(classes):
            diff = np.setdiff1d(classes, self.classes_)
            raise ValueError("y contains new labels: %s" % str(diff))

        y = np.asarray(y)
        y_out = np.zeros_like(y, dtype=np.int)
        ex_special_mask = np.in1d(y, self.special_classes, invert=True)
        y_out[ex_special_mask] = np.searchsorted(self.classes_,
                                                 y[ex_special_mask])

        # special labels are -1, -2, -3, ...
        y_out[~ex_special_mask] = -(1 +
                                    self._le_special.transform(y[~ex_special_mask]))

        return y_out

    def inverse_transform(self, y):

        check_is_fitted(self, 'classes_')

        diff = np.setdiff1d(y, np.arange(-len(self._le_special.classes_),
                                         len(self.classes_)))
        if diff:
            raise ValueError("y contains new labels: %s" % str(diff))
        y = np.asarray(y)

        ex_special_mask = np.in1d(y, self._special_labels, invert=True)
        y_out = np.empty_like(y, dtype=object)
        y_out[ex_special_mask] = self.classes_[y[ex_special_mask]]
        y_out[~ex_special_mask] = self._le_special.inverse_transform(-y[~ex_special_mask] - 1)
        return y_out


class LabelMergeTransform(BaseEstimator, TransformerMixin):

    def __init__(self, labels_to_merge, group_label=None):
        self.labels_to_merge_ = labels_to_merge

        if group_label is not None:
            self.group_label_ = group_label
        elif len(self.labels_to_merge_) > 0:
            self.group_label_ = self.labels_to_merge_[0]
        else:
            self.group_label_ = 'merged'

    def fit(self, y=None):
        return self

    def fit_transform(self, y):
        return self.transform(y)

    def transform(self, y):

        y = column_or_1d(y, warn=True).copy()
        n_labels = len(self.labels_to_merge_)

        if n_labels == 0:
            return y
        elif n_labels == 2 and self.group_label_ == self.labels_to_merge_[0]:
            y[ y==self.labels_to_merge_[1] ] = self.group_label_
        else:
            mask = np.in1d( y, self.labels_to_merge_ )
            y[ mask ] = self.group_label_

        return y


class LabelEncoderUnsorted(BaseEstimator, TransformerMixin):

    def _check_fitted(self):
        if not hasattr(self, "classes_"):
            raise ValueError("LabelEncoder was not fitted yet.")

    def fit(self, y):
        """Fit label encoder

        Parameters
        ----------
        y : array-like of shape (n_samples,)
            Target values.

        Returns
        -------
        self : returns an instance of self.
        """
        y = column_or_1d(y, warn=True)
        _, idx = np.unique(y, return_index=True)
        self.classes_ = np.asarray([y[i] for i in sorted(idx)]) # create class list in the order of occurrence in y
        return self

    def fit_transform(self, y):
        """Fit label encoder and return encoded labels

        Parameters
        ----------
        y : array-like of shape [n_samples]
            Target values.

        Returns
        -------
        y : array-like of shape [n_samples]
        """

        self.fit(y)
        return self.transform(y)

    def transform(self, y):
        """Transform labels to normalized encoding.

        Parameters
        ----------
        y : array-like of shape [n_samples]
            Target values.

        Returns
        -------
        y : array-like of shape [n_samples]
        """

        self._check_fitted()

        classes = np.unique(y)
        if len(np.intersect1d(classes, self.classes_)) < len(classes):
            diff = np.setdiff1d(classes, self.classes_)
            raise ValueError("y contains new labels: %s" % str(diff))

        return LabelReplaceTransform(self.classes_, range(len(self.classes_))).transform(y)

    def inverse_transform(self, y):
        """Transform labels back to original encoding.

        Parameters
        ----------
        y : numpy array of shape [n_samples]
            Target values.

        Returns
        -------
        y : numpy array of shape [n_samples]
        """
        self._check_fitted()

        y = np.asarray(y)
        return self.classes_[y]


class LabelReplaceTransform(BaseEstimator, TransformerMixin):

    def __init__(self, label_to_replace, replace_by):
        self.label_to_replace_ = label_to_replace
        self.replace_by_ = replace_by


    def fit(self, y=None):
        return self

    def fit_transform(self, y):
        return self.transform(y)

    def transform(self, y):

        if isinstance(y, (pd.DataFrame, pd.Series)):
            return y.replace(to_replace=self.label_to_replace_.tolist(), value=self.replace_by_)
        else:
            y = column_or_1d(y, warn=True).astype('object')

            if isinstance(self.label_to_replace_, (list, tuple, np.ndarray)) and len(self.label_to_replace_) > 0:
                newtype = type(self.replace_by_[0])
                for (src, dst) in zip(self.label_to_replace_, self.replace_by_):
                    y[y == src] = dst
            else:
                newtype = type(self.replace_by_)
                y[y == self.label_to_replace_] = self.replace_by_

            try:
                y = y.astype(newtype, copy=False)
            except ValueError:
                pass

            return y


class DummyTransformer(BaseEstimator, TransformerMixin):
    '''
    Doesn't do anything. Provides the standard Transformer API.
    '''

    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def fit_transform(self, X, y=None):
        return self.transform(X, y)

    def transform(self, X, y=None):
        return X


class ModeSmoother(BaseEstimator, TransformerMixin):
    '''
    Smoothes a label sequence using a rolling mode operation.
    '''

    def __init__(self, win=1):
        self.win = win
        self.centered = True

    def fit(self, y=None):
        self.win = np.int(np.round(1./((np.diff(y, axis=0) != 0).sum() / float(2*y.shape[0]))))
        self.win += (self.win % 2) - 1
        return self

    def fit_transform(self, y):
        return self.fit(y).transform(y)

    def transform(self, y):
        return rolling_mode(y, self.win, self.centered)
