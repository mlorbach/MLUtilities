# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-08-06 13:49:44
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-12-19 16:38:49

import numpy as np
from scipy import sparse

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import check_array
from sklearn.utils.validation import check_is_fitted, FLOAT_DTYPES
from sklearn.preprocessing.data import _handle_zeros_in_scale


VALUEERROR_MSG_1D = (
    "Passing 1d arrays as data is wrong. Reshape your data either using "
    "X.reshape(-1, 1) if your data has a single feature or "
    "X.reshape(1, -1) if it contains a single sample."
)


class RobustMinMaxScaler(BaseEstimator, TransformerMixin):
    """docstring for RobustMinMaxScaler"""
    def __init__(self, range_min=0, range_max=1, copy=True):
        super(RobustMinMaxScaler, self).__init__()
        self.range_min = range_min
        self.range_max = range_max
        self.copy = copy

    def fit(self, X, y=None):
        """Compute the minimum and maximum to be used for later scaling.

        Parameters
        ----------
        X : array-like, shape [n_samples, n_features]
            The data used to compute the mean and standard deviation
            used for later scaling along the features axis.

        y : Passthrough for ``Pipeline`` compatibility.
        """
        feature_range = (self.range_min, self.range_max)
        if feature_range[0] >= feature_range[1]:
            raise ValueError("Minimum of desired feature range must be smaller"
                             " than maximum. Got %s." % str(feature_range))
        if sparse.issparse(X):
            raise TypeError("RobustMinMaxScaler cannot be fitted on sparse inputs")
        X = check_array(X, copy=self.copy, ensure_2d=False, warn_on_dtype=True,
                        estimator=self, dtype=FLOAT_DTYPES)

        if X.ndim == 1:
            raise ValueError(VALUEERROR_MSG_1D)

        # here we take the 5% and 95% percentiles instead of the min/max over all samples
        data_min, data_max = np.percentile(X, (5, 95), axis=0)

        self.n_samples_seen_ = X.shape[0]

        data_range = data_max - data_min
        self.scale_ = ((feature_range[1] - feature_range[0]) /
                       _handle_zeros_in_scale(data_range))
        self.min_ = feature_range[0] - data_min * self.scale_
        self.data_min_ = data_min
        self.data_max_ = data_max
        self.data_range_ = data_range
        return self

    def transform(self, X, y=None):
        """Center and scale the data

        Parameters
        ----------
        X : array-like
            The data used to scale along the specified axis.
        """
        check_is_fitted(self, 'scale_')

        X = check_array(X, copy=self.copy, ensure_2d=False, dtype=FLOAT_DTYPES)
        if X.ndim == 1:
            raise ValueError(VALUEERROR_MSG_1D)

        X *= self.scale_
        X += self.min_
        return X

    def inverse_transform(self, X):
        """Scale back the data to the original representation

        Parameters
        ----------
        X : array-like
            The data used to scale along the specified axis.
        """
        check_is_fitted(self, 'scale_')

        X = check_array(X, copy=self.copy, ensure_2d=False, dtype=FLOAT_DTYPES)
        if X.ndim == 1:
            raise ValueError(VALUEERROR_MSG_1D)

        X -= self.min_
        X /= self.scale_
        return X
