#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-01-28 11:47:30
# @Last Modified by:   MAL
# @Last Modified time: 2016-02-11 12:21:48

import sys
import os
import json
import pickle
import logging

from sklearn.pipeline import Pipeline

from ..utils import instantiate_dynamic, set_nested
from ..transfer_learning.base import SampleWeightPipeline, make_sample_weight_pipeline
from ..transfer_learning.base import TransferEstimator, TransferTransformer, TransferMixin

# check for formatting options: https://docs.python.org/2/library/logging.html#logrecord-attributes
_LOGFORMAT = '%(asctime)-9s %(levelname)-6s %(message)s'
_LOGDATEFORMAT = '%H:%M:%S'


def load_pickled_transformer(filename):

    if os.path.isfile(filename):
        with open(filename, 'rb') as fp:
            transformer = pickle.load(fp)
    else:
        raise IOError('File not found: {}'.format(filename))

    return transformer


def _check_boolean_parameter(ddict, parameter):
    return parameter in ddict and ddict[parameter]


def build_pipeline(model, ignore_sample_weight_estimator=False):

    logger = logging.getLogger(__name__)

    steps = []

    # possible transformer types in the model:
    transformer_types = ['preprocessing', 'transfer']
    estimator_types = ['classification']

    # If we have a SampleWeightEstimator then we need to pack it together with the classifier
    #  into a SampleWeightPipeline before adding it to this pipeline.
    # If not, then just process the classifier as part of the transformers:
    if ignore_sample_weight_estimator or not('sample_weight_estimation' in model and model['sample_weight_estimation']['type'].lower() != 'none'):
        transformer_types += estimator_types
        estimator_types = []

    # go through each transformer type (needs to be ordered) and either load the pickled
    #  transformer or dynamically instantiate a new transformer
    for step_type in transformer_types:

        if len(model[step_type]) == 0:
            continue

        # count multiple steps per transformer type
        i = 0
        for sub_step in model[step_type]:

            if sub_step['type'].lower == 'none' or len(sub_step['type'].strip()) == 0:
                continue

            if 'file' in sub_step:
                # load from pickle
                transformer = load_pickled_transformer(sub_step['file'])
            else:
                # create object dynamically
                transformer = instantiate_dynamic(sub_step['type'], sub_step['parameters'])

            if transformer is not None:
                # wrap transformer in TransferTransformer to be able to deal with source and target
                #  data at the same time
                if step_type == 'classification':
                    # logger.info('Create TransferEstimator for {} from {}'.format(sub_step['type'],transformer))
                    t_wrap = TransferEstimator(transformer, _check_boolean_parameter(sub_step, 'fit_target_data'))
                else:
                    # logger.info('Create TransferTransformer for {} from {}'.format(sub_step['type'],transformer))
                    t_wrap = TransferTransformer(transformer, _check_boolean_parameter(sub_step, 'fit_target_data'))
                steps.append( (step_type + '_{}'.format(i), t_wrap) )
                i += 1

    # Pack the SampleWeightEstimator together with the Classifier into a SampleWeightPipeline
    for step_type in estimator_types:
        # we should only get here if there is a sample_weight_estimation specified...

        # no estimators of this type? do nothing
        if len(model[step_type]) == 0:
            continue

        # we can only have exactly ONE classifier
        #  if you would want to use a classifier as transform then put it as transformer in the pipeline
        if len(model[step_type]) > 1:
            raise AttributeError('There can only be one classifier in conjunction with a '
                                 'SampleWeightEstimation but the model specifies {} {}s.'.format(len(model[step_type]), step_type))

        # create sample weight estimator
        if 'file' in model['sample_weight_estimation']:
            sw_estimator = load_pickled_transformer(model['sample_weight_estimation']['file'])
        else:
            sw_estimator = instantiate_dynamic(model['sample_weight_estimation']['type'],
                                               model['sample_weight_estimation']['parameters'])

        # wrap transformer in TransferTransformer
        # logger.info('Create TransferTransformer for {} from {}'.format(step_type,sw_estimator))
        swe_wrap = TransferTransformer(sw_estimator,
                                     _check_boolean_parameter(model['sample_weight_estimation'], 'fit_target_data'))

        # create the estimator
        sub_step = model[step_type][0]

        if 'file' in sub_step:
            # load from pickle
            estimator = load_pickled_transformer(sub_step['file'])
        else:
            # create object dynamically
            estimator = instantiate_dynamic(sub_step['type'], sub_step['parameters'])

        # logger.info('Create TransferEstimator for {} from {}'.format(sub_step, estimator))
        e_wrap = TransferEstimator(estimator, _check_boolean_parameter(sub_step, 'fit_target_data'))

        sw_pipeline = make_sample_weight_pipeline(swe_wrap, e_wrap)

        steps.append( (step_type + '_0', sw_pipeline) )

    return Pipeline(steps)


def store_pipeline(pipeline, output_path, settings=None, store_all=False, file_suffix=''):
    """
    Stores the transformers in the pipeline to pickled files.

    Parameters
    ----------
    pipeline : sklearn.pipeline.Pipeline
        Pipeline to store
    output_path : str
        Path to store pickled files in
    settings : dict, optional
        Store filenames in model settings dict. Be aware that settings is modified in
        this method as it stores the filenames of the stored objects in each step's
        dictionary (using 'file' key).
    store_all : bool, optional
        If true stores all transformers (also pro-processing steps), default is False.

    Returns
    -------
    settings : dict
        The updated settings dict if given or None
    """
    logger = logging.getLogger(__name__)

    # ~~~~~~~~~~~~~~~~~~~~
    # Store objects
    # ~~~~~~~~~~~~~~~~~~~~

    for name, transform in pipeline.steps:

        # do not store pre-processing steps as these will be fitted to every dataset itself
        if name.startswith('preprocessing'):
            continue

        if isinstance(transform, (Pipeline, SampleWeightPipeline)):
            settings = store_pipeline(transform, output_path, settings, store_all, file_suffix)
        else:
            if isinstance(transform, TransferMixin):
                transform_obj = transform.estimator
            else:
                transform_obj = transform

            output_file = os.path.join(output_path, '{}{}.pkl'.format(name, file_suffix))
            with open(output_file, 'wb') as fp:
                pickle.dump(transform_obj, fp)

            if settings is not None:
                if name[-1].isdigit():
                    keys = [int(k) if k.isdigit() else k for k in name.split('_')]
                else:
                    keys = [name]
                set_nested(settings, keys + ['file'], os.path.normpath(output_file))
            logger.info('Stored {} in {}.'.format(name, output_file))

    return settings
