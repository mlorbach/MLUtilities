# -*- coding: utf-8 -*-
"""
Created on Thu Jul 24 09:32:30 2014

@author: MAL
"""

from sklearn.preprocessing import StandardScaler
from sklearn.utils.validation import check_arrays

class SelectiveStandardScaler(StandardScaler):
    """Standardize selected features by removing the mean and scaling to unit variance

    Centering and scaling happen independently on each selected feature by computing
    the relevant statistics on the samples in the training set. Mean and
    standard deviation are then stored to be used on later data using the
    `transform` method.

    Standardization of a dataset is a common requirement for many
    machine learning estimators: they might behave badly if the
    individual feature do not more or less look like standard normally
    distributed data (e.g. Gaussian with 0 mean and unit variance).

    For instance many elements used in the objective function of
    a learning algorithm (such as the RBF kernel of Support Vector
    Machines or the L1 and L2 regularizers of linear models) assume that
    all features are centered around 0 and have variance in the same
    order. If a feature has a variance that is orders of magnitude larger
    that others, it might dominate the objective function and make the
    estimator unable to learn from other features correctly as expected.

    Parameters
    ----------
    columns : array of integers
        The indices of the columns to scale where each column corresponds to a feature.
        Columns that are not listed in this array will not be considered during fit, transform,
        or inverse_transform.
    
    with_mean : boolean, True by default
        If True, center the data before scaling.
        This does not work (and will raise an exception) when attempted on
        sparse matrices, because centering them entails building a dense
        matrix which in common use cases is likely to be too large to fit in
        memory.

    with_std : boolean, True by default
        If True, scale the data to unit variance (or equivalently,
        unit standard deviation).

    copy : boolean, optional, default is True
        If False, try to avoid a copy and do inplace scaling instead.
        This is not guaranteed to always work inplace; e.g. if the data is
        not a NumPy array or scipy.sparse CSR matrix, a copy may still be
        returned.

    Attributes
    ----------
    `mean_` : array of floats with shape [n_features]
        The mean value for each feature in the training set.

    `std_` : array of floats with shape [n_features]
        The standard deviation for each feature in the training set.

    See also
    --------
    :func:`sklearn.preprocessing.scale` to perform centering and
    scaling without using the ``Transformer`` object oriented API

    :class:`sklearn.decomposition.RandomizedPCA` with `whiten=True`
    to further remove the linear correlation across features.
    """
    
    def __init__(self, columns, copy=True, with_mean=True, with_std=True):
        super(SelectiveStandardScaler, self).__init__(copy, with_mean, with_std)
        
        self.columns = columns
        
    
    def fit(self, X, y=None):
        """Compute the mean and std to be used for later scaling.

        Parameters
        ----------
        X : array-like or CSR matrix with shape [n_samples, n_features]
            The data used to compute the mean and standard deviation
            used for later scaling along the features axis.
        """
        
        if max(self.columns) >= X.shape[1]:
            raise IndexError('Selected columns out-of-bounds for input X: {}'.format(self.columns))
            
        return super(SelectiveStandardScaler, self).fit(X[:, self.columns], y)
        
    
    def transform(self, X, y=None, copy=None):
        """Perform standardization by centering and scaling

        Parameters
        ----------
        X : array-like with shape [n_samples, n_features]
            The data used to scale along the features axis.
        """        
        copy = copy if copy is not None else self.copy
        X = check_arrays(X, copy=copy, sparse_format="csr")[0]
        X[:, self.columns] = super(SelectiveStandardScaler, self).transform(X[:,self.columns], y, copy)
        return X
        
    
    def inverse_transform(self, X, copy=None):
        """Scale back the data to the original representation

        Parameters
        ----------
        X : array-like with shape [n_samples, n_features]
            The data used to scale along the features axis.
        """
        copy = copy if copy is not None else self.copy        
        X = check_arrays(X, copy=copy, sparse_format="csr")[0]
        X[:, self.columns] = super(SelectiveStandardScaler, self).inverse_transform(X[:,self.columns], copy)
        return X