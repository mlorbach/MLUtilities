# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-02-03 13:48:47
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-08 12:10:35

import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin

__all__ = ['SampleWeightEstimatorMixin', 'SampleWeightPipeline', 'DummySampleWeightEstimator']


class SampleWeightEstimatorMixin(BaseEstimator):
    """docstring for SampleWeightEstimatorMixin"""
    def __init__(self):
        super(SampleWeightEstimatorMixin, self).__init__()
        self._sample_weight = None


def make_sample_weight_pipeline(sample_weight_estimator, estimator):
    return SampleWeightPipeline(steps=[ ('sample_weight_estimation', sample_weight_estimator),
                                ('classification_0', estimator)])


class SampleWeightPipeline(BaseEstimator, ClassifierMixin):
    """
    SampleWeightPipeline combines a SampleWeightEstimator with a Classifier.

    Use the make_sample_weight_pipeline() method for convenient creation of this pipeline.

    Attributes
    ----------
    steps : list of tuples
        The steps in the pipeline [(SWE_name, SWE), (clf_name, classifier)]
    """
    def __init__(self, steps):
        super(SampleWeightPipeline, self).__init__()
        # self.sample_weight_estimator = sample_weight_estimator
        # self.estimator = estimator
        #self.steps = [('sample_weight_estimator', sample_weight_estimator), ('estimator', estimator)]
        self.steps = steps
        self._sample_weight = None
        self._is_fitted = False

        if len(steps) != 2:
            raise AttributeError('SampleWeightPipeline has to have exactly two steps. You specified {}.'.format(len(steps)))

        if not hasattr(steps[0][1], "_sample_weight") or not hasattr(steps[0][1], "fit"):
            raise TypeError("The sample_weight_estimation should be SampleWeightEstimatorMixin "
                            "and implement fit and have the field _sample_weight."
                            " '%s' (type %s) doesn't)" % (steps[0][1], type(steps[0][1])))

        if not hasattr(steps[1][1], "fit"):
            raise TypeError("The estimator should be ClassifierMixin and implement fit."
                            " '%s' (type %s) doesn't)" % (steps[1][1], type(steps[1][1])))

    @property
    def _estimator_type(self):
        return self.steps[-1][1]._estimator_type

    def get_params(self, deep=True):
        if not deep:
            return super(SampleWeightPipeline, self).get_params(deep=False)
        else:
            out = self.named_steps
            for name, step in self.named_steps.iteritems():
                for key, value in step.get_params(deep=True).iteritems():
                    out['%s__%s' % (name, key)] = value

            out.update(super(SampleWeightPipeline, self).get_params(deep=False))
            return out

    @property
    def named_steps(self):
        return dict(self.steps)

    @property
    def _final_estimator(self):
        return self.steps[-1][1]


    def _pre_transform(self, X, y=None, **fit_params):
        fit_params_steps = dict((step, {}) for step, _ in self.steps)
        for pname, pval in fit_params.iteritems():
            step, param = pname.split('__', 1)
            fit_params_steps[step][param] = pval
        Xt = X
        for name, transform in self.steps[:-1]:
            if hasattr(transform, "fit_transform"):
                Xt = transform.fit_transform(Xt, y, **fit_params_steps[name])
            else:
                Xt = transform.fit(Xt, y, **fit_params_steps[name]) \
                              .transform(Xt)
        return Xt, fit_params_steps[self.steps[-1][0]]


    def fit(self, X, y, **fit_params):

        Xt, fit_params = self._pre_transform(X, y, **fit_params)
        self._sample_weight = self.steps[0][1]._sample_weight
        self.steps[-1][1].fit(X, y, sample_weight=self._sample_weight, **fit_params)
        self._is_fitted = True
        return self


    def predict(self, X):
        return self.steps[-1][1].predict(X)

    def predict_proba(self, X):
        return self.steps[-1][1].predict_proba(X)

    def decision_function(self, X):
        return self.steps[-1][1].decision_function(X)


class DummySampleWeightEstimator(SampleWeightEstimatorMixin):
    """
    Implements the SampleWeightEstimatorMixin API but does not do estimation.

    The returned sample weights are either uniform or random (normal distributed with mean = 1 and std according to the scale parameter).

    Attributes
    ----------
    random : bool
        If False (default), returns uniform weights for all active samples. If True, return random weights.
    scale : float
        Standard deviation for random weights.
    sub_sample : int
        Take only every nth sample (set all other sample weights to zero)

    """
    def __init__(self, random=False, scale=.5, sub_sample=None):
        super(DummySampleWeightEstimator, self).__init__()
        self._random = random
        self._scale = scale
        self.sub_sample = 1 if sub_sample is None else int(sub_sample)

    def fit(self, X, y=None):

        if isinstance(X, tuple):
            X = X[0]

        self._sample_weight = np.zeros(X.shape[0])
        N = X[::self.sub_sample].shape[0]
        if self._random:
            self._sample_weight[::self.sub_sample] = np.abs(self._scale * np.random.randn(N) + 1.)
        else:
            self._sample_weight[::self.sub_sample] = np.ones(N)

        return self

    def transform(self, X):
        return X


class TransferMixin():
    """docstring for TransferMixin"""
    def __init__(self, estimator):
        super(TransferMixin, self).__init__()
        self.estimator = estimator



class TransferTransformer(BaseEstimator, TransformerMixin, TransferMixin):
    """
    Summary

    Attributes
    ----------
    transformer : TYPE
        Description
    """
    def __init__(self, estimator, fit_target_data=False):
        super(TransferTransformer, self).__init__()
        # self.__class__ = type(estimator.__class__.__name__,
        #                       (self.__class__, estimator.__class__),
        #                       {})
        self.__dict__ = estimator.__dict__
        self.estimator = estimator
        self.fit_target_data = fit_target_data


    def __getstate__(self):
        result = {}
        result['estimator_dict'] = self.__dict__.copy()
        result['estimator'] = self.estimator
        result['fit_target_data'] = self.fit_target_data
        return result


    def __setstate__(self, state):
        self.__dict__ = state['estimator_dict'].copy()
        self.estimator = state['estimator']
        self.fit_target_data = state['fit_target_data']


    # forward all attributes of the estimator to this wrapper class
    #  overwrite the ones that you want to overwrite below
    def __getattr__(self, attr):
        return getattr(self.estimator, attr)


    def fit(self, X, y=None, **kwargs):
        # print 'TransferTransformer.fit()', type(self.estimator)

        if (self.fit_target_data and isinstance(X, tuple)) or not isinstance(X, tuple):
            self.estimator.fit(X, y, **kwargs)
        else:
            self.estimator.fit(X[0], y, **kwargs)

        return self


    def transform(self, X, **kwargs):
        # print 'TransferTransformer.transform()', type(self.estimator)

        if isinstance(X, tuple):
            return self.estimator.transform(X[0], **kwargs), self.estimator.transform(X[1], **kwargs)
        else:
            return self.estimator.transform(X, **kwargs)


    def fit_transform(self, X, y=None, **kwargs):
        self.fit(X, y, **kwargs)
        return self.transform(X, **kwargs)


class TransferEstimator(BaseEstimator, ClassifierMixin, TransferMixin):
    """
    Summary

    Attributes
    ----------
    estimator : TYPE
        Description
    """
    def __init__(self, estimator, fit_target_data=False):
        super(TransferEstimator, self).__init__()
        # self.__class__ = type(estimator.__class__.__name__,
        #                       (self.__class__, estimator.__class__),
        #                       {})
        self.__dict__ = estimator.__dict__
        self.estimator = estimator
        self.fit_target_data = fit_target_data


    def __getstate__(self):
        result = {}
        result['estimator_dict'] = self.__dict__.copy()
        result['estimator'] = self.estimator
        result['fit_target_data'] = self.fit_target_data
        return result


    def __setstate__(self, state):
        self.fit_target_data = state['fit_target_data']
        self.estimator = state['estimator']
        self.__dict__ = state['estimator_dict'].copy()


    # forward all attributes of the estimator to this wrapper class
    #  overwrite the ones that you want to overwrite below
    def __getattr__(self, attr):
        return getattr(self.estimator, attr)


    def fit(self, X, y=None, **kwargs):
        # print 'TransferEstimator.fit()', type(self.estimator)
        if (self.fit_target_data and isinstance(X, tuple)) or not isinstance(X, tuple):
            self.estimator.fit(X, y, **kwargs)
        else:
            if isinstance(y, tuple):
                # also target labels in y:
                self.estimator.fit(X[0], y[0], **kwargs)
            else:
                self.estimator.fit(X[0], y, **kwargs)

        return self


    def predict(self, X, **kwargs):
        # print 'TransferEstimator.predict()', type(self.estimator)
        if isinstance(X, tuple):
            X = X[0]

        return self.estimator.predict(X, **kwargs)


    def score(self, X, y, **kwargs):
        # print 'TransferEstimator.score()', type(self.estimator)
        if isinstance(X, tuple):
            X = X[0]

        return self.estimator.score(X, y, **kwargs)
