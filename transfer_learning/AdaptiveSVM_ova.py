# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-02-29 11:58:19
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-29 12:53:33


import os
import array
import numpy as np
import scipy.sparse as sp
import logging
from subprocess import Popen, PIPE
import tempfile
import datetime
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.class_weight import compute_class_weight
from sklearn.datasets.svmlight_format import dump_svmlight_file
from sklearn.preprocessing import LabelBinarizer
from sklearn.externals.joblib import Parallel, delayed

from MLUtilities.classification.libSVM import SVM, _get_libSVM_kernel_type
from MLUtilities.libSVM.python.svmutil import svm_load_model, svm_predict, svm_train, svm_save_model, svm_problem, svm_parameter


# _adaSVM_cmd = r'C:\Users\mal\Downloads\libraries\C\adapt-svm-train\adapt_svm_train.exe'
# _adaSVM_cmd = r'C:\Users\mal\Downloads\libraries\C\adapt-svm-train\Release\adapt-svm-train.exe'
# _adaSVM_cmd = r'C:\Users\mal\Downloads\libraries\C\adapt-svm-train\x64\Release\adapt-svm-train.exe'
_adaSVM_cmd = r"C:\Users\mal\phenorat_repo\code\_libs\MLUtilities\external\adapt_svm_train\adapt_svm_train.exe"


def _fit_binary(fname_base_model, X, y, sample_weights=None, classes=None, **params):
    """
    Fit a SVM to the binary classification problem given by (X, y).

    Parameters
    ----------
    X : tuple of array-like, arrays of shape [n_samples, n_features]
        The source and target data (X_src, X_trg)
    y : tuple of array-like, arrays of shape [n_samples]
        The source and target labels (y_src, y_trg)
    sample_weights : array-like, optional
        Weights of source training samples (not used)
    classes : list, optional
        List of classes (only used for verbose logging information)
    **params : keyword-arguments
        Keyword arguments passed to the SVM fitting algorithm.

    Returns
    -------
    estimator_src, estimator_trg, invert_target : (svm_model, svm_model, boolean)
        Source and target model as well as flag specifying whether predictions of target model need to be inverted (this is due to weird behavior of libSVM implementation used by aSVM)

    Raises
    ------
    AttributeError
        Description
    RuntimeError
        Description
    ValueError
        Description
    """
    # if not isinstance(X, tuple) or len(X) < 2:
    #     raise ValueError('Expecting source and target data in a tuple.')

    # if not isinstance(y, tuple) or len(y) < 2:
    #     raise ValueError('Expecting source and target labels in a tuple.')

    logger = logging.getLogger(__name__)

    parameters = dict(tau=1.0, kernel='rbf',
                      gamma='auto', coef0=0,
                      probability=False,
                      cache_size=200, class_weight='balanced',
                      verbose=False, max_iter=-1)
    parameters.update(params)

    if parameters['verbose'] > 0:
        logger.setLevel(logging.DEBUG)

    logger.debug('Fitting for classes: {}'.format(classes))

    # ~~~~~~~~~~~~~~~~~~
    # Prepare input data
    # ~~~~~~~~~~~~~~~~~~

    # X_src = np.asarray(X[0])
    X_trg = np.asarray(X)

    # y_src = np.asarray(y[0]).copy()
    y_trg = np.asarray(y).copy()

    # N_src = X_src.shape[0]
    N_trg = X_trg.shape[0]

    # libSVM expects binary labels: (-1, 1)
    # If input labels don't match, encode them accordingly
    lb = LabelBinarizer(-1, 1)
    y_src = lb.fit_transform(y_trg).squeeze()

    # Any target samples with other labels are left out for adaptation.
    # This give the user the option to not provide labels for all data points:
    target_avail_labels = np.in1d(y_trg, (0,1))
    y_trg[target_avail_labels] = lb.transform(y_trg[target_avail_labels])
    y_trg = np.squeeze(y_trg)

    label_names = np.unique(y_trg)
    logger.debug('Labels: {}'.format(label_names))
    # logger.debug('Label dist (source) = {}'.format(np.unique(y_src, return_counts=True)))
    logger.debug('Label dist (target) = {}'.format(np.unique(y_trg, return_counts=True)))

    # if y_src.shape[0] != N_src:
    #     raise AttributeError('Source data (X) and labels (y) need same first dimension.')

    if y_trg.shape[0] != N_trg:
        raise AttributeError('Target data (X) and labels (y) need same first dimension.')

    # # ~~~~~~~~~~~~~~~~~~
    # # Train source model
    # # ~~~~~~~~~~~~~~~~~~

    # problem = svm_problem(y_src.tolist(), X_src.tolist())
    # kernelflag = _get_libSVM_kernel_type(parameters['kernel'])
    # para_str = '-s 0 -t {:d} -m {:d} -c {:f} -g {} -r {:f} -q '.format(kernelflag,
    #                                                      parameters['cache_size'],
    #                                                      parameters['C'],
    #                                                      parameters['gamma'],
    #                                                      parameters['coef0'])

    # # compute class weights (Ci) for source classifier
    # wi = compute_class_weight('balanced', label_names, y_src)
    # weight_str = ' '.join(['-w{} {:6f}'.format(l, wi[i]) for i, l in enumerate(label_names)])
    # para_str += weight_str

    # para = svm_parameter(para_str)

    # logger.debug('Source model libSVM parameters: {}'.format(para_str))
    # estimator_src = svm_train(problem, para)

    # fp_source_model, fname_source_model = tempfile.mkstemp(prefix='adasvm_')
    # try:
    #     svm_save_model(fname_source_model, base_model)
    # except IOError as e:
    #     logger.error('Error creating temporary source model file "{}": {}'.format(fname_source_model, e.message))
    #     raise e
    # finally:
    #     os.close(fp_source_model)

    # ~~~~~~~~~~~~~~~~~~
    # Prepare for target SVM
    # ~~~~~~~~~~~~~~~~~~

    # dump target data to temporary file (dump only samples with available labels)
    fp_target_data, fname_target_data = tempfile.mkstemp(prefix='adasvm_')
    try:
        dump_svmlight_file(X_trg[target_avail_labels],
                       y_trg[target_avail_labels],
                       fname_target_data,
                       zero_based=False)
    except IOError as e:
        logger.error('Error creating temporary target data file "{}": {}'.format(fname_target_data, e.message))
        raise e
    finally:
        os.close(fp_target_data)

    # ~~~~~~~~~~~~~~~~~~
    # Train adaptive SVM on target data
    # ~~~~~~~~~~~~~~~~~~


    # datetime_now = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
    # fname_target_model = 'target_model_{}.svm'.format(datetime_now)
    fp_target_model, fname_target_model = tempfile.mkstemp(prefix='adasvm_')

    kernelflag = _get_libSVM_kernel_type(parameters['kernel'])
    wi = compute_class_weight(parameters['class_weight'], label_names, y_trg[target_avail_labels])
    weight_str = ' '.join(['-w{} {:6f}'.format(l, wi[i]) for i, l in enumerate(label_names)])
    paras = ' -t {:d} -m {:d} -c {:f} -g {} {}'
    paras = paras.format(kernelflag, parameters['cache_size'], parameters['tau'],
                         parameters['gamma'],
                         weight_str)

    paras += ' + "{}",1.0 "{}" "{}"'.format(fname_base_model, fname_target_data, fname_target_model)

    try:
        logger.debug('AdaSVM cmd = {}'.format(_adaSVM_cmd + paras))
        proc_child = Popen(_adaSVM_cmd + paras, shell=False, stdout=PIPE)
        proc_child.communicate() # run
        if proc_child.returncode > 0:
            logger.error("adaSVM was terminated by signal {}".format(proc_child.returncode))
            raise RuntimeError('adaSVM was terminated by signal {}. Check terminal output!'.format(proc_child.returncode))
        else:
            logger.debug("adaSVM returned {}".format(proc_child.returncode))

        # estimator_trg = svm_load_model(fname_target_model)
        if y_trg[target_avail_labels][0] <= 0:
            inv_estimator_trg = True
            logger.debug('{}: First label is negative. Inverse coeff.'.format(classes))
        else:
            inv_estimator_trg = False

    except OSError as e:
        print "adaSVM failed: {}".format(e)
        logger.error("adaSVM failed: {}".format(e))
    except RuntimeError:
        raise # pass it on but also run the finally clause
    finally:
        os.close(fp_target_model)
        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Clean up temporary data
        # ~~~~~~~~~~~~~~~~~~~~~~~
        for fname in [fname_target_data]:
            if not os.path.isfile(fname):
                continue # don't try to delete files that don't exist
            try:
                os.unlink(fname)
            except (IOError, WindowsError) as e:
                logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

    # ~~~~~~~~~~~~~~~~~~
    # Done fitting. Return.
    # ~~~~~~~~~~~~~~~~~~

    return fname_target_model, inv_estimator_trg


def _predict_binary(X, fname_model):
    """
    Predict the labels for the features in X using the trained SVM.

    The SVM "estimator" is a binary SVM. The returned values are the SVM score of the positive class.

    Parameters
    ----------
    estimator : svm_model
        libSVM model
    X : np.array of shape [n_samples, n_features]
        Data

    Returns
    -------
    np.array of shape [n_samples]
        Predicted labels (-1, 1)
    """

    model = svm_load_model(fname_model)

    # prepare features
    # we need to provide data in libSVM "sparse" dict format.
    #  This is a list of dictionaries (key is feature index, value is corresponding value).
    # Passing a list of lists (only feature values) messes up libSVMs internal indexing which assumes the first feature to start at index 1. Unfortunately, we cannot change that behavior in the python interface.
    idx = range(X.shape[1])
    Xsp = [dict(zip(idx, xi)) for xi in X]

    _, _, score = svm_predict([0]*X.shape[0],
                               X.tolist(),
                               model,
                               '-q')

    # return score of positive class (1) only
    score = np.asarray(score)

    if score.ndim > 1 and score.shape[1] > 1:
        pos_class = model.get_labels().index(1)
        return score[:, pos_class]
    else:
        return score.ravel()

    # return np.asarray(score).ravel()


class AdaptiveSVM(BaseEstimator, ClassifierMixin):
    """
    Implementation of AdaptiveSVM [1] with Python interface to the pre-compiled binaries of [2].

    The interface follows scitkit-learn classifier structure.

    At initialization you need to provide the trained source classifier of type MLU.classification.libSVM.SVM(). That classifiers needs to be a one-vs-all classifier with any kernel. The source classifier is accessible via self.base_model.

    The fit() and predict() methods adapts the source classifier to the provided data or predicts the labels using the fitted, adapted classifier, respectively.

    Attributes
    ----------
    base_model : MLUtilities.classification.libSVM.SVM
        Trained source classifier
    cache_size : int
        Cache size used for adaptation optimization
    estimators_ : list
        After fitting, holds the target classifiers (one per label)
    inv_estimator_ : list of bool
        Internal parameter storing whether prediction scores are to be inverted for a particular class.
    label_binarizer_ : TYPE
        Description
    max_iter : TYPE
        Description
    n_jobs : TYPE
        Description
    random_state : TYPE
        Description
    tau : float
        The cost parameter (C in SVM formulation) that controls the strenghts of the adaptation (penalizing classifier weight change vs error on target data)
    verbose : TYPE
        Description
    """
    def __init__(self, base_model, tau=1., cache_size=200, verbose=False, max_iter=-1, random_state=None, n_jobs=1):
        super(AdaptiveSVM, self).__init__()

        # if not isinstance(base_model, SVM):
        #     raise TypeError('base_model must be SVM-type "{}" but is "{}".'.format(SVM, type(base_model)))

        if not hasattr(base_model, 'estimators_'):
            raise TypeError('base_model must be SVM-type "{}" but is "{}".'.format(SVM, type(base_model)))

        self.base_model = base_model

        # self.kernel = self.base_model.kernel
        # self.degree = self.base_model.degree
        # self.gamma = self.base_model.gamma
        # self.coef0 = self.base_model.coef0
        # self.tol = self.base_model.tol
        # self.C = self.base_model.C # the cost factor of the source SVM
        self.tau = tau # the 'C' of the adaptation minimiztion
        # self.shrinking = self.base_model.shrinking
        # self.probability = self.base_model.probability
        self.cache_size = cache_size
        # self.class_weight = class_weight if class_weight is not None and class_weight.lower() not in ['none', 'uniform'] else None
        self.verbose = verbose
        self.max_iter = max_iter
        self.random_state = random_state
        # self.sub_sample = 1 if sub_sample is None else int(sub_sample)
        # self.neg_pos_labels = neg_pos_labels if neg_pos_labels is not None else (0,1)
        self.n_jobs = n_jobs


    def fit(self, X, y, sample_weights=None, **params):

        # if not isinstance(X, tuple) or len(X) < 2:
        #     raise ValueError('AdaptiveSVM.fit() expect source and target data in a tuple.')

        # if not isinstance(y, tuple) or len(y) < 2:
        #     raise ValueError('AdaptiveSVM.fit() expect source and target labels in a tuple.')

        logger = logging.getLogger(__name__)

        # ~~~~~~~~~~~~~~~~~~
        # Prepare input data
        # ~~~~~~~~~~~~~~~~~~

        # X_src = np.asarray(X[0])
        X_trg = np.asarray(X)

        # y_src = np.asarray(y[0]).copy()
        y_trg = np.asarray(y).copy()

        # N_src = X_src.shape[0]
        N_trg = X_trg.shape[0]

        unique_y = np.unique(y_trg)
        n_labels = len(unique_y)

        self.label_binarizer_ = LabelBinarizer(sparse_output=True)
        # y_src = self.label_binarizer_.fit_transform(y_src)
        y_trg = self.label_binarizer_.fit_transform(y_trg)
        # y_src = y_src.tocsc()
        y_trg = y_trg.tocsc()

        # columns_src = (col.toarray().ravel() for col in y_src.T)
        columns_trg = (col.toarray().ravel() for col in y_trg.T)
        # In cases where individual estimators are very fast to train setting
        # n_jobs > 1 can result in slower performance due to the overhead
        # of spawning threads.  See joblib issue #112.
        parameters = dict(kernel=self.base_model.kernel,
            degree=self.base_model.degree,
            gamma=self.base_model.gamma,
            coef0=self.base_model.coef0,
            tol=self.base_model.tol,
            C=self.base_model.C,
            tau=self.tau,
            shrinking=self.base_model.shrinking,
            probability=self.base_model.probability,
            cache_size=self.cache_size,
            class_weight=self.base_model.class_weight,
            verbose=self.verbose,
            max_iter=self.max_iter)
        parameters.update(params)


        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Prepare source models
        # ~~~~~~~~~~~~~~~~~~~~~~~

        model_files = []
        for model in self.base_model.estimators_:
            # dump trained models to file
            fp_model, fname_model = tempfile.mkstemp(prefix='MLUlibSVM_')
            try:
                # save the model to disk
                svm_save_model(fname_model, model)
            except (IOError, WindowsError) as e:
                logger.error('Failed to write model to temporary file: {}'.format(e))
                raise e
            finally:
                os.close(fp_model)
            model_files.append(fname_model)



        self.estimators_ = Parallel(n_jobs=self.n_jobs)(delayed(_fit_binary)(
            model_files[i], X_trg, columns_trg.next(),
            classes=["not %s" % self.label_binarizer_.classes_[i],
                self.label_binarizer_.classes_[i]],
            **parameters)
            for i in range(n_labels))

        # split the returned estimators_ tuples into the "inverse"-flag and the estimator
        self.inv_estimator_ = [e[1] for e in self.estimators_]
        estimator_filenames = [e[0] for e in self.estimators_]
        self.estimators_ = map(svm_load_model, estimator_filenames)

        self._is_fitted = True

        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Clean up temporary data
        # ~~~~~~~~~~~~~~~~~~~~~~~
        for fname in model_files + estimator_filenames:
            if not os.path.isfile(fname):
                continue # don't try to delete files that don't exist
            try:
                os.unlink(fname)
            except (IOError, WindowsError) as e:
                logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

        return self


    def predict_proba(self, X):
        """
        Predict multi-class labels using underlying target domain estimators.

        Parameters
        ----------
        X : (sparse) array-like, shape = [n_samples, n_features]
            Data

        Returns
        -------
        y : (sparse) array-like, shape = [n_samples] or [n_samples, n_classes]
            Predicted multi-class labels
        """

        if not self._is_fitted or self.estimators_ is None:
            raise AttributeError('Target estimators have not been fitted yet.')


        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Prepare models
        # ~~~~~~~~~~~~~~~~~~~~~~~

        model_files = []
        for model in self.estimators_:
            # dump trained models to file
            fp_model, fname_model = tempfile.mkstemp(prefix='adasvm_')
            try:
                # save the model to disk
                svm_save_model(fname_model, model)
            except (IOError, WindowsError) as e:
                logger.error('Failed to write model to temporary file: {}'.format(e))
                raise e
            finally:
                os.close(fp_model)
            model_files.append(fname_model)


        if self.label_binarizer_.y_type_ == 'multiclass':

            scores = Parallel(n_jobs=self.n_jobs)(delayed(_predict_binary)(
                    X, model_files[i])
                    for i in range( len(model_files) ))

            # merge scores to one large (n_samples, n_classes) array
            scores = np.stack(scores, axis=-1)

            # aSVMs libSVM implementation returns the negative scores
            #  if the first label in the training set is a negative label.
            #  Swap it back.
            scores[:, np.where(self.inv_estimator_)[0]] *= -1



            # maxima = np.empty(n_samples, dtype=float)
            # maxima.fill(-np.inf)
            # argmaxima = np.zeros(n_samples, dtype=int)
            # for i, (model_src, model_trg) in enumerate(self.estimators_):
            #     model = model_src if use_source_model else model_trg
            #     score = _predict_binary(model, X)
            #     if not use_source_model and self.inv_estimator_trg_[i]:
            #         # aSVMs libSVM implementation returns the negative scores
            #         #  if the first label in the training set is a negative label.
            #         # Swap it back.
            #         score *= -1.
            #     np.maximum(maxima, score, out=maxima)
            #     argmaxima[maxima == score] = i

            # return self.label_binarizer_.classes_[argmaxima.T]
        else:
            scores = _predict_binary(fname_data, model_files[0])

            # model = self.estimators_[0][0] if use_source_model else self.estimators_[0][1]
            # score = _predict_binary(model, X)
            # if not use_source_model and self.inv_estimator_trg_[i]:
            #     # aSVMs libSVM implementation returns the negative scores
            #     #  if the first label in the training set is a negative label.
            #     # Swap it back.
            #     score *= -1
            # argmaxima = score > thresh

            # return self.label_binarizer_.inverse_transform(np.asarray(argmaxima))

        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Clean up temporary data
        # ~~~~~~~~~~~~~~~~~~~~~~~

        for fname in model_files:
            if not os.path.isfile(fname):
                continue # don't try to delete files that don't exist
            try:
                os.unlink(fname)
            except (IOError, WindowsError) as e:
                logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

        return scores


    def predict(self, X):

        scores = self.predict_proba(X)

        if self.label_binarizer_.y_type_ == 'multiclass':
            argmaxima = np.argmax(scores, axis=1)
            return self.label_binarizer_.classes_[argmaxima]
        else:
            thresh = 0
            argmaxima = scores > thresh
            return self.label_binarizer_.inverse_transform(np.asarray(argmaxima))


    def __getstate__(self):
        result = self.__dict__.copy()

        # We cannot pickle the model directly as it is a C object:
        #  Instead, we'll dump the model into a temporary file and then
        #  pickle the content (string) of that file
        if 'estimators_' in result:

            result['estimators_'] = []

            for model in self.estimators_:
                fp, fname = tempfile.mkstemp(prefix='asvm_')
                try:
                    # dump the model
                    svm_save_model(fname, model)

                    # read the model as string
                    with open(fname, 'r') as fpo:
                        result['estimators_'].append(fpo.read())
                except:
                    raise
                finally:
                    os.close(fp)
                    os.unlink(fname) # remove temp file

        return result


    def __setstate__(self, state):
        self.__dict__ = state.copy()

        # We cannot unpickle the model directly as it is a C object:
        #  Instead, we'll dump the model that is represented as a string
        #   into a temporary file and then load the model from that file
        #   using libSVM's load method
        if 'estimators_' in state:

            self.estimators_ = []

            for model in state['estimators_']:

                fp, fname = tempfile.mkstemp(prefix='asvm_')
                try:
                    # dump the (string) model to file
                    with open(fname, 'w') as fpo:
                        fpo.write(model)

                    # load the model
                    self.estimators_.append(svm_load_model(fname))
                except:
                    raise
                finally:
                    os.close(fp)
                    os.unlink(fname) # remove temp file
