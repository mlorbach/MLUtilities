# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-08-02 13:07:16
# @Last Modified by:   MAL
# @Last Modified time: 2016-08-03 16:16:16

import numpy as np
from MLUtilities.classification.gmm import GMM as GMMC

from sklearn.mixture.gmm import log_multivariate_normal_density

from sklearn.externals.joblib import Parallel, delayed
from sklearn.utils.class_weight import compute_class_weight
from copy import deepcopy


def _partial_fit(model, X, y=None, tau=1., classes=None, sample_weights=None):
    """
    Implements a modifed update rule similar to the M-Step of the EM algorithm.

    Currently only updates the means of each cluster i:

    >> E_i(X) = 1/Z * SUM[ Pr(i | x_t) * x_t ]

    The weightes means of the target samples are then combined with the original mean values via a weighted interpolation controled by tau. When tau = 0, no update takes place. If tau = 1 the original mean is neglected.


    Parameters
    ----------
    model : TYPE
        Description
    X : TYPE
        Description
    y : None, optional
        Description
    tau : float, optional
        Description
    classes : None, optional
        Description
    sample_weights : None, optional
        Description

    Returns
    -------
    TYPE
        Description
    """
    if X.shape[0] == 0:
        return model

    # previously, we tried a soft assignment of samples to clusters, propagating the cluster responsibilities into a weighted mean. That didn't work well, presumably because it is conservative and partly due to numerical issues that occur when clusters don't get any samples assigned (Z very small, Pr very small --> Pr / Z = instable)
    # Z = np.sum(Pr, axis=0, keepdims=True)  # Z.shape = (1, k)

    # # E.shape = (k, f)
    # E = 1. / Z * np.sum(Pr[:, np.newaxis, :] * X[:, :, np.newaxis], axis=0)
    # E = E.T


    # assignment of each sample to exactly one cluster:

    # X.shape = (N, f)
    # to which cluster i does x belong? Pr(i | x)
    _, Pr = model.score_samples(X)  # Pr.shape = (N, k)
    assign = np.argmax(Pr, axis=1)

    new_model = deepcopy(model)
    for k in range(Pr.shape[1]):
        idx = np.where(assign==k)[0]
        if len(idx) > 0:
            new_model.means_[k] = (tau *
                                   np.mean(X[idx, : ], axis=0) +
                                   (1. - tau) * new_model.means_[k])

            # print 'Updating mean of cluster {} with tau = {}'.format(k, tau)
            # print 'Average change of mean = {}'.format( np.mean(np.abs(model.means_[k] - new_model.means_[k])) )

    # new_model.means_ = tau * E + (1. - tau) * model.means_
    if tau == 0:
        if not np.allclose(model.means_, new_model.means_):
            print 'Uups.'

    return new_model


class AdaptiveGMM(GMMC):
    """docstring for AdaptiveGMM"""

    def __init__(self, baseModel, tau=1., class_weight='balanced', n_jobs=1):
        super(AdaptiveGMM, self).__init__(baseModel, class_weight, n_jobs)
        self.baseModel = baseModel
        self.tau = tau
        self.class_weight = class_weight
        self.n_jobs = n_jobs

    def partial_fit(self, X, y=None, classes=None, sample_weights=None):

        if not self._is_fitted:
            raise RuntimeError('partial_fit requires calling fit() first.')

        X = np.asarray(X)
        labels = range(self.n_labels)

        if y is not None:
            # Supervised adaptation case
            # --------------------------

            print 'Supervised adaptation... tau = {}'.format(self.tau)

            y = self.le_.transform(np.asarray(y))

            updated_estimators = Parallel(n_jobs=self.n_jobs)(
                delayed(_partial_fit)(self.estimators_[i],
                                      X[y == i], tau=self.tau)
                for i in labels)

            self.estimators_ = updated_estimators

            # class priors
            if isinstance(self.class_weight, (list, tuple, np.ndarray)):
                if len(self.class_weight) == self.n_labels:
                    self.class_weight = dict(zip(labels, self.class_weight))
                else:
                    raise AttributeError('Number of manually specified class weights does not match number of labels ({} != {}).'.format(len(self.class_weight), self.n_labels))

            if self.class_weight == 'keep':
                pass
            else:
                self.w_ = compute_class_weight(self.class_weight,
                                               np.asarray(labels),
                                               y)
        else:
            # Unsupervised adaptation case
            # ----------------------------

            print 'Unsupervised adaptation...'
            return self._partial_fit_unsupervised(X)

        return self


    def _model_density_scores(self, model, X):
        return (log_multivariate_normal_density(X, model.means_,
                                                model.covars_,
                                                model.covariance_type) +
                np.log(model.weights_))


    def _partial_fit_unsupervised(self, X):

        if X.shape[0] == 0:
            return self

        # X.shape = (N, f)

        logPr = np.stack(map(lambda model: self._model_density_scores(model, X),
                          self.estimators_), axis=-1) # Pr.shape = (N, k, l)

        # Get max over 2d-flattened matrix:
        max_idx = logPr.reshape(logPr.shape[0], -1).argmax(1)

        # Get assignment by unravelling indices: assign.shape = [N, 2]
        assign = np.column_stack(np.unravel_index(max_idx, logPr.shape[1:]))

        for k in range(logPr.shape[1]):
            for l in range(logPr.shape[2]):

                # k : component
                # l : label (estimator)

                mask = np.where((assign[:, 0] == k) & (assign[:, 1] == l))[0]

                if len(mask) > 0:
                    self.estimators_[l].means_[k] = (self.tau *
                                                     np.mean(X[mask, :],
                                                             axis=0) +
                                                     (1. - self.tau) *
                                                      self.estimators_[l].means_[k])

        return self


        # for i, model in enumerate(self.estimators_):
        #     # to which cluster i does x belong? Pr(i | x)
        #     _, Pr = model.score_samples(X)  # Pr.shape = (N, k)



        #     assign = np.argmax(Pr, axis=1)

        #     new_model = deepcopy(model)
        #     for k in range(Pr.shape[1]):
        #         mask = np.where(assign==k)[0]
        #         if mask.sum() > 0:
        #             new_model.means_[k] = (tau *
        #                                    np.mean(X[ np.where(assign==k)[0], : ],
        #                                            axis=0) +
        #                                    (1. - tau) * new_model.means_[k])
