# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-02-29 11:58:19
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-16 15:31:39


import os
import array
import numpy as np
import scipy.sparse as sp
import logging
import subprocess
import tempfile
import datetime
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.class_weight import compute_class_weight
from sklearn.datasets.svmlight_format import dump_svmlight_file
from sklearn.preprocessing import LabelBinarizer
from sklearn.externals.joblib import Parallel, delayed

from MLUtilities.libSVM.python.svmutil import svm_load_model, svm_predict, svm_train, svm_save_model, svm_problem, svm_parameter


_adaSVM_cmd = r'C:\Users\mal\Downloads\libraries\C\adapt-svm-train\adapt_svm_train.exe'


def _get_libSVM_kernel_type(kernel_str):
    d = {'linear':0, 'poly':1, 'rbf':2, 'sigmoid':3}
    return d[kernel_str]


def _fit_binary(X, y, sample_weights=None, classes=None, **params):
    """
    Fit a SVM to the binary classification problem given by (X, y).

    Parameters
    ----------
    X : tuple of array-like, arrays of shape [n_samples, n_features]
        The source and target data (X_src, X_trg)
    y : tuple of array-like, arrays of shape [n_samples]
        The source and target labels (y_src, y_trg)
    sample_weights : array-like, optional
        Weights of source training samples (not used)
    classes : list, optional
        List of classes (only used for verbose logging information)
    **params : keyword-arguments
        Keyword arguments passed to the SVM fitting algorithm.

    Returns
    -------
    estimator_src, estimator_trg, invert_target : (svm_model, svm_model, boolean)
        Source and target model as well as flag specifying whether predictions of target model need to be inverted (this is due to weird behavior of libSVM implementation used by aSVM)

    Raises
    ------
    AttributeError
        Description
    RuntimeError
        Description
    ValueError
        Description
    """
    if not isinstance(X, tuple) or len(X) < 2:
        raise ValueError('Expecting source and target data in a tuple.')

    if not isinstance(y, tuple) or len(y) < 2:
        raise ValueError('Expecting source and target labels in a tuple.')

    logger = logging.getLogger(__name__)

    parameters = dict(C=1.0, tau=1.0, kernel='rbf', degree=3, gamma=0,
                 coef0=0.0, shrinking=True, probability=False,
                 tol=1e-3, cache_size=200, class_weight=None,
                 verbose=False, max_iter=-1, decision_function_shape=None)
    parameters.update(params)

    if parameters['verbose'] > 0:
        logger.setLevel(logging.DEBUG)

    logger.debug('Fitting for classes: {}'.format(classes))

    # ~~~~~~~~~~~~~~~~~~
    # Prepare input data
    # ~~~~~~~~~~~~~~~~~~

    X_src = np.asarray(X[0])
    X_trg = np.asarray(X[1])

    y_src = np.asarray(y[0]).copy()
    y_trg = np.asarray(y[1]).copy()

    N_src = X_src.shape[0]
    N_trg = X_trg.shape[0]

    # libSVM expects binary labels: (-1, 1)
    # If input labels don't match, encode them accordingly
    lb = LabelBinarizer(-1, 1)
    y_src = lb.fit_transform(y_src).squeeze()

    # Any target samples with other labels are left out for adaptation.
    # This give the user the option to not provide labels for all data points:
    target_avail_labels = np.in1d(y_trg, (0,1))
    y_trg[target_avail_labels] = lb.transform(y_trg[target_avail_labels])
    y_trg = np.squeeze(y_trg)

    label_names = np.unique(y_src)
    logger.debug('Labels: {}'.format(label_names))
    logger.debug('Label dist (source) = {}'.format(np.unique(y_src, return_counts=True)))
    logger.debug('Label dist (target) = {}'.format(np.unique(y_trg, return_counts=True)))

    if y_src.shape[0] != N_src:
        raise AttributeError('Source data (X) and labels (y) need same first dimension.')

    if y_trg.shape[0] != N_trg:
        raise AttributeError('Target data (X) and labels (y) need same first dimension.')

    # ~~~~~~~~~~~~~~~~~~
    # Train source model
    # ~~~~~~~~~~~~~~~~~~

    problem = svm_problem(y_src.tolist(), X_src.tolist())
    kernelflag = _get_libSVM_kernel_type(parameters['kernel'])
    para_str = '-s 0 -t {:d} -m {:d} -c {:f} -g {} -r {:f} -q '.format(kernelflag,
                                                         parameters['cache_size'],
                                                         parameters['C'],
                                                         parameters['gamma'],
                                                         parameters['coef0'])

    # compute class weights (Ci) for source classifier
    wi = compute_class_weight('balanced', label_names, y_src)
    weight_str = ' '.join(['-w{} {:6f}'.format(l, wi[i]) for i, l in enumerate(label_names)])
    para_str += weight_str

    para = svm_parameter(para_str)

    logger.debug('Source model libSVM parameters: {}'.format(para_str))
    estimator_src = svm_train(problem, para)

    fp_source_model, fname_source_model = tempfile.mkstemp(prefix='adasvm_')
    try:
        svm_save_model(fname_source_model, estimator_src)
    except IOError as e:
        logger.error('Error creating temporary source model file "{}": {}'.format(fname_source_model, e.message))
        raise e
    finally:
        os.close(fp_source_model)

    # ~~~~~~~~~~~~~~~~~~
    # Prepare for target SVM
    # ~~~~~~~~~~~~~~~~~~

    # dump target data to temporary file (dump only samples with available labels)
    fp_target_data, fname_target_data = tempfile.mkstemp(prefix='adasvm_')
    try:
        dump_svmlight_file(X_trg[target_avail_labels],
                       y_trg[target_avail_labels],
                       fname_target_data,
                       zero_based=False)
    except IOError as e:
        logger.error('Error creating temporary target data file "{}": {}'.format(fname_target_data, e.message))
        raise e
    finally:
        os.close(fp_target_data)

    # ~~~~~~~~~~~~~~~~~~
    # Train adaptive SVM on target data
    # ~~~~~~~~~~~~~~~~~~


    datetime_now = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
    fname_target_model = 'target_model_{}.svm'.format(datetime_now)
    paras = ' -t {:d} -m {:d} -c {:f} -g {} '.format(kernelflag, parameters['cache_size'],
                                                       parameters['tau'], parameters['gamma'])

    wi = compute_class_weight(parameters['class_weight'], label_names, y_trg[target_avail_labels])
    weight_str = ' '.join(['-w{} {:6f}'.format(l, wi[i]) for i, l in enumerate(label_names)])
    paras += weight_str

    paras += ' + "{}",1.0 {} {}'.format(fname_source_model, fname_target_data, fname_target_model)

    try:
        logger.debug('AdaSVM cmd = {}'.format(_adaSVM_cmd + paras))
        retcode = subprocess.call(_adaSVM_cmd + paras, shell=False)
        if retcode > 0:
            logger.error("adaSVM was terminated by signal {}".format(retcode))
            raise RuntimeError('adaSVM was terminated by signal {}. Check terminal output!'.format(retcode))
        else:
            logger.debug("adaSVM returned {}".format(retcode))

        estimator_trg = svm_load_model(fname_target_model)
        if y_trg[target_avail_labels][0] <= 0:
            inv_estimator_trg = True
            logger.debug('{}: First label is negative. Inverse coeff.'.format(classes))
        else:
            inv_estimator_trg = False

    except OSError as e:
        logger.error("adaSVM failed: {}".format(e))
    except RuntimeError:
        raise # pass it on but also run the finally clause
    finally:
        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Clean up temporary data
        # ~~~~~~~~~~~~~~~~~~~~~~~
        for fname in [fname_source_model, fname_target_data, fname_target_model]:
            if not os.path.isfile(fname):
                continue # don't try to delete files that don't exist
            try:
                os.unlink(fname)
            except (IOError, WindowsError) as e:
                logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

    # ~~~~~~~~~~~~~~~~~~
    # Done fitting. Return.
    # ~~~~~~~~~~~~~~~~~~

    return estimator_src, estimator_trg, inv_estimator_trg


def _predict_binary(estimator, X):
    """
    Predict the labels for the features in X using the trained SVM.

    The SVM "estimator" is a binary SVM. The returned values are the SVM score of the positive class.

    Parameters
    ----------
    estimator : svm_model
        libSVM model
    X : np.array of shape [n_samples, n_features]
        Data

    Returns
    -------
    np.array of shape [n_samples]
        Predicted labels (-1, 1)
    """
    _, _, score = svm_predict([1]*X.shape[0],
                               np.asarray(X).tolist(),
                               estimator,
                               '-q')

    return np.asarray(score).ravel()


class AdaptiveSVM(BaseEstimator, ClassifierMixin):
    """docstring for AdaptiveSVM"""
    def __init__(self, C=1.0, tau=1., kernel='rbf', degree=3, gamma='auto',
                 coef0=0.0, shrinking=True, probability=False,
                 tol=1e-3, cache_size=200, class_weight=None,
                 verbose=False, max_iter=-1, decision_function_shape=None,
                 random_state=None, sub_sample=None, neg_pos_labels=(0,1),
                 n_jobs=1):
        super(AdaptiveSVM, self).__init__()

        self.kernel = kernel
        self.degree = degree
        self.gamma = gamma if gamma != 'auto' else 0
        self.coef0 = coef0
        self.tol = tol
        self.C = C # the cost factor of the source SVM
        self.tau = tau # the 'C' of the adaptation minimiztion
        self.shrinking = shrinking
        self.probability = probability
        self.cache_size = cache_size
        self.class_weight = class_weight if class_weight is not None and class_weight.lower() not in ['none', 'uniform'] else None
        self.verbose = verbose
        self.max_iter = max_iter
        self.random_state = random_state
        self.sub_sample = 1 if sub_sample is None else int(sub_sample)
        self.neg_pos_labels = neg_pos_labels if neg_pos_labels is not None else (0,1)
        self.n_jobs = n_jobs


    def fit(self, X, y, sample_weights=None, **params):

        if not isinstance(X, tuple) or len(X) < 2:
            raise ValueError('AdaptiveSVM.fit() expect source and target data in a tuple.')

        if not isinstance(y, tuple) or len(y) < 2:
            raise ValueError('AdaptiveSVM.fit() expect source and target labels in a tuple.')

        logger = logging.getLogger(__name__)

        # ~~~~~~~~~~~~~~~~~~
        # Prepare input data
        # ~~~~~~~~~~~~~~~~~~

        X_src = np.asarray(X[0])
        X_trg = np.asarray(X[1])

        y_src = np.asarray(y[0]).copy()
        y_trg = np.asarray(y[1]).copy()

        N_src = X_src.shape[0]
        N_trg = X_trg.shape[0]

        unique_y = np.unique(y_src)
        n_labels = len(unique_y)

        self.label_binarizer_ = LabelBinarizer(sparse_output=True)
        y_src = self.label_binarizer_.fit_transform(y_src)
        y_trg = self.label_binarizer_.transform(y_trg)
        y_src = y_src.tocsc()
        y_trg = y_trg.tocsc()

        columns_src = (col.toarray().ravel() for col in y_src.T)
        columns_trg = (col.toarray().ravel() for col in y_trg.T)
        # In cases where individual estimators are very fast to train setting
        # n_jobs > 1 can result in slower performance due to the overhead
        # of spawning threads.  See joblib issue #112.
        parameters = dict(kernel=self.kernel,
            degree=self.degree,
            gamma=self.gamma,
            coef0=self.coef0,
            tol=self.tol,
            C=self.C,
            tau=self.tau,
            shrinking=self.shrinking,
            probability=self.probability,
            cache_size=self.cache_size,
            class_weight=self.class_weight,
            verbose=self.verbose,
            max_iter=self.max_iter)
        parameters.update(params)
        self.estimators_ = Parallel(n_jobs=self.n_jobs)(delayed(_fit_binary)(
            (X_src, X_trg), (columns_src.next(), columns_trg.next()),
            classes=["not %s" % self.label_binarizer_.classes_[i],
                self.label_binarizer_.classes_[i]],
            **parameters)
            for i in range(n_labels))

        self._is_fitted = True

        # split the returned estimators_ tuples into the "inverse"-flag and the (model_src, model_trg) tuple
        self.inv_estimator_trg_ = [e[2] for e in self.estimators_]
        self.estimators_ = [e[:2] for e in self.estimators_]

        return self


    def predict(self, X, use_source_model=False):
        """
        Predict multi-class labels using underlying target domain estimators.

        Parameters
        ----------
        X : (sparse) array-like, shape = [n_samples, n_features]
            Data

        Returns
        -------
        y : (sparse) array-like, shape = [n_samples] or [n_samples, n_classes]
            Predicted multi-class labels
        """

        if not self._is_fitted:
            raise AttributeError('Target estimators have not been fitted yet.')

        n_samples = X.shape[0]
        thresh = 0

        if self.label_binarizer_.y_type_ == 'multiclass':
            maxima = np.empty(n_samples, dtype=float)
            maxima.fill(-np.inf)
            argmaxima = np.zeros(n_samples, dtype=int)
            for i, (model_src, model_trg) in enumerate(self.estimators_):
                model = model_src if use_source_model else model_trg
                score = _predict_binary(model, X)
                if not use_source_model and self.inv_estimator_trg_[i]:
                    # aSVMs libSVM implementation returns the negative scores
                    #  if the first label in the training set is a negative label.
                    # Swap it back.
                    score *= -1.
                np.maximum(maxima, score, out=maxima)
                argmaxima[maxima == score] = i

            return self.label_binarizer_.classes_[argmaxima.T]
        else:
            model = self.estimators_[0][0] if use_source_model else self.estimators_[0][1]
            score = _predict_binary(model, X)
            if not use_source_model and self.inv_estimator_trg_[i]:
                # aSVMs libSVM implementation returns the negative scores
                #  if the first label in the training set is a negative label.
                # Swap it back.
                score *= -1
            argmaxima = score > thresh

            return self.label_binarizer_.inverse_transform(np.asarray(argmaxima))


    def __getstate__(self):
        result = self.__dict__.copy()

        # We cannot pickle the model directly as it is a C object:
        #  Instead, we'll dump the model into a temporary file and then
        #  pickle the content (string) of that file
        if 'model_src' in result:
            fp, fname = tempfile.mkstemp(prefix='asvm_')
            try:
                # dump the model
                svm_save_model(fname, self.model_src)

                # read the model as string
                with open(fname, 'r') as fpo:
                    result['model_src'] = fpo.read()
            except:
                raise
            finally:
                os.close(fp)
                os.unlink(fname) # remove temp file

        if 'model_trg' in result:
            fp, fname = tempfile.mkstemp(prefix='asvm_')
            try:
                # dump the model
                svm_save_model(fname, self.model_trg)

                # read the model as string
                with open(fname, 'r') as fpo:
                    result['model_trg'] = fpo.read()
            except:
                raise
            finally:
                os.close(fp)
                os.unlink(fname) # remove temp file

        return result


    def __setstate__(self, state):
        self.__dict__ = state.copy()

        # We cannot unpickle the model directly as it is a C object:
        #  Instead, we'll dump the model that is represented as a string
        #   into a temporary file and then load the model from that file
        #   using libSVM's load method
        if 'model_src' in state:
            fp, fname = tempfile.mkstemp(prefix='asvm_')
            try:
                # dump the (string) model to file
                with open(fname, 'w') as fpo:
                    fpo.write(state['model_src'])

                # load the model
                self.model_src = svm_load_model(fname)
            except:
                raise
            finally:
                os.close(fp)
                os.unlink(fname) # remove temp file

        if 'model_trg' in state:
            fp, fname = tempfile.mkstemp(prefix='asvm_')
            try:
                # dump the (string) model to file
                with open(fname, 'w') as fpo:
                    fpo.write(state['model_trg'])

                # load the model
                self.model_trg = svm_load_model(fname)
            except:
                raise
            finally:
                os.close(fp)
                os.unlink(fname) # remove temp file
