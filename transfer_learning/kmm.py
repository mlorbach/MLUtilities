# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-02-01 10:13:04
# @Last Modified by:   MAL
# @Last Modified time: 2016-02-29 11:58:50

import numpy as np
import logging
from base import SampleWeightEstimatorMixin
from scipy.spatial.distance import pdist
from cvxopt import matrix, solvers


class KMM(SampleWeightEstimatorMixin):
    """docstring for KMM"""

    def __init__(self, sigma=None, eps=None, B=1000., max_iter=-1, regression=False, sigma_ratio=1., sub_sample=None, verbose=False):
        super(KMM, self).__init__()
        self.sigma = sigma
        self.eps = eps
        self.B = B
        self.max_iter = max_iter
        self.regression = regression
        self.sigma_ratio = sigma_ratio
        self.sub_sample = 1 if sub_sample is None else int(sub_sample)
        self._is_fitted = False
        self.verbose = verbose


    def _rbf_dot(self, A, B, degree):

        G = np.sum(np.power(A, 2), 1, keepdims=True)
        H = np.sum(np.power(B, 2), 1, keepdims=True)

        H = G.repeat(B.shape[0], 1) + H.repeat(A.shape[0], 1).T - 2*(A.dot(B.T))

        return np.exp(-H/degree)


    def fit(self, X, y=None):

        logger = logging.getLogger(__name__)

        if not isinstance(X, tuple) or len(X) < 2:
            raise ValueError('KMM.fit() did not get target data.')

        X_src = X[0]
        X_trg = X[1]

        N_src_orig = X_src.shape[0]
        N_trg_orig = X_trg.shape[0]

        X_src = X_src[::self.sub_sample]
        X_trg = X_trg[::self.sub_sample]
        N_src = X_src.shape[0]
        N_trg = X_trg.shape[0]

        logger.debug('N_src = {}, N_trg = {}'.format(N_src, N_trg))

        if not self.verbose:
            solvers.options['show_progress'] = False

        # if not specified, attempt to estimate a good sigma automatically
        #  auto-method is based on author's suggestions:
        # % if sigma < 0
        # %     ratio = abs(sigma);
        # %     PairDist = pdist2(X_trainset, X_testset);
        # %     sigma = (median(PairDist(:)) * ratio)^2;
        # %     fprintf(1,'--> auto-select sigma = %d', sigma);
        # % end

        if self.sigma is None or self.sigma <= 0:
            pair_dist = pdist(X_src)
            self.sigma = np.power(np.median(pair_dist * self.sigma_ratio), 2)
            logger.info('Auto select sigma = {}'.format(self.sigma))


        # % variables: (here in the program / in (12) in the paper)
        # % H is K
        # % f is kappa

        # % minimize...
        # % 'calculating H=K...'
        # H = rbf_dot(X,X,sigma);
        # H=(H+H')/2;

        H = self._rbf_dot(X_src, X_src, self.sigma)

        # make the matrix symmetric (it isn't symmetric before because of bad precision)
        H = (H + H.T)/2

        # % 'calculating f=kappa...'
        # %R3 = rbf_dot(X,Xtst,sigma,0); %by MYQ@april-10-2012
        # R3 = rbf_dot(X,Xtst,sigma);
        # f=(R3*ones(ntestsamples, 1));
        # f=-nsamples/ntestsamples*f;

        R3 = self._rbf_dot(X_src, X_trg, self.sigma)

        f = (R3.dot( np.ones((N_trg, 1))) )
        f *= -N_src/float(N_trg)

        # A=ones(1,nsamples);
        # A(2,:)=-ones(1,nsamples);
        # b=[nsamples*(eps+1); nsamples*(eps-1)];

        if self.eps is None:
            self.eps = (np.sqrt(N_src)-1.)/np.sqrt(N_src)
            #self.eps = self.B / (2.*np.sqrt(N_src))

        LB = np.zeros((N_src, 1))
        UB = np.ones((N_src, 1)) * self.B

        A = np.ones((2,N_src))
        A[1, :] *= -1
        A = np.vstack ((A, np.eye(N_src)))
        A = np.vstack ((A, -np.eye(N_src)))
        b = np.vstack( (N_src*(self.eps+1), N_src*(self.eps+1), UB, LB) )

        # Aeq = [];
        # beq = [];
        # % 0 <= beta_i <= 1000 for all i
        # % default B = 1000;
        # LB = zeros(nsamples,1);
        # UB = ones(nsamples,1).* B;

        # % X=QUADPROG(H,f,A,b,Aeq,beq,LB,UB) attempts to solve the quadratic programming problem:
        # %              min 0.5*x'*H*x + f'*x
        # % subject to:  A*x <= b
        # %              Aeq*x = beq
        # %              LB <= x <= UB

        H = matrix(H)
        f = matrix(f)
        A = matrix(A)
        b = matrix(b)

        # matlab equivalent variable namings:
        #  Python --> Matlab
        #  x --> x
        #  P --> H
        #  q --> f
        #  G --> A
        #  h --> b
        #  A --> Aeq
        #  b --> beq
        #  the below call is equivalent to: solvers.qp(P, q, G, h)
        sol = solvers.qp(H, f, A, b)
        self._sample_weight = np.zeros( (N_src_orig) )
        subsample_sample_weights = np.ravel(np.asarray(sol['x']))

        if not self.regression:
            # guarantee that all beta greater than 0 for the non-regression case
            threshold = .01 * np.abs(np.median(subsample_sample_weights))
            subsample_sample_weights[ subsample_sample_weights < threshold ] = threshold
            logger.info('Number of beta < {}: {} (0 is good)'.format(threshold, np.sum(subsample_sample_weights < threshold)))

        self._sample_weight[::self.sub_sample] = subsample_sample_weights

        self._is_fitted = sol['status'] == 'optimal'
        self._sol_status = sol['status']
        logger.debug('Solver converged to "{}" solution.'.format(sol['status']))

        return self

    def transform(self, X):
        return X
