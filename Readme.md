MLUtilities
-----------

MLUtilities is a Python library comprising a number of algorithms and utilities for applied Machine Learning and Pattern Recognition. It contains code that I have produced for the PhenoRat project in the period between 01/04/2014 until about 31/10/2016 and beyond that at University Utrecht.

The core algorithms either use or extend the scikit-learn (sklearn) Machine Learning library. We further make extensive use of Pandas (a library for handling matrix-like data structures and time-series data), numpy (low-level matrix and math algorithms) and scipy (scientific computations, mostly the statistics module).

MLUtilities is a living under-development library. Please don't expect thoroughly tested functions that are usable with all kind of input data. Some functions are very specialized.

## Prerequisites
- scikit-learn (sklearn): http://scikit-learn.org/
- Pandas: http://pandas.pydata.org/
- numpy: http://www.numpy.org/
- scipy: https://www.scipy.org/
- scikit-image (skimage): http://scikit-image.org/
- matplotlib: http://matplotlib.org/
- seaborn: https://seaborn.github.io/

## Modules
The package is divided into several modules. Please refer to the module documentation for more details on what is included.

- [annotation](annotation): import/export and processing of time-series labeling
- [classification](classification): extensions of existing classification algorithms
- [dataset](dataset): exposes high-level dataset handling, e.g., loading datasets from disk
- [evaluation](evaluation): classification performance metrics
- [external](external): external libraries that are not available as official Python packages
- [features](features): feature extraction for social interaction detection from trajectory data
- [models](models): extended (statistical) data models, e.g., mixtures of Gaussian
- [parallelization](parallelization): utilities to parallelize heavy computations
- [semi_supervised](semi_supervised): framework for active learning
- [sklearn_extensions](sklearn_extensions): direct extensions or small modifications of existing sklearn classes
- [transfer_learning](transfer_learning): algorithms for transfer learning and domain adaptation
- [visualization](visualization): plotting and graphical output
