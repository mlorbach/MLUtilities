# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-08-08 11:57:32
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-11-21 16:29:19


import numpy as np
import pandas as pd
import logging

from scipy.stats import entropy

from sklearn.preprocessing import LabelEncoder


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class RandomLearner(object):
    """docstring for RandomLearner"""
    def __init__(self):
        super(RandomLearner, self).__init__()

    def fit(self, X, y=None):
        self.X = X
        self.U = set(range(X.shape[0]))

    def query(self, oracle):

        # select sample
        idx = np.random.choice(tuple(self.U))
        x = self.X[idx]

        # query sample
        y = oracle.predict_from_index(idx)

        # update model
        self.U.remove(idx)

        # return
        return x, y, idx


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class ClusterLearner(object):
    """
    Base class for cluster based active learning. This class is fully functional but for something smart to happen, sub-class from ClusterLearner and overwrite the `_select_cluster()` and `_select_sample()` methods.

    Attributes
    ----------
    C : TYPE
        Cluster assignment of X
    cluster_alg : BaseEstimator
        Clustering class providing fit(), predict() and score_samples()
    L : set
        Pool of labeled instances (stores indices of X)
    n_classes : int
        Number of classes in the classification problem
    n_clusters : int
        Upper bound or number of known clusters
    U : set
        Pool of unlabeled instances (stores indices of X)
    X : np.ndarray
        The data of entire training set (n_samples, n_features)
    Y : np.ndarray
        Holding the labels of the training set with -1 representing an unknown label (n_samples, )
    """
    def __init__(self, cluster_alg, n_classes):
        super(ClusterLearner, self).__init__()
        self.cluster_alg = cluster_alg
        self.n_classes = n_classes

    def fit(self, X, y=None):

        self._le = LabelEncoder()
        if not hasattr(self.cluster_alg, 'gamma_') and not hasattr(self.cluster_alg, 'inertia_') and not (hasattr(self.cluster_alg, 'converged_') and self.cluster_alg.converged_):
            logger = logging.getLogger()
            logger.info('Clusterer not fitted. Perform clustering...')
            self.cluster_alg.fit(X)

        self.X = X
        self.C = self.cluster_alg.predict(X)
        self.C = self._le.fit_transform(self.C)  # cluster assignment
        self.n_clusters = len(self._le.classes_)
        self.Y = np.empty_like(self.C)  # known labels
        self.Y.fill(-1)  # label is -1 if unknown

        self.U = set(range(len(self.C)))  # set of unlabeled instances
        self.L = set()  # set of labeled instances

    def _get_cluster_distributions(self, include_unlabeled=True):

        # group Y based on C, then count
        d = pd.DataFrame(np.stack((self.Y + 1,
                                   self.C),
                                   axis=1)).groupby(1)[0].apply(np.bincount, minlength=self.n_classes + 1)
        if include_unlabeled:
            return np.stack(d.values)
        else:
            return np.stack(d.values)[:, 1:]

    def _get_cluster_label_entropy(self):
        y_counts = self._get_cluster_distributions(include_unlabeled=False)
        H = np.apply_along_axis(entropy, 1, y_counts)
        return H

    def _select_cluster(self, candidate_mask):
        """
        Picks a random cluster.

        Parameters
        ----------
        candidate_mask : array-like, bool
            Boolean array indicating cluster candidates to choose from.

        Returns
        -------
        int
            Index of chose cluster
        """
        return np.random.choice(np.where(candidate_mask)[0])

    def _select_sample(self, cluster_index):
        """
        Picks a random sample from the cluster.

        Parameters
        ----------
        cluster_index : int
            Index of cluster

        Returns
        -------
        int
            Index of chosen sample (index of self.X)
        """

        # get all indices of samples in this cluster
        cluster_mask_idx = np.where(self.C == cluster_index)[0]

        # only allow samples that we haven't sampled yet
        cluster_mask_idx = cluster_mask_idx[np.in1d(cluster_mask_idx,
                                                    tuple(self.L),
                                                    assume_unique=True,
                                                    invert=True)]

        # pick at random
        return np.random.choice(cluster_mask_idx)

    def query(self, oracle):
        """
        Query the oracle for a new label.

        Parameters
        ----------
        oracle : Oracle
            The oracle that gives us the true label of a chosen sample.

        Returns
        -------
        x, y, idx
            Return the sample x, its label y and its index in self.X
        """

        logger = logging.getLogger()

        # select cluster candidates
        # -------------------------

        # we know we can't query exhausted clusters anymore, so remove
        #  the clusters from c_count without unlabeled examples:
        u_count = np.bincount(self.C.take(list(self.U)),
                              minlength=self.n_clusters)

        c_candidate_mask = u_count > 0

        # determine cluster and sample
        # ----------------------------
        selected_c = self._select_cluster(c_candidate_mask)
        selected_idx = self._select_sample(selected_c)

        # query sample
        # ------------
        x = self.X[selected_idx]
        y = oracle.predict_from_index(selected_idx)

        # update model
        # ------------
        self.U.remove(selected_idx)
        self.L.add(selected_idx)
        self.Y[selected_idx] = y

        # return
        # ------
        return x, y, selected_idx


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class MinClusterClusterLearner(ClusterLearner):
    """
    Picks the cluster with the least amount of labeled samples in it.
    """
    def __init__(self, cluster_alg, n_classes):
        super(MinClusterClusterLearner, self).__init__(cluster_alg, n_classes)

    def _select_cluster(self, candidate_mask):

        # count the number of labeled examples in each cluster
        c_count = np.bincount(self.C.take(list(self.L)),
                              minlength=self.n_clusters)

        c_count[~candidate_mask] = np.iinfo(np.int).max  # setting to sth large

        # return the cluster with the least labeled samples
        return np.argmin(c_count)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class LikelihoodSampleClusterLearner(MinClusterClusterLearner):
    """
    Cluster input data, then for a query pick the cluster with the least
    amount of labeled samples. Pick an unlabeled sample of that cluster
    according to its probability given the cluster model.

    I.e., within the clusters we are more likely to sample samples close to the mean instead of picking a random sample as in ClusterRandomLearner.
    """
    def __init__(self, cluster_alg, n_classes):
        super(LikelihoodSampleClusterLearner, self).__init__(cluster_alg, n_classes)

    def _select_sample(self, cluster_index):

        cluster_mask_idx = np.where(self.C == cluster_index)[0]
        cluster_mask_idx = cluster_mask_idx[np.in1d(cluster_mask_idx,
                                                    tuple(self.L),
                                                    assume_unique=True,
                                                    invert=True)]

        # if hasattr(self.cluster_alg, 'score_samples'):
        #     p = self.cluster_alg.score_samples(self.X[cluster_mask_idx])
        if hasattr(self.cluster_alg, 'predict_proba'):
            p = self.cluster_alg.predict_proba(self.X[cluster_mask_idx])
        else:
            p = -self.cluster_alg.transform(self.X[cluster_mask_idx])
        p = p[:, self._le.inverse_transform(cluster_index)]
        p = p / np.sum(p, keepdims=True)

        return np.random.choice(cluster_mask_idx, p=p)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class MaxEntropyClusterLearner(ClusterLearner):
    """
    Picks cluster with the highest entropy of known labels.

    To ensure to be able to start off, the entropy criteria only becomes active once all clusters have at least `r` labeled samples. Until then clusters are picked in alternating order.

    Attributes
    ----------
    cluster_alg : TYPE
        Description
    n_classes : TYPE
        Description
    """
    def __init__(self, cluster_alg, n_classes, r=3):
        super(MaxEntropyClusterLearner, self).__init__(cluster_alg, n_classes)
        self.r = r

    def _select_cluster(self, candidate_mask):
        # select the cluster with the highest entropy:

        # number of known labels in each cluster:
        c_count = np.bincount(self.C.take(list(self.L)),
                              minlength=self.n_clusters)

        c_count[~candidate_mask] = np.iinfo(np.int).max  # setting to sth large

        if any(c_count < self.r):
            return np.argmin(c_count)
        else:
            H = self._get_cluster_label_entropy()
            H[~candidate_mask] = -np.inf  # only considering candidates
            return np.argmax(H)


class PEntropyClusterLearner(MaxEntropyClusterLearner):
    """docstring for PEntropyClusterLearner"""
    def __init__(self, cluster_alg, n_classes, r=3):
        super(PEntropyClusterLearner, self).__init__(cluster_alg, n_classes, r)

    def _select_cluster(self, candidate_mask):
        # select the cluster with a specific entropy:

        # number of known labels in each cluster:
        c_count = np.bincount(self.C.take(list(self.L)),
                              minlength=self.n_clusters)

        c_count[~candidate_mask] = np.iinfo(np.int).max  # setting to sth large

        if any(c_count < self.r):
            return np.argmin(c_count)
        else:
            H = self._get_cluster_label_entropy()

            # add some small value to enable all clusters to be picked
            #  even if their entropy is 0.
            H += 1e-3
            H[~candidate_mask] = 0  # but only considering candidates

            H = H / np.sum(H, keepdims=True)

            # pick a random cluster according to a probability mass proportional to the cluster's entropy
            return np.random.choice(range(len(H)), p=H)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
