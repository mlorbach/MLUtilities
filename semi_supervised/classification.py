# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-12-21 13:10:19
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-09-13 16:35:14

import numpy as np
import warnings

from sklearn.externals.joblib import Parallel, delayed

from sklearn.utils import check_X_y, compute_class_weight
from sklearn.utils.multiclass import _check_partial_fit_first_call
from sklearn.linear_model.stochastic_gradient import (SGDClassifier,
                                                      fit_binary,
                                                      DEFAULT_EPSILON)
from sklearn.exceptions import ConvergenceWarning

from MLUtilities.semi_supervised.utils import compute_class_weight_semi_supervised


# fit(X, y)  -->  always calls _fit(X, y)

# _fit(X, y) -->  check X y, set classes & initialization,
#   then call _partial_fit(X, y)

# _partial_fit(X, y) --> check X y (again), allocate data structures,
# then call either _fit_multiclass(X, y) if n_classes > 2, or _fit_binary(X, y)

# _fit_multiclass(X, y) --> split ovr and call in parallel fit_binary(i, X, y)
# _fit_binary(X, y) --> calls fit_binary(1, X, y)

# fit_binary(i_class, X, y) --> actual call to SGD

######
#  Need to modify
#  - _fit --> modify check and determine unique y
#  - _partial_fit --> modify check, compute_class_weight
#  - _fit_multiclass --> change call to fit_binary such that (X,y) contain only the pos (1) and neg (0) samples for each specific class, kick out everything that is not 0/1. Then call fit positive class = 1.
#  - _fit_binary --> nothing

class SemiSupervisedSGDClassifier(SGDClassifier):
    """docstring for SemiSupervisedSGDClassifier"""
    def __init__(self, loss="hinge", penalty='l2', alpha=0.0001, l1_ratio=0.15,
                 fit_intercept=True, max_iter=5, tol=None, shuffle=True, verbose=0,
                 epsilon=DEFAULT_EPSILON, n_jobs=1, random_state=None,
                 learning_rate="optimal", eta0=0.0, power_t=0.5,
                 class_weight=None, warm_start=False, average=False):
        super(SemiSupervisedSGDClassifier, self).__init__(
            loss=loss, penalty=penalty, alpha=alpha, l1_ratio=l1_ratio,
            fit_intercept=fit_intercept, max_iter=max_iter, tol=tol, shuffle=shuffle,
            verbose=verbose, epsilon=epsilon, n_jobs=n_jobs,
            random_state=random_state, learning_rate=learning_rate, eta0=eta0,
            power_t=power_t, class_weight=class_weight, warm_start=warm_start,
            average=average)

        self.supports_indicator_matrix = True

    def _partial_fit(self, X, y, alpha, C,
                     loss, learning_rate, max_iter,
                     classes, sample_weight,
                     coef_init, intercept_init):

        y_is_indicator_matrix = y.ndim > 1
        X, y = check_X_y(X, y, 'csr', dtype=np.float64, order="C",
                         multi_output=y_is_indicator_matrix)

        n_samples, n_features = X.shape

        self._validate_params()
        _check_partial_fit_first_call(self, classes)

        n_classes = self.classes_.shape[0]

        # Allocate datastructures from input arguments
        if y_is_indicator_matrix:
            self._expanded_class_weight = compute_class_weight_semi_supervised(
                                                self.class_weight,
                                                y)
        else:
            self._expanded_class_weight = compute_class_weight(self.class_weight, self.classes_, y)

        sample_weight = self._validate_sample_weight(sample_weight, n_samples)

        if getattr(self, "coef_", None) is None or coef_init is not None:
            self._allocate_parameter_mem(n_classes, n_features,
                                         coef_init, intercept_init)
        elif n_features != self.coef_.shape[-1]:
            raise ValueError("Number of features %d does not match previous "
                             "data %d." % (n_features, self.coef_.shape[-1]))

        self.loss_function_ = self._get_loss_function(loss)
        if not hasattr(self, "t_"):
            self.t_ = 1.0

        # delegate to concrete training procedure
        if n_classes > 2:
            self._fit_multiclass(X, y, alpha=alpha, C=C,
                                 learning_rate=learning_rate,
                                 sample_weight=sample_weight,
                                 max_iter=max_iter)
        elif n_classes == 2:
            self._fit_binary(X, y, alpha=alpha, C=C,
                             learning_rate=learning_rate,
                             sample_weight=sample_weight,
                             max_iter=max_iter)
        else:
            raise ValueError("The number of class labels must be "
                             "greater than one.")

        return self

    def _fit(self, X, y, alpha, C, loss, learning_rate, coef_init=None,
             intercept_init=None, sample_weight=None):
        if hasattr(self, "classes_"):
            self.classes_ = None

        y_is_indicator_matrix = y.ndim > 1

        X, y = check_X_y(X, y, 'csr', dtype=np.float64, order="C",
                         multi_output=y_is_indicator_matrix)
        n_samples, n_features = X.shape

        if y_is_indicator_matrix:
            classes = np.arange(y.shape[1])
        else:
            # labels can be encoded as float, int, or string literals
            # np.unique sorts in asc order; largest class id is positive class
            classes = np.unique(y)

        if self.warm_start and hasattr(self, "coef_"):
            if coef_init is None:
                coef_init = self.coef_
            if intercept_init is None:
                intercept_init = self.intercept_
        else:
            self.coef_ = None
            self.intercept_ = None

        if self.average > 0:
            self.standard_coef_ = self.coef_
            self.standard_intercept_ = self.intercept_
            self.average_coef_ = None
            self.average_intercept_ = None

        # Clear iteration count for multiple call to fit.
        self.t_ = 1.0

        self._partial_fit(X, y, alpha, C, loss, learning_rate, self.max_iter,
                          classes, sample_weight, coef_init, intercept_init)

        if (self.tol is not None and self.tol > -np.inf
                and self.n_iter_ == self.max_iter):
            warnings.warn("Maximum number of iteration reached before "
                          "convergence. Consider increasing max_iter to "
                          "improve the fit.",
                          ConvergenceWarning)
        return self

    def _fit_multiclass(self, X, y, alpha, C, learning_rate,
                        sample_weight, max_iter):
        """Fit a multi-class classifier by combining binary classifiers

        Each binary classifier predicts one class versus all others. This
        strategy is called OVA: One Versus All.
        """

        y_is_indicator_matrix = y.ndim > 1

        # Use joblib to fit OvA in parallel.
        if y_is_indicator_matrix:
            # supporting unlabeled samples (assumes pos_label = 1, neg_label = 0, unlabeled_label = 2)

            # we need to fake the class labels so that fit_binary doesn't get confused because y doesn't contain the actual class labels
            # we will restore the original class set after fitting
            orig_classes = self.classes_
            self.classes_ = np.asarray([1] * self.classes_.shape[0])

            result = Parallel(n_jobs=self.n_jobs, backend="threading",
                              verbose=self.verbose)(
                delayed(fit_binary)(self, i, X[y[:, i] < 2, :],
                                    y[y[:, i] < 2, i], alpha, C,
                                    learning_rate,
                                    max_iter, self._expanded_class_weight[i],
                                    1., sample_weight[y[:, i] < 2])
                for i in range(len(self.classes_)))

            # restore original class set
            self.classes_ = orig_classes
        else:
            # standard implementation for normal multiclass
            result = Parallel(n_jobs=self.n_jobs, backend="threading",
                              verbose=self.verbose)(
                delayed(fit_binary)(self, i, X, y, alpha, C, learning_rate,
                                    max_iter, self._expanded_class_weight[i],
                                    1., sample_weight)
                for i in range(len(self.classes_)))

        # take the maximum of n_iter_ over every binary fit
        n_iter_ = 0.
        for i, (_, intercept, n_iter_i) in enumerate(result):
            self.intercept_[i] = intercept
            n_iter_ = max(n_iter_, n_iter_i)

        self.t_ += n_iter_ * X.shape[0]
        self.n_iter_ = n_iter_

        if self.average > 0:
            if self.average <= self.t_ - 1.0:
                self.coef_ = self.average_coef_
                self.intercept_ = self.average_intercept_
            else:
                self.coef_ = self.standard_coef_
                self.standard_intercept_ = np.atleast_1d(self.intercept_)
                self.intercept_ = self.standard_intercept_
