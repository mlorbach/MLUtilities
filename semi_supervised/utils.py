# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-21 13:50:56
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-12-21 16:52:35

import numpy as np
from sklearn.metrics import precision_recall_fscore_support


def compute_class_weight_semi_supervised(class_weight, y, pos_label=1, neg_label=0, unlabeled_label=2):
    """Estimate class weights for unbalanced datasets in semi-supervised or multilabel settings where y is given by an indicator matrix.

    Parameters
    ----------
    class_weight : dict, 'balanced' or None
        If 'balanced', class weights will be given by
        ``n_samples / (n_classes * np.bincount(y))``.
        If a dictionary is given, keys are classes and values
        are corresponding class weights.
        If None is given, the class weights will be uniform.

    y : array-like, shape (n_samples, n_classes)
        Array of original class labels per sample;

    pos_label : optional
        The label that indicates positive samples.
    neg_label : optional
        The label that indicates negative samples.
    unlabeled_label : optional
        The label that indicates that a sample has no label.

    Returns
    -------
    class_weight_vect : ndarray, shape (n_classes,)
        Array with class_weight_vect[i] the weight for i-th class

    References
    ----------
    The "balanced" heuristic is inspired by
    Logistic Regression in Rare Events Data, King, Zen, 2001.
    """
    classes = np.arange(y.shape[1])

    if class_weight is None or len(class_weight) == 0:
        # uniform class weights
        weight = np.ones(classes.shape[0], dtype=np.float64, order='C')
    elif class_weight in ['auto', 'balanced']:
        # Find the weight of each class as present in y.
        # le = LabelEncoder()
        # y_ind = le.fit_transform(y)
        # if not all(np.in1d(classes, le.classes_)):
        #     raise ValueError("classes should have valid labels that are in y")
        # recip_freq = len(y) / (len(le.classes_) *
        #                        bincount(y_ind).astype(np.float64))
        # weight = recip_freq[le.transform(classes)]

        pos_freq = (y == pos_label).sum(axis=0)
        neg_freq = (y == neg_label).sum(axis=0)

        weight = (pos_freq + neg_freq) / (len(classes) *
                                          pos_freq).astype(np.float64)

    else:
        # user-defined dictionary
        weight = np.ones(classes.shape[0], dtype=np.float64, order='C')
        if not isinstance(class_weight, dict):
            raise ValueError("class_weight must be dict, 'balanced', or None,"
                             " got: %r" % class_weight)
        for c in class_weight:
            i = np.searchsorted(classes, c)
            if i >= len(classes) or classes[i] != c:
                raise ValueError("Class label %d not present." % c)
            else:
                weight[i] = class_weight[c]

    return weight


def precision_recall_fscore_support_semi_supervised(y_true, y_pred,
                                                    beta=1.0,
                                                    neg_label=0, pos_label=1):

    y_true = np.asarray(y_true)
    y_pred = np.asarray(y_pred)

    if y_true.ndim < 2 or y_pred.ndim < 2:
        raise TypeError('This function is meant for indicator matrix representations of labels. For normal labels, please use the sklearn method.')

    if y_true.shape != y_pred.shape:
        raise AttributeError('The true and prediction label matrices must have the same shape.')

    labels = np.asarray([neg_label, pos_label])

    def getscores(yt, yp, i):
        mask = np.in1d(yt[:, i], labels)
        return precision_recall_fscore_support(yt[mask, i], yp[mask, i],
                                               labels=[pos_label],
                                               pos_label=pos_label)

    scores = []
    for i in range(y_true.shape[1]):
        scores.append(getscores(y_true, y_pred, i))
    return np.hstack(scores).T
