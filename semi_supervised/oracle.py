# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-08-08 12:37:04
# @Last Modified by:   MAL
# @Last Modified time: 2016-08-15 15:50:57


import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.preprocessing import LabelEncoder


class BaseOracle(BaseEstimator, ClassifierMixin):
    """
    Base class for all Oracles. Not to be instantiated.

    This is a base class for "fake" oracles that are based on having access to the ground truth labels (i.e. from a labeled dataset). Such oracle can only be used in development environments, but not in practice.

    Attributes
    ----------
    other_label : int
        Which label to use for undefined/other classes (non-target classes).
    uncertain_label : int
        Which label to use for rejected or uncertain instances.
    """
    def __init__(self, other_label=-2, uncertain_label=-3):
        super(BaseOracle, self).__init__()
        self.other_label = other_label
        self.uncertain_label = uncertain_label

    def fit(self, X, y):
        raise NotImplementedError()

    def predict_from_index(self, Idx):
        raise NotImplementedError()

    def predict(self, X):
        raise NotImplementedError()


class TrueOracle(BaseOracle):
    """
    Oracle that always returns the ground truth label provided during fitting.

    Attributes
    ----------
    y : np.ndarray
        The ground truth labels (n_samples,)
    """
    def __init__(self, other_label=-2, uncertain_label=-3):
        super(TrueOracle, self).__init__(other_label, uncertain_label)

    def fit(self, X, y):
        self.y = y

    def predict_from_index(self, Idx):
        return self.y.take(Idx, axis=0)


class NoisyOracle(BaseOracle):
    """
    Oracle that provides noisy labeling. With a certain probability the oracle will return a wrong label instead of the ground truth label.

    The level of noise is specified via parameter `p`. `p` can be a single float between [0,1], a list of floats with length equal to the number of classes, or a full confusion matrix.

      - If `p` is a single float, it determines the probability that a query is answered with a wrong label. The label returned is randomly picked.
      - If `p` is a list (of length `n_classes`), the items correspond to each class' probability of being answered with a wrong label. Thus the noise can be specified individually per label.
      - If `p` is a matrix (n_classes, n_classes), it specifies the full confusion matrix based upon which wrong labels are returned. Read in row-first order: rows correspond to true labels, columns to the returned labels. Each row in `p` is normalized to sum to one.

    Attributes
    ----------
    p : TYPE
        Description
    y : TYPE
        Description
    """
    def __init__(self, p=.01, other_label=-2, uncertain_label=-3):
        super(NoisyOracle, self).__init__(other_label, uncertain_label)
        self.p = p

    def _compute_pmatrix(self, p):
        """
        Convenience function to get a confusion matrix for the noise level `p`.

        Parameters
        ----------
        p : float, array-like of floats (n_classes), or matrix (n_classes, n_classes)
            Noise parameter `p`. See class doc for details.
        """

        if isinstance(p, (int, float)):
            if (p < 0) or (p > 1):
                raise ValueError('p must be in range [0,1] but is {}'.format(p))

            # p is single float: repeat to match number of labels
            p = np.ones(len(self.labels)) * float(p)

        if isinstance(p, (list, tuple, np.ndarray)):
            p = np.asarray(p).squeeze()

            if p.ndim == 1:
                # p is a list/array; make a confusion matrix out of it

                # number of elements == number of labels
                N = p.shape[0]

                if N != len(self.labels):
                    raise AttributeError('p has to match number of labels ({}) but is of length {}.'.format(len(self.labels), N))

                # force all p's to be within 0 and 1:
                p = np.fmin(np.fmax(p, 0), 1)

                # construct confusion matrix: first the off-diagonals
                pm = np.multiply(1. - np.eye(N),
                                 p[:, np.newaxis] / float(N - 1))

                # then the diagonal
                pm += np.eye(N) * (1. - p)

            elif p.ndim == 2:
                # we are given a confusion matrix

                # make sure it's square and matches the number of labels
                if p.shape[0] != p.shape[1] or p.shape[0] != len(self.labels):
                    raise AttributeError('If p is a matrix it must of square shape ({}) but it is of shape {}.'.format((len(self.labels), len(self.labels)), p.shape))

                # make sure it's floaty
                pm = p.astype(np.float)

                # normalize rows to sum to one
                pm = pm / np.sum(pm, axis=1, keepdims=True)

            else:
                raise AttributeError('p must be 1 or 2-dimensional but is of dimension {}'.format(p.ndim))
        else:
            raise TypeError('p must be either a float or a np.ndarray but is of type {}.'.type(p))

        return pm

    def fit(self, X, y):
        self._le = LabelEncoder()
        self.y = self._le.fit_transform(y)
        self.labels = self._le.classes_
        self._p = self._compute_pmatrix(self.p)

    def predict_from_index(self, Idx):
        true_y = self.y.take(Idx, axis=0)
        pred_y = np.zeros_like(true_y, dtype=np.int)

        # since we most often are going to query/predict a small number of instances, this for-loop is fine. If we ever want to predict 10s of thousands instances, we probably want to make this more efficient:
        for i, y in enumerate(true_y):
            pred_y[i] = np.random.choice(self.labels, size=1, p=self._p[y, :])

        return self._le.inverse_transform(pred_y)


    # def predict(self, X):

    #     X = np.asarray(X)
    #     if X.ndim < 2:
    #         raise AttributeError('Samples X must be 2d array.')

    #     l = []
    #     for x in X:
    #         res = np.where(np.all(x == self.X, axis=1))
    #         if len(res[0]) == 0 and self.raise_unknown:
    #             raise ValueError('Oracle doesn''t know this sample: {}'.format(x))
    #         elif len(res) == 0:
    #             res = [[0]]

    #         l.append(res[0][0])

    #     return self.y.take(l, axis=0)
