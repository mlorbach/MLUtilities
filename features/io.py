# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-07-04 14:19:34
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-10-18 15:03:59

import os
import numpy as np
import pandas as pd
import datetime

from ..io_custom import mkdir_p


def save_features_to_observer(df, filename, timeoffset=0, feature_cols=None, fps=None):


    if not os.path.isdir(os.path.realpath(os.path.dirname(filename))):
        mkdir_p(os.path.realpath(os.path.dirname(filename)))



    if df.index.nlevels > 1:
        raise AttributeError('MultiIndex is not supported.')


    df = df.copy()

    if df.index.name == 'frame' and fps is not None:
        df.index = df.index / float(fps)

    df.index.name = 'time'

    # start time offset in seconds
    if timeoffset is not None:
        if isinstance(timeoffset, datetime.timedelta):
            secs_offset = timeoffset.total_seconds()
        else:
            secs_offset = float(timeoffset)
    else:
        secs_offset = 0

    df.index = df.index + secs_offset


    if feature_cols is not None and isinstance(feature_cols, (list, tuple)):
        df = df.loc[:, feature_cols]

    with open(filename, 'wb') as outfile:
        outfile.writelines([
            'Dataset:\t{}\n'.format(os.path.splitext(os.path.basename(filename))[0]),
            'Start time:\t{0:.3f}\n'.format(secs_offset),
            'FPS:\t{}\n'.format(fps)])
        df.to_csv(outfile, sep=';', encoding='utf8')
