# -*- coding: utf-8 -*-
"""
Created on Mon Nov 24 13:21:11 2014

@author: MAL
"""

import pandas as pd
import numpy as np
import logging


mapping_numerical_z_order = {
'equal': 0.,
'below': -1.,
'above': 1.,
'unknown': np.nan
}


def load_z_order_dataframe_from_event_log(eventlogfile, scored_subject_name='subject_white'):
    
    logger = logging.getLogger(__name__)    
    
    try:
        df = pd.read_csv(eventlogfile, sep=';', index_col=['Time_Relative_sf'])
    except ValueError as err:
        logger.error(err.message)
        print err.message
        raise ValueError('Observer exports in weird encoding, please convert the file to UTF8.')

    df.index.name = 'time start'
    df = df.reset_index()

    # remove irrelevant columns:
    drop_columns = ['Date_Time_Absolute_dmy_hmsf', 'Date_dmy', 'Time_Absolute_hms', 'Time_Absolute_f', 'Time_Relative_hmsf', 'Time_Relative_hms','Time_Relative_f', 'Event_Log', 'Duration_sf', 'Observation']
    unnamed_columns = [name for name in df.columns if name.startswith('Unnamed: ')]
    drop_columns += unnamed_columns
    df = df.drop(drop_columns, axis=1)
    
    df.rename(columns={'Behavior':'action'}, inplace=True)
    df['track'] = scored_subject_name
    
    # remove the Observation Start event
    df = df.query('action != "Start"')
    
    
    # replace text label by numeric values:
    df.replace( {'action': mapping_numerical_z_order}, inplace=True )
    
    
    # join start and stop event into one row per event
    t_stop = df.loc[ df['Event_Type'] == "State stop", 'time start'].values
    df = df.query('Event_Type == "State start"')
    df.is_copy = False # turns off warning about chained assignment. It's a false positive in this case.
    
    df['time end'] = t_stop
    return df.drop('Event_Type', axis=1).reset_index(drop=True)
    
    
def add_z_order_to_feature_dataframe(features, zorder_eventlog_dataframe, scored_subject_name='subject_white'):
    
    logger = logging.getLogger(__name__)   
    
    animal_names = features.keys()
    
    if scored_subject_name not in animal_names:
        logger.error('Features do not contain the chosen subject "{}": {}'.format(scored_subject_name, animal_names))
        raise AttributeError('Features do not contain the chosen subject "{}": {}'.format(scored_subject_name, animal_names))
        
    start_col = 'time start'
    end_col = 'time end'
        
    for subj in animal_names:
        
        features[subj]['z_order'] = 0.
        features[subj]['z_order_certainty'] = 0.
        
        if subj == scored_subject_name:
            
            for bout in zorder_eventlog_dataframe.iterrows():
                zorder = bout[1]['action']
                
                indexer = slice(bout[1][  start_col  ],bout[1][  end_col  ])
                if not np.isnan(zorder):
                    features[subj].loc[ indexer , ['z_order', 'z_order_certainty'] ] = (zorder, 1.)
                else:
                    features[subj].loc[ indexer , ['z_order', 'z_order_certainty'] ] = (0., 0.)
            
        else:
            
            # invert 
            for bout in zorder_eventlog_dataframe.iterrows():
                zorder = bout[1]['action']
                
                indexer = slice(bout[1][  start_col  ],bout[1][  end_col  ])
                if not np.isnan(zorder):
                    features[subj].loc[ indexer, ['z_order', 'z_order_certainty'] ] = (-zorder, 1.)
                else:
                    features[subj].loc[ indexer, ['z_order', 'z_order_certainty'] ] = (0., 0.)
                    
    
    return features