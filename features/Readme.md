Documentation of the feature computation for social interaction recognition in rodents
======================================================================================

## Individual features

Individual features are feature derived from one specific subject and its trajectory.

The features can be grouped in two main categories:

 - Pose features
 - Motion features


### Pre-processing

The raw body point trajectories are smoothed by triangular kernel in a sliding window of length 13 frames.


### Pose features

All features are derived from the trajectories of three tracked body points: the nose $\vec{n}$, the center $\vec{c}$ and the tail base $\vec{t}$. Each point is a vector $\mathbb{R}^2$ with an $x$ and an $y$ component.

We further define vectors between these points: from center to nose $\vec{cn}$ and from center to tail base $\vec{ct}$. Both vectors are divided by the measured animal length $l$ (i.e., not the `length` feature but a provided measurement).

We compute the following pose features.

#### Length
The total body length from tail base to nose via the center point.

$$ length = |\vec{cn}| + |\vec{ct}| $$

#### Body parts lengths ratio
The length of the center-nose vector in relation to the center-tail vector.

$$ cog_{ratio} = \frac{|\vec{cn}|}{|\vec{ct}|} $$

#### Bending
The bending of the body is expressed by the angle between the center-nose and center-tail vecctors.

$$ bending = \log( 1 - \angle(\vec{cn}, \vec{ct}) / \pi ) $$


### Motion features

All motion features are derived from the smoothed trajectories.

First we compute some intermediate values of each body point's $p$ own velocity. Note that the velocities are normalized by the measure animal length $l$.

$$ \frac{dp}{dt} = \frac{p_{t+1} - p_t}{dt \cdot l} $$

We obtain one velocity vector per body point:
 - `d{x,y}c_dt`: $$\frac{dx_c}{dt}, \frac{dy_c}{dt}$$
 - `d{x,y}n_dt`: $$\frac{dx_n}{dt}, \frac{dy_n}{dt}$$
 - `d{x,y}t_dt`: $$\frac{dx_t}{dt}, \frac{dy_t}{dt}$$

#### Velocity in nose direction
The main velocity feature is measured with respect to the current nose direction (center-nose vector) and perpendicular to it. The nose direction $\varphi_n$ is given by
$$ \varphi_n = \angle(\vec{cn}, \begin{bmatrix}1\\\\0\end{bmatrix}). $$

We show the computation for the center point velocity; the other points are computed analogous.

`xc_n_dt`: $$ \frac{x_{cn}}{dt} = \frac{dx_c}{dt} \cos(\varphi_n) + \frac{dy_c}{dt} \sin(\varphi_n) $$

`yc_n_dt`: $$ \frac{y_{cn}}{dt} = -\frac{dx_c}{dt} \sin(\varphi_n) + \frac{dy_c}{dt} \cos(\varphi_n) $$

#### Magnitude of velocity in nose direction
`c_n_abs_dt` $$ v_{cn} = \log\left( \left\vert \begin{bmatrix}x_{cn}/dt\\\\y_{cn}/dt\end{bmatrix} \right\vert + \nu \right) $$

Analogous for nose and tail-base.

#### Kinetic energy
The total energy is a linear combination of the parts individual energies. The unit of the energy measure is energy per unit mass: J/kg

`energy_kinetic`: $$ E = \log\left( \frac{1}{2} \vec{w}^\mathsf{T} \begin{bmatrix}v_{nn}\\\\v_{cn}\\\\v_{tn}\end{bmatrix}^2 + \nu \right) $$


## Pairwise features

Pairwise features are grouped into three main categories: distance, pose and motion features.

Note that there are symmetric measures where $f(A,B) = f(B,A)$ and asymmetric measures where $f(A,B) \neq f(B,A)$.

### Distance features

There is a large range of distances that can be computed between a pair of three points. First, we compute three low-level distances between the nose of one subject and the three body points of the other as well as the distances between their center points. Then we compute the distances between the nose and tail of one subject and the spine of the other.

Distance measures are normalized by the measured animal length (for pairwise measures $l$ is the mean length of the two animals).

#### Body point distances

`P_dn{n,c,t}`: $$d_{nn} = \log\left(\frac{\left\vert \vec{n}^A - \vec{n}^B \right\vert}{l}\right) $$

...

`P_dcc`: $$d_{nn} = \log\left(\frac{\left\vert \vec{c}^A - \vec{c}^B \right\vert}{l}\right) $$

`P_d{n,t}_spine` : $$d_{ns} = \log\left( dst( \vec{n}^A, \vec{s}^B ) \right)$$

### Pose features

We compute three pose features: a relative orientation features that describes whether an animal is directed towards to away from the other, the relative nose orientation, and the spine overlap.


### Motion features

We compute the relative velocity between the two animals with respect to the considered animal's nose direction.

First we project the other animals center-point velocity vector onto the considered animal's nose direction vector. The relative velocity is the difference between my own and the projected velocity vectors.
