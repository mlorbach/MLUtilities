# -*- coding: utf-8 -*-
"""
Created on Thu Oct 30 12:00:17 2014

@author: MAL
"""

import pandas as pd
import numpy as np
from collections import OrderedDict
import csv
import logging


def load_ethovision_csv(csvfile, header_info=None, return_frame_index=False, return_recording_time=False, keep_only_corrected=False):
    """
    Read an EthoVision export file (csv format) and perform some basic pre-processing.

    Parameters
    ----------
    csvfile : str
        Filename
    header_info : dict, optional
        If not None, store header information in this dict (uses dict.update()).
    return_frame_index : bool, optional
        Convert index to frame numbers instead of timestamps.
    return_recording_time : bool, optional
        Return relative recording time instead of absolute timestamp (timestamp of recording).
    keep_only_corrected : bool, optional
        If an appropriate column is available (such as "IsCorrected"), filter the result based on that column and return only "True" frames.

    Returns
    -------
    pd.DataFrame
        EthoVision tracking data
    """
    logger = logging.getLogger(__name__)

    if header_info is None or not isinstance(header_info, OrderedDict):
        header_info = OrderedDict()

    # read the first line of the header which should contain the number of header lines to come
    try:
        # num headers minus 3 rows that actually don't contain header info but rather csv column names etc.
        num_headers = pd.read_csv(csvfile, sep=';', nrows=1, header=None).iloc[0,1] - 3
    except ValueError as err:
        logger.error(err.message)
        if err.message == 'No columns to parse from file':
            raise ValueError('Observer exports in weird encoding, please convert the file to UTF8.')
        else:
            raise err

    # read the header of the tracking file
    header_file = pd.read_csv(csvfile, sep=';', nrows=num_headers, header=None, usecols=[0,1], names=['var', 'value'])
    header_info.update( OrderedDict(zip( header_file['var'], header_file['value'] )) )

    # check if the has_no_units field is available in the header
    # if it is not, then it has unit information and is an original EthoVision file
    if 'has no units' not in header_info or header_info['has no units'] != 'True':
        header_rows = range(num_headers+1)
        header_rows.append( num_headers+2)
    else:
        # there is no unit information and is thus our own file
        header_rows = num_headers + 2

    logger.debug('Number of header rows to skip when importing csv: {}'.format(header_rows))

    # read the tracking data discarding the header but including the column names:
    index_col = 'Trial time' if not return_recording_time else 'Recording time'
    dff = pd.read_csv(csvfile, sep=';', skiprows=header_rows, index_col=index_col, na_values='-')
    export_cols = ['X center', 'Y center', 'X nose', 'Y nose', 'X tail', 'Y tail']
    export_cols = [c for c in export_cols if c in dff.columns]

    if keep_only_corrected and 'IsCorrected' in dff.columns:
        # remove uncorrected tracking if desired
        dff = dff.loc[dff['IsCorrected'] == 1, :]

    dff = dff.loc[:, export_cols]

    if return_frame_index:
        if dff.index[0] > 0:
            est_fps = 1./np.diff(dff.index[0:10]).mean()
            first_frame = int(dff.index[0] * est_fps)
        else:
            first_frame = 0

        dff.index = np.arange(len(dff)) + first_frame
        dff.index.name = 'frame'
    else:
        dff.index.name = 'time'


    return dff


def write_ethovision_csv(dftracks, filename, header_info=None):

    logger = logging.getLogger(__name__)

    with open(filename, 'wb') as fid:


        if header_info is not None and isinstance(header_info, OrderedDict):

            header_info['has no units'] = True     # add this so we can identify the file as no original EthoVision file
            num_header_rows = len(header_info) + 3 # +3: two empty rows plus column names
            header_info['Number of header lines:'] = num_header_rows

            w = csv.writer(fid, delimiter=';')
            for (k,v) in header_info.viewitems():
                w.writerow([k,v])

            # write two extra rows to be consistent with ethovision's format (it's an empty row)
            w.writerow(['',''])
            w.writerow(['',''])

        dftracks.to_csv(fid, sep=';')

    return filename
