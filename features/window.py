"""
Created on 08-Feb-16 14:19

@author: MAL
"""

import logging
import pandas as pd
import numpy as np
from .math import diff_linreg

def get_window_parameter_dict(function, length, step=1, columns=[], align='center'):
    return dict( func=function, length=length, step=step, columns=columns, align=align )


def compute_window_features(dffeatures, window_functions):

    logger = logging.getLogger(__name__)

    # list of functions that can be dealt with quite fast (Cython implementations):
    fast_funcs = ['mean', 'sum', 'var', 'std', 'max', 'min', 'median']

    # sort feature columns by name
    dffeatures = dffeatures.sortlevel(0, axis=1).sort_index(axis=1)

    dfout = dffeatures.copy()

    for fdict in window_functions:

        name = fdict['func']

        # determine which columns to act on
        cols = fdict['columns']

        if len(cols) == 0:
            cols = slice(None)
        else:
            # skip non-existing columns
            if dffeatures.columns.nlevels > 1:
                cols = (slice(None),
                        [c for c in cols if c in dffeatures.columns.get_level_values(1).unique()])
            else:
                cols = [c for c in cols if c in dffeatures.columns.unique()]

        if name in fast_funcs:

            # define the window function
            rolling_func = getattr(pd, 'rolling_{}'.format(name))

            # suffix for new column names:
            suffix = '_{}'.format(name)

            # apply different parameters to rolling window depending on alignment of window
            if fdict['align'] == 'center':
                dffunc = rolling_func(dffeatures.loc[:, cols], fdict['length'], center=True, min_periods=1)
            elif fdict['align'] == 'right' or fdict['align'] == 'default' or fdict['align'] == 'backwards':
                dffunc = rolling_func(dffeatures.loc[:, cols], fdict['length'], center=False, min_periods=1)
                suffix = '_bw' + suffix
            elif fdict['align'] == 'left' or fdict['align'] == 'forwards':
                dffunc = rolling_func(dffeatures.loc[::-1, cols], fdict['length'], center=False, min_periods=1).sort_index(0)
                suffix = '_fw' + suffix

            # rename new columns
            if dffunc.columns.nlevels > 1:
                dffunc.columns = pd.MultiIndex.from_arrays([dffunc.columns.get_level_values(0),
                                                        [str(c)+suffix for c in dffunc.columns.get_level_values(1)]])
            else:
                dffunc.columns = [str(c)+suffix for c in dffunc.columns]

            # join to output dataframe
            dfout = dfout.join( dffunc )

        else:
            logger.warn('You want to use a very inefficient window-function {}. '
                        'See if you can use one of: {}.'.format(name, fast_funcs))

            # suffix for new column names:
            suffix = '_{}'.format(name)

            # apply different parameters to rolling window depending on alignment of window
            if fdict['align'] == 'center':
                dffunc = pd.rolling_apply(dffeatures.loc[:, cols], fdict['length'],
                                          eval(name), center=True, min_periods=1)
            elif fdict['align'] == 'right' or fdict['align'] == 'default' or fdict['align'] == 'backwards':
                dffunc = pd.rolling_apply(dffeatures.loc[:, cols], fdict['length'],
                                      eval(name), center=False, min_periods=1)
                suffix = '_bw' + suffix
            elif fdict['align'] == 'left' or fdict['align'] == 'forwards':
                dffunc = pd.rolling_apply(dffeatures.loc[::-1, cols], fdict['length'],
                                      eval(name), center=False, min_periods=1).sort_index(0)
                suffix = '_fw' + suffix

            # rename new columns
            if dffunc.columns.nlevels > 1:
                dffunc.columns = pd.MultiIndex.from_arrays([dffunc.columns.get_level_values(0),
                                                        [str(c)+suffix for c in dffunc.columns.get_level_values(1)]])
            else:
                dffunc.columns = [str(c)+suffix for c in dffunc.columns]

            # join to output dataframe
            dfout = dfout.join( dffunc )

    return dfout
