# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-26 11:45:07
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-02-12 17:59:22

import numpy as np
import pandas as pd

def first_valid_feature_vector(df, index_level=slice(None)):
    """
    Return the index of the first complete (non-NaN) row (row = feature vector).

    Returns the value of a specific index level if given.

    Parameters
    ----------
    df : pd.DataFrame or pd.Series
        Input data
    index_level : slice or int
        Level of index to return

    Returns
    -------
    dtype of index (could be a tuple if multiindex)
        Index of the first complete feature vector
    """
    idx = df.apply(pd.Series.first_valid_index, axis=0).max()
    if np.isscalar(idx):
        return idx
    else:
        return idx[index_level]

def last_valid_feature_vector(df, index_level=slice(None)):
    """
    Return the index of the last complete (non-NaN) row (row = feature vector).

    Returns the value of a specific index level if given.

    Parameters
    ----------
    df : pd.DataFrame or pd.Series
        Input data
    index_level : slice or int
        Level of index to return

    Returns
    -------
    dtype of index (could be a tuple if multiindex)
        Index of the last complete feature vector
    """
    idx = df.apply(pd.Series.last_valid_index).min()
    if np.isscalar(idx):
        return idx
    else:
        return idx[index_level]

def interpolate_gaps(df, limit, **kwargs):

    if df.index.nlevels > 1:
        raise AttributeError('Can''t handle multiindex.')

    # 1.) create output mask (where True, set back to NaN in the end)
    nanmask = pd.DataFrame(data=False, index=df.index, columns=df.columns)

    # 2.) remove invalid rows at beginning and end
    first_valid = first_valid_feature_vector(df)
    last_valid = last_valid_feature_vector(df)

    nanmask.loc[:first_valid - 1, :] = True
    nanmask.loc[last_valid + 1:, :] = True

    # 3.) find gaps longer than limit
    for c in df.columns:
        nanmask.loc[df[c].isnull().groupby(df[c].isnull().diff().fillna(False).cumsum()).transform(sum) > limit, c] = True

    # 4.) interpolate values
    df = df.interpolate(limit=limit, axis=0, **kwargs)

    # 5.) write back nans to gaps above limit
    df = df.mask(nanmask, np.nan)  # sets df to nan where nanmask == True

    return df
