# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 16:56:56 2014

@author: MAL
"""

import numpy as np
import pandas as pd

import logging




def select_by_intervals(features, intervals, interval_column_names=('window start', 'window end'), invert=False):
    '''
        Returns a the rows of features whose timestamp lies within any of the intervals.
        
        The features must be given as a pandas dataframe whose index is numerical or a timestamp.
        The intervals must be given in the same format as the features' index.
        
        Intervals borders are inclusive, i.e., the function returns $$x( t >= start & t <= end )$$
        
        Parameters:
        -----------
        
        features : pandas dataframe
            Dataframe containing the features.
            
        intervals : pandas dataframe or list of tuples
            Can be given as a dataframe in which the interval borders are given in two separate columns (start, end),
            or as a list of tuples (start, end).
            
        interval_column_names : 2-element tuple, default: ('window start', 'window end')
            Names of start and stop column (both are inclusive selections)
            
        invert : boolean, default: False
            If True, select all frames, that are NOT in the intervals.
        
    '''
    
    logger = logging.getLogger(__name__)
    
    if not isinstance(features, pd.DataFrame):
        logger.error('features must be a pandas DataFrame object.')
        raise AttributeError('features must be a pandas DataFrame object.')

        
    dflist = []
    seqids = range(len(intervals))
    start_col = interval_column_names[0]
    end_col = interval_column_names[1]

    if isinstance(intervals, pd.DataFrame):
    
        for bout in intervals.iterrows():
            dflist += [features.loc[ bout[1][  start_col  ]:bout[1][  end_col  ] ]]
            
        # use sequence ids if they are given in the dataframe
        if 'seqid' in intervals.columns:
            seqids = intervals['seqid'].tolist()
            
    elif isinstance(intervals, list):
        
        for bout in intervals:
            dflist += [features.loc[ bout[0]:bout[1] ]]
            
    else:
        logger.error('intervals must be either a pandas DataFrame object or a list of tuples.')
        raise AttributeError('intervals must be either a pandas DataFrame object or a list of tuples.')
        
        
    if not invert:
        return pd.concat(dflist, keys=seqids, names=['seqid'])
    else:
        df = pd.concat(dflist)
        return features.drop(df.index)