<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="pandoc">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <link rel="stylesheet" href="../pandoc.css">
  <script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="documentation-of-the-feature-computation-for-social-interaction-recognition-in-rodents">Documentation of the feature computation for social interaction recognition in rodents</h1>
<h2 id="individual-features">Individual features</h2>
<p>Individual features are feature derived from one specific subject and its trajectory.</p>
<p>The features can be grouped in two main categories:</p>
<ul>
<li>Pose features</li>
<li>Motion features</li>
</ul>
<h3 id="pre-processing">Pre-processing</h3>
<p>The raw body point trajectories are smoothed by triangular kernel in a sliding window of length 13 frames.</p>
<h3 id="pose-features">Pose features</h3>
<p>All features are derived from the trajectories of three tracked body points: the nose <span class="math inline">\(\vec{n}\)</span>, the center <span class="math inline">\(\vec{c}\)</span> and the tail base <span class="math inline">\(\vec{t}\)</span>. Each point is a vector <span class="math inline">\(\mathbb{R}^2\)</span> with an <span class="math inline">\(x\)</span> and an <span class="math inline">\(y\)</span> component.</p>
<p>We further define vectors between these points: from center to nose <span class="math inline">\(\vec{cn}\)</span> and from center to tail base <span class="math inline">\(\vec{ct}\)</span>. Both vectors are divided by the measured animal length <span class="math inline">\(l\)</span> (i.e., not the <code>length</code> feature but a provided measurement).</p>
<p>We compute the following pose features.</p>
<h4 id="length">Length</h4>
<p>The total body length from tail base to nose via the center point.</p>
<p><span class="math display">\[ length = |\vec{cn}| + |\vec{ct}| \]</span></p>
<h4 id="body-parts-lengths-ratio">Body parts lengths ratio</h4>
<p>The length of the center-nose vector in relation to the center-tail vector.</p>
<p><span class="math display">\[ cog_{ratio} = \frac{|\vec{cn}|}{|\vec{ct}|} \]</span></p>
<h4 id="bending">Bending</h4>
<p>The bending of the body is expressed by the angle between the center-nose and center-tail vecctors.</p>
<p><span class="math display">\[ bending = \log( 1 - \angle(\vec{cn}, \vec{ct}) / \pi ) \]</span></p>
<h3 id="motion-features">Motion features</h3>
<p>All motion features are derived from the smoothed trajectories.</p>
<p>First we compute some intermediate values of each body point's <span class="math inline">\(p\)</span> own velocity. Note that the velocities are normalized by the measure animal length <span class="math inline">\(l\)</span>.</p>
<p><span class="math display">\[ \frac{dp}{dt} = \frac{p_{t+1} - p_t}{dt \cdot l} \]</span></p>
<p>We obtain one velocity vector per body point:</p>
<ul>
<li><code>d{x,y}c_dt</code>: <span class="math display">\[\frac{dx_c}{dt}, \frac{dy_c}{dt}\]</span></li>
<li><code>d{x,y}n_dt</code>: <span class="math display">\[\frac{dx_n}{dt}, \frac{dy_n}{dt}\]</span></li>
<li><code>d{x,y}t_dt</code>: <span class="math display">\[\frac{dx_t}{dt}, \frac{dy_t}{dt}\]</span></li>
</ul>
<h4 id="velocity-in-nose-direction">Velocity in nose direction</h4>
<p>The main velocity feature is measured with respect to the current nose direction (center-nose vector) and perpendicular to it. The nose direction <span class="math inline">\(\varphi_n\)</span> is given by<br />
<span class="math display">\[ \varphi_n = \angle(\vec{cn}, \begin{bmatrix}1\\\\0\end{bmatrix}). \]</span></p>
<p>We show the computation for the center point velocity; the other points are computed analogous.</p>
<p><code>xc_n_dt</code>: <span class="math display">\[ \frac{x_{cn}}{dt} = \frac{dx_c}{dt} \cos(\varphi_n) + \frac{dy_c}{dt} \sin(\varphi_n) \]</span></p>
<p><code>yc_n_dt</code>: <span class="math display">\[ \frac{y_{cn}}{dt} = -\frac{dx_c}{dt} \sin(\varphi_n) + \frac{dy_c}{dt} \cos(\varphi_n) \]</span></p>
<h4 id="magnitude-of-velocity-in-nose-direction">Magnitude of velocity in nose direction</h4>
<p><code>c_n_abs_dt</code> <span class="math display">\[ v_{cn} = \log\left( \left\vert \begin{bmatrix}x_{cn}/dt\\\\y_{cn}/dt\end{bmatrix} \right\vert + \nu \right) \]</span></p>
<p>Analogous for nose and tail-base.</p>
<h4 id="kinetic-energy">Kinetic energy</h4>
<p>The total energy is a linear combination of the parts individual energies. The unit of the energy measure is energy per unit mass: J/kg</p>
<p><code>energy_kinetic</code>: <span class="math display">\[ E = \log\left( \frac{1}{2} \vec{w}^\mathsf{T} \begin{bmatrix}v_{nn}\\\\v_{cn}\\\\v_{tn}\end{bmatrix}^2 + \nu \right) \]</span></p>
<h2 id="pairwise-features">Pairwise features</h2>
<p>Pairwise features are grouped into three main categories: distance, pose and motion features.</p>
<p>Note that there are symmetric measures where <span class="math inline">\(f(A,B) = f(B,A)\)</span> and asymmetric measures where <span class="math inline">\(f(A,B) \neq f(B,A)\)</span>.</p>
<h3 id="distance-features">Distance features</h3>
<p>There is a large range of distances that can be computed between a pair of three points. First, we compute three low-level distances between the nose of one subject and the three body points of the other as well as the distances between their center points. Then we compute the distances between the nose and tail of one subject and the spine of the other.</p>
<p>Distance measures are normalized by the measured animal length (for pairwise measures <span class="math inline">\(l\)</span> is the mean length of the two animals).</p>
<h4 id="body-point-distances">Body point distances</h4>
<p><code>P_dn{n,c,t}</code>: <span class="math display">\[d_{nn} = \log\left(\frac{\left\vert \vec{n}^A - \vec{n}^B \right\vert}{l}\right) \]</span></p>
<p>...</p>
<p><code>P_dcc</code>: <span class="math display">\[d_{nn} = \log\left(\frac{\left\vert \vec{c}^A - \vec{c}^B \right\vert}{l}\right) \]</span></p>
<p><code>P_d{n,t}_spine</code> : <span class="math display">\[d_{ns} = \log\left( dst( \vec{n}^A, \vec{s}^B ) \right)\]</span></p>
<h3 id="pose-features-1">Pose features</h3>
<p>We compute three pose features: a relative orientation features that describes whether an animal is directed towards to away from the other, the relative nose orientation, and the spine overlap.</p>
<h3 id="motion-features-1">Motion features</h3>
<p>We compute the relative velocity between the two animals with respect to the considered animal's nose direction.</p>
<p>First we project the other animals center-point velocity vector onto the considered animal's nose direction vector. The relative velocity is the difference between my own and the projected velocity vectors.</p>
</body>
</html>
