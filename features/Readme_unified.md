Unified feature set for interaction recognition in rodents
==========================================================

## Individual features

### Pose features

 - length
 - bending

### Motion features

 - Velocity in nose direction (one vector per body point)
 - Magnitude of velocity in nose direction (one absolute velocity per body point)

## Pairwise features

### Distance features

 - Body point distances (nose to other three points, center to center, nose to spine, tail to spine)

### Pose features

 - relative orientation
 - spine overlap

### Motion features

 - relative velocity