# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-06-09 14:45:29
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-05-15 16:47:30

import pandas as pd
import numpy as np
import logging

from MLUtilities.features.math import bfun_norm, angle_vector_axis, angle_between, distance_2_pts
from MLUtilities.features.utils_metadata import get_default_scaling


def get_default_parameters():

    parameters = {}
    # default parameters
    parameters['pos_smoothing_win_length'] = 13
    parameters['weights_energy'] = np.array([0.2, 0.5, 0.3])
    parameters['bending_from'] = 'angle'  # options = ['angle', 'length']
    parameters['log_nu'] = 1e-3
    parameters['velocity_mapping'] = lambda x: np.log(x + parameters['log_nu'])
    parameters['remove_not_required_raw_features'] = True
    parameters['use_motion_for_orientation'] = True
    parameters['orientation_velocity_threshold'] = 0.02  # m/s

    return parameters


def compute_individual_features(features, dt=None, scaling_parameters=None, **kwargs):

    logger = logging.getLogger(__name__)


    if not isinstance(features, pd.DataFrame):
        logger.error('features must be a pandas DataFrame object but is %s.', type(features))
        raise AttributeError('features must be a pandas DataFrame object but is %s.', type(features))

    if features.columns.nlevels != 2:
        logger.error('features must have hierarchical column index containing all tracked subjected, but has {} column levels.'.format(features.columns.nlevels))
        raise AttributeError('features must have hierarchical column index containing all tracked subjected, but has {} column levels.'.format(features.columns.nlevels))

    ############################################

    # get default parameters for feature computation
    parameters = get_default_parameters()
    parameters.update(kwargs)

    logger.debug('Computing individual features using the following parameters: {}'.format(parameters))

    # get default scaling factors
    scaling = get_default_scaling()
    if scaling_parameters is not None:
        scaling.update(scaling_parameters)

    logger.debug('Computing individual features using the following scaling parameters: {}'.format(scaling))

    ############################################

    features = features.sortlevel(0, axis=1)

    raw_feature_names = ['X center', 'Y center', 'X nose', 'Y nose', 'X tail', 'Y tail']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    if parameters['remove_not_required_raw_features']:
        logger.debug('Removing not required raw features.')
        features = features.drop( [c for c in features.columns.get_level_values(1) if c not in raw_feature_names], axis=1, level=1 )

    if dt is None:
        dt = np.diff(features.iloc[0:2].index.get_level_values(features.index.nlevels-1).values)
        logger.info('dt automatically detected to be: %f', dt)

    ############################################

    features = _compute_pose_features(features, parameters, scaling)
    features = _compute_motion_features(features, dt, parameters, scaling)

    return features.sortlevel(0, axis=1)


def _check_required_features(features, required):
    '''
    Returns a list of required features that are not contained in features DataFrame.

    Each list element is a tuple: (subject, feature).

    If no features are missing, an empty list is returned.
    '''

    animal_names = features.columns.get_level_values(0).unique().tolist()

    missing = []

    for subj in animal_names:

        if not all([f in features[subj].columns.values for f in required]):
            # missing += [(subj, f not in features[subj].columns.values) for f in required]
            missing += [(subj, f) for f in required if f not in features[subj].columns.values]

    return missing


def _compute_pose_features(features, parameters, scaling):
    '''

    '''

    logger = logging.getLogger(__name__)

    required_feature_names = []

    missing_features = _check_required_features(features, required_feature_names)
    if len(missing_features) > 0:
        logger.error('Required features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required features {} are not in features DataFrame.'.format(missing_features))


    ############################################

    animal_names = features.columns.get_level_values(0).unique().tolist()

    # smooth the body point positions by Gaussian kernel

    for subj in animal_names:

        animal_length = scaling['mean_length'] if subj not in scaling['length'] else scaling['length'][subj]


        features.loc[:, [(subj, 'X center'), (subj, 'Y center'),
                         (subj, 'X nose'), (subj, 'Y nose'),
                         (subj, 'X tail'), (subj, 'Y tail')]] = features.loc[:, [(subj, 'X center'), (subj, 'Y center'),
                         (subj, 'X nose'), (subj, 'Y nose'),
                         (subj, 'X tail'), (subj, 'Y tail')]].rolling(parameters['pos_smoothing_win_length'], min_periods=1, center=True, win_type='triang').mean()

    features = features.rename(columns={
                               'X center': 'X center_temp',
                               'Y center': 'Y center_temp',
                               'X nose': 'X nose_temp',
                               'Y nose': 'Y nose_temp',
                               'X tail': 'X tail_temp',
                               'Y tail': 'Y tail_temp'})

    for subj in animal_names:

        animal_length = scaling['mean_length'] if subj not in scaling['length'] else scaling['length'][subj]

        ###
        # Body pose
        ###

        # cog-nose vector:
        features.loc[:, (subj,'cn_x_temp')] = features.loc[:, (subj, 'X nose_temp')].values - features.loc[:, (subj, 'X center_temp')].values
        features.loc[:, (subj, 'cn_y_temp')] = features.loc[:, (subj, 'Y nose_temp')].values - features.loc[:, (subj, 'Y center_temp')].values
        # features.loc[:, (subj, 'cn_length')] = bfun_norm( features[subj].loc[:, ['cn_x_temp', 'cn_y_temp'] ].values, axis=1 )/animal_length


        # cog-tail vector:
        features.loc[:, (subj, 'ct_x_temp')] = features.loc[:, (subj, 'X tail_temp')].values - features.loc[:, (subj, 'X center_temp')].values
        features.loc[:, (subj, 'ct_y_temp')] = features.loc[:, (subj, 'Y tail_temp')].values - features.loc[:, (subj, 'Y center_temp')].values
        # features.loc[:, (subj, 'ct_length')] = bfun_norm(features[subj].loc[:, ['ct_x_temp', 'ct_y_temp'] ].values, axis=1)/animal_length

        # angle between x-axis and cog-nose vector
        features.loc[:, (subj, 'phi_n_temp')] = angle_vector_axis( features[subj].loc[:, ['cn_x_temp', 'cn_y_temp'] ].values, axis=1 )

        # # total body length = |center-tail vector| + |center-nose vector|
        # features.loc[:, (subj, 'length')] = features.loc[:, (subj, 'cn_length')].values + features.loc[:, (subj, 'ct_length')].values

        #  # body part lengths ratio
        # features.loc[:, (subj, 'cog_ratio')] = features.loc[:, (subj, 'cn_length')].values/features.loc[:, (subj, 'ct_length')].values
        # features.loc[ np.isinf(features.loc[:, (subj, 'cog_ratio')]), (subj, 'cog_ratio')] = np.nan
        # features.loc[:, (subj, 'cog_ratio')] = np.log(features.loc[:, (subj, 'cog_ratio')].interpolate()) # in case ct_length is zero somewhere


        ###
        # Bending
        ###

        # if parameters['bending_from'] == 'angle':

        #     # bending in terms of the angle between the center-tail and center-nose vectors
        #     #  bending = 1 - angle(cn, ct)/pi
        #     # ranges from (0 .. 1], where bending is 0 if the rat is completely stretched, i.e., no bending
        #     # direction in mathematical positive way (left = postive, right = negative)

        #     features.loc[:, (subj, 'bending')] = np.log( 1. - angle_between( features[subj].loc[:, ['cn_x_temp', 'cn_y_temp'] ].values, features[subj].loc[:, ['ct_x_temp', 'ct_y_temp']].values, axis=1) / np.pi )

        # else:

        #     # bending in terms of the body part lengths: the ratio of the distance between tail and nose w.r.t. the body length
        #     #  bending = 1 - |nose-tail| / (|cn| + |ct|)
        #     # ranges from [0 .. 1], where bending is 0 if the rat is completely stretched, i.e., no bending
        #     # there is no direction information in this type of bending measure
        #     nt_length = distance_2_pts( features[subj].loc[:, ['cn_x_temp', 'cn_y_temp']].values, features[subj].loc[:, ['ct_x_temp', 'ct_y_temp']].values, axis=1 )
        #     #nt_length = features[subj].loc[:, ['cn_x_temp', 'cn_y_temp', 'ct_x_temp', 'ct_y_temp'] ].apply(lambda c: norm_2d(np.array([c[0]-c[2], c[1]-c[3]])), axis=1, raw=True)
        #     features.loc[:, (subj, 'bending')] = np.log( 1. - nt_length/features.loc[:, (subj, 'length')].values )


    features = features.sortlevel(0, axis=1)

    return features


def _compute_motion_features(features, dt, parameters, scaling):
    '''
    '''

    logger = logging.getLogger(__name__)

    required_feature_names = ['X center_temp', 'Y center_temp', 'X nose_temp', 'Y nose_temp', 'X tail_temp', 'Y tail_temp', 'phi_n_temp']

    missing_features = _check_required_features(features, required_feature_names)
    if len(missing_features) > 0:
        logger.error('Required features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    animal_names = features.columns.get_level_values(0).unique().tolist()

    for subj in animal_names:

        animal_length = scaling['mean_length'] if subj not in scaling['length'] else scaling['length'][subj]

        ###
        # Intermediate: Body point VELOCITIES
        ###

        # compute position derivates, positions have been smoothed by gaussian kernel
        xydiff_gauss = features[subj].loc[:, ['X center_temp', 'Y center_temp', 'X nose_temp', 'Y nose_temp', 'X tail_temp', 'Y tail_temp']].diff(periods=1).fillna(0.)/(dt*animal_length)


        # COG
        features.loc[:, (subj, 'dxc_dt_temp')] = xydiff_gauss['X center_temp']
        features.loc[:, (subj, 'dyc_dt_temp')] = xydiff_gauss['Y center_temp']

        # Nose
        features.loc[:, (subj, 'dxn_dt_temp')] = xydiff_gauss['X nose_temp']
        features.loc[:, (subj, 'dyn_dt_temp')] = xydiff_gauss['Y nose_temp']
        # Tail
        features.loc[:, (subj, 'dxt_dt_temp')] = xydiff_gauss['X tail_temp']
        features.loc[:, (subj, 'dyt_dt_temp')] = xydiff_gauss['Y tail_temp']


        # take orientation based on motion instead of cog-nose vector, which may be swapped during tracking:
        if parameters['use_motion_for_orientation']:
            # DIRECTON of motion
            features.loc[:, (subj, 'phi_motion_temp')] = angle_vector_axis(features.loc[:, [(subj, 'dxc_dt_temp'), (subj, 'dyc_dt_temp')]].values, axis=1)

            # do not use computed orientation value if velocity is too small (unreliable orientation)
            cog_velocity = bfun_norm(features[subj].loc[:, ['dxc_dt_temp', 'dyc_dt_temp']].values, axis=1)
            features.loc[cog_velocity < parameters['orientation_velocity_threshold'], (subj, 'phi_motion_temp')] = np.nan

            # interpolate during non-movement:
            features.loc[:, (subj, 'phi_motion_temp')] = features.loc[:, (subj, 'phi_motion_temp')].interpolate(method='spline', order=3)

            # OR
            # keep current orientation value
            # features.loc[:, (subj, 'phi_motion_temp')] = features.loc[:, (subj, 'phi_motion_temp')].fillna(method='ffill')
            #features.loc[:, (subj, 'phi_motion_temp')] = pd.rolling_median( features.loc[:, (subj, 'phi_motion_temp')], 7, min_periods=1, center=True)

            features.loc[:, (subj, 'phi_n_temp')] = features.loc[:, (subj, 'phi_motion_temp')]


        ###
        # VELOCITY in nose direction
        ###

        # change of position (velocity) in direction of cog-nose vector (x?_n_dt) and perpendicular (y?_n_dt)
        # Note that we use the smoothed position derivatives.
        # COG
        features.loc[:, (subj, 'xc_n_dt_temp')] = features.loc[:, (subj, 'dxc_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (subj, 'dyc_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
        features.loc[:, (subj, 'yc_n_dt_temp')] = -features.loc[:, (subj, 'dxc_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (subj, 'dyc_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)
        # Nose
        features.loc[:, (subj, 'xn_n_dt_temp')] = features.loc[:, (subj, 'dxn_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (subj, 'dyn_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
        features.loc[:, (subj, 'yn_n_dt_temp')] = -features.loc[:, (subj, 'dxn_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (subj, 'dyn_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)
        # # Tail
        # features.loc[:, (subj, 'xt_n_dt')] = features.loc[:, (subj, 'dxt_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (subj, 'dyt_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
        # features.loc[:, (subj, 'yt_n_dt')] = -features.loc[:, (subj, 'dxt_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (subj, 'dyt_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)


        # magnitude of cog velocity w.r.t. nose direction
        features.loc[:, (subj, 'c_n_abs_dt')] = parameters['velocity_mapping']( bfun_norm(features[subj].loc[:, ['xc_n_dt_temp', 'yc_n_dt_temp']].values, axis=1))
        features.loc[:, (subj, 'n_n_abs_dt')] = parameters['velocity_mapping']( bfun_norm(features[subj].loc[:, ['xn_n_dt_temp', 'yn_n_dt_temp']].values, axis=1))
        # features.loc[:, (subj, 't_n_abs_dt')] = np.log( bfun_norm(features[subj].loc[:, ['xt_n_dt', 'yt_n_dt']].values, axis=1) + parameters['log_nu'] )


        ###
        # ACCELERATION in nose direction
        ###

        # d(dx)/(dt^2)
        # compute velocity derivate (velocity is already smoothed by gaussian)
        # dd_dt2 = xydiff_gauss.diff(periods=1).fillna(0.) / dt
        # Transform derivatives into cog-nose vector based coordinate frame
        # COG
        # features.loc[:, (subj, 'xc_n_dt2_temp')] = dd_dt2['X center_temp'].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + dd_dt2['Y center_temp'].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
        # features.loc[:, (subj, 'yc_n_dt2_temp')] = -dd_dt2['X center_temp'].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + dd_dt2['Y center_temp'].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)
        # # Nose
        # features.loc[:, (subj, 'xn_n_dt2')] = dd_dt2['X nose_temp'].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + dd_dt2['Y nose_temp'].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
        # features.loc[:, (subj, 'yn_n_dt2')] = -dd_dt2['X nose_temp'].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + dd_dt2['Y nose_temp'].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)
        # # Tail
        # features.loc[:, (subj, 'xt_n_dt2')] = dd_dt2['X tail_temp'].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + dd_dt2['Y tail_temp'].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
        # features.loc[:, (subj, 'yt_n_dt2')] = -dd_dt2['X tail_temp'].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + dd_dt2['Y tail_temp'].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)


        # magnitude of cog acceleration w.r.t. nose direction
        # features.loc[:, (subj, 'c_n_abs_dt2')] = np.log( bfun_norm(features[subj].loc[:, ['xc_n_dt2_temp', 'yc_n_dt2_temp']].values, axis=1) + parameters['log_nu'] )
        # features.loc[:, (subj, 'n_n_abs_dt2')] = np.log( bfun_norm(features[subj].loc[:, ['xn_n_dt2', 'yn_n_dt2']].values, axis=1) + parameters['log_nu'] )
        # features.loc[:, (subj, 't_n_abs_dt2')] = np.log( bfun_norm(features[subj].loc[:, ['xt_n_dt2', 'yt_n_dt2']].values, axis=1) + parameters['log_nu'] )


        # ###
        # # Kinetic ENERGY
        # ###

        # # We will compute an estimation of the rat's kinetic energy based on the velocity of the three body points.
        # # The total energy is a linear combination of the parts individual energies. The weights of the parts can
        # # be specified by a parameter and are scaled to sum to one. That is, the resulting energy measure will be
        # # energy per unit mass: J/kg

        # features.loc[:, (subj, 'energy_kinetic')] = np.log( np.dot((features[subj].loc[:, ['n_n_abs_dt', 'c_n_abs_dt', 't_n_abs_dt']].values**2)/2., parameters['weights_energy']) + parameters['log_nu'])

    return features
