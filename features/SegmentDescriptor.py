# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 13:25:07 2015

@author: MAL
"""

import numpy as np

from sklearn.base import BaseEstimator, TransformerMixin


class SegmentDescriptor(BaseEstimator, TransformerMixin):

    def __init__(self, min_val=-7, max_val=7, n_bins=10, alpha=0.01):
        self.min_val_ = min_val
        self.max_val_ = max_val
        self.n_bins_ = n_bins
        self.bins_ = np.linspace(self.min_val_, self.max_val_, num=self.n_bins_)
        self.alpha_ = alpha
        
        
    def fit(self, X, y=None):
        return self
    
    
    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)
        
        
    def transform(self, X, y=None):
        
        X = np.asarray(X)
        
        n_features = X.shape[1]
        
        h = np.zeros((n_features, self.n_bins_ - 1))
        alpha_vector = self.alpha_*np.ones((h.shape[1]))
        
        for j in xrange(n_features):
            h[j, :] = np.histogram( X[:, j], bins=self.bins_, density=True)[0] + alpha_vector
            
        return h