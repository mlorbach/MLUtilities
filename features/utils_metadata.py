"""
Created on 30-Jul-15 16:02

@author: MAL
"""

import numpy as np
from collections import OrderedDict


def get_default_scaling(dims=2):

    return dict(
        default_length=1.,
        mean_length=1.,
        length={},
        cage=np.ones((int(dims),), dtype=np.float),
        image_size=np.ones((int(dims),), dtype=np.float)
    )


def get_scaling_from_metadata(meta):

    scaling = dict(default_length=1., mean_length=1., length={})

    keys = meta.keys()

    for k in [key for key in keys if key.startswith('subject_')]:
        if isinstance(meta[k], (int, float)):
            scaling['length'][k] = meta[k]
            continue
        elif not isinstance(meta[k], (dict, OrderedDict)):
            continue

        if 'animal_length' in meta[k]:
            # new version, field name = animal_length
            scaling['length'][k] = meta[k]['animal_length']
        elif 'length' in meta[k]:
            # old version, field name = length
            scaling['length'][k] = meta[k]['length']

    if len(scaling['length']) > 0:
        scaling['mean_length'] = np.mean(scaling['length'].values())

    if 'cage' in meta:
        scaling['cage'] = meta['cage']

    if 'image_size' in meta:
        scaling['image_size'] = meta['image_size']

    return scaling
