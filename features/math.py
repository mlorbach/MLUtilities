# -*- coding: utf-8 -*-
"""
Created on Thu Oct 30 14:36:42 2014

@author: MAL

Library of useful math functions.
Some are just here to speed up operations across a specific matrix dimension (bfun_*).

"""

import numpy as np
from scipy.stats import linregress


def absdiff(x, **kwargs):
    return np.abs(np.diff(x, **kwargs)).ravel()

def nan_max_min_range(x, **kwargs):
    return np.nanmax(x, **kwargs) - np.nanmin(x, **kwargs)

def normed(v, ord=None):
    ''' Returns the unit vector of the vector.  '''
    v = np.asarray(v)
    return v / np.linalg.norm(v, ord=ord)

def bfun_normed(v, ord=None, axis=0):
    ''' Returns the unit vectors of the vector along axis 'axis'. '''
    v = np.asarray(v)
    return v / np.linalg.norm(v, ord=ord, axis=axis)[:, np.newaxis]

def bfun_dot(v1,v2, axis=0):
    ''' 1D-dot product of vectors v1 and v2 with broadcasting along axis. '''
    v1 = np.asarray(v1)
    v2 = np.asarray(v2)

    return np.einsum(v1, range(v1.ndim), v2, range(v2.ndim), [(v1.ndim-axis-1)] if v1.ndim > 1 else [])

def angle_between(v1, v2, axis=0):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1 = np.asarray(v1)
    v2 = np.asarray(v2)

    v1_u = bfun_normed(v1, axis=axis)
    v2_u = bfun_normed(v2, axis=axis)
    angle = np.arccos(bfun_dot(v1_u, v2_u, axis=axis))

    angle[ np.isnan(angle) & (v1_u[np.isnan(angle)] == v2_u[np.isnan(angle)]).all()  ] = 0.0
    angle[ np.isnan(angle) & (v1_u[np.isnan(angle)] != v2_u[np.isnan(angle)]).all()  ] = np.pi

    return angle


def angle_vector_axis(v, axis=0):
    '''
    Computes the angle of vector 'v' with the first axis (x-axis).

    Returns
    -------
    phi : angle in radians [-pi, pi]
    '''

    return np.arctan2(v.take(1, axis=axis), v.take(0, axis=axis))


def angle_between_signed(v1, v2, axis=0):
    '''
    Returns the angle in radians between vectors 'v1' and 'v2' with respect to v1 executed along axis 'axis'.
    '''
    v1 = np.asarray(v1)
    v2 = np.asarray(v2)
    return np.arctan2( np.cross(v1, v2, axis=axis), bfun_dot(v1, v2, axis=axis) )


def distance_2_pts(p1, p2, axis=0):
    p1 = np.asarray(p1)
    p2 = np.asarray(p2)
    return np.sqrt( np.sum((p2-p1)**2, axis=axis ) )

def bfun_norm(v, axis=0):
    return np.sqrt( np.sum(np.asarray(v)**2, axis=axis) )

def norm_2d(v, axis=0):
    return np.sqrt( np.sum(np.asarray(v)**2, axis=axis) )

def norm_squared(v, axis=0):
    return np.sum(np.asarray(v)**2, axis=axis)

def transform_2d(phi, t, p):

    if len(p) != 2:
        raise AttributeError('p must be two-dimensional')

    if len(t) != 2:
        raise AttributeError('t must be two-dimensional')

    pd = np.empty_like(p)

    pd[0] = p[0] * np.cos(phi) - p[1] * np.sin(phi)
    pd[1] = p[0] * np.sin(phi) + p[1] * np.cos(phi)

    return pd


def distance_line_segment_point(p0, p1, s, axis=0):
    '''
    The distance of a point 's' to a line segment (p0->p1) with broadcasting functionality.

    Parameters
    ----------

    p0 : array-like
        first end point of line segment

    p1 : array-like
        second end point of line segment

    s : array-like
        point to compute distance to

    Returns
    -------
    d : float or array-like
        distance between s and line segment given by p0 and p1

    '''

    p0 = np.asarray(p0, np.float)
    p1 = np.asarray(p1, np.float)
    s = np.asarray(s, np.float)

    if any([v.ndim > 2 for v in [p0, p1, s] ]):
        raise AttributeError('Input arrays with more than 2 dimensions are not supported.')

    v = p1 - p0
    w = s - p0

    res = np.zeros( (s.shape[ 1-axis ]), dtype=np.float )

    c1 = bfun_dot(w, v, axis=axis)
    c2 = bfun_dot(v, v, axis=axis)

    c1mask = c1 <= 0
    c2mask = c2 <= c1

    if axis == 0:
        c1mask2d = np.broadcast_arrays(v, c1mask)[1]
        c2mask2d = np.broadcast_arrays(v, c2mask)[1]
    else:
        c1mask2d = c1mask
        c2mask2d = c2mask

    res[ c1mask ] = bfun_norm(w[ c1mask2d ], axis=axis)
    res[ c2mask ] = bfun_norm(  p1[ c2mask2d ] - s[ c2mask2d ], axis=axis)

    t = np.expand_dims(c1/c2, axis=axis)
    Pt = p0 + t*v

    res[ -(c1mask | c2mask) ] = bfun_norm( Pt[-(c1mask2d | c2mask2d)] - s[-(c1mask2d | c2mask2d)], axis=axis )

    return res


def two_point_segment_intersection( p, q ):
    '''
    Computes the intersection of two line segments 'p' and 'q' specified by two points each.

    Parameters
    ----------

    p : array-like
        n x 4 array with [[p0_x, p0_y, p1_x, p1_y], [...]]

    q : array-like
        n x 4 array with [[q0_x, q0_y, q1_x, q1_y], [...]]

    Returns
    -------
    (t,u) : float tuple
        intersection coordinates:
        .. math:: p0 + t(p1-p0) = q0 + u(q1-q0)

        or NaN if the segments do not intersect
    '''


    p = np.asarray(p, np.float)
    q = np.asarray(q, np.float)

    if p.shape != q.shape:
        raise AttributeError('p and q must have the same shape but are p: {} and q: {}.'.format(p.shape, q.shape))

    # p0 + t(p1-p0) = q0 + u(q1-q0)
    #
    #   t = cross((q0-p0), s) / cross(r, s)
    #   u = cross((q0-p0), r) / cross(r, s)

    p0 = p[:, 0:2]
    r = p[:, 2:4] - p0
    q0 = q[:, 0:2]
    s = q[:, 2:4] - q0

    cross_rs = np.cross(r,s, axis=1)
    cross_qp_s = np.cross(q0-p0, s, axis=1)
    cross_qp_r = np.cross(q0-p0, r, axis=1)

    tu = np.empty((p.shape[0], 2))
    tu.fill(np.nan)

    cross_rs_mask = (cross_rs != 0.) & ~np.isnan(cross_rs)
    tu[cross_rs_mask , :] = np.hstack([cross_qp_s[cross_rs_mask, np.newaxis], cross_qp_r[cross_rs_mask, np.newaxis]]) / cross_rs[cross_rs_mask, np.newaxis]
    tu[np.any((tu < 0.) | (tu > 1.), axis=1), :] = [np.nan, np.nan]

#    if cross_rs != 0.:
#        t = cross_qp_s / cross_rs
#        u = cross_qp_r / cross_rs
#
#        if (t < 0.) | (t > 1.) | (u < 0.) | (u > 1.):
#            t = np.nan
#            u = np.nan

    return tu


def sigmoid(x, x0=0., k=1):
    '''
    Logistic sigmoid function with parameters:

    .. math::
        y = \\frac{1}{1 + e^{-k (x-x_0)}}
    '''
    return 1./(1.+np.exp(-k*(x-x0)))



def distance_on_sphere(lat, lon=None, sphere_radius=6371000.):
    '''
    Computes the length of the path between two points on a sphere (spherical distance).

    Points are given as latitude, longitude pairs.

    If only `lat` is given, it is assumed to be a 2d array with first column being latitude and second column longitude.

    Returns
    -------

    dist : array-like, same length as lat.
        Spherical distance in meters (or whatever unit is sphere_radius, default is meters).
    '''

    if lon is None:
        lon = lat[:, 1]
        lat = lat[:, 0]

    # Convert latitude and longitude to
    # spherical coordinates in radians.

    # phi = 90 - latitude
    phi = np.deg2rad(90.0 - lat)

    # theta = longitude
    theta = np.deg2rad(lon)

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    sin_phi = np.sin(phi)
    cos_phi = np.cos(phi)

    d = np.zeros_like(lat)

    d[1:] = np.arccos( sin_phi[:-1] * sin_phi[1:] * np.cos( np.diff(theta) ) + cos_phi[:-1] * cos_phi[1:] )

    #cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + math.cos(phi1)*math.cos(phi2))
    #arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return d*sphere_radius


def diff_linreg(X, axis=0):
    s, _, _, _, _ = linregress(np.arange(X.shape[axis]), X)
    return s
