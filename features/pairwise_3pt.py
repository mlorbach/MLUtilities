# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-06-09 14:45:29
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-06-02 11:56:43


import pandas as pd
import numpy as np
from numpy import ma
import logging
from itertools import combinations, permutations

from MLUtilities.features.math import (distance_2_pts,
                                       distance_line_segment_point,
                                       angle_between,
                                       two_point_segment_intersection,
                                       bfun_norm)
from MLUtilities.features.utils_metadata import get_default_scaling


def get_default_parameters():

    parameters = {}

    # set default parameters here:
    parameters['log_nu'] = 1e-3
    parameters['distance_mapping'] = lambda x: np.log(x + parameters['log_nu'])

    return parameters


def compute_pairwise_features(features, dt, scaling_parameters=None, **kwargs):

    logger = logging.getLogger(__name__)


    if not isinstance(features, pd.DataFrame):
        logger.error('features must be a pandas DataFrame object but is %s.', type(features))
        raise AttributeError('features must be a pandas DataFrame object but is %s.', type(features))

    if features.columns.nlevels != 2:
        logger.error('features must have hierarchical column index containing all tracked subjected, but has {} column levels.'.format(features.columns.nlevels))
        raise AttributeError('features must have hierarchical column index containing all tracked subjected, but has {} column levels.'.format(features.columns.nlevels))

    ############################################

    # get default parameters for feature computation
    parameters = get_default_parameters()
    parameters.update(kwargs)
    logger.debug('Computing pairwise features using the following parameters: {}'.format(parameters))

    # get default scaling factors
    scaling = get_default_scaling()
    if scaling_parameters is not None:
        scaling.update(scaling_parameters)

    logger.debug('Computing pairwise features using the following scaling parameters: {}'.format(scaling))

    ############################################

    raw_feature_names = ['X center_temp', 'Y center_temp', 'X nose_temp', 'Y nose_temp', 'X tail_temp', 'Y tail_temp']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    features = _compute_distance_features(features, parameters, scaling)
    features = _compute_pose_features(features, parameters, scaling)
    features = _compute_motion_features(features, dt, parameters, scaling)

    return features.sortlevel(0, axis=1)



def _check_required_features(features, required):
    '''
    Returns a list of required features that are not contained in features DataFrame.

    Each list element is a tuple: (subject, feature).

    If no features are missing, an empty list is returned.
    '''

    animal_names = features.columns.get_level_values(0).unique().tolist()

    missing = []

    for subj in animal_names:

        if not all([f in features[subj].columns.values for f in required]):
            missing += [(subj, f) for f in required if f not in features[subj].columns.values]

    return missing


def _compute_distance_features(features, parameters, scaling):
    '''
    Compute all features that are related to the distance between subjects and store them in the 'features' DataFrame.
    :type features: pd.DataFrame
    '''

    logger = logging.getLogger(__name__)

    raw_feature_names = ['X center_temp', 'Y center_temp', 'X nose_temp', 'Y nose_temp', 'X tail_temp', 'Y tail_temp']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    animal_names = features.columns.get_level_values(0).unique().tolist()
    animal_length = scaling['mean_length']


    # ASYMMETRIC MEASURES
    # all permutations incl. inverse (a->b, b->a)
    for subj, other in permutations(animal_names, r=2):

        # # distances between nose and body parts of other (nose, cog, tail)
        features.loc[:, (subj, 'P_dnn')] = distance_2_pts( features[subj].loc[:,['X nose_temp', 'Y nose_temp']].values, features[other].loc[:,['X nose_temp', 'Y nose_temp']].values, axis=1 )/animal_length
        # features.loc[:, (subj, 'P_dnc')] = distance_2_pts( features[subj].loc[:,['X nose_temp', 'Y nose_temp']].values, features[other].loc[:,['X center_temp', 'Y center_temp']].values, axis=1 )/animal_length
        features.loc[:, (subj, 'P_dnt')] = distance_2_pts( features[subj].loc[:,['X nose_temp', 'Y nose_temp']].values, features[other].loc[:,['X tail_temp', 'Y tail_temp']].values, axis=1 )/animal_length

        # distance between nose and spine of other animal
        dist_n_front = distance_line_segment_point( features[other].loc[:, ['X center_temp', 'Y center_temp']].values, features[other].loc[:, ['X nose_temp', 'Y nose_temp']].values, features[subj].loc[:,['X nose_temp', 'Y nose_temp']].values, axis=1)
        dist_n_back = distance_line_segment_point( features[other].loc[:, ['X center_temp', 'Y center_temp']].values, features[other].loc[:, ['X tail_temp', 'Y tail_temp']].values, features[subj].loc[:,['X nose_temp', 'Y nose_temp']].values, axis=1)
        features.loc[:, (subj, 'P_dns')] = np.fmin(dist_n_front, dist_n_back)/animal_length

        # distance between tail and spine of other animal
        # dist_t_front = distance_line_segment_point( features[other].loc[:, ['X center_temp', 'Y center_temp']].values, features[other].loc[:, ['X nose_temp', 'Y nose_temp']].values, features[subj].loc[:,['X tail_temp', 'Y tail_temp']].values, axis=1)
        # dist_t_back = distance_line_segment_point( features[other].loc[:, ['X center_temp', 'Y center_temp']].values, features[other].loc[:, ['X tail_temp', 'Y tail_temp']].values, features[subj].loc[:,['X tail_temp', 'Y tail_temp']].values, axis=1)
        # features.loc[:, (subj, 'P_dts')] = np.fmin(dist_t_front, dist_t_back)/animal_length

        # relation between nose-spine and tail-spine distance:
        # features.loc[:, (subj, 'P_dns_dts_rel')] = (features.loc[:, (subj, 'P_dns')].values - features.loc[:, (subj, 'P_dts')].values) / (features.loc[:, (subj, 'P_dns')].values + features.loc[:, (subj, 'P_dts')].values)

        # converting the distances to log-scale makes them better normal distributed
        # (do not convert the dn vs dt ratio)
        # TODO: catch and interpolate zero-distance values before taking the log:
        features.loc[:, (subj, 'P_dnn')] = parameters['distance_mapping'](features.loc[:, (subj, 'P_dnn')].values)
        # features.loc[:, (subj, 'P_dnc')] = np.log(features.loc[:, (subj, 'P_dnc')].values)
        features.loc[:, (subj, 'P_dnt')] = parameters['distance_mapping'](features.loc[:, (subj, 'P_dnt')].values)
        # features.loc[:, (subj, 'P_dns')] = np.log(features.loc[:, (subj, 'P_dns')].values)
        # features.loc[:, (subj, 'P_dts')] = np.log(features.loc[:, (subj, 'P_dts')].values)

    # Distance between center points (symmetric measure)
    for subj, other in combinations(animal_names, r=2):
        features.loc[:, (subj, 'P_dcc')] = distance_2_pts(features[subj].loc[:,['X center_temp', 'Y center_temp']].values,
                                                   features[other].loc[:,['X center_temp', 'Y center_temp']].values, axis=1)/animal_length
        features.loc[:, (subj, 'P_dcc')] = parameters['distance_mapping'](features.loc[:, (subj, 'P_dcc')])

        features.loc[:, (other, 'P_dcc')] = features.loc[:, (subj, 'P_dcc')].values

    return features


def _compute_pose_features(features, parameters, scaling):
    '''
    Compute all features that are related to the pose of the subjects and store them in the 'features' DataFrame.
    '''

    logger = logging.getLogger(__name__)

    raw_feature_names = ['X center_temp', 'Y center_temp', 'X nose_temp', 'Y nose_temp', 'X tail_temp', 'Y tail_temp', 'phi_n_temp', 'cn_x_temp', 'cn_y_temp']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    animal_names = features.columns.get_level_values(0).unique().tolist()

    # ASYMMETRIC MEASURES
    # all permutations incl. inverse (a->b, b->a)
    for subj, other in permutations(animal_names, r=2):

        # position of other animal relative to my nose-direction (x and y)
        pos_rel_n_inv_x = (features[(other, 'X center_temp')].values - features[(subj, 'X center_temp')].values) \
                           * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) \
                           + (features[(other, 'Y center_temp')].values - features[(subj, 'Y center_temp')].values) \
                             * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)

        pos_rel_n_inv_y = -(features[(other, 'X center_temp')].values - features[(subj, 'X center_temp')].values) \
                          * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) \
                          + (features[(other, 'Y center_temp')].values - features[(subj, 'Y center_temp')].values) \
                            * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)

        features.loc[:, (subj, 'P_gamma_temp')] = np.arctan2(pos_rel_n_inv_y,
                                                             pos_rel_n_inv_x)

        # towards/away from measure as the cosine of the relative position w.r.t. nose-direction
        #  val = x / sqrt( x^2 + y^2 ) = cos(arctan(y/x))
        features.loc[:, (subj, 'P_n_towards')] = pos_rel_n_inv_x / (
                        np.sqrt(pos_rel_n_inv_x**2 + pos_rel_n_inv_y**2)
                    )

        # relative nose direction
        features[(subj, 'P_phi_n_rel')] = angle_between( features[subj].loc[:, ['cn_x_temp', 'cn_y_temp']].values, features[other].loc[:, ['cn_x_temp', 'cn_y_temp']].values, axis=1)


    # (A)SYMMETRIC MEASURES
    #
    #  Spine Overlap -> [0, 1]
    #   spine overlap is not a symmetric measure, but we better compute both values at the same time
    #
    # non-repeated pair (r=2) combinations  (a->b, but not b->a)
    for subj, other in combinations(animal_names, r=2):

        pnose = features[subj].loc[:, [u'X nose_temp', u'Y nose_temp', u'X center_temp', u'Y center_temp']].values
        ptail = features[subj].loc[:, [u'X tail_temp', u'Y tail_temp', u'X center_temp', u'Y center_temp']].values
        qnose = features[other].loc[:, [u'X nose_temp', u'Y nose_temp', u'X center_temp', u'Y center_temp']].values
        qtail = features[other].loc[:, [u'X tail_temp', u'Y tail_temp', u'X center_temp', u'Y center_temp']].values

        # return N-by-2 matrix, where the columns represent the intersection coordinates ([0..1]) for each animal
        #  first column is the point of intersection along first segment, second column along second segment
        tunn = two_point_segment_intersection(pnose, qnose)
        tunt = two_point_segment_intersection(pnose, qtail)
        tutn = two_point_segment_intersection(ptail, qnose)
        tutt = two_point_segment_intersection(ptail, qtail)

        # take the highest value of any of the four intersection possibilities
        # reduce applies fmax to the (first) dimension which in this case is the stack/list.
        # tu is thus a 2d array, with n rows (frames) and 2 columns (p, q).
        # tu = np.fmax.reduce(np.stack([tunn, tunt, tutn, tutt]))

        # stack into (4, N, 2) array
        intersec = np.stack([tunn, tunt, tutn, tutt])

        # mask all rows without intersection (all nan rows)
        # masking is needed for np.nanargmax which raises on all-nan rows
        ma_intersec = ma.array(intersec,
                               mask=np.isnan(intersec).all(
                                axis=2, keepdims=True).repeat(2, axis=2))

        # find for each n in N, the intersection point with the highest mean, and then return that point into tu
        tu = intersec[np.nanargmax(ma_intersec.mean(axis=2), axis=0),
                      xrange(intersec.shape[1]), :]

        # set all remaining NaNs (no intersection) to zero
        tu = np.nan_to_num(tu)

        # assign the intersection coordinates to the according subjects
        features.loc[:, (subj, 'P_overlap_spine_0')] = tu[:, 0]
        features.loc[:, (subj, 'P_overlap_spine_1')] = tu[:, 1]

        features.loc[:, (other, 'P_overlap_spine_0')] = tu[:, 1]
        features.loc[:, (other, 'P_overlap_spine_1')] = tu[:, 0]


    return features



def _compute_motion_features(features, dt, parameters, scaling):
    '''
    Compute all features that are related to the motion of the subjects and store them in the 'features' DataFrame.
    '''

    logger = logging.getLogger(__name__)

    raw_feature_names = ['X center_temp', 'Y center_temp', 'c_n_abs_dt']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    animal_names = features.columns.get_level_values(0).unique().tolist()


    # # ASYMMETRIC MEASURES
    # # all permutations incl. inverse (a->b, b->a)
    # for subj, other in permutations(animal_names, r=2):

    #     # Relative center-point velocity w.r.t. to my own nose direction
    #     other_xc_my_n_dt = features.loc[:, (other, 'dxc_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (other, 'dyc_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
    #     other_yc_my_n_dt = -features.loc[:, (other, 'dxc_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (other, 'dyc_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)

    #     # Relative velocities use the already scaled animal velocities. Each velocity is scaled by the corresponding animal's length.
    #     #  As long as the animals are similar in size, I don't see a problem with this. If we ever compare elephants and mice, we might
    #     #  want to reconsider if we want to scale by a common length value.
    #     features.loc[:, (subj, 'P_xc_n_rel_dt_temp')] = other_xc_my_n_dt - features.loc[:, (subj, 'xc_n_dt')].values
    #     features.loc[:, (subj, 'P_yc_n_rel_dt_temp')] = other_yc_my_n_dt - features.loc[:, (subj, 'yc_n_dt')].values

    # SYMMETRIC MEASURES
    # all combinations (a->b == b->a)
    # Relative absolute velocities
    # for subj, other in combinations(animal_names, r=2):

    #     # magnitude of relative velocity
    #     features.loc[:, (subj, 'P_rel_vel_abs_dt')] = np.abs(features.loc[:, (subj, 'c_n_abs_dt')] - features.loc[:, (other, 'c_n_abs_dt')])

    #     features.loc[:, (other, 'P_rel_vel_abs_dt')] = features.loc[:, (subj, 'P_rel_vel_abs_dt')]

    return features
