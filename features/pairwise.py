# -*- coding: utf-8 -*-
"""
Created on Thu Nov 20 15:41:55 2014

@author: MAL
"""

import pandas as pd
import numpy as np
import logging
from itertools import combinations, permutations

from MLUtilities.features.math import (distance_2_pts,
                                       distance_line_segment_point,
                                       angle_between,
                                       two_point_segment_intersection,
                                       bfun_norm)
from MLUtilities.features.utils_metadata import get_default_scaling


def get_default_parameters():

    parameters = {}

    # set default parameters here:
    parameters['log_nu'] = 1e-3
    parameters['distance_mapping'] = lambda x: np.log(x + parameters['log_nu'])

    return parameters


def compute_pairwise_features(features, dt, scaling_parameters=None, **kwargs):

    logger = logging.getLogger(__name__)


    if not isinstance(features, pd.DataFrame):
        logger.error('features must be a pandas DataFrame object but is %s.', type(features))
        raise AttributeError('features must be a pandas DataFrame object but is %s.', type(features))

    if features.columns.nlevels != 2:
        logger.error('features must have hierarchical column index containing all tracked subjected, but has {} column levels.'.format(features.columns.nlevels))
        raise AttributeError('features must have hierarchical column index containing all tracked subjected, but has {} column levels.'.format(features.columns.nlevels))

    ############################################

    # get default parameters for feature computation
    parameters = get_default_parameters()
    parameters.update(kwargs)
    logger.debug('Computing pairwise features using the following parameters: {}'.format(parameters))

    # get default scaling factors
    scaling = get_default_scaling()
    if scaling_parameters is not None:
        scaling.update(scaling_parameters)

    logger.debug('Computing pairwise features using the following scaling parameters: {}'.format(scaling))

    ############################################

    raw_feature_names = ['X center_temp', 'Y center_temp']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    features = _compute_distance_features(features, parameters, scaling)
    features = _compute_pose_features(features, parameters, scaling)
    features = _compute_motion_features(features, dt, parameters, scaling)

    return features.sortlevel(0, axis=1)



def _check_required_features(features, required):
    '''
    Returns a list of required features that are not contained in features DataFrame.

    Each list element is a tuple: (subject, feature).

    If no features are missing, an empty list is returned.
    '''

    animal_names = features.columns.get_level_values(0).unique().tolist()

    missing = []

    for subj in animal_names:

        if not all([f in features[subj].columns.values for f in required]):
            missing += [(subj, f) for f in required if f not in features[subj].columns.values]

    return missing


def _compute_distance_features(features, parameters, scaling):
    '''
    Compute all features that are related to the distance between subjects and store them in the 'features' DataFrame.
    '''

    logger = logging.getLogger(__name__)


    raw_feature_names = ['X center_temp', 'Y center_temp']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    animal_names = features.columns.get_level_values(0).unique().tolist()
    animal_length = scaling['mean_length']


    # Distance between center points (symmetric measure)
    for subj, other in combinations(animal_names, r=2):
        features.loc[:, (subj, 'P_dcc')] = distance_2_pts(features[subj].loc[:,['X center_temp', 'Y center_temp']].values,
                                                   features[other].loc[:,['X center_temp', 'Y center_temp']].values, axis=1)/animal_length
        features.loc[:, (subj, 'P_dcc')] = parameters['distance_mapping'](features.loc[:, (subj, 'P_dcc')])

        features.loc[:, (other, 'P_dcc')] = features.loc[:, (subj, 'P_dcc')].values

    return features



def _compute_pose_features(features, parameters, scaling):
    '''
    Compute all features that are related to the pose of the subjects and store them in the 'features' DataFrame.
    '''

    logger = logging.getLogger(__name__)


    raw_feature_names = ['X center_temp', 'Y center_temp']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    animal_names = features.columns.get_level_values(0).unique().tolist()

    # ASYMMETRIC MEASURES
    # all permutations incl. inverse (a->b, b->a)
    for subj, other in permutations(animal_names, r=2):

        # position of other animal relative to my nose-direction (x and y)
        pos_rel_n_inv_x = (features[(other, 'X center_temp')].values - features[(subj, 'X center_temp')].values) \
                           * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) \
                           + (features[(other, 'Y center_temp')].values - features[(subj, 'Y center_temp')].values) \
                             * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)

        pos_rel_n_inv_y = -(features[(other, 'X center_temp')].values - features[(subj, 'X center_temp')].values) \
                          * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) \
                          + (features[(other, 'Y center_temp')].values - features[(subj, 'Y center_temp')].values) \
                            * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)

        features.loc[:, (subj, 'P_gamma_temp')] = np.arctan2(pos_rel_n_inv_y,
                                                             pos_rel_n_inv_x)

        # towards/away from measure as the cosine of the relative position w.r.t. nose-direction
        #  val = x / sqrt( x^2 + y^2 ) = cos(arctan(y/x))
        features.loc[:, (subj, 'P_n_towards')] = pos_rel_n_inv_x / ( np.sqrt( pos_rel_n_inv_x**2 + pos_rel_n_inv_y**2 ) )

        # relative motion direction, wrap to [0, pi]
        features[(subj, 'P_phi_n_rel')] = np.abs((features.loc[:, (subj, 'phi_n_temp')].values - features.loc[:, (other, 'phi_n_temp')].values + np.pi) % (2 * np.pi) - np.pi)


    # (A)SYMMETRIC MEASURES
    #
    # #  Spine Overlap -> [0, 1]
    # #   spine overlap is not a symmetric measure, but we better compute both values at the same time
    # #
    # # non-repeated pair (r=2) combinations  (a->b, but not b->a)
    # for subj, other in combinations(animal_names, r=2):

    #    pnose = features[subj].loc[:, [u'X nose_temp', u'Y nose_temp', u'X center_temp', u'Y center_temp']].values
    #    ptail = features[subj].loc[:, [u'X tail_temp', u'Y tail_temp', u'X center_temp', u'Y center_temp']].values
    #    qnose = features[other].loc[:, [u'X nose_temp', u'Y nose_temp', u'X center_temp', u'Y center_temp']].values
    #    qtail = features[other].loc[:, [u'X tail_temp', u'Y tail_temp', u'X center_temp', u'Y center_temp']].values

    #    # return N-by-2 matrix, where the columns represent the intersection coordinates ([0..1]) for each animal
    #    #  first column is the point of intersection along first segment, second column along second segment
    #    tunn = two_point_segment_intersection(pnose, qnose)
    #    tunt = two_point_segment_intersection(pnose, qtail)
    #    tutn = two_point_segment_intersection(ptail, qnose)
    #    tutt = two_point_segment_intersection(ptail, qtail)

    #    # take the highest value of any of the four intersection possibilities
    #    # reduce applies fmax to the (first) dimension which in this case is the list.
    #    # tu is thus a 2d array, with n rows (frames) and 2 columns (p, q).
    #    tu = np.fmax.reduce([tunn, tunt, tutn, tutt])

    #    # set all remaining NaNs (no intersection) to zero
    #    tu = np.nan_to_num(tu)

    #    # assign the intersection coordinates to the according subjects
    #    features.loc[:, (subj, 'P_overlap_spine')] = tu[:, 0]
    #    features.loc[:, (other, 'P_overlap_spine')] = tu[:, 1]


    return features



def _compute_motion_features(features, dt, parameters, scaling):
    '''
    Compute all features that are related to the motion of the subjects and store them in the 'features' DataFrame.
    '''

    logger = logging.getLogger(__name__)

    raw_feature_names = ['X center_temp', 'Y center_temp']

    missing_features = _check_required_features(features, raw_feature_names)
    if len(missing_features) > 0:
        logger.error('Required base features {} are not in features DataFrame.'.format(missing_features))
        raise AttributeError('Required base features {} are not in features DataFrame.'.format(missing_features))

    ############################################

    # animal_names = features.columns.get_level_values(0).unique().tolist()

    # # ASYMMETRIC MEASURES
    # # all permutations incl. inverse (a->b, b->a)
    # for subj, other in permutations(animal_names, r=2):

    #     # Relative center-point velocity w.r.t. to my own nose direction
    #     other_xc_my_n_dt = features.loc[:, (other, 'dxc_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (other, 'dyc_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values)
    #     other_yc_my_n_dt = -features.loc[:, (other, 'dxc_dt_temp')].values * np.sin(features.loc[:, (subj, 'phi_n_temp')].values) + features.loc[:, (other, 'dyc_dt_temp')].values * np.cos(features.loc[:, (subj, 'phi_n_temp')].values)

    #     # Relative velocities use the already scaled animal velocities. Each velocity is scaled by the corresponding animal's length.
    #     #  As long as the animals are similar in size, I don't see a problem with this. If we ever compare elephants and mice, we might
    #     #  want to reconsider if we want to scale by a common length value.
    #     features.loc[:, (subj, 'P_xc_n_rel_dt_temp')] = other_xc_my_n_dt - features.loc[:, (subj, 'xc_n_dt')].values
    #     features.loc[:, (subj, 'P_yc_n_rel_dt_temp')] = other_yc_my_n_dt - features.loc[:, (subj, 'yc_n_dt')].values

    # SYMMETRIC MEASURES
    # all combinations (a->b == b->a)
    # Relative absolute velocities
    # for subj, other in combinations(animal_names, r=2):

    #     # magnitude of relative velocity
    #     features.loc[:, (subj, 'P_rel_vel_abs_dt')] = np.abs(features.loc[:, (subj, 'c_n_abs_dt')] - features.loc[:, (other, 'c_n_abs_dt')])

    #     features.loc[:, (other, 'P_rel_vel_abs_dt')] = features.loc[:, (subj, 'P_rel_vel_abs_dt')]

    return features

