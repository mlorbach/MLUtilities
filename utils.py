# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 17:08:05 2014

@author: MAL
"""

import numpy as np
from datetime import datetime
import pandas as pd
import logging
import string
import itertools

# imports for rolling mode
from skimage.filters import rank
from skimage.morphology import selem

from sklearn.preprocessing import LabelEncoder


def now_iso():
        return datetime.now().isoformat()


def now(microsec=True):
    if microsec:
        return datetime.now().strftime('%Y%m%d%H%M%S%f')
    else:
        return datetime.now().strftime('%Y%m%d%H%M%S')


def np2csv_string(a, precision=6, sep=';'):
    fmtitem = '{:.%dg}' % precision
    fmt = sep.join([fmtitem] * len(a))
    return fmt.format(*a)


def cast_to_number(s):
    """
    Cast a string containing a number to either int or float; or keep as string if it is neither.

    Parameters
    ----------
    s : str
        input string

    Returns
    -------
    int, float, string
        Casted output
    """
    for ftype in [int, float]:
        try:
            return ftype(s)
        except:
            continue
    return s


def argmin_set(a):
    """
    Return indices of the minimum value(s) in a. Returns an array of indices if the minimum value occurs more than once.

    Parameters
    ----------
    a : TYPE
        Description

    Returns
    -------
    TYPE
        Description
    """
    min_value = np.min(a)
    return np.squeeze(np.where(a == min_value))

def argmax_k(a, k=2, axis=-1):
    """
    Returns the k'th highest value in a.

    If k is 1, is equivalent to argmax(a).

    Parameters
    ----------
    a : TYPE
        Description
    k : int, optional
        0 < k < a.shape[axis]
    axis : TYPE, optional
        Description

    Returns
    -------
    TYPE
        Description
    """
    return np.sort(a, axis=axis).take(-k, axis=axis)

def argmin_k(a, k=2, axis=-1):
    """
    Returns the k'th lowest value in a.

    If k is 1, is equivalent to argmin(a).

    Parameters
    ----------
    a : TYPE
        Description
    k : int, optional
        0 < k < a.shape[axis]
    axis : TYPE, optional
        Description

    Returns
    -------
    TYPE
        Description
    """
    return np.sort(a, axis=axis).take(k - 1, axis=axis)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


def char_range_num(num_chars):
    chariter = itertools.cycle(string.ascii_lowercase)
    for i in xrange(num_chars):
        yield chariter.next()


def char_range(c1, c2):
    """Generates the characters from `c1` to `c2`, inclusive."""
    for c in xrange(ord(c1), ord(c2)+1):
        yield chr(c)


def uni2str(input):
    """
    Turns the unicode input into (byte) string. Input can be dict, list or str.

    Parameters
    ----------
    input : dict, list, str
        Input to be converted

    Returns
    -------
    input : dict, list, str
        Converted input
    """
    if isinstance(input, dict):
        return {uni2str(key): uni2str(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [uni2str(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input


def get_nested(d, keylist):
    """
    Returns value in nested dictionary given the keys as list.

    Works also for accessing lists within dicts. Note that the key in keylist that accesses
    the list item then needs to be and integer and not a str.

    Example:
        >>> get_nested({'a': {'b': 1}}, ['a', 'b'])
        >>> 1

    Parameters
    ----------
    d : dict
        Dictionary to get value from
    keylist : iterable (list, tuple, ...)
        Names of the nested keys

    Returns
    -------
    val : any
        Value
    """

    # old: works only with nested dicts
    # return reduce(dict.get, keylist, d)

    # new: works also with lists and lists in dicts etc.
    return reduce(lambda a, k: a.__getitem__(k), keylist, d)


def set_nested(d, keylist, value):
    """
    Sets a value in a nested dictionary given the keys as list.

    Example:
        >>> d = {'a': {'b': 1}}
        >>> set_nested(d, ['a', 'b'], 5)
        >>> d
        >>> {'a': {'b': 5}}

    Parameters
    ----------
    d : dict
        (Nested) dictionary to set the value in
    keylist : iterable
        Names of the nested keys
    value : any
        Value to set

    Returns
    -------
    None
    """
    get_nested(d, keylist[:-1])[keylist[-1]] = value


def instantiate_dynamic(type_str, kwargs={}):
    """
    Instantiates an object of class type_str dynamically handling the proper imports.

    Parameters
    ----------
    type_str : str
        Full specifier of class (including all packages and modules).
        Example: 'pandas.DataFrame'
    kwargs : dict, optional
        Keyword arguments passed to __init__

    Raises
    ------
    ValueError
        Raised if kwargs contains invalid parameters that do not fit the class definition.

    Returns
    -------
    obj : <type_str>
        Instantiated object of class type_str
    """

    if type_str.lower() == 'none':
        return None

    # dynamically import the preprocessor type
    _package, _class = type_str.rsplit('.', 1)
    _module = __import__(_package, fromlist=[_class])
    _type = getattr(_module, _class) # use _type to instantiate class

    try:
        obj = _type(**kwargs)
    except TypeError as err:
        msg = 'Unexpected parameter. Please check definition of class "{}".\n' \
              '[{}]'.format(_type, err.message)
        raise ValueError(msg)

    return obj


def balanced_subset(X, y, subset_size=1.0, noise=None, return_indices=False, random_state=None):
    '''
    Creates a balanced (sub-)set from the data given in X and y.

    This function samples from X such that in the outupt each class has the same number of samples.
    Data might be omitted or replicated depending on the `subset_size` parameter.

    Parameters
    ----------

    X : array-like
        Data

    y : array-like
        labels

    subset_size : float, int
        Output size. If < 1, size is the fraction of the smallest class in the data set.

        If > 1, size is this absolute number of samples.

        If the output size is larger than a classes' number of samples, those samples are duplicated to match the output size.

    noise : float, optional (default: None)
        Standard deviation of additive, white noise that is used to distort possibly duplicated samples.
        If None, no noise is added.

    return_indices : bool, default: False
        If True, returns the row indices of the subset instead of the data itself.

    random_state : int
        State of the random number generator used to generate white noise.

    Returns
    -------

    Xs, ys : array-like
        Sampled data and corresponding targets.

        Note that output is sorted by the labels in ys.

    '''

    np.random.seed(random_state)

    class_ind = []
    min_elems = None
    indset = np.arange(y.shape[0])

    for yi in np.unique(y):
        elems = indset[(y == yi)]
        class_ind.append((yi, elems))
        if min_elems == None or elems.shape[0] < min_elems:
            min_elems = elems.shape[0]

    use_elems = min_elems
    if subset_size < 1:
        use_elems = int(min_elems*subset_size)
    elif subset_size > 1:
        use_elems = int(subset_size)

    xs = []
    ys = []

    for ci, this_ind in class_ind:
        orig_length = this_ind.shape[0]

        if orig_length > use_elems:
            #print 'downsampling'
            np.random.shuffle(this_ind)

        elif orig_length < use_elems:
            #print 'duplicating'
            # replicate data
            np.random.shuffle(this_ind)

            # full copies
            this_ind = np.tile(this_ind, (int(use_elems / orig_length)) )

            # remaining samples
            this_ind = np.concatenate( (this_ind, this_ind[: use_elems % orig_length ]) )

            #if noise is not None and isinstance(noise, (int, float)):
            #    this_xs[orig_length:] += np.random.normal(scale=noise, size=(this_xs.shape[0]-orig_length, this_xs.shape[1]) )

        idx_ = this_ind[:use_elems]
        y_ = np.empty(use_elems)
        y_.fill(ci)

        xs.append(idx_)
        ys.append(y_)

    xs = np.concatenate(xs)
    ys = np.concatenate(ys)

    if not return_indices:
        return X[xs, :], ys

    return xs#, ys


def gradual_balanced_subset(X, y, balance_factor=1.0, return_indices=False, random_state=None):


    if X.shape[0] != y.shape[0]:
        raise AttributeError('X and y must have same first dimension.')

    if balance_factor < 0 or balance_factor > 1:
        raise ValueError('balance_factor must be in range [0,1] but is {}.'.format(balance_factor))

    if balance_factor == 0:
        if not return_indices:
            return X, y
        else:
            return np.arange(y.shape[0])

    np.random.seed(random_state)

    y = np.asarray(y)

    class_ind = []
    min_elems = None
    indset = np.arange(y.shape[0])

    le = LabelEncoder()
    y = le.fit_transform(y)

    for yi in xrange(len(le.classes_)):
        elems = indset[(y == yi)]
        class_ind.append((yi, elems))
        if min_elems == None or elems.shape[0] < min_elems:
            min_elems = elems.shape[0]

    # compute linear balancing factor per class:
    use_elems = []
    for yi in xrange(len(le.classes_)):

        # linear function mapping [0,1] to [N_class, min_elems]
        # M_class = m * balance_factor + N_class : m <= 0
        #  m = (min_elems - N_class)/(1-0)
        N_class = len(class_ind[yi][1])
        M_class = max(min_elems, min(N_class, int(np.round( (min_elems - N_class)*balance_factor + N_class))))
        use_elems.append( M_class )

    xs = []
    ys = []

    for ci, this_ind in class_ind:
        orig_length = this_ind.shape[0]
        np.random.shuffle(this_ind)

        idx_ = this_ind[:use_elems[ci]]
        y_ = np.ones(use_elems[ci], dtype=np.int)*ci

        xs.append(idx_)
        ys.append(y_)


    xs = np.concatenate(xs)
    ys = np.concatenate(ys)

    if not return_indices:
        ys = le.inverse_transform(ys)
        return X[xs, :], ys

    return xs


def rolling_mode(sequence, win, centered=True):
    '''
    Compute the rolling/sliding mode in the sequence.
    :param sequence: 1D sequence of unsigned integer (8 bit) np.uint8
    :param win: width of window
    :param centered: True if output is mode of centered window, False for right aligned (past values).
    :return: mode-smoothed sequence of same shape as input sequence.
    '''

    sequence = np.squeeze(np.asarray(sequence, dtype=np.uint8))

    if sequence.ndim > 1:
        raise ValueError('Input sequence must be 1-dimensional but has {} dimensions: {}.'.format(sequence.ndim, sequence.shape))

    sequence = sequence[:, np.newaxis]

    return np.squeeze(np.argmax(rank.windowed_histogram(sequence, selem.rectangle(win,1)), axis=2))


    # return np.squeeze(np.argmax(np.random.rand(sequence.shape[0], 2), axis=1))
    #return np.squeeze(np.argmax(rank.windowed_histogram(sequence, selem.rectangle(1,win)), axis=2))
