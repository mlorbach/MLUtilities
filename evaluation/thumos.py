# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2015-07-21 13:24:25
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-09-06 10:00:09

import numpy as np
import pandas as pd

from MLUtilities.annotation.utils import frame_to_segment_annotations


def evaluate_detection_performance(df_true, df_pred, overlap_crit=.7, normalize=True, given_segments=False):

    df_overlap_stats = compute_overlap_statistics(df_true, df_pred, given_segments)

    return apply_overlap_threshold(df_overlap_stats, overlap_crit, normalize)



def apply_overlap_threshold(df_overlap_stats, overlap_crit=.7, normalize=False):
    # make sure overlap_crit is a list
    if not isinstance(overlap_crit, (list, tuple)):
        overlap_crit = [overlap_crit]

    stat_dict = {}

    for crit in overlap_crit:
        if normalize:
            stat_dict[crit] = (df_overlap_stats.overlap_coef >= crit).groupby(level='video').sum() / df_overlap_stats.groupby(level='video').size()
        else:
            stat_dict[crit] = (df_overlap_stats.overlap_coef >= crit).groupby(level='video').sum()

    return pd.concat(stat_dict)


def compute_overlap_statistics(df_true, df_pred, given_segments=False):
    '''


    '''

    # convert frames to segments unless they are already given
    if not given_segments:
        df_gt_segments = frame_to_segment_annotations(df_true)
        df_pr_segments = frame_to_segment_annotations(df_pred)
    else:
        df_gt_segments = df_true
        df_pr_segments = df_pred





    # get names of videos or a fake index if only one video is given
    if df_gt_segments.index.nlevels > 1:
        video_names = df_gt_segments.index.get_level_values('video').unique()
    else:
        video_names = [slice(None)]


    df_detection = pd.DataFrame(index=df_gt_segments.index, data={'intersection':0, 'union':0, 'n_predictions': 0, 'gt_duration': 0})
    #df_detection.loc[:, 'union'] = df_gt_segments.frame_stop - df_gt_segments.frame + 1

    # apply to all videos separately
    for vid in video_names:
        # TODO: parallelize this job

        gt_segs = df_gt_segments.loc[vid]
        pr_segs = df_pr_segments.loc[vid]

        gt_segs['frame_start'] = gt_segs.index.get_level_values('frame')
        pr_segs['frame_start'] = pr_segs.index.get_level_values('frame')

        # find all matching segments with at least one frame overlap
        df_matches = match_segments(gt_segs, pr_segs)

        df_detection.loc[ (vid, slice(None)), 'intersection'] = df_matches['intersection'].values
        df_detection.loc[ (vid, slice(None)), 'n_predictions'] = df_matches['n_matches'].values
        df_detection.loc[ (vid, slice(None)), 'union'] = np.fmax(gt_segs['frame_stop'].values, df_matches['match_max'].values) - np.fmin(gt_segs['frame_start'].values, df_matches['match_min'].values) + 1
        df_detection.loc[ (vid, slice(None)), 'gt_duration'] = gt_segs['frame_stop'].values - gt_segs['frame_start'].values + 1

    df_detection['overlap_coef'] = df_detection['intersection'] / df_detection['union']

    return df_detection






##################################################################

def match_segments(gt_segments, pr_segments):
    '''
    Finds segments in two lists of segments that overlap by at least one frame.

    The segment lists should be given as pandas.DataFrames in which each segment is a row.
    The required columns are `frame_start`, `frame_stop`, and `action` specifying the first frame, the last frame,
    and the action label of the segment. The index of the dataframe should provide a unique identifier per
    segment. The identifiers are used in the returned dictionaries.

    Results are returned in a dictionary containing the matches of gt_segments to (multiple) pr_segments.

    Returns
    -------

    gt_match_dict : dict

    The resulting dictionary has the format:

    >>>  gt_match_dict =
         {
          gt_segment_idx0 : [pr_segment_idx0, pr_segment_idx1, pr_segment_idx2, ...],
          gt_segment_idx1 : [pr_segment_idx2, pr_segment_idx5, ...],
          ...
         }
    '''

    #gt_match_dict = {} # ground truth segments --> prediction segments
    df_matches = pd.DataFrame(index=gt_segments.index, data={'match_min':np.nan, 'match_max':np.nan, 'intersection': 0, 'n_matches':0})
    df_matches['gt_action'] = gt_segments.action

    if 'Confidence' in gt_segments.columns:
        df_matches['gt_confidence'] = gt_segments.Confidence

    #gt_temp = gt_segments.copy()

    for idx, segment in gt_segments.iterrows():

        match_idx = pr_segments.index[ (pr_segments.action == segment.action) & (pr_segments.frame_start <= segment.frame_stop) & (pr_segments.frame_stop >= segment.frame_start) ]
        #gt_match_dict[idx] = match_idx.values.tolist()

        if len(match_idx) > 0:
            df_matches.loc[idx, 'n_matches'] = len(match_idx)
            df_matches.loc[idx, 'match_min'] = pr_segments.loc[match_idx, 'frame_start'].min()
            df_matches.loc[idx, 'match_max'] = pr_segments.loc[match_idx, 'frame_stop'].max()

            # length of GT segment: intersection can never be more than this
            intersection = segment.frame_stop - segment.frame_start + 1

            # subtract if first prediction segment start later than GT
            intersection -= max(0, pr_segments.loc[match_idx[0], 'frame_start'] - segment.frame_start)

            # subtract if last prediction segments ends earlier than GT
            intersection -= max(0, segment.frame_stop - pr_segments.loc[match_idx[-1], 'frame_stop'])

            # subtract frames inbetween prediction segments (those are wrongly labeled)
            if len(match_idx) > 1:
                intersection -= (pr_segments.loc[match_idx, 'frame_start'].shift(-1) - pr_segments.loc[match_idx, 'frame_stop'] - 1).sum()

            df_matches.loc[idx, 'intersection'] = intersection

    return df_matches


def match_segments_separate(gt_segments, pr_segments):

    # similar to match_segments() but keeps each matched event explicit (i.e., doesn't merge multiple matches them to a combined intersection-union measure)

    df_matches = {}

    for idx, segment in gt_segments.iterrows():

        match_list = []
        match_idx = pr_segments.index[ (pr_segments.action == segment.action) & (pr_segments.frame_start <= segment.frame_stop) & (pr_segments.frame_stop >= segment.frame_start) ]

        if len(match_idx) == 0:
            continue

        for pr_idx in match_idx:

            match = pd.Series(name=pr_idx,
                              data=[
                                  pr_segments.loc[pr_idx, 'frame_start'],
                                  pr_segments.loc[pr_idx, 'frame_stop']
                              ],
                             index=['match_start', 'match_stop'])
            match_list.append(match)

        df_match = pd.concat(match_list, axis=1).T
        df_match['gt_start'] = segment.frame_start
        df_match['gt_stop'] = segment.frame_stop
        df_match['action'] = segment.action
        df_match['intersection'] = df_match.loc[:, ['gt_stop', 'match_stop']].min(axis=1) - df_match.loc[:, ['gt_start', 'match_start']].max(axis=1) + 1
        df_match['union'] = df_match.loc[:, ['gt_stop', 'match_stop']].max(axis=1) - df_match.loc[:, ['gt_start', 'match_start']].min(axis=1) + 1

        df_matches[idx] = df_match

    df_matches = pd.concat(df_matches, names=['gt_index', 'match_index'])
    df_matches['gt_duration'] = df_matches['gt_stop'] - df_matches['gt_start'] + 1
    df_matches['match_duration'] = df_matches['match_stop'] - df_matches['match_start'] + 1

    return df_matches

##################################################################
