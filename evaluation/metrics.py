# -*- coding: utf-8 -*-
"""
Created on Thu Dec 04 13:42:38 2014

@author: MAL
"""

import numpy as np
from sklearn.preprocessing import LabelEncoder
from scipy.sparse import coo_matrix
import sklearn.metrics
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics.classification import _prf_divide


def cohen_kappa_with_max(y1, y2, labels=None):

    confusion = sklearn.metrics.confusion_matrix(y1, y2, labels=labels)

    # normalize by total number of elements (sum of all marginals is 1)
    confusion = confusion / np.sum(confusion, dtype=float)

    # number of categories
    n_classes = confusion.shape[0]

    # marginals of observer A (p_iA)
    sum0 = np.sum(confusion, axis=0)

    # marginals of observer B (p_iB)
    sum1 = np.sum(confusion, axis=1)

    # expected (chance) agreement given the different marginals
    expected = np.outer(sum0, sum1) / np.sum(sum0, dtype=float)

    # kappa is observed agreement corrected by expected agreement / divided by expected disagreement
    #  the trace is the proportion of units in which the observers agreed (observed or expected, respectively)
    k = (np.trace(confusion) - np.trace(expected)) / (1 - np.trace(expected))

    # the maximally possible agreement given the different marginals (prior distribution of labels of each observer)
    k_max = (np.sum(np.fmin(sum0, sum1)) - np.trace(expected)) / (1 - np.trace(expected))

    return k, k_max


def confusion_matrix_weighted(y_true, y_pred, weights, labels=None):
    """Compute weighted confusion matrix to evaluate the accuracy of a classification

    By definition a confusion matrix :math:`C` is such that :math:`C_{i, j}`
    is equal to the number of observations known to be in group :math:`i` but
    predicted to be in group :math:`j`, optionally weighted.

    This function is an exact copy of sklearn's confusion_matrix() but with the
    additional weights parameter specifying a weight for each sample.
    The resulting confusion matrix this counts the weighted number of observations.

    Parameters
    ----------
    y_true : array, shape = [n_samples]
        Ground truth (correct) target values.

    y_pred : array, shape = [n_samples]
        Estimated targets as returned by a classifier.

    weights : array, shape = [n_samples]
        Sample weights.

    labels : array, shape = [n_classes], optional
        List of labels to index the matrix. This may be used to reorder
        or select a subset of labels.
        If none is given, those that appear at least once
        in ``y_true`` or ``y_pred`` are used in sorted order.

    Returns
    -------
    C : array, shape = [n_classes, n_classes]
        Confusion matrix

    References
    ----------
    .. [1] `Wikipedia entry for the Confusion matrix
           <http://en.wikipedia.org/wiki/Confusion_matrix>`_

    Examples
    --------
    >>> from MLUtilities.evaluation.metrics import confusion_matrix_weighted
    >>> y_true = [1, 0, 1, 0, 0, 0]
    >>> y_pred = [0, 0, 1, 1, 0, 1]
    >>> w =  [1, 1, 0.75, 1, 1, .5]
    >>> confusion_matrix_weighted(y_true, y_pred, w)
    array([[ 2.  ,  1.5 ],
           [ 1.  ,  0.75]])

    """

    if labels is None:
        labels = unique_labels(y_true, y_pred)
    else:
        labels = np.asarray(labels)

    n_labels = labels.size
    label_to_ind = dict((y, x) for x, y in enumerate(labels))
    # convert yt, yp into index
    y_pred = np.array([label_to_ind.get(x, n_labels + 1) for x in y_pred])
    y_true = np.array([label_to_ind.get(x, n_labels + 1) for x in y_true])

    # intersect y_pred, y_true with labels, eliminate items not in labels
    ind = np.logical_and(y_pred < n_labels, y_true < n_labels)
    y_pred = y_pred[ind]
    y_true = y_true[ind]

    CM = coo_matrix((weights, (y_true, y_pred)),
                    shape=(n_labels, n_labels)
                    ).toarray()

    return CM


def classification_report(y_true, y_pred, labels=None, target_names=None, latex=False):
    '''
    Returns classification report that only considers the labels provided in 'labels'.

    Use this function to leave out specific targets in your results (e.g., the 'other' class),
    or samples for which you don't have a label.

    Both true and predicted labels are filtered.

    '''

    y_true = np.asarray(y_true)
    y_pred = np.asarray(y_pred)

    if labels is not None:
        valid_targets = np.in1d( y_true, labels )
        valid_targets &= np.in1d( y_pred, labels )
    else:
        valid_targets = np.ones_like(y_true, np.bool)

    if not latex:
        return _classification_report(y_true[valid_targets], y_pred[valid_targets], labels=labels, target_names=target_names)
    else:
        return classification_report_latex(y_true[valid_targets], y_pred[valid_targets], labels=labels, target_names=target_names)


def _classification_report(y_true, y_pred, labels, target_names=None, sample_weight=None, digits=2):

    """Build a text report showing the main classification metrics

    Parameters
    ----------
    y_true : 1d array-like, or label indicator array / sparse matrix
        Ground truth (correct) target values.

    y_pred : 1d array-like, or label indicator array / sparse matrix
        Estimated targets as returned by a classifier.

    labels : array, shape = [n_labels]
        Optional list of label indices to include in the report.

    target_names : list of strings
        Optional display names matching the labels (same order).

    sample_weight : array-like of shape = [n_samples], optional
        Sample weights.

    digits : int
        Number of digits for formatting output floating point values

    Returns
    -------
    report : string
        Text summary of the precision, recall, F1 score for each class.

    Examples
    --------
    >>> from sklearn.metrics import classification_report
    >>> y_true = [0, 1, 2, 2, 2]
    >>> y_pred = [0, 0, 2, 2, 1]
    >>> target_names = ['class 0', 'class 1', 'class 2']
    >>> print(classification_report(y_true, y_pred, target_names=target_names))
                 precision    recall  f1-score   support
    <BLANKLINE>
        class 0       0.50      1.00      0.67         1
        class 1       0.00      0.00      0.00         1
        class 2       1.00      0.67      0.80         3
    <BLANKLINE>
    avg / total       0.70      0.60      0.61         5
    <BLANKLINE>

    """

    if labels is None:
        labels = unique_labels(y_true, y_pred)
    else:
        labels = np.asarray(labels)

    avgs_line_heading = 'avg / total'
    avgc_line_heading = 'avg / class'

    if target_names is None:
        target_names = ['%s' % l for l in labels]
        width = max(len(avgs_line_heading), max(len(str(cn)) for cn in labels))
    else:
        width = max(len(str(cn)) for cn in target_names)
        width = max(width, len(avgs_line_heading), digits)

    # remove underscores from target names
    target_names = [str(t).replace('_', ' ') for t in target_names]

    headers = ["precision", "recall", "f1-score", "support"]
    fmt = '%% %ds' % width  # first column: class name
    fmt += '  '
    fmt += ' '.join(['% 9s' for _ in headers])
    fmt += '\n'

    headers = [""] + headers
    report = fmt % tuple(headers)
    report += '\n'

    p, r, f1, s = precision_recall_fscore_support(y_true, y_pred,
                                                  labels=labels,
                                                  average=None,
                                                  sample_weight=sample_weight)

    for i, label in enumerate(labels):
        values = [target_names[i]]
        for v in (p[i], r[i], f1[i]):
            values += ["{0:0.{1}f}".format(v, digits)]
        values += ["{0}".format(s[i])]
        report += fmt % tuple(values)

    report += '\n'

    # compute averages
    values = [avgs_line_heading]
    for v in (np.average(p, weights=s),
              np.average(r, weights=s),
              np.average(f1, weights=s)):
        values += ["{0:0.{1}f}".format(v, digits)]
    values += ['{0}'.format(np.sum(s))]
    report += fmt % tuple(values)

    # compute class averages
    values = [avgc_line_heading]
    for v in (np.average(p),
              np.average(r),
              np.average(f1)):
        values += ["{0:0.2f}".format(v)]
    values += ['{0}'.format(len(labels))]
    report += fmt % tuple(values)

    return report



def classification_report_latex(y_true, y_pred, labels=None, target_names=None,
                          sample_weight=None):
    """Build a text report showing the main classification metrics in latex tabular format

    Parameters
    ----------
    y_true : array-like or label indicator matrix
        Ground truth (correct) target values.

    y_pred : array-like or label indicator matrix
        Estimated targets as returned by a classifier.

    labels : array, shape = [n_labels]
        Optional list of label indices to include in the report.

    target_names : list of strings
        Optional display names matching the labels (same order).

    sample_weight : array-like of shape = [n_samples], optional
        Sample weights.

    Returns
    -------
    report : string
        Text summary of the precision, recall, F1 score for each class.

    Examples
    --------
    >>> from sklearn.metrics import classification_report
    >>> y_true = [0, 1, 2, 2, 2]
    >>> y_pred = [0, 0, 2, 2, 1]
    >>> target_names = ['class 0', 'class 1', 'class 2']
    >>> print(classification_report(y_true, y_pred, target_names=target_names))
                 precision    recall  f1-score   support
    <BLANKLINE>
        class 0       0.50      1.00      0.67         1
        class 1       0.00      0.00      0.00         1
        class 2       1.00      0.67      0.80         3
    <BLANKLINE>
    avg / total       0.70      0.60      0.61         5
    <BLANKLINE>

    """

    if labels is None:
        labels = unique_labels(y_true, y_pred)
    else:
        labels = np.asarray(labels)

    avgs_line_heading = 'Osamples'
    avgc_line_heading = 'Oclass'

    if target_names is None:
        target_names = ['%s' % l for l in labels]
    width = max(len(cn) for cn in target_names)
    width = max(width, len(avgs_line_heading))

    # remove underscores from target names
    target_names = [t.replace('_', ' ') + '\t' * ((width - len(l)) / 8) for t in target_names]

    avgs_line_heading = '\\rowcolor{gray!10}\nAvg. frames'
    avgc_line_heading = '\\rowcolor{gray!10}\nAvg. classes'

    headers = ["Prec.", "Recall", "F1", "\#"]
    fmt = '%% %ds' % width  # first column: class name
    fmt += ' & '
    fmt += ' & '.join(['% 9s' for _ in headers])
    fmt += '\\\\\n'

    headers = [""] + headers
    report = '\\begin{tabular}{l ccc r}\n'
    report += '\\rowcolor{gray!10}\n'
    report += fmt % tuple(headers)
    report += '\n'

    p, r, f1, s = precision_recall_fscore_support(y_true, y_pred,
                                                  labels=labels,
                                                  average=None,
                                                  sample_weight=sample_weight)

    for i, label in enumerate(labels):
        values = [target_names[i]]
        for v in (p[i], r[i], f1[i]):
            values += ["{0: 9.2f}".format(v)]
        values += ["{0}".format(s[i])]
        report += fmt % tuple(values)

    report += '\n'


    # compute averages
    values = [avgs_line_heading]
    for v in (np.average(p, weights=s),
              np.average(r, weights=s),
              np.average(f1, weights=s)):
        values += ["{0: 9.2f}".format(v)]
    values += ['{0}'.format(np.sum(s))]
    report += fmt % tuple(values)

    p, r, f1, _ = precision_recall_fscore_support(y_true, y_pred,
                                                  labels=labels,
                                                  average='macro')
    # compute class averages
    values = [avgc_line_heading]
    for v in (np.average(p),
              np.average(r),
              np.average(f1)):
        values += ["{0: 9.2f}".format(v)]
    values[-1] = '\\cellcolor{gray!30}' + values[-1]
    values += ['{0}'.format(len(labels))]
    report += fmt % tuple(values)

    report += '\\end{tabular}'

    return report


def classification_report_multiseq(scores, target_names=None):
    """Build a text report showing the main classification metrics

    Parameters
    ----------
    scores : array

    target_names : list of strings
        Optional display names matching the labels (same order).

    Returns
    -------
    report : string
        Text summary of the precision, recall, F1 score for each class.

    Examples
    --------
    >>> from sklearn.metrics import classification_report
    >>> y_true = [0, 1, 2, 2, 2]
    >>> y_pred = [0, 0, 2, 2, 1]
    >>> target_names = ['class 0', 'class 1', 'class 2']
    >>> print(classification_report(y_true, y_pred, target_names=target_names))
                 precision    recall  f1-score   support
    <BLANKLINE>
        class 0       0.50      1.00      0.67         1
        class 1       0.00      0.00      0.00         1
        class 2       1.00      0.67      0.80         3
    <BLANKLINE>
    avg / total       0.70      0.60      0.61         5
    <BLANKLINE>

    """

    if scores.shape[0] > 1:
        means = np.mean(scores[:,:,:3], axis=0)
        stds = np.std(scores[:,:,:3], axis=0)
    else:
        means = np.squeeze(scores[:,:,:3])
        stds = None
    supp_sum = np.sum(scores[:,:,3], axis=0, dtype=np.int)


    last_line_heading = 'avg / total'

    if target_names is None:
        width = len(last_line_heading)
        target_names = ['%s' % l for l in range(means.shape[0])]
    else:
        width = max(len(cn) for cn in target_names)
        width = max(width, len(last_line_heading))

    headers = ["precision", "recall", "f1-score", "support"]
    fmt = '%%%ds' % width  # first column: class name
    fmt += '  '
    if stds is not None:
        fmt += ' '.join(['%13s' for _ in headers])
    else:
        fmt += ' '.join(['%9s' for _ in headers])
    fmt += '\n'

    headers = [""] + headers
    report = fmt % tuple(headers)
    report += '\n'


    for i, label in enumerate(target_names):
        values = [label]

        if stds is not None:
            for j in xrange(3):
                values += ["{0:0.2f} ({1:0.2f})".format( means[i,j], stds[i,j] )]
        else:
            for j in xrange(3):
                values += ["{0:0.2f}".format( means[i,j] )]

        values += ["{0}".format(supp_sum[i])]
        report += fmt % tuple(values)

    report += '\n'

    # compute weighted averages
    values = [last_line_heading]
    for v in (np.average(means[:,0], weights=supp_sum),
              np.average(means[:,1], weights=supp_sum),
              np.average(means[:,2], weights=supp_sum)):
        values += ["{0:0.2f}".format(v)]
    values += ['{0}'.format(np.sum(supp_sum))]
    report += fmt % tuple(values)

    # compute weighted averages
    values = ['avg / class']
    for v in (np.average(means[:,0]),
              np.average(means[:,1]),
              np.average(means[:,2])):
        values += ["{0:0.2f}".format(v)]
    values += ['{0}'.format(len(target_names))]
    report += fmt % tuple(values)

    return report


def top_k_recall(y_true, prob, labels=None, k=2):

    present_labels = unique_labels(y_true)
    if labels is None:
        labels = present_labels
        n_labels = None
    else:
        n_labels = len(labels)
        labels = np.hstack([labels, np.setdiff1d(present_labels,
                                                 labels,
                                                 assume_unique=True)])

    le = LabelEncoder()
    le.fit(labels)
    y_true = le.transform(y_true)
    sorted_labels = le.classes_

    top_k = np.argsort(prob, axis=1)[:, -k:]  # k best predictions
    tp = map(lambda i: y_true[i] in top_k[i, :], range(y_true.shape[0]))

    tp_bins = y_true[np.where(tp)]
    tp_sum = np.bincount(tp_bins, minlength=len(labels))
    true_sum = np.bincount(y_true, minlength=len(labels))

    # Retain only selected labels
    indices = np.searchsorted(sorted_labels, labels[:n_labels])
    tp_sum = tp_sum[indices]
    true_sum = true_sum[indices]

    with np.errstate(divide='ignore', invalid='ignore'):
        # Divide, and on zero-division, set scores to 0 and warn:
        recall = _prf_divide(tp_sum, true_sum, 'recall', 'true', None, ('recall',))

    return recall


def precision_recall_fscore_support(y_true, y_pred, labels, as_matrix=False, **kwargs):
    '''
    Returns precision_recall_fscore_support that only considers the labels provided in 'labels'.

    Use this function to leave out specific targets in your results (e.g., the 'other' class),
    or samples for which you don't have a label.

    Both true and predicted labels are filtered.

    '''

    valid_targets = np.in1d( y_true, labels )
    valid_targets &= np.in1d( y_pred, labels )

    result = sklearn.metrics.precision_recall_fscore_support(np.compress(valid_targets, y_true), np.compress(valid_targets, y_pred), labels=labels, **kwargs)

    if as_matrix:
        return np.vstack(result).T
    else:
        return result


def precision_recall_fscore_support_multiseq(Y, labels, concatenate=False, **kwargs):
    '''
    Returns precision_recall_fscore_support that only considers the labels provided in 'labels'.

    Use this function to leave out specific targets in your results (e.g., the 'other' class),
    or samples for which you don't have a label.

    Both true and predicted labels are filtered.

    Parameters
    ----------

    Y : iterable of tuples (true, pred)
        True and predicted label sequences.

    labels : list of labels (str, int)
        Labels to include in the computation. Use this to leave out specific labels.

    concatenate : boolean, default: False
        If True, concatenates the sequences and computes overall scores over all sequences.
        If False, return scores per sequence.

    kwargs : kwargs
        kwargs passed to precision_recall_fscore_support function which computes the scores.

    '''

    if not concatenate:
        scores = []
        for y in Y:
            if len(y[0].shape) > 1:
                true_labels = y[0][:,0]
                conf = y[0][:,1]
            else:
                true_labels = y[0]
                conf= None

            valid_targets = np.in1d( true_labels, labels ) & np.in1d( y[1], labels )

            scores.append( np.vstack(sklearn.metrics.precision_recall_fscore_support(np.compress(valid_targets, true_labels), np.compress(valid_targets, y[1]), labels=labels, sample_weight=conf, **kwargs)).T)
        return np.rollaxis(np.dstack(scores), 2, 0)
    else:

        y_true = []
        y_pred = []
        conf = []

        if len(Y[0][0].shape) > 1:
            for y in Y:
                y_true += y[0][:,0].tolist()
                conf += y[0][:,1].tolist()
                y_pred += y[1].tolist()
        else:
            for y in Y:
                y_true += y[0].tolist()
                y_pred += y[1].tolist()
            conf = None

        return precision_recall_fscore_support(np.asarray(y_true), np.asarray(y_pred), labels, sample_weight=conf, **kwargs)




def filter_by_targets(array, to_filter, valid_targets, return_indices=False):
    '''
    Returns all of 'array' where 'to_filter' is in 'valid_targets'.

    Parameters
    ----------

    array : array-like
        Values to returned filtered

    to_filter : array-like
        Values to filter on. Must be of same shape as 'array'.

    valid_targets : array-like
        The values of 'to_filter' that are valid and hence should be returned.

    return_indices : boolean, optional, default = False
        If true, in addition to the filtered values in 'array', a boolean array is returned which specifies the filtered values.


    Returns
    -------
        array : array-like
            filtered values from 'array'

        ind : array-like, optional
            boolean array specifying the valid elements in the incoming 'array'


    '''

    to_filter = np.asarray(to_filter)

    if array.shape[0] != to_filter.shape[0]:
        raise AttributeError('First dimension of array must match the only dimension of to_filter.')

    ind = np.in1d(to_filter, valid_targets)

    if return_indices:
        return array[ ind ], ind
    else:
        return array[ ind ]


def filter_by_mask(array, mask, true_value=True):

    mask = np.asarray(mask)

    if array.shape[0] != mask.shape[0]:
        raise AttributeError('First dimension of array must match the only dimension of mask.')

    return array[mask==true_value]
