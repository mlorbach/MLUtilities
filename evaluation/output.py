# -*- coding: utf-8 -*-
"""
Created on Tue Feb 17 16:18:22 2015

@author: MAL
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from metrics import filter_by_mask, classification_report, precision_recall_fscore_support
from ..visualization.matrix import plot_confusion_matrix


def get_pandas_score_entry(score, only_mean=False):
    """
    Takes one entry of a _CVScoreTuple and turns it into a pandas DataFrame.

    Map on all GridSearch scores to get a complete score DataFrame:
    > dfscores = map(get_pandas_score_entry, grid.grid_scores_)  # list
    > dfscores = pd.concat(dfscores).reset_index()

    Is deprecated, GridSearchCV now allows to export the results in dict-structure that is directly usable with pandas. Check http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html#sklearn.model_selection.GridSearchCV

    Parameters
    ----------
    score : _CVScoreTuple
        One entry of GridSearch.grid_scores_
    only_mean : bool, optional
        If True, returns only one row: the mean over all folds.

    Returns
    -------
    pd.DataFrame
        Scores DataFrame with k rows or 1 row if only_mean is true.
    """
    if only_mean:
        df = pd.DataFrame(data=score.parameters, index=[0])
        df['score'] = score.mean_validation_score
    else:
        entries = range(len(score.cv_validation_scores))
        df = pd.DataFrame(data=score.parameters, index=entries)
        df['score'] = score.cv_validation_scores
    df.index.name = 'fold'
    return df


def transform_data_for_evaluation(dtrain, dtest_subj, include_other=True, other_name='oth', pred_field='pred_processed', mask='mask'):


    target_names = list(dtrain['target_names']) # copy

    # include the 'other' label in the set of valid labels for the test set:
    if include_other and (other_name not in target_names):
        target_names.append(other_name)

    if not include_other and (other_name in target_names):
        target_names = [t for t in target_names if t != other_name]

    target_labels = dtrain['target_encoder'].transform( target_names ).tolist()
    other_label = dtrain['target_encoder'].transform(other_name)


    # only consider samples of which we know the annotation and are track corrected
    if mask is not None and mask in dtest_subj:
        y_true = filter_by_mask(dtest_subj['target'], dtest_subj[mask], true_value=True)
        y_pred = filter_by_mask(dtest_subj[pred_field], dtest_subj[mask], true_value=True)
    else:
        y_true = dtest_subj['target']
        y_pred = dtest_subj[pred_field]


    if not include_other:
        other_mask = y_true != other_label
        y_true = filter_by_mask(y_true, other_mask)
        y_pred = filter_by_mask(y_pred, other_mask)


    return y_true, y_pred, target_labels, target_names, other_label



def plot_subject_confusion_matrices(dtrain, dtest, include_other=True, other_name='oth', pred_field='pred_processed', subplots=False, mask='mask'):

    figs = []
    axs = []
    subjects = sorted(dtest.keys())
    n_subjects = len(subjects)

    if subplots:
        fig, axs = plt.subplots(1, n_subjects, figsize=(8.5*n_subjects, 8))

    for i, subj in enumerate(subjects):
        y_true, y_pred, target_labels, target_names, other_label = transform_data_for_evaluation(dtrain, dtest[subj], include_other, other_name, pred_field, mask)

        if not subplots:
            fig = plt.figure(figsize=(8, 8))
            ax = fig.gca()
            axs.append(ax)
        else:
            ax = axs.flat[i]

        _,ax,_ = plot_confusion_matrix(y_true, y_pred, labels=target_labels, target_names=target_names, cmap='RdYlBu_r', norm=True, font='print', ax=ax)
        ax.set_title('Confusion matrix {}'.format(subj))
        figs.append(fig)


    return figs, axs


def plot_joined_subject_confusion_matrix(dtrain, dtest, include_other=True, other_name='oth', pred_field='pred_processed', mask='mask', **kwargs):

    y_true_subs = []
    y_pred_subs = []

    for subj in sorted(dtest.keys()):
        y_true, y_pred, target_labels, target_names, _ = transform_data_for_evaluation(dtrain, dtest[subj], include_other, other_name, pred_field, mask)
        y_true_subs.append(y_true)
        y_pred_subs.append(y_pred)

    # join samples from all subjects
    y_true_joined = np.concatenate(y_true_subs)
    y_pred_joined = np.concatenate(y_pred_subs)

    return plot_confusion_matrix(y_true_joined, y_pred_joined, labels=target_labels, target_names=target_names, norm=True, **kwargs)



def print_subject_statistics(dtrain, dtest, include_other=True, other_name='oth', pred_field='pred_processed', mask='mask', latex=False):


    for subj in sorted(dtest.keys()):
        print 'Results subject: {}\n------------------------------\n'.format(subj)

        y_true, y_pred, target_labels, target_names, other_label = transform_data_for_evaluation(dtrain, dtest[subj], include_other, other_name, pred_field, mask)

        if include_other:
            n_other = np.sum(y_true==other_label)
            print 'This set contains {} true "other" samples ({:.1f}%).\n'.format(n_other , n_other/float(y_true.shape[0])*100. )

        print_statistics(y_true, y_pred, target_labels, target_names, latex=latex)



def print_joined_subject_statistics(dtrain, dtest, include_other=True, other_name='oth', pred_field='pred_processed', mask='mask', latex=False):

    y_true_subs = []
    y_pred_subs = []

    for subj in sorted(dtest.keys()):
        y_true, y_pred, target_labels, target_names, _ = transform_data_for_evaluation(dtrain, dtest[subj], include_other, other_name, pred_field, mask)
        y_true_subs.append(y_true)
        y_pred_subs.append(y_pred)

    # join samples from all subjects
    y_true_joined = np.concatenate(y_true_subs)
    y_pred_joined = np.concatenate(y_pred_subs)

    print_statistics(y_true_joined, y_pred_joined, target_labels, target_names, latex=latex)



def print_statistics(y_true, y_pred, labels=None, names=None, latex=False):

    y_true = np.asarray(y_true)
    y_pred = np.asarray(y_pred)

    if labels is None:
        labels = np.union1d(np.unique(y_true), np.unique(y_pred))

    if names is None or len(names) < len(labels):
        names = ['%s' % l for l in labels]

    report = classification_report(y_true, y_pred, labels, names, latex=latex)
    [prec, recall, f1, _] = precision_recall_fscore_support(y_true, y_pred, labels=labels, average='macro', pos_label=None)


    # Textual result output:

    print report
    print 'Ratio correct samples = {0:0.3f}\n'.format(np.sum(y_true==y_pred)/float(y_true.shape[0]))
    print 'Avg. precision = {0:0.3f}\tAvg. recall = {1:0.3f}\tAvg. F1 = {2:0.3f}\n\n'.format(prec,recall, f1)
