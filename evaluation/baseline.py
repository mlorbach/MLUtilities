# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-06-06 16:19:31
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-16 12:59:50


import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder

def _get_counts(y, normalized=True):
    """
    Get label counts (class priors) in y. By default returns normalized counts.

    Parameters
    ----------
    y : array-like
        Labels
    normalized : bool, optional
        Normalize counts to sum to 1 (default: True)

    Returns
    -------
    np.array
        Frequencies/counts per label ordered as by LabelEncoder()
    """
    y = LabelEncoder().fit_transform(y)
    py = np.bincount(y)
    if normalized:
        return py/np.sum(py, dtype=np.float)
    else:
        return py


def accuracy(y, priors=None):
    """
    Calculate the theoretic baseline accuracy score of a random classifier on y with different class priors.

    Parameters
    ----------
    y : array-like
        True labels
    priors : None, optional
        Class priors: ['uniform', 'stratified', 'most_frequent', array-like].
        If array-like, length must match number of unique labels in y (ordered labels as returned by LabelEncoder).

    Returns
    -------
    float
        Accuracy

    Raises
    ------
    AttributeError
        If priors format is invalid.
    """
    py = _get_counts(np.asarray(y))

    if priors is None or priors == 'uniform': # uniform prior
        priors = np.ones_like(py)
    elif priors == 'stratified':
        priors = py
    elif priors == 'most_frequent':
        priors = np.zeros_like(py)
        priors[np.argmax(py)] = 1
    else:
        priors = np.asarray(priors)

    priors /= np.sum(priors, dtype=np.float)

    if priors.ndim > 1 or priors.shape[0] != py.shape[0]:
        raise AttributeError('priors has wrong dimensions: {}. Should have {}.'.format(priors.shape, py.shape))

    return np.sum(py*priors, dtype=np.float)


def f1(y, priors=None):
    """
    Calculate the theoretic baseline F1 score of a random classifier on y with different class priors.

    Score is averaged over classes.

    Parameters
    ----------
    y : array-like
        True labels
    priors : None, optional
        Class priors: ['uniform', 'stratified', 'most_frequent', array-like].
        If array-like, length must match number of unique labels in y (ordered labels as returned by LabelEncoder).

    Returns
    -------
    float
        F1 score averaged over classes

    Raises
    ------
    AttributeError
        If priors format is invalid.
    """
    py = _get_counts(np.asarray(y))

    if priors is not None and isinstance(priors, (np.ndarray, list, tuple)):
        priors = np.asarray(priors)
    elif priors is None or priors == 'uniform': # uniform prior
        priors = np.ones_like(py)
    elif priors == 'stratified':
        priors = py
    elif priors == 'most_frequent':
        priors = np.zeros_like(py)
        priors[np.argmax(py)] = 1
    else:
        raise ValueError('Invalid priors given: {}.'.format(priors))

    priors /= np.sum(priors, dtype=np.float)

    if priors.ndim > 1 or priors.shape[0] != py.shape[0]:
        raise AttributeError('priors has wrong dimensions: {}. Should have {}.'.format(priors.shape, py.shape))

    return np.mean(2.*py*priors / (py + priors))
