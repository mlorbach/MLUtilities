# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2015-04-20 11:43:33
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-06-13 12:34:54
# -*- coding: utf-8 -*-

import numpy as np
from scipy.misc import logsumexp
from scipy.stats import multivariate_normal
from scipy.special import entr, kl_div

from sklearn.covariance import ledoit_wolf, oas, empirical_covariance
from sklearn.preprocessing import LabelEncoder



def entropy(pk, qk=None, base=None, axis=0):
    """Calculate the entropy of a distribution for given probability values.

    If only probabilities `pk` are given, the entropy is calculated as
    ``S = -sum(pk * log(pk), axis=axis)``.

    If `qk` is not None, then compute the Kullback-Leibler divergence
    ``S = sum(pk * log(pk / qk), axis=axis)``.

    This routine will normalize `pk` and `qk` if they don't sum to 1.

    This routine is an exact copy of the original scipy.stats implementation but accepts an axis argument.

    Parameters
    ----------
    pk : sequence
        Defines the (discrete) distribution. ``pk[i]`` is the (possibly
        unnormalized) probability of event ``i``.
    qk : sequence, optional
        Sequence against which the relative entropy is computed. Should be in
        the same format as `pk`.
    base : float, optional
        The logarithmic base to use, defaults to ``e`` (natural logarithm).
    axis : int, optional, default=0
        The axis along which to compute the entropy.

    Returns
    -------
    S : float or sequence if pk, qk are 2D arrays.
        The calculated entropy.

    """
    pk = np.asarray(pk)
    pk = 1.0*pk / np.sum(pk, axis=axis, keepdims=True)
    if qk is None:
        vec = entr(pk)
    else:
        qk = np.asarray(qk)
        if qk.shape[axis] != pk.shape[axis]:
            raise ValueError("qk and pk must have same length.")
        qk = 1.0*qk / np.sum(qk, axis=axis, keepdims=True)
        vec = kl_div(pk, qk)
    S = np.sum(vec, axis=axis, keepdims=True)
    if base is not None:
        S /= np.log(base)
    return S


def logProbGMM(gmmodel, X):
    """
    Compute log likelihoods of X given a GMM.

    Parameters
    ----------
    gmmodel : sklearn GMM
        Model
    X : array-like of shape [n_samples, n_features]
        Data

    Returns
    -------
    logl : np.array of shape [n_samples]
        Log-likelihood of data
    """
    X = np.asarray(X)
    K = gmmodel.n_components
    w = np.log(gmmodel.weights_)
    T = X.shape[0]

    logPz = np.empty((T, K), dtype=np.float)

    for k in xrange(K):

        # get this component's covariance matrix
        if gmmodel.covariance_type == 'tied':
            # if 'tied', the same cov is used for all components,
            #  hence cov.shape = [n_features, n_features]
            cov = gmmodel.covariances_
        else:
            # in all other cases (diag, full, spherical), the first axis of gmmodel.covariances_ corresponds to the components. Hence extract the cov from that component (the results can be a scalar (speherical), a 1d-array (diag) or a 2d-array (full)).
            cov = np.take(gmmodel.covariances_, k, axis=0)

        logPz[:, k] = w[k] + multivariate_normal.logpdf(X, mean=gmmodel.means_[k, :], cov=cov)

    _, logl = normLogProb(logPz, axis=1)

    return logl


def normLogProb(logP, axis=None, keepdims=False):
    """
    Normalize log-likelihood values to pseudo log-probabilities.

    Parameters
    ----------
    logP : array-like
        Log-likelihoods
    axis : None, optional
        Along which axis to normalize
    keepdims : bool, optional
        If True, output array has same number of dimensions as input array.
        If False, output array is squeezed to remove singleton dimension.

    Returns
    -------
    logP, zlog : array-like
        Pseude log-probabilities and z-scores (log) that were used to normalize.
    """
    zlog = logsumexp(logP, axis=axis, keepdims=True)

    if keepdims:
        return logP - zlog, zlog
    else:
        return np.squeeze(logP - zlog), np.squeeze(zlog)



def estimate_mean_and_covariance(X, y, regularize=True):
    '''
    Estimates the mean and covariance matrix from observation data X for each class in y.

    The estimation can be made more robust against outliers in the sampled data by regularization.

    Parameters
    ----------

    X : array-like [n_samples, n_features]
        Features

    y : array-like [n_samples]
        Class labels

    regularize : bool, str
        Whether to regularize the covariance matrix. Default: True (Ledoit-Wolf).

        Set to True/False to enable/disable. Set to either 'ledoit' or 'oas' to choose between two
        methods of approximating the shrinking coefficients (ledoit: Ledoit-Wolf, oas: Oracle Approximating Shrinkage).

    Returns
    -------

    mu : np.array [n_features, n_classes]
        Mean per features and class

    sigma : np.array [n_features, n_features, n_classes]
        Covariance matrix per class

    coeff : np.array [n_classes]
        Shrinkage coeffcicients per class. Only returned if regularization is enabled.
    '''

    X = np.asarray(X)
    y = np.asarray(y)

    if isinstance(regularize, bool) and regularize:
        regularize = 'ledoit' # default regularization

    n_samples, n_features = X.shape
    le = LabelEncoder()
    unique_y = le.fit_transform(y)
    n_classes =  len(le.classes_)

    mu = np.zeros( (n_features, n_classes) )
    sigma = np.zeros( (n_features, n_features, n_classes) )
    coeff = np.zeros( (n_classes, ))

    for label in xrange(n_classes):
        mu[:, label] = np.mean( np.compress(unique_y == label, X, axis=0 ), axis=0 )

        if regularize == False:
            #sigma[:, :, label] = np.cov( np.compress(unique_y == label, X, axis=0 ), rowvar=0  )
            sigma[:, :, label] = empirical_covariance( np.compress(unique_y == label, X, axis=0 ), assume_centered=False  )
        elif regularize == 'oas':
            temp_sigma, coeff[label] = oas(np.compress(unique_y == label, X, axis=0 ), assume_centered=False)
            temp_mu = np.trace(sigma[:,:,label]) / n_features
            sigma[:, :, label] = (1 - coeff[label]) * temp_sigma + coeff[label] * temp_mu * np.identity(n_features)
        else:
            temp_sigma, coeff[label] = ledoit_wolf(np.compress(unique_y == label, X, axis=0 ), assume_centered=False)
            temp_mu = np.trace(sigma[:,:,label]) / n_features
            sigma[:, :, label] = (1 - coeff[label]) * temp_sigma + coeff[label] * temp_mu * np.identity(n_features)


        try:
            np.linalg.cholesky(sigma[:,:, label])
        except np.linalg.LinAlgError:
            print 'Covariance matrix for class {} is not positive definite. Training HMM won\'t be possible.'.format(le.inverse_transform(label))

    return mu, sigma
