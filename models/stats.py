# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2015-11-06 09:25:29
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-08 12:09:39


import numpy as np
import pandas as pd
import scipy.stats
import logging
from itertools import product


def get_gaussian_sample_weights(duration_segment, alpha=8.):
    '''
    Computes Gaussian weights for a segment of length duration_segment, such that the center frames
     have the highest weights.
    :param duration_segment: length of segment for which to compute weights
    :param alpha: Scaling parameter. A higher value leads to a denser Gaussian distribution.
    :return: Array of length duration_segment with Gaussian weights.
    '''
    duration_segment = int(duration_segment)
    gauss = scipy.stats.norm((duration_segment-1)/2., (duration_segment-1)/alpha)
    xs = np.linspace(0, duration_segment-1, duration_segment)
    return gauss.pdf(xs)


def pick_samples_per_class_uniform(data, N_per_class, action_column='action', replace=False):
    output_set = set()

    for a, g in data.groupby(action_column):
        output_set |= set(np.random.choice(g.index, N_per_class, replace=replace).tolist())

    return output_set


def pick_samples_from_segments(dfs, N_segments, N_per_segment, replace=True):

    logger = logging.getLogger(__name__)

    # final sample set to return to caller
    output_set = []#pd.MultiIndex(levels=[[],[],[]], labels=[[],[],[]], names=[u'action', u'segid', u'frameid'])

    # sample segments per action
    for action, data in dfs.groupby(level='action'):

        # get a copy from the sample set that we can modify
        #data_action = data.copy()

        # repeat this until we have sampled enough segments
        #  (Ideally we can sample as many segments as needed in one go, but sometimes we want to sample
        #   from more segments than we have left over. Then we need to loop a couple of times.)
        k_remaining_segments = N_segments
        while k_remaining_segments > 0:

            # get all available segment IDs
            #segids = data_action.index.get_level_values('segid').unique()
            segids = (data.groupby(level='segid')['inbag'].sum() > 0).index.get_level_values('segid').unique()

            if len(segids) == 0:
                logger.debug('There are no segments left for action {}.'.format(action))
                break

            # sample
            chosen_segments = np.random.choice(segids, min(k_remaining_segments, len(segids)), replace=False)
            #chosen_set = pd.MultiIndex(levels=[[],[],[]], labels=[[],[],[]], names=[u'action', u'segid', u'frameid'])
            chosen_set = []

            for segid in chosen_segments:

                framedata = data.loc[(action, segid), ['inbag', 'p']]
                framedata = framedata.loc[framedata.inbag == True, 'p']

                p = framedata
                p /= np.sum(p)
                #chosen_frames = np.random.choice(range(len(framedata)), N_per_segment, replace=False, p=p)
                chosen_frames = np.random.choice(framedata.index.values, N_per_segment, replace=False, p=p)

                k_remaining_segments -= 1

                #chosen_set += list(product([action], [segid], chosen_frames))
                # chosen_set.append(pd.Index(product([action], [segid], chosen_frames)))
                # chosen_set = chosen_set.append(pd.MultiIndex.from_product([[action],[segid],chosen_frames],
                #                                                           names=['action','segid','frameid']))

                chosen_set += chosen_frames

                data.loc[list(product([action], [segid], chosen_frames)), 'inbag' ] = False

            #output_set += chosen_set

            # remove the sampled frames from the set:
            # if not replace:
            #     data_action = data_action.drop(chosen_set, axis=0)


    # return only video id and frame number (as in dffa) from the sampled frames:
    #return_set = dfs.loc[output_set, ['video', 'frame']].set_index(['video','frame']).sortlevel(0)
    return_set = dfs.loc[:, ['video', 'frame']].set_index(['video','frame']).sortlevel(0)

    # remove the sampled frames from the set:
    # if not replace:
    #     dfs = dfs.drop(output_set, axis=0)

    return return_set, dfs


def pick_samples_per_segment(data, N_segments):
    '''
    Get samples from N_segments one-by-one: pick a sample from the first segment, then one from the second, and so on.

    Which samples are picked depends on their weights (stored in column 'p' in data).

    :param data: Available samples (in a particular format)
    :param N_segments: number of segments to sample
    :return: Same as input data with sampled frames marked in column 'inbag'
    '''

    logger = logging.getLogger(__name__)

    N_per_segment = 1

    # get IDs of all segments with at least one frame that has not been sampled yet
    segids = set((data.groupby(level='segid')['inbag'].sum() > 0).index.get_level_values('segid').unique())

    # repeat this until we have sampled enough segments
    #  (Ideally we can sample as many segments as needed in one go, but sometimes we want to sample
    #   from more segments than we have left over. Then we need to loop a couple of times.)
    k_remaining_segments = N_segments
    while k_remaining_segments > 0:

        if len(segids) == 0:
            logger.debug('There are no segments left for this action.')
            # print 'There are no segments left for this action.'
            break

        # sample segments
        chosen_segments = np.random.choice(list(segids), min(k_remaining_segments, len(segids)), replace=False)

        for segid in chosen_segments:

            framedata = data.loc[(slice(None), segid), ['inbag', 'p']]
            framedata = framedata.loc[framedata.inbag == True, 'p']

            # normalize sample weights
            p = framedata.values / np.sum(framedata, axis=0)
            chosen_frames = np.random.choice(framedata.index.values, N_per_segment, replace=False, p=p)

            k_remaining_segments -= 1

            # mark samples as being sampled
            data.loc[chosen_frames, 'inbag'] = False

            # remove segid from sample list if we have sampled all of them
            if (len(chosen_segments) < N_segments) and (data.loc[(slice(None), segid), 'inbag'].sum() == 0):
                segids.discard(segid)

    return data


def _rearrange_by_indirect_sort_index(s, a=None):

    if a is None:
        a = np.arange(len(s))

    b = np.zeros_like(a)
    b[s] = a

    return b


def shuffle_per_segment(dfs, segment_col='segid', frame_col='frameid', uniform=True, w_col=None):
    '''
    Shuffles the rows of a DataFrame while keeping another index level intact. We can apply this to randomize the order of frames within segments.
    :param dfs: DataFrame with at least two columns/index levels: [segmend_col, 'frameid'].
    :param segment_col: Name of columns/index to keep intact.
    :param frame_col: Name of column/index to shuffle.
    :param uniform: If true, shuffles rows uniformly. If false, arbitrary weight can be given in 'w_col'.
    :param w_col: Name of column that contains the numberic weight used for non-uniform sampling.
    :return: Shuffled DataFrame

    See also: http://programmers.stackexchange.com/a/299661
    '''

    original_index_cols = dfs.index.names
    dft = dfs.reset_index()

    if uniform:
        dft[frame_col] = dft.groupby(segment_col)[frame_col].transform(np.random.permutation)
    elif w_col is not None:
        # Rearrange frames such that frames with higher weight are more likely to be in the beginning.
        #  If we sample from an exponential distribution with a scaling factor that corresponds to the sample weight,
        #  then we can use those samples for rearranging the frames.
        #  Read here for an explanation:  http://programmers.stackexchange.com/a/299661

        # dft[frame_col] = dft.groupby(segment_col)[w_col].transform(lambda x: np.argsort(np.random.exponential(x))[::-1])
        dft[frame_col] = dft.groupby(segment_col)[w_col].transform(
            lambda x: _rearrange_by_indirect_sort_index(np.argsort(np.random.exponential(x))[::-1]) )
    else:
        raise AttributeError('If you choose non-uniform sampling, you need to provide the sample weight in "w_col".')

    return dft.set_index(original_index_cols, append=False, drop=True).sortlevel(0)


def _rearrange_center_to_outside(a, axis=0):

    a = np.asarray(a)

    la = a.shape[axis]
    b = np.zeros(la, dtype=np.int)

    # set the cut point c0 such that the first half is always larger or equal to the second:
    c0 = (la + la%2) / 2
    b[ np.arange(0, la, 2) ] = np.arange(c0)[::-1]
    b[ np.arange(1, la, 2) ] = np.arange(la/2) + c0

    return a.take(np.argsort(b), axis=axis)


def sort_center_to_outside_per_segment(dfs, segment_col='segid', frame_col='frameid'):

    original_index_cols = dfs.index.names
    dft = dfs.reset_index()

    dft[frame_col] = dft.groupby(segment_col)[frame_col].transform(_rearrange_center_to_outside)

    return dft.set_index(original_index_cols, append=False, drop=True).sortlevel(0)


def shuffle_segments_per_class(dfs, segment_col='segid', action_col='action'):

    original_index_cols = dfs.index.names
    dft = dfs.reset_index()

    dft[segment_col] = dft.groupby(action_col)[segment_col].transform(shuffle_segments, segment_col)

    return dft.set_index(original_index_cols, append=False, drop=True).sortlevel(0)


def shuffle_segments(dfs, segment_col='segid'):

    segids = dfs.unique()
    shuffled_segids = np.random.permutation(segids)
    dfs = dfs.replace(segids, shuffled_segids)
    return dfs


def segment_sampling(dfs, return_values=False, value_col='index'):
    '''

    :param dfs:
    :return:
    '''

    dft = dfs.reset_index()

    pointers = []

    while len(dft) > 0:
        new = dft.loc[dft.segid.diff() != 0, :]
        if return_values:
            pointers += new[value_col].values.tolist()
        else:
            pointers += new.index.values.tolist()
        dft = dft.drop(new.index, axis=0)

    return pointers
