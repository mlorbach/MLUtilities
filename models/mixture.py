# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-08-10 13:58:29
# @Last Modified by:   MAL
# @Last Modified time: 2016-08-10 15:11:02


import numpy as np
import logging

from helit.dpgmm.dpgmm import DPGMM as hDPGMM

from sklearn.utils.validation import NotFittedError
from sklearn.base import BaseEstimator, ClassifierMixin


class DPGMM(BaseEstimator, ClassifierMixin):
    """
    Wrapper around the DP-GMM implemention by helit to provide the sklearn API.

    https://github.com/thaines/helit/tree/master/dpgmm

    Attributes
    ----------
    arg : TYPE
        Description
    """
    def __init__(self, n_components=1, alpha=1.0,
                 random_state=None, tol=1e-3, verbose=0,
                 min_covar=None, n_iter=100, grow_fitting=False):
        super(DPGMM, self).__init__()

        self.n_components = n_components
        self.alpha = alpha
        self.tol = tol
        self.min_covar = min_covar
        self.random_state = random_state
        self.n_iter = n_iter
        self.verbose = verbose
        self.grow_fitting = grow_fitting
        self._converged = False

    def _is_fitted(self):
        return hasattr(self, '_model')

    def fit(self, X, y=None):

        logger = logging.getLogger()

        X = np.asarray(X)
        if X.ndim == 1:
            X = X[np.newaxis, :]

        self._model = hDPGMM(X.shape[1], self.n_components)
        self._model.setThreshold(self.tol)
        self._model.add(X)
        self._model.setPrior()
        self._model.setConcGamma(self.alpha, self.alpha)
        if self.grow_fitting:
            niter = self._model.solveGrow(self.n_iter)
            logger.debug('n_iter = {}'.format(niter))
        else:
            niter = self._model.solve(self.n_iter)
            logger.debug('n_iter = {}'.format(niter))

        self._converged = self.n_iter > niter

        return self

    def _prob_per_component(self, X):
        if not self._is_fitted():
            raise NotFittedError()

        p = self._model.stickProb(X)[:, :-1]
        return p / np.sum(p, keepdims=True)

    def score_samples(self, X):
        """Return the likelihood of the data under the model.

        Compute the bound on log probability of X under the model
        and return the posterior distribution (responsibilities) of
        each mixture component for each element of X.
        """

        if not self._is_fitted():
            raise NotFittedError()

        return self.score(X), self._prob_per_component(X)

    def score(self, X, y=None):
        """Compute the log probability under the model.
        """

        if not self._is_fitted():
            raise NotFittedError()

        return self._model.prob(X)

    def predict(self, X):
        """Predict label for data.

        Parameters
        ----------
        X : array-like, shape = [n_samples, n_features]

        Returns
        -------
        C : array, shape = (n_samples,) component memberships
        """

        if not self._is_fitted():
            raise NotFittedError()

        return np.argmax(self._prob_per_component(X), axis=1)

    def sample(self, n_samples=1, random_state=None):
        """Generate random samples from the model.

        Parameters
        ----------
        n_samples : int, optional
            Number of samples to generate. Defaults to 1.

        Returns
        -------
        X : array_like, shape (n_samples, n_features)
            List of samples
        """

        if not self._is_fitted():
            raise NotFittedError()

        return np.asarray(map(self._model.sampleMixture(), range(n_samples)))
