# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 13:23:58 2015

@author: MAL
"""

import numpy as np

from sklearn.cluster import MiniBatchKMeans
from sklearn.base import BaseEstimator, TransformerMixin

class SegmentConfusionModel(BaseEstimator, TransformerMixin):
    
    def __init__(self, descriptor, n_clusters=5, batch_size=25):
        
        self.descriptor_ = descriptor
        self.n_clusters_ = n_clusters
        self.batch_size_ = batch_size
                
        self.algo_ = MiniBatchKMeans(n_clusters=n_clusters, batch_size=batch_size, compute_labels=False)
        self.init_size_ = self.algo_.get_params()['init_size']
        if self.init_size_ is None:
            self.init_size_ = 3*self.batch_size_
        
        
    def fit(self, X, y=None):
        ''' X must be an iterable over a number of 2D features matrices '''
                
        counter = 0
        processed_all = False
        
        while processed_all == False:
        
            H = []
    
            for i in xrange(self.init_size_ if counter == 0 else self.batch_size_):
                
                try:
                    segment = X.next()
                except StopIteration:
                    processed_all = True
                    break
                
                H.append( self.descriptor_.transform(segment) )
                counter += 1
                
            if len(H) > 0:
                PMFs = np.reshape(np.dstack(H), (-1, len(H))).T
                self.algo_.partial_fit(PMFs)
        
        print 'Done fitting. Processed {} segments'.format(counter)
        
        return self
        
    
    
    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)
        
        
    def transform(self, X, y=None):
        H = map(self.descriptor_.transform, X)
        PMFs = np.reshape(np.dstack(H), (-1, len(H))).T
        return self.algo_.transform(PMFs, y)

        
    def predict(self, X):
        H = map(self.descriptor_.transform, X)
        PMFs = np.reshape(np.dstack(H), (-1, len(H))).T
        return self.algo_.predict(PMFs)
        
        
    def score(self, X, y=None):
        H = map(self.descriptor_.transform, X)
        PMFs = np.reshape(np.dstack(H), (-1, len(H))).T
        return self.algo_.score(PMFs, y)