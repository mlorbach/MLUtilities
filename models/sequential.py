# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2015-04-24 12:17:29
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-08 12:10:09
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from scipy.sparse import coo_matrix

from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix


def compute_transition_matrix(label_sequences, classes, lag=1, normed=True, include_start_end_state=False):
    '''
    Computes the transition matrix from sequences of labels/classes.

    Parameters
    ----------
    label_sequences : list of arrays/pd.Series
        The sequences of labels/classes

    classes : int or array-like
        The number of classes, or a list of all occurring classes.

    lag : int, default = 1
        The transition lag, i.e, the distance between the two events.

        A lag of 1 (default) corresponds to two events that follow each other immediately.

    normed : boolean, default: True
        Whether to normalize the transition matrix such that each row sums to 1.
        Otherwise, the absolute transition counts are returned.

    include_start_end_state : boolean, default: False
        Whether to add two additional states (start and end) to each sequence.
        If True, the resulting transition matrix is of size [N+2, N+2], where N is the number of classes.

    Returns
    -------
    T : np.array
    le : sklearn.LabelEncoder

    '''

    if not isinstance(lag, int) or lag < 1:
        raise ValueError('lag must be positive integer, but is {} of type {}.'.format(lag, type(lag)))

    if isinstance(classes, int):
            unique_labels = np.arange(classes)
    else:
        unique_labels = np.asarray(classes)

    unique_orig_labels = unique_labels.copy()
    if include_start_end_state:
        unique_orig_labels = np.concatenate((unique_orig_labels, ['seq_start', 'seq_end']))

    le = LabelEncoder()
    unique_labels = sorted(le.fit_transform(unique_orig_labels))

    if include_start_end_state:
        [seq_start_label, seq_end_label] = le.transform(['seq_start', 'seq_end'])

    n_labels = len(unique_labels)

    # allocate transition matrix
    T = np.zeros( (n_labels, n_labels), dtype=np.int )

    # If we are given a grouped pandas dataframe, then the iterator return tuples (group label, data).
    # Wrap the iterator in another generator which passes only the second value, i.e. the data.
    # This way, we can use the same for loop, irrespective of whether label_sequences is a list or a grouped data frame.
    if isinstance(label_sequences, pd.core.groupby.SeriesGroupBy):
        label_sequences = (g[1] for g in label_sequences)

    # Loop over all sequences / groups
    for seq in label_sequences:

        seq = le.transform(seq)
        T1 = confusion_matrix( seq[:-lag], seq[lag:], labels=unique_labels )

        T += T1
        if include_start_end_state:
            T[seq_start_label, seq[0]] += 1
            T[seq[-1], seq_end_label] += 1

    # normalize per row
    if normed:
        T = np.divide(T.astype(np.float), np.nansum(T, axis=1)[:, np.newaxis])
        T = np.nan_to_num(T)

    return T, le



def compute_lag_matrix(label_sequences, classes, lags=[0,1]):
    '''
    Computes the transition matrix from sequences of labels/classes.

    Parameters
    ----------
    label_sequences : list of arrays/pd.Series
        The sequences of labels/classes

    classes : int or array-like
        The number of classes, or a list of all occurring classes.

    lags : list or array-like, default = [0, 1]
        List of transition lags, i.e, the relative positions of the events. First element must be 0.

        The default [0,1] corresponds to two events that succeed each other immediately.

    Returns
    -------
    T : np.array
    le : sklearn.LabelEncoder

    '''

    if not isinstance(lags, (list, np.generic, np.ndarray)):
        raise ValueError('Lags must be given as positive integer or list of positive integer, but is of type {}.'.format(type(lags)))

    if any(np.asarray(lags) < 0):
        raise ValueError('All lags must be positive integer, but they are not.')

    if len(lags) < 2:
        raise ValueError('The list of lags must contain at least two elements (lag i -> lag j).')

    if isinstance(classes, int):
        unique_labels = np.arange(classes)
    else:
        unique_labels = np.asarray(classes)

    unique_orig_labels = unique_labels.copy()

    le = LabelEncoder()
    unique_labels = sorted(le.fit_transform(unique_orig_labels))

    n_labels = len(unique_labels)

    # lag propoerties
    n_lags = len(lags)
    max_lag = max(lags)
    coo_shape = [n_labels]*n_lags

    # allocate transition matrix: k^n_lags
    T = np.zeros( coo_shape, dtype=np.int )

    for seq in label_sequences:
        seq = le.transform(seq)
        lagged_seqs = [seq[ i:(len(seq)-(max_lag-i)) ] for i in lags]
        np.add.at(T, lagged_seqs, 1) # increment T at index positions

    return T, le
