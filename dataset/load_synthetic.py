# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-02-26 15:55:05
# @Last Modified by:   MAL
# @Last Modified time: 2016-02-29 11:04:19


import os
import pandas as pd
import json

_dataset_basepath = r"E:\Datasets\Synthetic"


def get_video_names(t_set, suffix=''):
    '''
    Load names of videos of the data set from metadata.
    :param t_set: Dataset name
    :param suffix: Optional suffix appended to dinfo-metadata filename
    :return: List of video names
    '''

    filepath = os.path.join(_dataset_basepath, 'dinfo{}.json'.format(suffix))

    dinfo = None
    with open(filepath, 'rb') as fp:
        dinfo = json.load(fp)

    if dinfo is None:
        return []

    if '_source' in t_set:
        return dinfo['TL_source_video_names']
    elif '_target' in t_set:
        return dinfo['TL_target_video_names']
    else:
        return dinfo['video_names']


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_feature_names(suffix='', subset='full', win_funcs='all'):
    return ['x0', 'x1']


def load_label_mapping():
    '''
    Loads class labels with IDs from metadata.
    :return: DataFrame containing class labels and their id as index.
    '''

    metadata = None
    with open(os.path.join(_dataset_basepath, 'metadata.json')) as fp:
        metadata = json.load(fp)

    if metadata is not None:
        mappingfile = os.path.join(_dataset_basepath, metadata['label_mapping_file'])
    else:
        mappingfile = os.path.join(_dataset_basepath, 'mapping_label_id.txt')

    labeldf = pd.read_csv(mappingfile, sep=';', index_col='id')
    return labeldf.dropna()


def get_label_names(subset=None):
    '''
    Get list of specific subset of labels.
    :param subset: not used
    :return: List of label names (strings)
    '''

    labels = sorted(load_label_mapping()['action'].values.tolist())
    return labels


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def _load_data_set(name, filter=None):

    input_file = os.path.join(_dataset_basepath, '{}.h5'.format(name))
    if not os.path.isfile(input_file):
        raise ValueError('Dataset {} not found.'.format(input_file))

    if filter is not None and not isinstance(filter, dict):
        raise TypeError('filter must be dict but is of type {}.'.format(type(filter)))

    if filter is not None:
        where_str = []
        for k,v in filter.viewitems():

            if isinstance(v, (tuple, list)):
                key_or = []
                for v_or in v:
                    key_or += ['{} == {}'.format(k, v_or)]
                where_str += ['(' + ' | '.join(key_or) + ')']
            else:
                where_str += ['{} == {}'.format(k, v)]

        where_str = ' & '.join(where_str)
    else:
        where_str = None

    return pd.read_hdf(input_file, 'data', where=where_str)


def load_data_set(video_ids, t_set, feature_names=None, action_col='label', omit_anynan=False):


    filter = {}
    if video_ids is not None:
        filter['dataset'] = video_ids

    if len(filter) == 0:
        filter = None

    return _load_data_set(t_set, filter=filter), video_ids
