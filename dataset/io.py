# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-06-10 10:53:26
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-16 10:07:31

import os
import json
import logging

import pandas as pd
import numpy as np

from ..features.ethovision import load_ethovision_csv


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def load_data_set(metadata, video_filter=None, subject=None, feature_file_suffix='', annotation_file_suffix=''):
    """
    Load complete dataset with features and annotations.

    Parameters
    ----------
    metadata : dict
        Dataset metadata dictionary
    video_filter : list of str, optional
        Names of video to load, otherwise load all
    subject : str, optional
        Load features of this subject
    feature_file_suffix : str, optional
        Suffix of feature file name
    annotation_file_suffix : str, optional
        Suffix of annotation file name

    Returns
    -------
    pd.DataFrame
        DataFrame with features and annotations in columns. Multiple videos are sorted in MultiIndex. Levels are called ['video', 'frame'/'time'] depending on whether it is a frame or time index.
    """
    logger = logging.getLogger(__name__)

    if isinstance(subject, (list, tuple, np.ndarray)):
        raise ValueError('Multiple subjects are not supported ({}). Please specify only one subject.'.format(subject))

    # load features
    dff = load_features(metadata, video_filter, subject, feature_file_suffix)
    feat_cols = dff.columns.tolist()

    # load annotations
    dfa = load_frame_annotations(metadata, video_filter, subject, annotation_file_suffix)
    annot_cols = dfa.columns.tolist()

    # merge dataframes
    dffa = pd.concat((dff, dfa), axis=1)

    # Intersect feature and annotation index (remove frames without features or anntotations):
    # ---------
    # find frames without any features:
    mask_no_features = dffa.loc[:, feat_cols].isnull().all(axis=1)

    # find frame without any annotations:
    mask_no_annotations = dffa.loc[:, annot_cols].isnull().all(axis=1)

    # drop all of those frames
    dffa = dffa.loc[~(mask_no_features | mask_no_annotations), :]
    # ---------

    return dffa

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_features(metadata, video_filter=None, subject=None, file_suffix=''):
    '''
    Loads feature DataFrame.

    Parameters
    ----------
    metadata : dict
        Metadata dictionary

    video_filter : list of str
        Full video name, e.g. '012609_A29_Block2_anaBCma1_t'

    subject : str, default = 'subject_1'
        Subject to load features for.

    file_suffix : str, optional
        Suffix to append to feature file name.

    Returns
    -------
    dffeatures : pd.DataFrame

    Raises
    ------
    IOError
        Raised if the feature file cannot be found.
    '''

    logger = logging.getLogger(__name__)

    input_path = os.path.join(metadata['basepath'], metadata['features_folder'])

    dffeatures = {}
    if video_filter is None:
        video_filter = metadata['video_names'] # load all videos
    elif not isinstance(video_filter, (list, tuple)):
        video_filter = [video_filter] # make sure it is a list

    if subject is None and metadata['has_multi_subject_annotations']:
        if 'subject_default' in metadata:
            subject = metadata['subject_default']
        else:
            subject = 'subject_1'

    for vidname in video_filter:

        event_file = os.path.join(input_path, metadata['features_file_pattern'].format(vidname, subject, file_suffix))
        if not os.path.isfile(event_file):
            logger.error('Feature file for video %s is missing: %s' % (vidname, event_file))
            raise IOError('Feature file for video %s is missing: %s' % (vidname, event_file))

        logger.debug('Loading features from {}.'.format(event_file))

        # load and sort columns by name
        dffeatures[vidname] = pd.read_hdf(event_file, 'features').sort_index(axis=1)

    dffeatures = pd.concat(dffeatures, names=['video', metadata['index_type']])

    # add 'Ft_' in front of each feature column name for easier identification
    if dffeatures.columns.nlevels > 1:
        prev_level_values = dffeatures.columns.levels[-1]
        new_level_values = ['Ft_' + f for f in prev_level_values]
        dffeatures.columns = dffeatures.columns.set_levels(new_level_values,
                                                         level=-1)
    else:
        dffeatures.columns = ['Ft_' + f for f in dffeatures.columns]

    return dffeatures

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_frame_annotations(metadata, video_filter=None, subject=None, file_suffix=''):
    '''
    Loads frame annotation DataFrame.

    Parameters
    ----------
    metadata : dict
        Metadata dictionary

    video_filter : list of str
        Full video names, e.g. '012609_A29_Block2_anaBCma1_t'

    subject : str, optional
        Subject name

    file_suffix : str, optional
        Suffix to append to annotation file name.

    Returns
    -------
    dfa : pd.DataFrame

    Raises
    ------
    IOError
        Missing annotation file
    '''

    logger = logging.getLogger(__name__)


    input_path = os.path.join(metadata['basepath'], metadata['frame_annotations_folder'])

    if video_filter is None:
        video_filter = metadata['video_names'] # load all videos
    elif not isinstance(video_filter, (list, tuple)):
        video_filter = [video_filter] # make sure it is a list

    if subject is None and metadata['has_multi_subject_annotations']:
        if 'subject_default' in metadata:
            subject = metadata['subject_default']
        else:
            subject = 'subject_1'

    dfa = {}
    for vidname in video_filter:

        event_file = os.path.join(input_path, metadata['frame_annotation_file_pattern'].format(vidname, subject, file_suffix))

        if not os.path.isfile(event_file):
            logger.error('Annotations for video %s are missing: %s' % (vidname, event_file))
            raise IOError('Annotations for video %s are missing: %s' % (vidname, event_file))

        logger.debug('Loading annotations from {}.'.format(event_file))

        # load
        if os.path.splitext(event_file)[-1] == '.h5':
            dfa[vidname] = pd.read_hdf(event_file, 'annotations')
        elif os.path.splitext(event_file)[-1] in ['.csv', '.txt']:
            dfa[vidname] = pd.read_csv(event_file, sep=';', index_col=[0], na_filter=False)
        else:
            raise ValueError('Unknown annotation file format: {}.'.format(os.path.splitext(event_file)[-1]))

    dfa = pd.concat(dfa, names=['video', metadata['index_type']])

    if 'Comment' in dfa.columns:
        dfa['Comment'] = dfa['Comment'].fillna('')

    # rename column "track" to "subject" if exists:
    if 'track' in dfa.columns:
        dfa = dfa.rename(columns={'track':'subject'})

    return dfa

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_bout_annotations(metadata, video_filter=None, subject=None, file_suffix=''):
    '''
    Load bout annotation DataFrame.

    Parameters
    ----------
    metadata : dict
        Metadata dictionary

    video_filter : list of str
        Full video names, e.g. '012609_A29_Block2_anaBCma1_t'

    subject : str, optional
        Subject name. If None, default subject is loaded.

    file_suffix : str, optional
        Suffix to append to annotation file name.

    Returns
    -------
    dfa : pd.DataFrame

    Raises
    ------
    IOError
        Missing annotation file
    '''

    logger = logging.getLogger(__name__)


    input_path = os.path.join(metadata['basepath'],  metadata['annotations_folder'])

    if video_filter is None:
        video_filter = metadata['video_names'] # load all videos
    elif not isinstance(video_filter, (list, tuple)):
        video_filter = [video_filter] # make sure it is a list

    if subject is None and metadata['has_multi_subject_annotations']:
        if 'subject_default' in metadata:
            subject = metadata['subject_default']
        else:
            subject = 'subject_1'

    dfa = {}
    for vidname in video_filter:

        event_file = os.path.join(input_path, metadata['annotation_file_pattern'].format(vidname, subject, file_suffix))

        if not os.path.isfile(event_file):
            logger.error('Annotations for video %s are missing: %s' % (vidname, event_file))
            raise IOError('Annotations for video %s are missing: %s' % (vidname, event_file))

        logger.debug('Loading annotations from {}.'.format(event_file))

        # load
        dfa[vidname] = pd.read_csv(event_file, sep=';', index_col=[0], na_filter=False)

    dfa = pd.concat(dfa, names=['video', metadata['index_type']])

    if 'Comment' in dfa.columns:
        dfa['Comment'] = dfa['Comment'].fillna('')

    return dfa

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_tracking_data(metadata, video_filter=None, subject=None, file_suffix='', return_frame_index=False):

    logger = logging.getLogger(__name__)

    input_path = os.path.join(metadata['basepath'],  metadata['raw_tracks_folder'])

    if video_filter is None:
        video_filter = metadata['video_names'] # load all videos
    elif not isinstance(video_filter, (list, tuple)):
        video_filter = [video_filter] # make sure it is a list

    if subject is None:
        if 'subjects' in metadata:
            subject = metadata['subjects']
        else:
            raise ValueError('Subject names missing in metadata and no subjects specified as argument.')
    else:
        if isinstance(subject, str):
            subject = [subject]

    dfa = {}
    for vidname in video_filter:

        dfs = {}
        for subj in subject:

            tracks_file = os.path.join(input_path, metadata['raw_tracks_file_pattern'].format(vidname, subj, file_suffix))

            if not os.path.isfile(tracks_file):
                logger.error('Raw tracks for video {}, subject {} are missing: {}'.format(vidname, subj, tracks_file))
                raise IOError('Raw tracks for video {}, subject {} are missing: {}'.format(vidname, subj, tracks_file))

            logger.debug('Loading raw tracks from {}.'.format(tracks_file))

            dfs[subj] = load_ethovision_csv(tracks_file,
                                            return_frame_index=return_frame_index)

        dfa[vidname] = pd.concat(dfs, names=['subject'], axis=1)


    dfa = pd.concat(dfa, names=['video', 'frame' if return_frame_index else 'time'])

    return dfa

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
