# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-03-09 10:36:24
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-09 16:52:01


import pandas as pd
import numpy as np


def compute_class_distribution(df, label_names=None, action_col='action'):

    if label_names is None:
        label_names = sorted(df[action_col].unique().tolist())

    counts = df[action_col].value_counts(sort=False)
    counts = counts.reindex(label_names).fillna(0).astype(int)
    counts.name = 'count'
    counts.index.name = action_col

    return counts


def filter_on_labels(df, labels, action_col='action_str'):
    '''
    Keeps only the rows whose label is one of multiple positive values.
    :param df: Input DataFrame
    :param labels: Positive labels to keep
    :param action_col: Name of the column to look for the labels
    :return: DataFrame
    '''

    if not isinstance(labels, (list, tuple, np.array)):
        labels = [labels]

    return df.loc[ df[action_col].isin(labels), : ]
