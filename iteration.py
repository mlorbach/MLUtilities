# -*- coding: utf-8 -*-
"""
Created on Thu Jul 24 13:15:36 2014

@author: MAL
"""

from itertools import islice


def window_iterator(seq, n=2):
    '''
    Returns a sliding window over data from the iterable seq -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...

    Parameters
    ----------
    seq : iterable
        The sequence to iterate over.
    n : int, default = 2
        The window width

    Returns
    -------
    '''
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result
