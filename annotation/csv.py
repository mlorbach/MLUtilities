# -*- coding: utf-8 -*-
"""
Created on Wed Oct 29 16:05:38 2014

@author: MAL
"""

import numpy as np
import pandas as pd
import logging
import re

# time parsing pattern for format:   (hh:)mm:ss:f+
#  hours are optional, and number of f is unlimited
TIMEPAT = re.compile('((?P<h>\d\d):)?(?P<m>\d\d):(?P<sf>\d\d.\d+)')


def load_label_mapping(mappingfile):
    
    labeldf = pd.read_csv(mappingfile, sep=';', header=0)
    labeldf = labeldf.dropna()
    
    labeldf['action'] = labeldf['action'].apply(lambda l: l.strip())
    labeldf['action_suzanne'] = labeldf['action_suzanne'].apply(lambda l: l.strip())
    
    # hacky way of converting the typical float index to and integer index:
    #if labeldf.index.dtype != int:
    #    labeldf.index = map(lambda x: int(x), labeldf.index)
        
    return labeldf
    
    
def convert_time_to_secf(string):
    '''
    Parses time string into seconds + fraction.
    
    Pattern for format:   (hh:)mm:ss:f+
    Hours are optional and the number of digits for f is unconstrained (at least 1 digit).
    
    Returns
    -------
    float
    '''
    
    patmatch = TIMEPAT.search(string)
    if patmatch.group('h') is not None:
        return float(patmatch.group('h'))*3600 + float(patmatch.group('m'))*60. + float(patmatch.group('sf'))
    else:
        return float(patmatch.group('m'))*60. + float(patmatch.group('sf'))
    
    
    
def load_csv_event_log(csvfile, delimiter=',', convert_time_to_sf=False, create_sequence_id=False, drop_comments_and_unnamed=True):
    
    logger = logging.getLogger(__name__)
    
    try:
        if not convert_time_to_sf:
            df = pd.read_csv(csvfile, sep=delimiter, index_col=[0])
        else:
            df = pd.read_csv(csvfile, sep=delimiter, index_col=[0], parse_dates=['time_start', 'time_stop', 'start off', 'end off', 'window start', 'window end'], date_parser=convert_time_to_secf)
    except ValueError as err:
        logger.error(err.message)
        raise ValueError('Observer exports in weird encoding, please convert the file to UTF8.')
    
    # remove irrelevant columns:
    if drop_comments_and_unnamed:
        drop_columns = ['Comment', 'Comment_stop']
        drop_columns = [c for c in drop_columns if c in df.columns]
        drop_columns += [name for name in df.columns if name.startswith('Unnamed: ')]
        
        df = df.drop(drop_columns, axis=1)
    
    if create_sequence_id:
        df['seqid'] = xrange(len(df))
        
    # rename the columns to fit my naming scheme:
    df = df.rename( columns={'Behavior':'action', 'Observation':'scorer'} )
    
    return df
    
    
        
    
def load_annotations_from_csv_event_log(csvfile, fps=25):
    
    df = load_csv_event_log(csvfile)
    
    return df