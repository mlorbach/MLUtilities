# -*- coding: utf-8 -*-
"""
Created on Thu Jul 31 15:35:59 2014

@author: MAL
"""

import numpy as np
import pandas as pd
import logging
import os
import re
import datetime

from MLUtilities.utils import char_range_num


#########################################################################################
#########################################################################################


OBSERVER_REGEX = re.compile('^(?P<project>[\w-]*) - Observation (?P<obsid>[0-9]+).* - Event Logs')

def parse_observer_filename(filename):

    m = OBSERVER_REGEX.match(filename)

    if m is not None:
        return (m.group('project'), int(m.group('obsid')))
    else:
        raise ValueError('Invalid Observer filename, could not match {}.'.format(filename))


#########################################################################################


def load_labels_lut(labelfile):
    labeldf = pd.read_csv(labelfile, sep='=', nrows=10, skiprows=3, index_col=0, header=None, names=['index', 'action'])
    labeldf = labeldf.dropna()

    labeldf['action'] = labeldf['action'].apply(lambda l: l.strip())

    # hacky way of converting the typical float index to and integer index:
    if labeldf.index.dtype != int:
        labeldf.index = map(lambda x: int(x), labeldf.index)

    return labeldf


#########################################################################################


def load_annotation_dataframe_from_event_log(eventlogfile, labelfile=None, tracks=['subject_1_0', 'subject_1_2'], scorerid=None, fps=25, keep_times=False):
    '''
    Load annotations from an Observer-exported event log into a Pandas DataFrame object.

    Parameters
    ----------

    eventlogfile : str
        Event log file to read

    labelfile : str, optional
        file that contains the mapping between behavior ids and names (MBADA format required)

    tracks : list of str, optional
        Not used currently. Specifies names to use for subject tracks.

    scorerid: int, optional, default: None
        If set, the scorer field is set to this id instead of what is given by the event log.

    fps : float, default = 25
        Frames per seconds used to resample the data in between the start events.
        Max resolution is 1ns, i.e. @30fps the resolution is 33333ns (== 0.033333s)


    Returns
    -------

    df : DataFrame
        DataFrame containing the annotations per frame, possibly with multiple tracks

    '''

    # TODO: give more options to put the labels in, e.g., as labels_dict

    df = load_event_log(eventlogfile, tracks, keep_times=keep_times)

    # get only the start points of the annotations (we assume that there are no unscored times)
    dfstartpoints = df[ df['Event_Type'] == 'State start' ]

    # get the stop points of the last annotations per track and add them so that the last annotations are complete
    existing_tracks = df['track'].unique()
    for track in existing_tracks:
        laststartframe = df[ (df['Event_Type'] == 'State start') & (df['track'] == track)].iloc[-1:]
        laststopframe = df[ (df['Event_Type'] == 'State stop') & (df['track'] == track)].iloc[-1:]

        # only add the last stop event if it is later than the start event (might happen if the last event is only 1 frame long)
        if (laststopframe.index) > (laststartframe.index):
            dfstartpoints = dfstartpoints.append( laststopframe )

    # replace the text action labels by their numeric id
    if labelfile is not None:
        dflabel = load_labels_lut(labelfile)

        dflabel['id'] = dflabel.index
        dflabel.index = dflabel['action']
        dflabel = dflabel.drop('action', axis=1)

        dfstartpoints = dfstartpoints.replace({'action': dflabel['id'].to_dict()})


    # string representation of the resampling frequency used by Pandas
    # e.g., for 30fps: 33333U which represents 33333ns (1 nanosecond is the lowest possible resolution)
    computed_freq = '{0:d}U'.format( int(1000000.0/float(fps)) )

    fullindex = pd.DatetimeIndex( start=dfstartpoints.index.min(), end=dfstartpoints.index.max(), freq=computed_freq )
    multiindex = pd.MultiIndex.from_arrays([ pd.Index(xrange(len(fullindex))), fullindex], names=['frame', 'sequence_time'])

    dftrackgrps = dfstartpoints.groupby('track')

    dfs = []
    for track, dfannot in dftrackgrps:
        dfannot = dfannot.reindex(fullindex, method='ffill')
        dfannot.index = multiindex
        if scorerid is not None:
            dfannot['scorer'] = scorerid
        dfs.append( dfannot.drop(['Event_Type', 'Time_Relative_hmsf', 'Duration_sf'], axis=1) )

    return pd.concat( dfs ).sortlevel()


#########################################################################################


def load_event_log(eventlogfile, tracks=None, remove_unnamed_columns=True, keep_times=False):

    # TODO: use custom tracks field for replacing the items in the subject column

    logger = logging.getLogger(__name__)

    try:
        df = pd.read_csv(eventlogfile, sep=';', index_col=[0], infer_datetime_format=True, parse_dates=True)
    except ValueError as err:
        logger.error(err.message)
        raise ValueError('Observer exports in weird encoding, please convert the file to UTF8.')

    df.index.name = 'sequence_time'

    # remove irrelevant columns:
    drop_columns = ['Event_Log']

    if not keep_times:
        drop_columns += ['Date_dmy', 'Time_Absolute_hms', 'Time_Absolute_f','Time_Relative_hms','Time_Relative_f','Time_Relative_sf']
    drop_columns = [c for c in drop_columns if c in df.columns]

    if remove_unnamed_columns:
        unnamed_columns = [name for name in df.columns if name.startswith('Unnamed: ')]
        drop_columns += unnamed_columns

    df = df.drop(drop_columns, axis=1)

    # MBADA workaround:
    df = df.replace(to_replace='ref_2_tar_1', value='subject_1_0')
    df = df.replace(to_replace='ref_2_tar_3', value='subject_1_2')

    if 'Observation' in df.columns:
        df['Observation'] = df['Observation'][0].split('_')[-1]

    # rename the columns to fit my naming scheme:
    df = df.rename( columns={'Behavior':'action', 'Observation':'scorer'} )

    # only rename Subject column if it exists (if there is only one subject scored, then this column is not exported by Observer)
    if 'Subject' in df.columns:
        df = df.rename( columns={'Subject':'track'} )
    else:
        df['track'] = str(tracks)

    return df


#########################################################################################


def save_annotation_as_event_log(path, annotation, labels=None, timeoffset=None, file_suffix='', overwrite_existing=False):
    '''
    Store the annotation dataframe as an event log that can be imported by The Observer.

    Create multiple event log files if there are multiple scorers in the dataframe.

    Parameters
    ----------
    path : str
        Path to write the event log files to.

    dataframe : pandas.DataFrame
        Annotation dataframe with DateTimeIndex and at least the columns 'scorer', 'track', and 'action'.

    labels : dict, optional
        Dictionary mapping action id to a string representation (label). If set, the event log will contain the labels instead of number in the behavior column.

    timeoffset : float or datetime.timedelta, optional
        Time offset to add to all events. Can be used to align the annotations with another data stream (video, features, ...).
        You can specify the offset in seconds (float) or as custom timedelta of any unit.
        Example:
            datetime.timedelta(seconds=0.5)

    overwrite_existing : bool, default=False
        Set to True to overwrite possibly existing event log files.

    Returns
    -------
    List of written files.

    '''

    logger = logging.getLogger(__name__)

    # check if necessary columns exist
    if not all([True if c in annotation.columns else False for c in ['scorer', 'track', 'action']]):
        raise ValueError('Annotation DataFrame has to have the columns scorer, track and action, but has {}.'.format(annotation.columns))

    # check output path
    if not os.path.isdir(os.path.realpath(path)):
        raise ValueError('Specified path does not exist. Please make sure to provide an existing path: {}'.format(os.path.realpath(path)))

    # get rid of unnecessary index columns in multiindex:
    if isinstance(annotation.index, pd.MultiIndex):
        dropindex = []
        for iidx, idx in enumerate(annotation.index.levels):
            if not idx.is_all_dates:
                dropindex.append(iidx)

        #for iidx in dropindex:
        annotation.index = annotation.index.droplevel(dropindex)

    dfannot_score_grps = annotation.groupby('scorer')
    outfiles = []

    for scorer, scoregroup in dfannot_score_grps:

        logger.info('Processing scorer {}'.format(scorer))

        outfile = os.path.join(path, 'event_log_{}{}.csv'.format(scorer, file_suffix))
        if not overwrite_existing and os.path.isfile(outfile):
            logger.debug('File exists. Skip {}.'.format(scorer))
            continue

        df_annot_track_grps = scoregroup.groupby('track')
        lstout = []

        for track, trackgroup in df_annot_track_grps:

            logger.info('Processing {} {} ...'.format(scorer, track))

            if len(trackgroup) == 0:
                logger.debug('Skipping {} {} because it is empty.'.format(scorer, track))
                continue

            start_indices = trackgroup[ trackgroup['action'].diff() != 0 ].index
            stop_indices = start_indices[1:]
            stop_indices = stop_indices.insert(len(stop_indices), trackgroup.index[-1])

            if timeoffset is not None:
                if isinstance(timeoffset, datetime.timedelta):
                    start_indices_index = start_indices + timeoffset
                    stop_indices_index = stop_indices + timeoffset
                else:
                    start_indices_index.shift(timeoffset, freq=pd.datetools.Second())
                    stop_indices_index.shift(timeoffset, freq=pd.datetools.Second())
            else:
                start_indices_index = start_indices
                stop_indices_index = stop_indices


            subject_str = track
            #ref, tar = track.split('_')[1:3]
            #subject_str = 'ref_{}_tar_{}'.format(int(ref)+1, int(tar)+1)


            print labels
            if labels is not None:

                if isinstance(labels, list):
                    labels = dict(zip(range(len(labels)), labels))


                if isinstance(labels, dict):
                    lstout.append( pd.DataFrame(index=start_indices_index, data={'Behavior':map(lambda a: labels[a], trackgroup.loc[start_indices,'action']),'Subject':subject_str}) )
            else:
                lstout.append( pd.DataFrame(index=start_indices_index, data={'Behavior': trackgroup.loc[start_indices,'action'],'Subject':subject_str}) )


            dfout = pd.concat( lstout, join='inner' )
            dfout.index.name = 'Start'

        dfout.to_csv(outfile, sep=';', encoding='utf8', date_format='%H:%M:%S.%f')
        outfiles.append(outfile)

    return outfiles


#########################################################################################


def save_bout_annotation_for_observer(filename, dfa, convert_index=False, action_column='action'):
    '''
    Save a DataFrame with bout/segment annotations to an Observer-readable csv-file.

    Parameters
    ----------


    Returns
    -------


    '''

    logger = logging.getLogger(__name__)


    if not isinstance(dfa, pd.DataFrame):
        raise ValueError('Annotations dfa must be of type pd.DataFrame but is {}.'.format(type(dfa)))

    # check if necessary columns exist
    if action_column not in dfa.columns:
        raise ValueError('Annotation DataFrame has to have an "{}" column but has columns: {}.'.format(action_column, dfa.columns))

    # check output path
    if not os.path.isdir(os.path.dirname(filename)):
        raise ValueError('Specified path does not exist. Please make sure to provide an existing path: {}'.format(os.path.dirname(filename)))

    dfcopy = dfa.copy()

    # get rid of unnecessary index columns in multiindex:
    if isinstance(dfcopy.index, pd.MultiIndex):
        dropindex = []
        for iidx, idx in enumerate(dfcopy.index.levels):
            if not idx.is_all_dates:
                dropindex.append(iidx)

        #for iidx in dropindex:
        print 'Dropping index levels: {}'.format(dropindex)
        dfcopy.index = dfcopy.index.droplevel(dropindex)

    # create track/Subject column if it doesn't exist yet
    if 'track' not in dfcopy.columns:
        dfcopy['track'] = 'subject_1'
        logger.info('Created missing Subject column with default value: subject_1.')


    # append final stop event
    if 'frame_stop' in dfcopy.columns:
        #
        #print dfcopy.iloc[-1]['frame_stop']
        #print {action_column:'Stop', 'track':dfcopy.iloc[-1]['track']}
        dfcopy = dfcopy.append( pd.DataFrame( data={action_column:'Stop', 'track':dfcopy.iloc[-1]['track']}, index=[dfcopy.iloc[-1]['frame_stop']] ) )


    if convert_index and isinstance(convert_index, (int, float)):
        dfcopy.index = np.round(dfcopy.index.values/float(convert_index), decimals=3)

    dfcopy = dfcopy.rename(columns={action_column:'Behavior', 'track':'Subject'})
    dfcopy.index.name = 'Start'

    dfcopy.loc[0, ['Behavior', 'Subject']] = ['Start Observation', dfcopy.head(1)['Subject'].values[0]]

    dfcopy = dfcopy.sortlevel(0)

    include_in_export = ['Behavior', 'Subject']
    include_in_export += [c for c in ['Modifier'] if c in dfcopy.columns]

    dfcopy.loc[:, include_in_export].to_csv(filename, sep=';', encoding='utf8', date_format='%H:%M:%S.%.f')


#########################################################################################


def save_prediction_probabilities_for_observer(filename, pred_proba, index, target_names=None, overwrite_existing=False):

    logger = logging.getLogger(__name__)

    if not overwrite_existing and os.path.isfile(filename):
        return []

    # create fake column names straight from the alphabet (a, b, c, ...) if no target names are given
    if target_names is None:
        target_names = [c for c in char_range_num(pred_proba.shape[1])]

    df = pd.DataFrame(data=pred_proba, index=index, columns=target_names)
    df.index.name = 'Trial time'

    with open(filename, 'wb') as fid:

        for target in target_names:
            fid.write( '{};Probability\n'.format(target) )
        df.to_csv(fid, sep=';')

    return filename


#########################################################################################


def save_feature_dataframe_for_observer(path, features, timeoffset=None, tracks=[(1,0),(1,2)], fps=30., feature_list_ref=None, feature_list_tar=None, overwrite_existing=False):
    '''
    Save feature dataframe to csv-file that can be imported as 'External Data' in The Observer.

    Create separate files for individual and pairwise features.

    Parameters
    ----------
    path : str
        Directory to write files to.

    features : pandas.DataFrame
        Features to save.

    timeoffset : float or datetime.timedelta, optional
        Offset to write as Start Time into headers of files.

    tracks : List of 2-tuples, default=[(1,0),(1,2)]
        Subject pairs to be written, specified as tuples of integers (subject ids). The first is the reference subject, the second is the target subject.

    fps : int, float
        Frames per seconds to use for sampling features/writing output

    feature_list_ref : list of str
        Names of individual features to write out

    feature_list_tar : list of str
        Names of pairwise features to write out

    overwrite_existing : bool, default=False
        Set to True to overwrite possibly existing files without warning.

    Returns
    -------

    List of written files.
    '''

    logger = logging.getLogger(__name__)

    # this is the fixed list of features to output
    if feature_list_ref is None:
        feature_list_ref = ['Moment1', 'Area', 'dist_m1_diff', 'dist_p1_diff', 'dist_m5_diff', 'dist_p5_diff', 'dist_m15_diff', 'dist_p15_diff', 'dist2closest']
    if feature_list_tar is None:
        feature_list_tar = ['genitals2genitals', 'head2body', 'head2genitals', 'head2head', 'body2body', 'body2body_m5_diff']

    # start time offset in seconds
    if timeoffset is not None:
        if isinstance(timeoffset, datetime.timedelta):
            secs_offset = timeoffset.total_seconds()
        else:
            secs_offset = float(timeoffset)
    else:
        secs_offset = 0

    lstwritten = []

    # output separate feature files for each reference subject
    refs = np.unique(zip(*tracks)[0])
    for ref in refs:

        refstr = 'subject_{}'.format(ref)
        refstrout = 'subject_{}'.format(ref+1)
        ref_cols = (refstr, refstr, feature_list_ref)
        dfref = features.loc[:, ref_cols]
        dfref.columns = dfref.columns.droplevel([0,1])
        dfref.index.name = 'frame'

        refoutfile = os.path.join(path, 'features_{}_observer.csv'.format(refstrout))

        if not overwrite_existing and os.path.isfile(refoutfile):
            logger.debug('File exists. Skip {}.'.format(refoutfile))
            continue

        with open(refoutfile, 'w') as outfile:
            outfile.writelines([
                'Dataset:\t{}\n'.format(refstrout),
                'Start time:\t{0:.3f}\n'.format(secs_offset),
                'FPS:\t{}\n'.format(fps)])
            dfref.to_csv(outfile, sep=';', encoding='utf8')

        lstwritten.append(refoutfile)


    # output separate feature files for each reference-target pair
    for ref,tar in tracks:

        refstr = 'subject_{}'.format(ref)
        tarstr = 'subject_{}'.format(tar)
        reftarstrout = 'subject_{}_{}'.format(ref+1,tar+1)

        tar_cols = (refstr, tarstr, feature_list_tar)
        dftar = features.loc[:, tar_cols]
        dftar.columns = dftar.columns.droplevel([0,1])
        dftar.index.name = 'frame'

        reftaroutfile = os.path.join(path, 'features_{}_observer.csv'.format(reftarstrout))

        if not overwrite_existing and os.path.isfile(reftaroutfile):
            logger.debug('File exists. Skip {}.'.format(reftaroutfile))
            continue

        with open(reftaroutfile, 'w') as outfile:
            outfile.writelines([
                'Dataset:\t{}\n'.format(reftarstrout),
                'Start time:\t{0:.3f}\n'.format(secs_offset),
                'FPS:\t{}\n'.format(fps)])
            dftar.to_csv(outfile, sep=';', encoding='utf8')

        lstwritten.append(reftaroutfile)

    return lstwritten


#########################################################################################
#########################################################################################
