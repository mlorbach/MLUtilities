# -*- coding: utf-8 -*-
"""
Created on Thu Dec 04 11:51:03 2014

@author: MAL
"""

import pandas as pd
import numpy as np
from itertools import groupby
import logging
import scipy.interpolate

from sklearn.preprocessing import LabelEncoder, LabelBinarizer
from sklearn.pipeline import Pipeline
from MLUtilities.sklearn_extensions.transformers import LabelMergeTransform


def get_combined_subject_labels(labels, wildcard='Unknown'):
    """
    Merges exclusive interaction labels of two subjects to one joint label.

    Condition: subject labels must be exlusive, that is for every sample there can only be one valid label among the subjects. Subjects without proper label must have the label specified as parameter 'wildcard' (multiple wildcard labels are possible, pass as list).

    Parameters
    ----------
    labels : array-like (n_samples, 2)
        subject labels
    wildcard : str, list
        Placeholder string for 'inactive' or 'unknown' label.

    Returns
    -------
    np.ndarray (n_samples,)
        Merged labels

    Raises
    ------
    AttributeError
        Description
    ValueError
        If conflicting labels are found.
    """

    labels = np.asarray(labels)

    if labels.ndim < 2 or labels.shape[1] != 2:
        raise AttributeError('input labels must be of shape (n_samples, 2) but is {}.'.format(labels.shape))

    if not isinstance(wildcard, (tuple, list)):
        wildcard = [wildcard]

    # copy labels of first subject
    out = labels[:, 0].copy()

    # take label of second subject if first is wildcard
    mask = np.in1d(out, wildcard)
    out[ mask ] = labels[ mask, :][:, 1]

    # check for conflicts: are there any frames with two different labels which are not a wildcard?
    if np.any( (~np.in1d(out, wildcard))
              & np.all(~np.apply_along_axis(np.in1d, 1, labels, wildcard ), axis=1 )
              & (labels[:, 0] != labels[:, 1]), axis=0):
        raise ValueError('{} conflicting labels found.'.format( (np.all(~np.apply_along_axis(np.in1d, 1, labels, wildcard ), axis=1 ) & (labels[:, 0] != labels[:, 1])).sum() ))

    return out


def intersect(df1, df2, level=None):

    if level is not None:
        # If level is given, we'll temporarily remove all other levels from the MultiIndex.
        # We'll stitch them back together after intersection.

        # remember the original index levels (note that they can be different for df1 and df2)
        original_index1 = list(df1.index.names)
        original_index2 = list(df2.index.names)

        # remove all but the one level in df1
        all_index_but1 = [l for l in original_index1 if l != level]
        df1 = df1.reset_index(level=all_index_but1)

        # remove all but the one level in df1
        all_index_but2 = [l for l in original_index2 if l != level]
        df2 = df2.reset_index(level=all_index_but2)

    elif df1.index.nlevels > 1 or df2.index.nlevels > 1:
        # if level is not given but we are dealing with a MultiIndex, then we can't do this.
        raise AttributeError('If Index is MultiIndex the level for intersecting must be specified but level=None.')

    newidx = df1.index.intersection(df2.index)
    df1 = df1.reindex(newidx)
    df2 = df2.reindex(newidx)

    if level is not None:
        df1 = df1.set_index(all_index_but1, append=True)
        df1.index = df1.index.reorder_levels(original_index1)

        df2 = df2.set_index(all_index_but2, append=True)
        df2.index = df2.index.reorder_levels(original_index2)

    return df1, df2


def binarize_mutual_exclusive(dfa, label_col='action', labels=None):
    """
    Turns a mutual exclusive annotation DataFrame (n_samples, 1) to a binarized indicator annotation DataFrame (n_samples, n_classes).

    Parameters
    ----------
    dfa : pd.DataFrame
        Mutual exclusive annotations

    Returns
    -------
    pd.DataFrame
        Binary indicator annotations with columns named as the labels
    """
    lb = LabelBinarizer(sparse_output=True)

    if isinstance(dfa, pd.Series):
        dfa = pd.DataFrame(dfa.values, dfa.index, columns=[label_col])

    unique_labels = set(dfa[label_col].unique().tolist())

    if labels is not None:
        unique_labels |= set(labels)

    lb.fit(list(unique_labels))

    dfe = pd.DataFrame(index=dfa.index, columns=lb.classes_, dtype=np.int)
    dfe.loc[:, :] = lb.transform(dfa[label_col]).toarray()  # toarray turns sparse output array to full array
    return dfe


def create_dataframe_from_label_sequence(label_sequence, index, subject_name=None, scorer_name=None, label_encoder=None):

    if label_encoder is not None:
        label_sequence = label_encoder.inverse_transform(label_sequence)

    dfa = pd.DataFrame( data=label_sequence, index=index, columns=['action'] )

    if subject_name is not None:
        dfa['track'] = subject_name
    else:
        dfa['track'] = 'subject'

    if scorer_name is not None:
        dfa['scorer'] = scorer_name
    else:
        dfa['scorer'] = 'scorer'

    return dfa


def get_annotation_within_intervals(index, intervals, interval_column_names=('window start', 'window end'), copy_column=None):

    logger = logging.getLogger(__name__)

    dflist = []
    seqids = range(len(intervals))
    start_col = interval_column_names[0]
    end_col = interval_column_names[1]

    if copy_column is not None:
        empty_frame = pd.DataFrame(index=index, columns=[copy_column])
    else:
        empty_frame = pd.DataFrame(index=index)

    if isinstance(intervals, pd.DataFrame):

        for bout in intervals.iterrows():
            #tempframe = empty_frame.loc[ bout[1][  start_col  ]:bout[1][  end_col  ] ]
            if copy_column is not None:
                empty_frame.loc[ bout[1][  start_col  ]:bout[1][  end_col  ], copy_column ] = bout[1][copy_column]

                #tempframe[copy_column] = bout[1][copy_column]
            dflist += [empty_frame.loc[ bout[1][  start_col  ]:bout[1][  end_col  ] ]]

        # use sequence ids if they are given in the dataframe
        if 'seqid' in intervals.columns:
            seqids = intervals['seqid'].tolist()

    elif isinstance(intervals, list):

        for bout in intervals:
            dflist += [empty_frame.loc[ bout[0]:bout[1] ]]

    else:
        logger.error('intervals must be either a pandas DataFrame object or a list of tuples.')
        raise AttributeError('intervals must be either a pandas DataFrame object or a list of tuples.')

    return pd.concat(dflist, keys=seqids, names=['seqid'])




def convert_mutual_exclusive_event_log_to_frames(df_labels, index, fill_label='oth'):
    '''
    Converts an event log-like DataFrame with mutual exclusive labels to a frame-based representation.

    Gaps are filled by fill_label (default: 'oth').

    '''

    logger = logging.getLogger(__name__)

    if not isinstance(df_labels, (pd.DataFrame, pd.Series)):
        raise AttributeError('df_labels must be a pandas DataFrame but is of type {}.'.format(type(df_labels)))

    if not isinstance(index, pd.Index):
        raise AttributeError('index must be a pandas Index but is of type {}.'.format(type(index)))


    df_labels = df_labels.reindex(index, method='ffill', fill_value=fill_label)


    return df_labels


def frame_to_segment_annotations(dfannot, include_stop_column=True, action_col='action', agg_dict=None):
    '''
    Converts frame based annotations to segment annotations with start and stop frame.

    Parameters
    ----------
    dfannot : pd.DataFrame
        Frame annotations.

    include_stop_column : bool, default: True
        Whether to include the segments last frame (column name: frame_stop).

    action_col : str or list, default: 'action'
        Name(s) of column(s) in dfannot that specify the label/action/class of each frame.

        If list, changes in any of the columns leads to the start of a new segment.

        Segments output columns will have the same name.

    agg_dict : dict, optional
        If specified, is used to determine how to aggregate other columns in dfannot across segments.

        Same structure as pd.DataFrame.agg(). I.e., dictionary where keys are the columns names to aggregate,
        and the corresponding value is a string that determines which function to use for aggregation. The function
        must be a valid function that can be applied to a pandas GroupBy group (e.g, 'mean', 'std', 'min', ...).

        Alternatively, the value can be a tuple in which case the first tuple element specifies the function as
        explained above. The second element is a string that is evaluated in the argument of the function.
        For example, the tuple ('mean', 'keepdims=True') is evaluated as data.mean(keepdims=True).

        The dictionary can contained mixed forms of the two variants explained above.

    Returns
    -------

    dfbouts : pd.DataFrame containing all segments.
        Additional index levels are kept (e.g., video id). Last index level contains start frame of segments.
        Optional columns, depending on parameters: frame_stop, aggregated columns.

    '''

    if not isinstance(action_col, (tuple, list)):
        action_col = [action_col] # make sure action_col is a list, even with only one entry

    if dfannot.index.nlevels > 1:
        video_ids = dfannot.index.get_level_values(0).unique()
    else:
        video_ids = [slice(None)]

    y_bouts = {}
    for vidid in video_ids:

        dfannotvid = dfannot.loc[vidid]

        # workaround for one-level dataframes: vidid is slice(None) which is okay for indexing,
        #  but we can't put this as key in a dict. So reassign vidid in that case to something else.
        if vidid == slice(None):
            vidid = 0

        # a new segment starts either when the label changes:
        try:
            start_indices_labels = dfannotvid.loc[ (dfannotvid.loc[:, action_col].diff() != 0).any(axis=1) ].index
        except TypeError:
            # it's possible that we can't directly .diff() the datatype in
            # the column ("minus" may not be defined). In that case, we'll
            # compare the two subsequent values on equality:
            start_indices_labels = dfannotvid.loc[
            (dfannotvid.loc[:, action_col] != dfannotvid.loc[:, action_col].shift(1)).any(axis = 1) ].index


        # or when the index increases more than usually (+1 frame):
        np_mask = np.zeros_like( dfannotvid.index.values, dtype=np.bool )
        np_mask[1:] = (np.diff(dfannotvid.index.values) > 1)
        start_indices_index = dfannotvid.loc[ np_mask ].index

        # join the segment starts
        start_indices = start_indices_labels.union( start_indices_index )

        stop_indices_labels = start_indices_labels[1:] - 1
        stop_indices_index = dfannotvid.loc[ np_mask[1:,] ].index

        stop_indices = stop_indices_labels.union( stop_indices_index )

        # add very last stop index:
        stop_indices = stop_indices.insert( len(stop_indices), dfannotvid.index[-1] )

        # kick out all start/stops that are not in the original index
        start_indices = start_indices.intersection( dfannotvid.index )
        stop_indices = stop_indices.intersection( dfannotvid.index )

        data = {}
        for col in action_col:
            data[col] = dfannotvid.loc[start_indices, col].copy()

        if include_stop_column:
            data['frame_stop'] = stop_indices

        y_bouts[vidid] = pd.DataFrame(index=start_indices, data=data)

        # aggregate all other columns according to specified function
        # aggregation is evaluated across segments (e.g., the mean value of a column across one segment)
        markers = None
        if agg_dict is not None and isinstance(agg_dict, dict):

            # get names of all columns other than the action column (potential aggregation columns)
            other_columns = [c for c in dfannot.columns if c not in action_col]

            # check if any of the specified columns actually exist, before we start making expensive computations
            if np.in1d(agg_dict.keys(), other_columns).any():

                # The first part is universal for all columns. Mark the segments by an increasing counter,
                #  so that we can group the DataFrame by that counter.

                # Set all markers to 0; all frame indices as incoming (possibly filtered)
                markers = pd.Series(data=np.zeros((dfannotvid.shape[0]), dtype=np.int), index=dfannotvid.index)

                # mark all starts of segments
                markers.loc[ start_indices ] = 1
                # mark all end of segments (but make sure they are in the Index as they might not if dfannot is already filtered)
                markers.loc[ (stop_indices + 1).intersection(dfannotvid.index) ] = 1

                # generate segment markers
                markers = markers.cumsum() # cumsum = increase counter at every segment start

                # aggregate column within each segment
                for k,v in agg_dict.viewitems():
                    if k in dfannot.columns:

                        if isinstance(v, tuple):
                            col_agg = eval('dfannotvid.groupby(markers)[ "{}" ].{}({})'.format(k, v[0], v[1]))
                        else:
                            col_agg = eval('dfannotvid.groupby(markers)[ "{}" ].{}()'.format(k,v))

                        # y_bouts index is start frame of segment
                        y_bouts[vidid][ k ] = col_agg.loc[ markers.loc[ y_bouts[vidid].index ].values ].values


    df_bouts = pd.concat(y_bouts, names=['video', 'frame'])

    # drop fake video level if there was only one level in the annotation dataframe:
    if dfannot.index.nlevels <= 1:
        df_bouts.index = df_bouts.index.droplevel('video')

    return df_bouts


def add_segment_ids(dfannot, action_col='action'):
    '''
    Adds an segid column to dfannot such that every consecutive segment with the same value in action_col, has a unique
    number. The numbers are unique across one video, if there are multiple videos. That is, the segment ids start at 0
    for every video.

    The internals are exactly the same as in frame_to_segment_annotations but without the actual grouping into segments.

    :param dfannot: pd.DataFrame with at least one column: action_col
    :param action_col: name of the column to group consecutive segments by
    :return: dfannot with additional column 'segid'
    '''

    if isinstance(action_col, str):
        action_col = [action_col] # make sure action_col is a list, even with only one entry

    if isinstance(dfannot.index, pd.MultiIndex) and dfannot.index.nlevels > 1:
        video_ids = dfannot.index.get_level_values(0).unique()
    else:
        video_ids = [slice(None)]

    # create output column with int type
    dfannot['segid'] = 0

    y_bouts = {}
    last_marker = 0
    for vidid in video_ids:

        dfannotvid = dfannot.loc[vidid]

        # a new segment starts either when the label changes:
        start_indices_labels = dfannotvid.loc[ (dfannotvid.loc[:, action_col].diff() != 0).any(axis=1) ].index

        # or when the index increases more than usually (+1 frame):
        np_mask = np.zeros_like(dfannotvid.index.values, dtype=np.bool)
        np_mask[1:] = (np.diff(dfannotvid.index.values) > 1)
        start_indices_index = dfannotvid.loc[ np_mask ].index

        # join the segment starts
        start_indices = start_indices_labels.union( start_indices_index )

        stop_indices_labels = start_indices_labels[1:] - 1
        stop_indices_index = dfannotvid.loc[ np_mask[1:,] ].index

        stop_indices = stop_indices_labels.union( stop_indices_index )

        # add very last stop index:
        stop_indices = stop_indices.insert( len(stop_indices), dfannotvid.index[-1] )

        # kick out all start/stops that are not in the original index
        start_indices = start_indices.intersection( dfannotvid.index )
        stop_indices = stop_indices.intersection( dfannotvid.index )

        # Set all markers to 0; all frame indices as incoming (possibly filtered)
        markers = pd.Series(data=np.zeros((dfannotvid.shape[0]), dtype=np.int), index=dfannotvid.index)

        # mark all starts of segments
        markers.loc[ start_indices ] = 1
        # mark all end of segments (but make sure they are in the Index as they might not if dfannot is already filtered)
        markers.loc[ (stop_indices + 1).intersection(dfannotvid.index) ] = 1

        # generate segment markers
        markers = markers.cumsum() + np.int(last_marker) # cumsum = increase counter at every segment start
        last_marker = markers.max()

        dfannot.loc[vidid, 'segid'] = markers.values

    return dfannot


def merge_consecutive_segments(dfannot, merge_func, key='action'):
    '''
    Merges DataFrame rows of consecutive key values using a custom merge function.

    Parameters
    ----------

    dfannot : pandas.DataFrame
        DataFrame to merge

    merge_func : function handle
        Custom merge function that is called for each group of consecutive key values.

        The function is called with one parameter `group` which is a tuple of two lists.
        The first list contains the indices of the group elements to merge. The second
        list contains the corresponding data rows.

        merge_func must return a tuple (`idx`, `data`) where `idx` is the index of the merged
        row and `data` is the merged data row.

    .. note:: Note that merge_func is also called for groups of single rows.

    key : string, default='action'
        The name of the column to look for groups of consecutive values.

    Returns
    -------

    dfannot_merged : pandas.DataFrame
        DataFrame with the rows merged

    .. seealso:: :func:`itertools.groupby`

    '''

    d_merged = {}

    # Use the itertools.groupby function to iterate over groups of consecutive values
    # The values are given by the column specified by `key`.
    for _, segment in groupby(dfannot.iterrows(), key=lambda x: x[1][key]):
        group = zip(*segment)

        # call the custom merge function, which returns the merged row's index and data
        idx, data = merge_func(group)

        # save the merged row as a dict entry
        d_merged[ idx ] = data

    # create a new DataFrame from all merged rows and sort by the index
    dfannot_merged = pd.DataFrame.from_dict(d_merged, orient='index')
    #dfannot_merged = pd.concat(d_merged, names=['video', 'frame'])
    dfannot_merged = dfannot_merged.sort_index()

    # rename the DataFrame's index as the original
    if dfannot.index.name is not None:
        dfannot_merged.index.name = dfannot.index.name

    return dfannot_merged



def get_time_since_another_bout(df_bouts, other_labels):

    #social_actions = ['approach', 'attack', 'chase', 'circle', 'copulation', 'sniff', 'walk_away']


    if isinstance(df_bouts, dict):
        iterator = df_bouts.viewkeys()

    elif isinstance(df_bouts, pd.DataFrame):
        if df_bouts.index.nlevels > 1:
            iterator = df_bouts.index.get_level_values(0).unique()
        else:
            iterator = [slice(None)]

    output = {}

    for vid in iterator:
        if isinstance(df_bouts, dict):
            dfa = df_bouts[vid]
        else:
            dfa = df_bouts.loc[vid]

        dfout = pd.DataFrame(index=dfa.index)
        dfout['frames_since'] = dfa['frame_stop'] - dfa.index.values + 1
        dfout.loc[dfa.action.isin(other_labels), 'frames_since'] = 0
        dfout['is_other'] = (dfout['frames_since'] == 0).cumsum()
        dfout['frames_since'] = dfout.groupby('is_other')['frames_since'].transform(np.cumsum).shift(1).fillna(0).astype(np.int)


        #first_social = dfa.loc[dfa.action.isin(social_actions)].head(1).index.values[0]
        #last_social = dfa.loc[dfa.action.isin(social_actions)].tail(1).index.values[0]
        #df_bouts[vid] = df_bouts[vid].loc[ first_social+1:last_social, : ]
        dfout = dfout.drop('is_other', axis=1)

        output[vid] = dfout

    #dfa = pd.concat( dfbouts_ordered, names=['video','frame']).drop(['track', 'Comment'], axis=1)
    if isinstance(df_bouts, pd.DataFrame):
        output = pd.concat(output, names=['video', 'frame'])

    return output



def get_time_until_another_bout(df_bouts, other_labels):

    #social_actions = ['approach', 'attack', 'chase', 'circle', 'copulation', 'sniff', 'walk_away']
    def reverse_cumsum(x):
        return np.cumsum(x[::-1])[::-1]

    if isinstance(df_bouts, dict):
        iterator = df_bouts.viewkeys()

    elif isinstance(df_bouts, pd.DataFrame):
        if df_bouts.index.nlevels > 1:
            iterator = df_bouts.index.get_level_values(0).unique()
        else:
            iterator = [slice(None)]

    output = {}

    for vid in iterator:
        if isinstance(df_bouts, dict):
            dfa = df_bouts[vid]
        else:
            dfa = df_bouts.loc[vid]

        dfout = pd.DataFrame(index=dfa.index)


        dfout['frames_until'] = dfa['frame_stop'] - dfa.index.values + 1
        dfout.loc[dfa.action.isin(other_labels), 'frames_until'] = 0
        dfout['frames_until'] = dfout['frames_until'].shift(-1)

        dfout['is_other'] = (dfout['frames_until'] == 0).cumsum().shift(1).fillna(0)
        dfout['frames_until'] = dfout.groupby('is_other')['frames_until'].transform(reverse_cumsum).fillna(0).astype(np.int)


        #first_social = dfa.loc[dfa.action.isin(social_actions)].head(1).index.values[0]
        #last_social = dfa.loc[dfa.action.isin(social_actions)].tail(1).index.values[0]
        #df_bouts[vid] = df_bouts[vid].loc[ first_social+1:last_social, : ]
        dfout = dfout.drop('is_other', axis=1)

        output[vid] = dfout

    #dfa = pd.concat( dfbouts_ordered, names=['video','frame']).drop(['track', 'Comment'], axis=1)
    if isinstance(df_bouts, pd.DataFrame):
        output = pd.concat(output, names=['video', 'frame'])

    return output


def get_fraction_surrounding_positives(df_bouts, df_frames, win, positive_labels=[1], negative_labels=[0], action_column='action'):

    enc_pos = 1
    enc_neg = 0

    lemerge_positive = LabelMergeTransform(positive_labels, enc_pos)#'positive')
    lemerge_negative = LabelMergeTransform(negative_labels, enc_neg)#'negative')
    letrans = LabelEncoder().fit([enc_pos,enc_neg])
    #letrans = LabelEncoder().fit(['positive', 'negative'])

    le_pipe = Pipeline([('merge_pos', lemerge_positive), ('merge_neg', lemerge_negative), ('encode', letrans)])


    if isinstance(df_bouts, dict):
        iterator = df_bouts.viewkeys()

    elif isinstance(df_bouts, pd.DataFrame):
        if df_bouts.index.nlevels > 1:
            iterator = df_bouts.index.get_level_values(0).unique()
        else:
            iterator = [slice(None)]

    output = {}

    for vid in iterator:
        if isinstance(df_bouts, dict):
            dfb = df_bouts[vid]
            dff = df_frames[vid]
        else:
            dfb = df_bouts.loc[vid]
            dff = df_frames.loc[vid]

        dfout = pd.DataFrame(index=dfb.index)


        next25_data = np.vstack( map( lambda i: np.bincount( le_pipe.transform(dff.loc[i+1:i+win, action_column].values), minlength=2 ), dfb['frame_stop'].values) )
        next25_data = next25_data.astype(np.float) / float(win)

        prev25_data = np.vstack( map( lambda i: np.bincount( le_pipe.transform(dff.loc[i-win:i-1, action_column].values), minlength=2 ), dfb.index.values) )
        prev25_data = prev25_data.astype(np.float) / float(win)

        dfout['next_positive'] = next25_data[:, letrans.transform(enc_pos)]
        dfout['next_negative'] = next25_data[:, letrans.transform(enc_neg)]
        dfout.loc[dfout.index[-1], 'next_negative'] = 1.

        dfout['prev_positive'] = prev25_data[:, letrans.transform(enc_pos)]
        dfout['prev_negative'] = prev25_data[:, letrans.transform(enc_neg)]
        dfout.loc[dfout.index[0], 'prev_negative'] = 1.

        output[vid] = dfout

    if isinstance(df_bouts, pd.DataFrame):
        output = pd.concat(output, names=df_bouts.index.names)

    return output
