# -*- coding: utf-8 -*-
"""
Summary
"""
# @Author: MAL
# @Date:   2016-06-08 12:15:01
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-11-18 11:44:25


import numpy as np
from scipy.stats import multivariate_normal

import logging

from sklearn.preprocessing import LabelEncoder
from sklearn.base import clone
from sklearn.utils.class_weight import compute_class_weight
from sklearn.utils.validation import NotFittedError
from sklearn.mixture import GaussianMixture as skGMM
from sklearn.mixture import BayesianGaussianMixture as skBGMM
# from sklearn.mixture.dpgmm import VBGMM as skVBGMM # bug in VBGMM prevents us from using it currently: https://github.com/scikit-learn/scikit-learn/issues/5812
from sklearn.base import BaseEstimator, ClassifierMixin

from sklearn.externals.joblib import Parallel, delayed

from ..models.utils import logProbGMM, normLogProb


def _fit_GMM(estimator, X, y=None):
    """
    Fit a single GMM to data X.

    Parameters
    ----------
    estimator : BaseEstimator (GMM, DPGMM, VBGMM)
        Estimator to fit
    X : array-like of shape [n_samples, n_features]
        Data
    y : not used
        As all labels are the same, y is not used and is only here for compatibility reasons.

    Returns
    -------
    estimator
        Fitted GMM
    """
    # estimator = clone(estimator)
    estimator.fit(X, y)
    return estimator


class GMM(BaseEstimator, ClassifierMixin):
    """
    Mixture of Gaussian mixture models for generative classification tasks.

    Attributes
    ----------
    baseModel : one of sklearn's GMM, DPGMM or VBGMM
        Underyling base model used to model each class
    class_weight : None, 'balanced'/'auto' (default), or custom list/array/dict
        Class priors used for prediction
    estimators_ : one of sklearn's GMM, DPGMM, VBGMM
        GMM components of mixture (each estimator_ is a GMM)
    le_ : sklearn.preprocessing.LabelEncoder
        Encoder used to encode training labels
    n_jobs : int, default=1
        Number of jobs scheduled for parallel training of components
    n_labels : int
        Number of labels
    w_ : np.ndarray of shape [n_labels]
        Class priors used internally
    """
    def __init__(self, baseModel, class_weight='balanced',
                 n_jobs=1, duplicate_single_samples=True):
        """
        Mixture of Gaussian mixture models for generative classification tasks.

        Parameters
        ----------
        baseModel : one of sklearn's GMM, DPGMM or VBGMM
            Underyling base model used to model each class
        class_weight : None, 'balanced'/'auto' (default), or custom list/array/dict
            Class priors used for prediction
        n_jobs : int, default=1
            Number of jobs scheduled for parallel training of components
        duplicate_single_samples : bool, optional
            If True and fit receives only a single sample of some class, that sample is duplicated with some noise in order to enable fitting the GMM.

        Raises
        ------
        TypeError
            Description
        """
        super(GMM, self).__init__()
        self.baseModel = baseModel
        self.class_weight = class_weight
        self.n_jobs = n_jobs
        self.duplicate_single_samples = duplicate_single_samples

        valid_baseModels = (skGMM, skBGMM) # (skGMM, skDPGMM, skVBGMM)

        if not isinstance(self.baseModel, valid_baseModels):
            raise TypeError('baseModel must be one of sklearn\'s [{}] but is {}.'.format(valid_baseModels, type(self.baseModel)))

        self._is_fitted = False


    def __iter__(self):
        if not self._is_fitted:
            raise AttributeError('Cannot iterate model before fitting.')
        yield self.estimators_.next()


    def __getitem__(self, key):
        if not self._is_fitted:
            raise AttributeError('Cannot iterate model before fitting.')
        return self.estimators_[key]


    def _ensure_enough_samples(self, X, y, method='noisy_dup'):
        """
        Potentially adds samples such that there are at least n_components samples of every class.

        Parameters
        ----------
        X : array-like (n_samples, n_features)
            Data
        y : array-like (n_samples)
            Class labels
        method : str, optional
            Which method to use for generating fictive samples.

        Returns
        -------
        (np.ndarray, np.ndarray)
            New X, y
        """

        counts = np.bincount(y, minlength=self.n_labels)
        min_n = max(2, self.baseModel.n_components)

        if np.all(counts >= min_n):
            return X, y

        process_labels = np.where((counts < min_n) & (counts > 0))[0]

        # some guess on a reasonable scale of the additive noise
        nu = np.var(X, axis=0) * 1e-3
        if np.any(nu == 0):
            nu += 1e-3 # prevent singular matrix
        noise = multivariate_normal(cov=np.diag(nu))

        Xn = []
        yn = []
        for label in process_labels:
            k = min_n - counts[label]

            if method == 'noisy_dup':

                x = np.random.choice(np.where(y == label)[0], k)
                x = X.take(x, axis=0) + noise.rvs(k)

                Xn.append(x)
                yn += [label] * k  # append label k times

            else:
                raise NotImplementedError('Other duplication methods are not implemented.')

        if len(Xn) > 0:
            Xn = np.concatenate(Xn, axis=0)
            yn = np.asarray(yn)

            return np.concatenate((X, Xn)), np.concatenate((y, yn))
        else:
            return X, y

    def partial_fit(self, X, y=None, params='wmc', **fit_params):

        if not self._is_fitted:
            raise NotFittedError()

        if y is not None:
            # supervised update of estimator parameters

            # fix init parameters
            prev_init_params = []
            prev_params = []
            for est in self.estimators_:
                prev_init_params.append(est.init_params)
                prev_params.append(est.params)
                est.init_params = ''
                est.params = params

            y = self.le_.transform(y)

            # sample duplication
            if self.duplicate_single_samples:
                X, y = self._ensure_enough_samples(X, y, method='noisy_dup')

            labels = np.unique(y)

            # do fitting
            # fit GMMs
            updated_estimators = Parallel(n_jobs=self.n_jobs)(delayed(_fit_GMM)(
                self.estimators_[l], X[y == l])
                for l in labels)

            # only change the updated estimators
            for i, l in enumerate(labels):
                self.estimators_[l] = updated_estimators[i]

            # restore original parameters
            prev_init_params.reverse()
            prev_params.reverse()
            for est in self.estimators_:
                est.init_params = prev_init_params.pop()
                est.params = prev_params.pop()

            if len(labels) == self.n_labels:
                # update class weights only if we have seen all labels
                self.w_ = compute_class_weight(self.class_weight, np.asarray(labels), y)

            return self

        else:
            # partial_fit, unsupervised

            if not self._is_fitted:
                raise NotFittedError()

            return self._update_unsupervised(X, params)


    def fit(self, X, y, **fit_params):
        """
        Fit the classifier. Once the classifier has been fitted once with labels, subsequent calls of fit() can be without labels. Then the classifier is updated in an unsupervised manner based on the predictions of the current model.

        Parameters
        ----------
        X : array-like of shape [n_samples, n_features]
            Data
        y : array-like of shape, optional if already fitted [n_samples]
            Labels

        Returns
        -------
        self

        Raises
        ------
        AttributeError
            Description
        """
        logger = logging.getLogger(__name__)

        X = np.asarray(X)
        y = np.asarray(y)

        if len(y) != X.shape[0]:
            raise AttributeError('Length of y must match number of samples {} but is {}.'.format(X.shape[0], len(y)))

        # labels
        self.le_ = LabelEncoder().fit(y)
        y = self.le_.transform(y)
        labels_str = self.le_.classes_
        self.n_labels = len(self.le_.classes_)
        labels = range(self.n_labels)

        # if self.n_labels == 1:
        #     raise AttributeError('There must be at least two classes to classify.')

        # sample duplication
        if self.duplicate_single_samples:
            X, y = self._ensure_enough_samples(X, y, method='noisy_dup')

        # some useful variables
        n_samples, self.n_features = X.shape

        # fit GMMs
        self.estimators_ = Parallel(n_jobs=self.n_jobs)(delayed(_fit_GMM)(
            clone(self.baseModel), X[y==i])
            for i in labels)

        # class priors
        if isinstance(self.class_weight, (list, tuple, np.ndarray)):
            if len(self.class_weight) == self.n_labels:
                self.class_weight = dict( zip(labels, self.class_weight) )
            else:
                raise AttributeError('Number of manually specified class weights does not match number of labels ({} != {}).'.format(len(self.class_weight), self.n_labels))

        self.w_ = compute_class_weight(self.class_weight, np.asarray(labels), y)

        self._is_fitted = True

        return self

    def _update_unsupervised(self, X, params='wmc'):
        """
        Unsupervised updating of estimator parameters based on labels predicted by current model.

        Parameters
        ----------
        X : array-like (n_samples, n_features)
            Data

        Returns
        -------
        self

        Raises
        ------
        NotFittedError
            If model is not fitted yet.
        """
        if not self._is_fitted:
            raise NotFittedError()

        # predict labels using current model
        y = self.predict(X)

        # sample duplication
        if self.duplicate_single_samples:
            X, y = self._ensure_enough_samples(X, y, method='noisy_dup')

        labels = np.unique(y)


        # fix init parameters
        prev_init_params = []
        prev_params = []
        for est in self.estimators_:
            prev_init_params.append(est.init_params)
            prev_params.append(est.params)
            # est.init_params = ''
            est.params = params

        # print np.bincount(y, minlength=self.n_labels)

        # do fitting based on predicted labels
        new_estimators = Parallel(n_jobs=self.n_jobs)(delayed(_fit_GMM)(
            self.estimators_[l], np.random.permutation(X[y == l])[:200] )
            for l in labels)

        # only change the estimators for which we had labels
        for i, l in enumerate(labels):
            self.estimators_[l] = new_estimators[i]

        # restore original parameters
        prev_init_params.reverse()
        prev_params.reverse()
        for est in self.estimators_:
            est.init_params = prev_init_params.pop()
            est.params = prev_params.pop()

        self.w_ = compute_class_weight(self.class_weight, np.asarray(labels), y)

        return self

    def _score_log_prob(self, X):
        """
        Compute the log likelihoods under each model component (== each class).

        Note that these scores are not weighted by the class weights yet.

        Parameters
        ----------
        X : array-like of shape [n_samples, n_features]
            Data

        Returns
        -------
        np.ndarray
            log likelihoods for each class (n_samples, n_classes)

        Raises
        ------
        NotFittedError
            Description
        """
        if not self._is_fitted:
            raise NotFittedError('Model is not fitted yet. Cannot predict.')

        proba = np.zeros( (X.shape[0], len(self.estimators_)), dtype=np.float)

        for i,e in enumerate(self.estimators_):
            # We could use sklearn.GMM's score method to compute the log likelihoods, but it doesn't support NaNs in X. I have implemented my own in ..models.utils which simply returns NaN if any value in the sample is NaN (i.e., any value in a row).
            # The performance of mine is even slightly better (x = [10000, 8]):
            #  sklearn: 100 loops, best of 3: 6.96 ms per loop
            #  own:     100 loops, best of 3: 5.77 ms per loop
            proba[:, i] = logProbGMM(e, X)  # these are the log likelihoods not probabilities!

        return proba


    def predict_proba(self, X):
        """
        Compute (pseudo) probability of data under each model component (class). Probabilities are weighed by class prior according to 'class_weight'.

        Parameters
        ----------
        X : array-like of shape [n_samples, n_features]
            Data

        Returns
        -------
        p : array of shape [n_samples, n_classes]
            Probability of the sample for each class
        """
        proba, _ = normLogProb(self._score_log_prob(X), axis=1)
        return np.exp(proba) * self.w_


    def predict_log_proba(self, X):
        """
        Compute (pseudo) log probability of data under each model component (class). Probabilities are weighed by class prior according to 'class_weight'.

        Parameters
        ----------
        X : array-like of shape [n_samples, n_features]
            Data

        Returns
        -------
        logp : array of shape [n_samples, n_classes]
            Log probability of the sample for each class.
        """
        proba, _ = normLogProb(self._score_log_prob(X), axis=1)

        return proba + np.log(self.w_)


    def predict(self, X):
        """
        Predict the labels for data

        Parameters
        ----------
        X : array-like of shape [n_samples, n_features]
            Data

        Returns
        -------
        C : array of shape [n_samples]
            Labels
        """
        if self.n_labels == 1:
            return self.le_.inverse_transform(np.asarray([0] * X.shape[0]))
        else:
            return self.le_.inverse_transform(np.argmax(self.predict_log_proba(X), axis=1))
