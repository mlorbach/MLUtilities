# -*- coding: utf-8 -*-
"""
The :mod:`classification.llda` module implements Local Linear (Fisher) Discriminant Analysis (LLDA).

Reimplementation of Matlab version by:
Sugiyama, M.
Dimensionality reduction of multimodal labeled data by local Fisher discriminant analysis.
Journal of Machine Learning Research, vol.8 (May), pp.1027-1061, 2007. 

Created on Fri Feb 06 12:02:55 2015

@author: MAL
"""

from __future__ import print_function

import numpy as np
from scipy import linalg
from scipy.spatial.distance import pdist, squareform

from sklearn.utils import check_arrays, array2d, column_or_1d
from sklearn.lda import LDA

__all__ = ['LLDA']


class LLDA(LDA):
    '''
    Local Linear Discriminant Analysis (LLDA)
    '''
    
    def __init__(self, n_components=None, priors=None):
        super(LLDA, self).__init__(n_components, priors)
        
        # n_components == r
        
        self.kNN = 7 # parameter used in local scaling method, default: 7
        
        
    def fit(self, X, y, store_covariance=False, tol=1.0e-4):
        """
        Fit the LLDA model according to the given training data and parameters.

        Parameters
        ----------
        X : array-like, shape = [n_samples, n_features]
            Training vector, where n_samples in the number of samples and
            n_features is the number of features.

        y : array, shape = [n_samples]
            Target values (integers)

        store_covariance : boolean
            If True the covariance matrix (shared by all classes) is computed
            and stored in `self.covariance_` attribute.
        """
        X, y = check_arrays(X, y, sparse_format='dense')
        y = column_or_1d(y, warn=True)
        self.classes_, y = np.unique(y, return_inverse=True)
        n_samples, n_features = X.shape  # == n, d
        n_classes = len(self.classes_)
        if n_classes < 2:
            raise ValueError('y has less than 2 classes')
        if self.priors is None:
            self.priors_ = np.bincount(y) / float(n_samples)
        else:
            self.priors_ = self.priors
        
        
        # --------------------------------
        
        tSb = np.zeros( (n_features, n_features), dtype=X.dtype )
        tSw = np.zeros( (n_features, n_features), dtype=X.dtype )
        
        
        for ind in range(n_classes):
            Xg = X[y == ind, :]
            ng = Xg.shape[0]
            
#           % Define classwise affinity matrix
            # 
            #Xg2 = np.sum(Xg**2, axis=1)
            
            #dist2 = Xg2[:, np.newaxis] + Xg2[np.newaxis, :] - 2.*Xg.dot(Xg.T)
            dist2 = squareform(pdist(Xg))
            dist2_sorted = np.sort(dist2, axis=0)
            kNNdist2 = dist2_sorted[self.kNN+1, :] # 20,
            
            sigma = np.sqrt(kNNdist2)
            localscale = sigma[:,np.newaxis] * sigma[np.newaxis, :]
            
            flag = localscale != 0
            A = np.zeros( (ng,ng) )
            A[flag] = np.exp( -dist2[flag]/localscale[flag] )
            
            Xg1 = np.sum(Xg, 1, keepdims=True)
            
            G = Xg.T.dot( np.sum(A,1,keepdims=True) * Xg ) - Xg.T.dot(A).dot(Xg)
            
            tSb += G/float(n_samples) + Xg.T.dot(Xg) * (1.-ng/float(n_samples)) + Xg1.T.dot(Xg1)/float(n_samples)
            tSw += G/float(ng)
            
            
        X1 = np.sum(X, 0, keepdims=True)
        tSb -= X1.T.dot(X1)/float(n_samples) - tSw
        
        tSb = (tSb + tSb.T)/2.
        tSw = (tSw + tSw.T)/2.
        
        eigval, eigvec = linalg.eig(tSb, tSw)
        
        eigval = np.real(eigval)
        eigvec = np.real(eigvec)
        
        
        sortkeys = np.flipud( np.argsort(eigval) )
        eigval = eigval[sortkeys[:self.n_components]]
        eigvec = eigvec[sortkeys[:self.n_components], :]
        

        self.T = (eigvec * np.sqrt(eigval)[:,np.newaxis]).T
        
        return self
        
        
    def transform(self, X):
        """
        Project the data so as to maximize class separation (large separation
        between projected class means and small variance within each class).

        Parameters
        ----------
        X : array-like, shape = [n_samples, n_features]

        Returns
        -------
        X_new : array, shape = [n_samples, n_components]
        """
        X = array2d(X)
        # center and scale data
        #X_new = np.dot(X - self.xbar_, self.scalings_)
        X_new = np.dot(X, self.T)
        n_components = X.shape[1] if self.n_components is None \
            else self.n_components
        return X_new[:, :n_components]