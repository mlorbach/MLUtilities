# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-08-25 14:35:00
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-08-25 15:15:57


class ActionRule(object):
    """
    Rule is a list of tuples. List elements are combined by OR,
    elements in tuples are combined by AND. Items are strings which
    correspond to the attribute names and must be valid column names
    in A (passed to predict(A)).

    Example:
    > rule = [('velocity', 'distance'), ('height')]
    translates to
    > (velocity & distance) | height

    Attributes
    ----------
    name : str
        name of the action
    rule : list of tuples of str
        logical rule
    """
    def __init__(self, name, rule):
        super(ActionRule, self).__init__()
        self.name = name
        self.rule = rule

    def get_attributes(self):
        attributes = set()
        for tpl in self.rule:
            if isinstance(tpl, str):
                attributes.add(tpl)
            else:
                for att in tpl:
                    attributes.add(att)
        return sorted(list(attributes))

    def predict(self, A):

        evallst = []
        for tplor in self.rule:
            if isinstance(tplor, tuple):
                evalstr = '('
                evalstr += ' & '.join('A["{}"]'.format(a) for a in tplor)
                evalstr += ')'
            else:
                evalstr = 'A["{}"]'.format(tplor)
            evallst.append(evalstr)
        evalstr = ' | '.join(evallst)

        return eval(evalstr)


class AttributeClassifier(object):
    """docstring for AttributeClassifier"""
    def __init__(self, name, func):
        super(AttributeClassifier, self).__init__()
        self.name = name
        self.func = func

    def predict(self, X):
        return self.func(X)
