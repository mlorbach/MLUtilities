# -*- coding: utf-8 -*-
"""
Created on Mon Jan 19 10:23:49 2015

@author: MAL
"""

import numpy as np

from itertools import product, permutations

from pyfac.graph import Graph



def create_bayesian_inference_graph(interaction_matrix, n_classes=10, subjects=['a', 'b'], class_names=None):
    '''
    
    '''

    if interaction_matrix.shape != (n_classes, n_classes):
        raise AttributeError('The interaction matrix must be of size [n_classes, n_classes] but is {}.'.format(interaction_matrix.shape))
        
    n_subjects = len(subjects)
    if n_subjects != 2:
        raise NotImplementedError('The joint subject inference graph currently only supports exactly two subjects.')
        
    if class_names is None:
        class_names = ['%d' % d for d in xrange(n_classes)]
        
    class_dict = dict(zip(class_names, range(n_classes)))
        
        
    P_uniform = np.array([.5,.5])
        
    ######################################
    # setup graph structure
    
    # graph
    jGraph = Graph()
    
    # variable nodes    
    jVars = {}
    for subj in subjects:
        jVars[subj] = {}
        
        for i,act in enumerate(class_names):
            jVars[subj][act] = jGraph.addVarNode(act, 1)
    
    # unary factor nodes (observations)
    jFacs = {}
    for subj in subjects:
        jFacs[subj] = {}
        
        for i,act in enumerate(class_names):
            jFacs[subj][act] = jGraph.addFacNode(P_uniform, jVars[subj][act])
        
        
    # connecting, multi-variate factor nodes (interaction matrix)
    jMFacs = {}
    
    for a,b in permutations(subjects, 2):
        print a,b
        
        jMFacs[a] = []
        
        # P(oth | alo, nap, pin):
        
        P = np.ones((2,2,2,2), dtype=np.float)/16.
        
        
        jMFacs[a].append(  jGraph.addFacNode(P, 
            jVars[a]['oth'], jVars[b]['alo'], jVars[b]['nap'], jVars[b]['pin'])  )
            
        # P(awy | app)
        P = np.ones((2,2), dtype=np.float)/4.
        
        for idx in product([0,1],[0,1]):
            pass
        
        jMFacs[a].append(  jGraph.addFacNode(P, jVars[a]['awy'], jVars[b]['app'])  )
            
        # P(fol | awy)
        jMFacs[a].append(  jGraph.addFacNode(np.ones((2,2), dtype=np.float)/4., jVars[a]['fol'], jVars[b]['awy'])  )
            
        # P(app | awy, sol)
        jMFacs[a].append(  jGraph.addFacNode(np.ones((2,2,2), dtype=np.float)/8., 
            jVars[a]['app'], jVars[b]['awy'], jVars[b]['sol'])  )
            
        # P(sol | sol)
        jMFacs[a].append(  jGraph.addFacNode(np.ones((2,2), dtype=np.float)/4., jVars[a]['sol'], jVars[b]['sol'])  )
        
    
    ######################################
    return jGraph
    
    
    
def set_observations(graph, observations):
    '''
    
    Set observations for unary factors assigned to subject state variables.
    
    Parameters
    ----------
    graph : Graph
        Graph to update.
        
    observations : dict
        {variable name : probability distribution}, p must be of size (n_classes,)
    
    Returns
    -------
    update graph
    '''
    
    if not isinstance(graph, Graph):
        raise TypeError('graph must be of type pyfac.Graph but is {}.'.format(type(graph)))
        
    if not isinstance(observations, dict):
        raise TypeError('observations must be of type dict but is {}.'.format(type(observations)))
        
           
    for name,p in observations.viewitems():
        nb = [f for f in graph.var[name].nbrs if len(f.nbrs) == 1]
        
        if len(nb) > 1:
            raise ValueError('The node "{}" has more than one unary factor associated. Don''t know which one to take...'.format(name))
            
        if len(nb) == 0:
            print 'The node "{}" has more no unary factors associated: Skip.'
            continue
            
        nb[0].P = p
        nb[0].reset()
        
        
    return graph
    
    
def set_observation_series_and_get_marginals(graph, observations):
    '''
    
    '''
    
    if not isinstance(graph, Graph):
        raise TypeError('graph must be of type pyfac.Graph but is {}.'.format(type(graph)))
        
    if not isinstance(observations, dict):
        raise TypeError('observations must be of type dict but is {}.'.format(type(observations)))
        
        
    # find the factor nodes for faster access:
        
    facNodes = {}
        
    for name in observations.viewkeys():
        nb = [f for f in graph.var[name].nbrs if len(f.nbrs) == 1]
        
        if len(nb) > 1:
            raise ValueError('The node "{}" has more than one unary factor associated. Don''t know which one to take...'.format(name))
            
        if len(nb) == 0:
            print '[WARNING] The node "{}" has more no unary factors associated: Skip.'
            continue
        
        facNodes[name] = nb[0]
          
         
            
    # get length of observation series
    dims = observations.itervalues().next().shape
    print dims
    subjects = [n for n in observations.viewkeys()]
    
    marginals = {}
    marg_temp = {}
    for subj in subjects:
        marginals[subj] = np.zeros(dims, dtype=np.float)

    for i in xrange(dims[0]):
        
        graph.reset() # resets messages
        
        for subj in subjects:
            facNodes[subj].P = observations[subj][i, :]
            
        marg_temp.update( graph.marginals() )
        for subj in subjects:
            marginals[subj][i, :] = marg_temp[subj][:,0]
        
    return marginals