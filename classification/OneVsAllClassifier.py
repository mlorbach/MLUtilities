# -*- coding: utf-8 -*-
"""
Created on Tue Dec 09 16:30:34 2014

@author: MAL
"""


# Author: Mathieu Blondel <mathieu@mblondel.org>
#
# License: BSD 3 clause
#
#
# Adapted by Malte Lorbach to return the unnormalized prediction probabilities of each classifier.


import numpy as np
import warnings

from sklearn.base import BaseEstimator, ClassifierMixin, clone, is_classifier
from sklearn.base import MetaEstimatorMixin
from sklearn.multiclass import OneVsRestClassifier, _ConstantPredictor, _check_estimator
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.utils import check_random_state
from sklearn.externals.joblib import Parallel
from sklearn.externals.joblib import delayed

from MLUtilities.utils import balanced_subset

def _fit_binary(estimator, X, y, classes=None, stratify=False):
    """Fit a single binary estimator."""
    unique_y = np.unique(y)
    if len(unique_y) == 1:
        if classes is not None:
            if y[0] == -1:
                c = 0
            else:
                c = y[0]
            warnings.warn("Label %s is present in all training examples." %
                          str(classes[c]))
        estimator = _ConstantPredictor().fit(X, unique_y)
    elif stratify == 'mining':
        
#        n_samples = X.shape[0]        
#        sample_set = np.arange(n_samples)
#        np.random.shuffle(sample_set)
        
        cnts = np.bincount(y)
        indset = balanced_subset(X, y, int((max(cnts)+min(cnts))/2.), return_indices=True)
        
        estimator = clone(estimator)
        
        for i in xrange(20):
            
            
            
            estimator.fit(X[indset], y[indset])
            
            # get all false positives (only predict the samples of the negative class for quicker assessment)
            FP = estimator.predict(X)
            FP = FP != y
            
            #print('# {:3d}. l = {}, FPs = {:.5f}'.format(i, indset.shape[0], np.sum(FP)/float(y.shape[0])))
            
            if np.sum(FP)/float(y.shape[0]) < 0.01:
                break
            
            fp_ind = np.arange(y.shape[0])[ FP ]            
            
            #print(indset.shape)
            #print(fp_ind.shape)
            indset = np.concatenate( [indset, fp_ind] )
            
            
            
        
        
    else:
        data = X
        target = y
        
        if stratify == True:
            data, target = balanced_subset(X, y)
        elif stratify == 'mid':
            cnts = np.bincount(target)
            data, target = balanced_subset(X, y, int((max(cnts)+min(cnts))/2.), noise=.001)
            
        
        #print np.unique(target, return_counts=True)
        estimator = clone(estimator)
        estimator.fit(data, target)
    return estimator


def fit_ovr(estimator, X, y, n_jobs=1, stratify=False):
    """Fit a one-vs-the-rest strategy."""
    _check_estimator(estimator)

    lb = LabelBinarizer()
    Y = lb.fit_transform(y)

    estimators = Parallel(n_jobs=n_jobs)(
        delayed(_fit_binary)(estimator, X, Y[:, i], classes=["not %s" % i, i], stratify=stratify)
        for i in range(Y.shape[1]))
    return estimators, lb
    

def predict_proba_ova(estimators, X, is_multilabel, aggregation_mode=None):
    """Estimate probabilities using the one-vs-the-rest strategy.

    Returned matrix will not sum to one.  Estimators
    must have a predict_proba method."""

    # Y[i,j] gives the probability that sample i has the label j.
    # In the multi-label case, these are not disjoint.
    Y = np.array([est.predict_proba(X)[:, 1] for est in estimators]).T
    
    if not is_multilabel:
        # aggregation options:
    
        if aggregation_mode == 'soft-max':
            # a) Soft-max:
            Y = np.exp(Y) / np.sum(np.exp(Y), axis=1)[:, np.newaxis]        
            
        elif aggregation_mode == 'norm':
            # b) Simple normalization so that Y sums to 1:
            Y /= np.sum(Y, axis=1)[:, np.newaxis]
        else:
            # c) nothing, return decision function scores
            pass
        
    return Y
    
    

class OneVsAllClassifier(OneVsRestClassifier):
    '''
    Parameters
    ----------
    
    stratify : True/False, 'mid'
        How to stratify classes before training one-vs-all.
        
    aggregation_mode : 'norm', 'soft-max', False
        How aggregate the classifier probabilities to one final "probability":
        
        - norm: normalize by sum
        - soft-max: normalize by soft-max function
        - False: do not normalize, simply return value per class (values do not sum to one).
    '''
    
    def __init__(self, estimator, n_jobs=1, stratify=False, aggregation_mode=None):
        super(OneVsAllClassifier, self).__init__(estimator, n_jobs)
        
        self.stratify = stratify
        self.aggregation_mode = aggregation_mode
        
        # the two classes have to be unbalanced by at least this tolerance before resampling is enabled
        self.stratify_tol = .1
        
    
    def fit(self, X, y):
        """Fit underlying estimators.

        Parameters
        ----------
        X : {array-like, sparse matrix}, shape = [n_samples, n_features]
            Data.

        y : array-like, shape = [n_samples] or [n_samples, n_classes]
            Multi-class targets. An indicator matrix turns on multilabel
            classification.

        Returns
        -------
        self
        """
        self.estimators_, self.label_binarizer_ = fit_ovr(self.estimator, X, y,
                                                          n_jobs=self.n_jobs, stratify=self.stratify)
        return self

    
    def predict_proba(self, X):
        """Probability estimates.
        
        The returned estimates for all classes are ordered by label of classes.
        
        Note that in the multilabel case, each sample can have any number of
        labels. This returns the marginal probability that the given sample has
        the label in question. For example, it is entirely consistent that two
        labels both have a 90% probability of applying to a given sample.
        
        In the single label multiclass case, the rows of the returned matrix
        do not sum to 1. Every classifier's own probability is returned.
        
        Parameters
        ----------
        X : array-like, shape = [n_samples, n_features]
        
        Returns
        -------
        T : array-like, shape = [n_samples, n_classes]
            Returns the probability of the sample for each class in the model,
            where classes are ordered as they are in `self.classes_`.
        """
        return predict_proba_ova(self.estimators_, X, is_multilabel=self.multilabel_, aggregation_mode=self.aggregation_mode)