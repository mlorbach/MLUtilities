# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-06-16 13:26:34
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-23 16:06:14

import os
from subprocess import Popen, PIPE
import logging
import tempfile
import datetime
import cPickle as pickle

import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.preprocessing import LabelBinarizer
from sklearn.utils.class_weight import compute_class_weight
from sklearn.datasets.svmlight_format import dump_svmlight_file
from sklearn.externals.joblib import Parallel, delayed
from ..libSVM.python.svmutil import svm_load_model, svm_save_model, svm_predict, svm_train


_libSVM_path = r'C:\Users\mal\phenorat_repo\code\_libs\libsvm-3.17-GPU_x64\windows'
# "C:\Users\mal\phenorat_repo\code\_libs\libsvm-3.17-GPU_x64\windows\libsvm_train_dense_gpu.exe"
_svmtrain_gpu_exe = os.path.join(_libSVM_path, 'libsvm_train_dense_gpu.exe')
_svmtrain_exe = os.path.join(_libSVM_path, 'svm-train.exe')
_svmpredict_exe = os.path.join(_libSVM_path, 'svm-predict.exe')


class OvREstimators(object):
    """
    Wrapper around a list of estimators (simply to prevent sklearn's clone() method to iterate the list and clone each item).

    This wrapper makes the SVM class compatible with sklearn methods such as GridSearchCV or learning_curve. The wrapper prevents applying clone() to each of the estimator in the list (which fails because under the hood they are non-pickable C-objects) and instead forces in to deepcopy() them which works fine.

    Attributes
    ----------
    estimators : list
        List of estimators
    """
    def __init__(self, estimators=[]):
        super(OvREstimators, self).__init__()
        self.estimators = estimators

    def set_estimators(self, estimators):
        self.estimators = estimators

    def get_estimators(self):
        return self.estimators

    def append(self, estimators):
        self.estimators.append(estimators)

    def clear(self):
        self.estimators = []

    def __len__(self):
        """Returns the number of estimators in the ensemble."""
        return len(self.estimators)

    def __getitem__(self, index):
        """Returns the index'th estimator in the ensemble."""
        return self.estimators[index]

    def __iter__(self):
        """Returns iterator over estimators in the ensemble."""
        return iter(self.estimators)


def _get_libSVM_kernel_type(kernel_str):
    """
    Return the integer flag corresponding to libSVM kernel types.

    Parameters
    ----------
    kernel_str : str
        One of kernel types: {'linear', 'poly', 'rbf', 'sigmoid'}

    Returns
    -------
    int
        integer flag
    """
    d = {'linear':0, 'poly':1, 'rbf':2, 'sigmoid':3}
    return d[kernel_str]


def _fit_binary(X, y, **params):
    """
    Fit a binary classifier on data X given labels y.

    Parameters
    ----------
    X : array-like, (n_samples, n_features)
        Data
    y : array-like, (n_samples,)
        Labels (binary)
    **params : keyword args
        keyword arguments passed to SVM

    Returns
    -------
    str
        filename of temporary file in which model is stored. Load with svm_load_model(fname).
        (Cannot return model because it is not pickable in multiprocessing contexts.)

    Raises
    ------
    e
        If writing to temporary file fails.
    """
    logger = logging.getLogger(__name__)

    parameters = dict(C=1.0, kernel='rbf', degree=3, gamma=0,
                 coef0=0.0, shrinking=True, probability=False,
                 tol=1e-3, cache_size=200, class_weight=None,
                 verbose=False, max_iter=-1)
    parameters.update(params)

    if parameters['verbose'] > 0:
        logger.setLevel(logging.DEBUG)


    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Prepare data
    # ~~~~~~~~~~~~~~~~~~~~~~~

    X = np.asarray(X)
    y = np.asarray(y).copy()

    # libSVM expects binary labels: (-1, 1)
    # If input labels don't match, encode them accordingly
    lb = LabelBinarizer(-1, 1)
    y = lb.fit_transform(y).squeeze()

    label_names = np.unique(y)
    N_samples = y.shape[0]


    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Pepare SVM
    # ~~~~~~~~~~~~~~~~~~~~~~~

    # SVM parameters
    kernelflag = _get_libSVM_kernel_type(parameters['kernel'])

    # compute class weights (Ci) for classifier
    wi = compute_class_weight(parameters['class_weight'], label_names, y)
    weight_str = ' '.join(['-w{} {:6f}'.format(l, wi[i]) for i, l in enumerate(label_names)])

    options = '-s 0 -t {:d} -d {:d} -m {:d} -c {:f} -g {} -r {:f} -b {:d} -h {:d} -e {:f} {} -q'
    options = options.format(
                     kernelflag, parameters['degree'], parameters['cache_size'], parameters['C'], parameters['gamma'], parameters['coef0'], parameters['probability'],
                     parameters['shrinking'],  parameters['tol'], weight_str)

    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Train
    # ~~~~~~~~~~~~~~~~~~~~~~~

    model = svm_train(y.tolist(), X.tolist(), options)

    fp_model, fname_model = tempfile.mkstemp(prefix='MLUlibSVM_')
    try:
        svm_save_model(fname_model, model)
    except (IOError, WindowsError) as e:
        logger.error('Failed to write model to temporary file: {}'.format(e))
        raise e
    finally:
        os.close(fp_model)

    return fname_model



def _fit_binary_GPU(X, y, **params):
    """
    Fit a binary classifier on data X given labels y using GPU optimized version.

    See: http://mklab.iti.gr/project/GPU-LIBSVM

    Parameters
    ----------
    X : array-like, (n_samples, n_features)
        Data
    y : array-like, (n_samples,)
        Labels (binary)
    **params : keyword args
        keyword arguments passed to SVM

    Returns
    -------
    str
        filename of temporary file in which model is stored. Load with svm_load_model(fname).
        (Cannot return model because it is not pickable in multiprocessing contexts.)

    Raises
    ------
    e
        If writing/accessing/deleting temporary file fails.
    """
    logger = logging.getLogger(__name__)

    parameters = dict(C=1.0, kernel='rbf', degree=3, gamma=0,
                 coef0=0.0, shrinking=True, probability=False,
                 tol=1e-3, cache_size=200, class_weight=None,
                 verbose=False, max_iter=-1, use_gpu=True)
    parameters.update(params)

    if parameters['verbose'] > 0:
        logger.setLevel(logging.DEBUG)


    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Prepare data
    # ~~~~~~~~~~~~~~~~~~~~~~~

    X = np.asarray(X)
    y = np.asarray(y).copy()

    # libSVM expects binary labels: (-1, 1)
    # If input labels don't match, encode them accordingly
    lb = LabelBinarizer(-1, 1)
    y = lb.fit_transform(y).squeeze()

    label_names = np.unique(y)

    N_samples = y.shape[0]

    # create temporary files on disk with training data and for the trained model
    fp_training_data, fname_training_data = tempfile.mkstemp(prefix='MLUlibSVM_')

    try:
        dump_svmlight_file(X, y, fname_training_data)
    except (IOError, WindowsError) as e:
        logger.error('Failed to store training data in temporary file, because {}'.format(e))
        raise e
    finally:
        os.close(fp_training_data)

    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Pepare SVM
    # ~~~~~~~~~~~~~~~~~~~~~~~

    # SVM parameters
    kernelflag = _get_libSVM_kernel_type(parameters['kernel'])

    # compute class weights (Ci) for classifier
    wi = compute_class_weight(parameters['class_weight'], label_names, y)
    weight_str = ' '.join(['-w{} {:6f}'.format(l, wi[i]) for i, l in enumerate(label_names)])

    # link to GPU accelerated version or normal version:
    if parameters['use_gpu']:
        train_executable = _svmtrain_gpu_exe
    else:
        train_executable = _svmtrain_exe

    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Train
    # ~~~~~~~~~~~~~~~~~~~~~~~

    fp_model, fname_model = tempfile.mkstemp(prefix='MLUlibSVM_')
    cmd = '{} -s 0 -t {:d} -m {:d} -c {:f} -g {} -r {:f} -b {:d} {} -q "{}" "{}"'
    cmd = cmd.format(train_executable,
                     kernelflag, parameters['cache_size'], parameters['C'], parameters['gamma'], parameters['coef0'], parameters['probability'],
                     weight_str,
                     fname_training_data, fname_model)
    try:
        Popen(cmd, shell = False, stdout = PIPE).communicate()

        # load the model from disk
        # model = svm_load_model(fname_model)
    except (IOError, WindowsError) as e:
        logger.error('Failed to write model to temporary file: {}'.format(e))
        raise e
    finally:
        os.close(fp_model)

        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Clean up temporary data
        # ~~~~~~~~~~~~~~~~~~~~~~~
        for fname in [fname_training_data]:
            if not os.path.isfile(fname):
                continue # don't try to delete files that don't exist
            try:
                os.unlink(fname)
            except (IOError, WindowsError) as e:
                logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

    return fname_model


def _predict_binary_py(X, fname_model):
    """
    Predict binary classification task with data X using Python interface.

    Parameters
    ----------
    X : array-like, (n_samples, n_features)
        Description
    fname_model : str
        Filename to file containing SVM model (to be loaded by svm_load_model(fname_model)).

        Model must have been trained with parameter probability=True ('-b 1').

    Returns
    -------
    np.ndarray, (n_samples,)
        Decision values for positive class for each sample.
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Load model from file and prepare input data
    # ~~~~~~~~~~~~~~~~~~~~~~~

    model = svm_load_model(fname_model)

    # prepare features
    # we need to provide data in libSVM "sparse" dict format.
    #  This is a list of dictionaries (key is feature index, value is corresponding value).
    # Passing a list of lists (only feature values) messes up libSVMs internal indexing which assumes the first feature to start at index 1. Unfortunately, we cannot change that behavior in the python interface.
    idx = range(X.shape[1])
    Xsp = [dict(zip(idx, xi)) for xi in X]

    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Run prediction
    # ~~~~~~~~~~~~~~~~~~~~~~~

    _, _, score = svm_predict([0]*X.shape[0], Xsp, model, '-q -b 1')

    # return score of positive class (1) only
    score = np.asarray(score)
    pos_class = model.get_labels().index(1)
    return score[:, pos_class]


def _predict_binary(fname_data, fname_model):
    """
    Predict binary classification task with data X using libSVM command line interface.

    Parameters
    ----------
    fname_data : str
        Filename to file containing test data in svmlight format.
    fname_model : str
        Filename to file containing SVM model (to be loaded by svm_load_model(fname_model)).

        Model must have been trained with parameter probability=True ('-b 1').

    Returns
    -------
    np.ndarray, (n_samples,)
        Decision values for positive class for each sample.

    Raises
    ------
    e
        If temporary file access fails.
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Run prediction
    # ~~~~~~~~~~~~~~~~~~~~~~~

    # create temporary files on disk for predictions
    fp_pred, fname_pred = tempfile.mkstemp(prefix='MLUlibSVM_')

    try:
        cmd = '{0} -b 1 "{1}" "{2}" "{3}"'.format(_svmpredict_exe, fname_data, fname_model, fname_pred)
        Popen(cmd, shell = False, stdout = PIPE).communicate()
    except (IOError, WindowsError) as e:
        logger.error('Failed to write predictions to temporary file: {}'.format(e))
        raise e
    finally:
        os.close(fp_pred)

    score = np.loadtxt(fname_pred, dtype=np.float, skiprows=1)

    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Clean up temporary data
    # ~~~~~~~~~~~~~~~~~~~~~~~
    for fname in [fname_pred]:
        if not os.path.isfile(fname):
            continue # don't try to delete files that don't exist
        try:
            os.unlink(fname)
        except (IOError, WindowsError) as e:
            logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

    # return score of positive class (which is always in column 1 in libSVM >= 3.17)
    #  (column 0 contains the predicted label)
    return score[:, 1]


class SVM(BaseEstimator, ClassifierMixin):
    """docstring for SVM"""
    def __init__(self, C=1.0, kernel='rbf', degree=3, gamma='auto',
                 coef0=0.0, shrinking=True, probability=False,
                 tol=1e-3, cache_size=200, class_weight=None,
                 verbose=False, max_iter=-1,
                 random_state=None, use_gpu=10000, n_jobs=-1, use_python_predict=True):
        super(SVM, self).__init__()

        self.kernel = kernel
        self.degree = degree
        self.gamma = gamma if gamma != 'auto' else 0
        self.coef0 = coef0
        self.tol = tol
        self.C = C
        self.shrinking = shrinking
        self.probability = probability
        self.cache_size = cache_size
        self.class_weight = class_weight if class_weight is not None and class_weight.lower() not in ['none', 'uniform'] else None
        self.verbose = verbose
        self.max_iter = max_iter
        self.random_state = random_state
        self.use_gpu = use_gpu
        self.n_jobs = n_jobs
        self.use_python_predict = use_python_predict
        self.estimators_ = OvREstimators()


    def fit(self, X, y):

        logger = logging.getLogger(__name__)

        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Prepare data
        # ~~~~~~~~~~~~~~~~~~~~~~~

        X = np.asarray(X)
        y = np.asarray(y).copy()
        unique_y = np.unique(y)

        N_samples = X.shape[0]
        n_labels = len(unique_y)

        self.label_binarizer_ = LabelBinarizer(sparse_output=True)
        y = self.label_binarizer_.fit_transform(y)
        y = y.tocsc()

        if self.label_binarizer_.y_type_ == 'multiclass':
            self.probability = True

        columns = (col.toarray().ravel() for col in y.T)

        parameters = dict(kernel=self.kernel,
            degree=self.degree,
            gamma=self.gamma,
            coef0=self.coef0,
            tol=self.tol,
            C=self.C,
            shrinking=self.shrinking,
            probability=self.probability,
            cache_size=self.cache_size,
            class_weight=self.class_weight,
            verbose=self.verbose,
            max_iter=self.max_iter,
            use_gpu=self.use_gpu)
        # parameters.update(params)

        # decide whether to use GPU accelerated version or normal version:
        if (isinstance(parameters['use_gpu'], bool) and parameters['use_gpu'] == True) or \
            (parameters['use_gpu'] > 1 and N_samples >= parameters['use_gpu']):
            parameters['use_gpu'] = True
            logger.debug('Using GPU-accelerated training')


            # TODO: fit only one estimator if binary classification task
            estimator_filenames = Parallel(n_jobs=self.n_jobs)(delayed(_fit_binary_GPU)(
                X, columns.next(), **parameters)
                for i in range(n_labels))

        else:
            parameters['use_gpu'] = False
            logger.debug('Using default (CPU-based) training')


            # TODO: fit only one estimator if binary classification task
            estimator_filenames = Parallel(n_jobs=self.n_jobs)(delayed(_fit_binary)(
                X, columns.next(), **parameters)
                for i in range(n_labels))


        self.estimators_.set_estimators(map(svm_load_model, estimator_filenames))
        self._is_fitted = True

        for fname in estimator_filenames:
            if not os.path.isfile(fname):
                continue # don't try to delete files that don't exist
            try:
                os.unlink(fname)
            except (IOError, WindowsError) as e:
                logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

        return self


    def predict_proba(self, X):

        if not self._is_fitted or len(self.estimators_) == 0:
            raise RuntimeError('Model has not been fitted yet.')

        logger = logging.getLogger(__name__)


        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Prepare data
        # ~~~~~~~~~~~~~~~~~~~~~~~
        if not self.use_python_predict:
            # if we use the command line version to predict, we need to write the test data to a temporary file.

            y_fake = np.zeros(X.shape[0])

            # create temporary files on disk with test data
            fp_data, fname_data = tempfile.mkstemp(prefix='MLUlibSVM_')

            try:
                dump_svmlight_file(X, y_fake, fname_data)
            except (IOError, WindowsError) as e:
                logger.error('Failed to store training data in temporary file, because {}'.format(e))
                raise e
            finally:
                os.close(fp_data)


        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Prepare models
        # ~~~~~~~~~~~~~~~~~~~~~~~

        model_files = []
        for model in self.estimators_:
            # dump trained models to file
            fp_model, fname_model = tempfile.mkstemp(prefix='MLUlibSVM_')
            try:
                # save the model to disk
                svm_save_model(fname_model, model)
            except (IOError, WindowsError) as e:
                logger.error('Failed to write model to temporary file: {}'.format(e))
                raise e
            finally:
                os.close(fp_model)
            model_files.append(fname_model)


        if self.label_binarizer_.y_type_ == 'multiclass':
            # predict the scores of all classes

            if self.use_python_predict:
                # Python version:
                scores = Parallel(n_jobs=self.n_jobs)(delayed(_predict_binary_py)(
                    X, model_files[i])
                    for i in range( len(model_files) ))
            else:
                # Command-line version:
                scores = Parallel(n_jobs=self.n_jobs)(delayed(_predict_binary)(
                    fname_data, model_files[i])
                    for i in range( len(model_files) ))

            # merge scores to one large (n_samples, n_classes) array
            scores = np.stack(scores, axis=-1)

            # normalize scores to 1
            # scores /= np.sum(scores, axis=1, keepdims=True)

        else:
            scores = _predict_binary(fname_data, model_files[0])

        # ~~~~~~~~~~~~~~~~~~~~~~~
        # Clean up temporary data
        # ~~~~~~~~~~~~~~~~~~~~~~~
        if not self.use_python_predict:
            model_files += [fname_data] # delete temporary test data file as well

        for fname in model_files:
            if not os.path.isfile(fname):
                continue # don't try to delete files that don't exist
            try:
                os.unlink(fname)
            except (IOError, WindowsError) as e:
                logger.error('Error deleting temporary file "{}": {}'.format(fname, e))

        return scores


    def predict(self, X):

        scores = self.predict_proba(X)

        if self.label_binarizer_.y_type_ == 'multiclass':
            argmaxima = np.argmax(scores, axis=1)
            return self.label_binarizer_.classes_[argmaxima]
        else:
            thresh = 0
            argmaxima = scores > thresh
            return self.label_binarizer_.inverse_transform(np.asarray(argmaxima))


    def __getstate__(self):
        result = self.__dict__.copy()

        # We cannot pickle the models directly as it is a C object:
        #  Instead, we'll dump the models into temporary files and then
        #  pickle the content (string) of the files
        if 'estimators_' in result:

            result['estimators_'] = []

            for model in self.estimators_:
                fp, fname = tempfile.mkstemp(prefix='asvm_')
                try:
                    # dump the model
                    svm_save_model(fname, model)

                    # read the model as string
                    with open(fname, 'r') as fpo:
                        result['estimators_'].append(fpo.read())
                except:
                    raise
                finally:
                    os.close(fp)
                    os.unlink(fname) # remove temp file

        return result


    def __setstate__(self, state):
        self.__dict__ = state.copy()

        # We cannot unpickle the models directly as it is a C object:
        #  Instead, we'll dump the models that are represented as a string
        #   into temporary files and then load the models from the files
        #   using libSVM's load method
        if 'estimators_' in state:

            self.estimators_ = OvREstimators()

            for model in state['estimators_']:

                fp, fname = tempfile.mkstemp(prefix='asvm_')
                try:
                    # dump the (string) model to file
                    with open(fname, 'w') as fpo:
                        fpo.write(model)

                    # load the model
                    self.estimators_.append(svm_load_model(fname))
                except:
                    raise
                finally:
                    os.close(fp)
                    os.unlink(fname) # remove temp file
