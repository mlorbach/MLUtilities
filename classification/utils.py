# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-06-28 10:46:48
# @Last Modified by:   MAL
# @Last Modified time: 2016-07-22 11:37:42

import os
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from MLUtilities.utils import cast_to_number


def write_libmsvm_model(svc, filename):
    """
    Convert a trained scitkit-learn.svm.SVC model into libSVM format and write to a file.

    Parameters
    ----------
    svc : scikit-learn.svm.SVC
        SVC model to convert
    filename : str
        File to store model in
    """
    def array2str(a):
        return ' '.join('{:g}'.format(v) for v in a)

    l, k = svc.support_vectors_.shape
    n_classes = len(svc.classes_)

    # swap labels in binary classification task to be compatible with libSVM
    if n_classes == 2 and svc.classes_[0] < svc.classes_[1]:
        # swap labels, nr_SV, dual_coef and SVs

        classes_ = svc.classes_[::-1]
        n_support_ = svc.n_support_[::-1]

        i1, i2 = svc.n_support_
        dual_coef_ = np.empty_like(svc.dual_coef_)
        dual_coef_[:, i2:] = svc.dual_coef_[:, :i1]
        dual_coef_[:, :i2] = svc.dual_coef_[:, i1:]

        support_vectors_ = np.empty_like(svc.support_vectors_)
        support_vectors_[i2:, :] = svc.support_vectors_[:i1, :]
        support_vectors_[:i2, :] = svc.support_vectors_[i1:, :]

    else:
        # don't swap otherwise
        classes_ = svc.classes_
        n_support_ = svc.n_support_
        dual_coef_ = svc.dual_coef_
        support_vectors_ = svc.support_vectors_



    with open(filename, 'wb') as fp:

        fp.write('svm_type {}\n'.format( 'c_svc' ))
        fp.write('kernel_type {}\n'.format(svc.kernel))
        fp.write('gamma {}\n'.format(svc._gamma))
        fp.write('nr_class {}\n'.format(len(svc.classes_)))
        fp.write('total_sv {}\n'.format(svc.support_vectors_.shape[0]))
        # intercept = -svc.intercept_
        # if len(svc.classes_) != 2: # swap sign of rho in the non-2-class case
        #     intercept *= -1
        fp.write('rho {}\n'.format(array2str(-svc.intercept_)))
        fp.write('label {}\n'.format(array2str(classes_)))
        fp.write('nr_sv {}\n'.format(array2str(n_support_)))
        fp.write('SV\n')

        for i in range(l): # for all support vectors

            fp.write('{} '.format(array2str(dual_coef_[:, i])))
            fp.write(' '.join(['{}:{}'.format(idx,np.round(v,8)) for idx,v in zip( range(1, k+1), support_vectors_[i, :])]))
            fp.write('\n')


def load_libsvm_model(filename):
    """
    Load a libSVM model from file and convert to a sklearn.svm.SVC classifier.

    Parameters
    ----------
    filename : str
        Filename of libSVM model

    Returns
    -------
    sklearn.svm.SVC
        sklearn SVC classifier

    Raises
    ------
    ValueError
        Input file does not exist
    """
    if not os.path.isfile(filename):
        raise ValueError('File does not exist: {}.'.format(filename))

    required_parameters = ['kernel_type', 'nr_class', 'total_sv', 'rho', 'label', 'nr_sv']

    with open(filename, 'rb') as fp:

        # find size of header (before SV)
        header_size = 0
        while fp.readline().strip() != 'SV':
            header_size += 1

        fp.seek(0) # go back to beginning

        # read header values (classifier parameters)
        header_dict = {}
        for i in range(header_size):
            l = fp.readline().strip().split(' ')
            if len(l) < 2:
                raise ValueError('Invalid header line. No values given: {}.'.format(' '.join(l)))
            header_dict[l[0]] = l[1:]

        # check for required parameters:
        if not all([p in header_dict for p in required_parameters]):
            missing = [p for p in required_parameters if p not in header_dict]
            raise AttributeError('Missing required parameters: {}'.format(missing))

        # parse header values
        for k,v in header_dict.iteritems():

            if len(v) == 1:
                header_dict[k] = cast_to_number(v[0]) # cast to int, float or leave as string
            else:
                header_dict[k] = np.asarray(map(cast_to_number, v))

        # check for correct SVM type
        if 'svm_type' in header_dict and header_dict['svm_type'] != 'c_svc':
            raise AttributeError('Only C-SVC type is supported at the moment but got {}'.format(header_dict['svm_type']))

        # special treatment of the intercept (need to inverse sign)
        header_dict['intercept'] = -header_dict['rho']

        # rename kernel_type to kernel (SVC() parameter)
        header_dict['kernel'] = header_dict['kernel_type']


        # Read support vectors

        fp.readline() # skip 'SV'

        nr_coef = None
        nr_feat = None

        dual_coef = []
        support_vectors = []

        # the rest are support vectors:
        for k, line in enumerate(fp):
            sv = line.strip().split(' ')

            # find the number of coefs and number of features (only once in the first row)
            if nr_coef is None:
                for i, s in enumerate(sv):
                    if s.find(':') >= 0:
                        nr_coef = i
                        nr_feat = len(sv) - nr_coef
                        break

            dual_coef.append( np.asarray(map(np.float, sv[:nr_coef])) )
            # we read the potentially sparse dict-like input into a pandas DataFrame, using the dict keys as column index. That way we keep the sparsity.
            support_vectors.append( pd.DataFrame( data=eval('{{{}}}'.format(','.join(sv[nr_coef:]))), index=[k] ) )

    dual_coef = np.vstack(dual_coef).T

    # join all support vectors into one array
    # TODO: handle sparse matrices here (NaN values in the DataFrames)
    support_vectors = pd.concat(support_vectors, axis=0).values

    total_SV = header_dict['total_sv']
    # check if we've got correct data
    if dual_coef.shape != (nr_coef, total_SV):
        raise ValueError('dual_coef must be of shape ({}, {}) but is ({})'.format(nr_coef, total_SV, dual_coef.shape))
    if support_vectors.shape != (total_SV, nr_feat):
        raise ValueError('support_vectors must be of shape ({}, {}) but is ({})'.format(total_SV, nr_feat, support_vectors.shape))

    # construct the SVC (this bit is quite hacky, as we need to set all fields properly and chances are that we miss something that is only used in rare cases; but its our best shot at the moment)

    init_parameters = dict(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0)
    for parameter in init_parameters.keys():
        if parameter in header_dict:
            init_parameters[parameter] = header_dict[parameter]

    clf = SVC(**init_parameters)
    clf._gamma = clf.gamma
    clf.classes_ = np.ascontiguousarray(header_dict['label'])
    clf.intercept_ = np.ascontiguousarray(header_dict['intercept'])
    clf._intercept_ = clf.intercept_.copy() # internal use in sklearn
    clf.dual_coef_ = np.ascontiguousarray(dual_coef)
    clf._dual_coef_ = clf.dual_coef_ # internal use in sklearn
    clf.support_vectors_ = np.ascontiguousarray(support_vectors)
    clf.n_support_ = np.ascontiguousarray(header_dict['nr_sv'])

    clf.support_ = np.zeros(total_SV, np.int, order='C')

    # TODO: change this if we ever implement proper sparse handling
    clf._sparse = False

    # this is not entirely true but we don't care about the number of training samples:
    clf.shape_fit_ = clf.support_vectors_.shape

    # not a probability model:
    clf.probA_ = np.empty((0), dtype=float, order='C')
    clf.probB_ = np.empty((0), dtype=float, order='C')

    return clf
