# -*- coding: utf-8 -*-
"""
Created on Mon Jan 19 10:23:49 2015

@author: MAL
"""

import numpy as np

from itertools import combinations

from pyfac.graph import Graph



def create_inference_graph(interaction_matrix, n_classes=10, subjects=['a', 'b']):
    '''
    
    '''

    if interaction_matrix.shape != (n_classes, n_classes):
        raise AttributeError('The interaction matrix must be of size [n_classes, n_classes] but is {}.'.format(interaction_matrix.shape))
        
    n_subjects = len(subjects)
    if n_subjects != 2:
        raise NotImplementedError('The joint subject inference graph currently only supports exactly two subjects.')
        
        
    P_uniform = 1./n_classes
        
    ######################################
    # setup graph structure
    
    # graph
    G = Graph()
    
    # variable nodes    
    for s in subjects:
        G.addVarNode(s, n_classes) 
    
    # unary factor nodes (observations)
    for v in G.var.viewvalues():
        Pv = np.empty((n_classes,), dtype=np.float)
        Pv.fill(P_uniform)
        
        G.addFacNode(Pv, v)
        
        
    # connecting, multi-variate factor nodes (interaction matrix)
    for (vi, vj) in combinations(G.var.viewvalues(), 2):
        
        print vi.name, vj.name
        G.addFacNode(interaction_matrix, vi, vj)
        
    
    ######################################
    return G
    
    
    
def set_observations(graph, observations):
    '''
    
    Set observations for unary factors assigned to subject state variables.
    
    Parameters
    ----------
    graph : Graph
        Graph to update.
        
    observations : dict
        {variable name : probability distribution}, p must be of size (n_classes,)
    
    Returns
    -------
    update graph
    '''
    
    if not isinstance(graph, Graph):
        raise TypeError('graph must be of type pyfac.Graph but is {}.'.format(type(graph)))
        
    if not isinstance(observations, dict):
        raise TypeError('observations must be of type dict but is {}.'.format(type(observations)))
        
           
    for name,p in observations.viewitems():
        nb = [f for f in graph.var[name].nbrs if len(f.nbrs) == 1]
        
        if len(nb) > 1:
            raise ValueError('The node "{}" has more than one unary factor associated. Don''t know which one to take...'.format(name))
            
        if len(nb) == 0:
            print 'The node "{}" has more no unary factors associated: Skip.'
            continue
            
        nb[0].P = p
        nb[0].reset()
        
        
    return graph
    
    
def set_observation_series_and_get_marginals(graph, observations):
    '''
    
    '''
    
    if not isinstance(graph, Graph):
        raise TypeError('graph must be of type pyfac.Graph but is {}.'.format(type(graph)))
        
    if not isinstance(observations, dict):
        raise TypeError('observations must be of type dict but is {}.'.format(type(observations)))
        
        
    # find the factor nodes for faster access:
        
    facNodes = {}
        
    for name in observations.viewkeys():
        nb = [f for f in graph.var[name].nbrs if len(f.nbrs) == 1]
        
        if len(nb) > 1:
            raise ValueError('The node "{}" has more than one unary factor associated. Don''t know which one to take...'.format(name))
            
        if len(nb) == 0:
            print '[WARNING] The node "{}" has more no unary factors associated: Skip.'
            continue
        
        facNodes[name] = nb[0]
          
         
            
    # get length of observation series
    dims = observations.itervalues().next().shape
    print dims
    subjects = [n for n in observations.viewkeys()]
    
    marginals = {}
    marg_temp = {}
    for subj in subjects:
        marginals[subj] = np.zeros(dims, dtype=np.float)

    for i in xrange(dims[0]):
        
        graph.reset() # resets messages
        
        for subj in subjects:
            facNodes[subj].P = observations[subj][i, :]
            
        marg_temp.update( graph.marginals() )
        for subj in subjects:
            marginals[subj][i, :] = marg_temp[subj][:,0]
        
    return marginals