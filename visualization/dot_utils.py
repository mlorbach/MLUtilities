# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-04-26 13:32:48
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-04-26 13:40:25

import os
import logging
import ipywidgets as widgets
from MLUtilities.io_custom import mkdir_p


def get_save_button_dot(dot_graph, filename, prog=None, fmt=None):
    """

    :param pdfstream:
    :param filename:
    :param dpi:
    :return:
    """

    logger = logging.getLogger(__name__)

    try:
        import pydotplus as pydot
    except ImportError:
        import pydot

    def save_figure(name, fmt=fmt):
        assert isinstance(dot_graph, pydot.Dot)
        if not os.path.isdir(os.path.dirname(filename_widget.value)):
            mkdir_p(os.path.dirname(filename_widget.value))

        if fmt is None or len(fmt) == 0:
            fmt = os.path.splitext(filename_widget.value)[1][1:]  # get extension without .

        dot_graph.write(filename_widget.value, prog=prog, format=fmt)
        # fig.savefig(filename_widget.value, bbox_inches='tight', dpi=dpi)
        logger.info('Saved graph to "{}".'.format(os.path.abspath(filename_widget.value)))

    filename_widget = widgets.Text(value=filename)
    btn_save = widgets.Button(description='Save graph')
    btn_save.on_click(save_figure)

    return widgets.HBox(children=[filename_widget, btn_save])
