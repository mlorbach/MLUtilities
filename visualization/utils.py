# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 10:52:19 2015

@author: MAL
"""

import os
import logging
import ipywidgets as widgets
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from MLUtilities.io_custom import mkdir_p

def get_some_colors(n=8):
    ''' Returns a list of n well distinguishable colors for plotting. '''

    clrs = ['#348ABD','#7A68A6','#A60628','#467821','#ffc022', '#1cb9e4', '#ac27ca', '#ff6600', '#9ec740', '#cb8e00', '#188487', '#555555', '#DDDDDD']
    if n > len(clrs) or n < 0:
        raise ValueError('n has to be in the range [0, {}] but is {}.'.format(len(clrs), n))
    return clrs[:n]


def hex_to_rgb(value):
    ''' Converts a hex color string into (R,G,B) tuple. '''
    value = value.lstrip('#')
    lv = len(value)
    return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))


def hex_to_bgr(value):
    ''' Converts a hex color string into (B,G,R) tuple (as often used by OpenCV). '''
    return hex_to_rgb(value)[::-1]


def get_save_button(fig, filename, dpi=None, **kwargs):
    """

    :param fig:
    :param filename:
    :param dpi:
    :return:
    """

    logger = logging.getLogger(__name__)

    def save_figure(name):
        assert isinstance(fig, Figure)
        if not os.path.isdir(os.path.dirname(filename_widget.value)):
            mkdir_p( os.path.dirname(filename_widget.value) )
        fig.savefig(filename_widget.value, bbox_inches='tight', dpi=dpi, **kwargs)
        logger.info('Saved figure to "{}".'.format(os.path.abspath(filename_widget.value)))

    filename_widget = widgets.Text(value=filename)
    btn_save = widgets.Button(description='Save figure')
    btn_save.on_click(save_figure)

    return widgets.HBox(children=[filename_widget, btn_save])
