# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2015-04-24 11:00:17
# @Last Modified by:   MAL
# @Last Modified time: 2016-08-03 11:17:34


import numpy as np
from scipy.stats import chi2

import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse


def plot_cov_ellipse(loc, cov, volume=.68, ax=None,
                     fc='none', ec='k', alpha=1, linewidth=2):
    """
    Plots an ellipse enclosing *volume* based on the specified covariance
    matrix (*cov*) and location (*pos*). Additional keyword arguments are passed on to the
    ellipse patch artist.

    Parameters
    ----------
        cov : The 2x2 covariance matrix to base the ellipse on
        pos : The location of the center of the ellipse. Expects a 2-element
            sequence of [x0, y0].
        volume : The volume inside the ellipse; defaults to 0.5
        ax : The axis that the ellipse will be plotted on. Defaults to the
            current axis.
    """

    def eigsorted(cov):
        vals, vecs = np.linalg.eigh(cov)
        order = vals.argsort()[::-1]
        return vals[order], vecs[:, order]

    if ax is None:
        ax = plt.gca()
    fig = ax.get_figure()

    # if cov is one-dimensional we assume that we are given the values of a diagonal covariance matrix
    if cov.ndim == 1:
        cov = np.diag(cov)

    vals, vecs = eigsorted(cov)
    theta = np.degrees(np.arctan2(*vecs[:, 0][::-1]))

    kwrgs = {
    'facecolor': fc,
    'edgecolor': ec,
    'alpha': alpha,
    'linewidth': linewidth}

    # Width and height are "full" widths, not radius
    width, height = 2 * np.sqrt(chi2.ppf(volume, 2)) * np.sqrt(vals)
    ellip = Ellipse(xy=loc, width=width, height=height, angle=theta, **kwrgs)

    ax.add_artist(ellip)
    return fig, ax, ellip


def plot_histogram(bins, values, bin_labels=None, label_rotation_deg=0,
                   bin_width=0.4, ax=None, yerr=None, **kwargs):
    '''
    Plot a histogram specified by `bins` and `values` and make it look nice.

    **kwargs is passed to plt.bar(..., **kwargs).
    '''

    if isinstance(bins, int):
        ind = np.arange(bins)
    elif isinstance(bins, list):
        ind = np.array(bins)
    else:
        ind = bins

    if(bin_labels is None or
       (len(bin_labels) != len(ind) and
        not isinstance(bin_labels, dict))):

        # generate new labels
        bin_labels = [str(c) for c in ind]
        print 'newlabels'

    if isinstance(bin_labels, dict):
        bin_labels = [bin_labels[l] for l in ind]

    # make sure the width is at least 0.1
    bin_width = max(abs(bin_width), 0.1)

    if ax is None:
        fig, ax = plt.subplots(1, 1)
    else:
        fig = ax.get_figure()

    ax.bar(ind, values, width=bin_width, yerr=yerr, **kwargs)

    for i, v in enumerate(values):
        ax.text(ind[i] + bin_width / 2., v, '{0:.3f}'.format(v),
                horizontalalignment='center',
                verticalalignment='bottom')

    ax.set_xticks(ind + bin_width / 2.)
    ax.set_xticklabels(bin_labels,
                       rotation=label_rotation_deg, rotation_mode="anchor",
                       ha="center")
    ax.set_xlim(ind[0] - 1. + bin_width, ind[-1] + 1)

    return fig, ax
