# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 10:57:06 2015

@author: MAL
"""

import numpy as np
import pandas as pd

import operator

import matplotlib.pyplot as plt
from matplotlib import rc, rcParams
from mpl_toolkits.axes_grid1 import make_axes_locatable
from sklearn.metrics import confusion_matrix


def plot_confusion_matrix_multiseq(Y_true_pred, labels=None, target_names=None, label_rotation_deg=0, cmap='jet', ax=None, colorbar='', norm=None, font=None):
    '''
    Plots the confusion matrix of a prediction result.

    Parameters
    ----------
    Y_true_pred : array-like
        list of tuples of (Ground truth data, Predictions), every item in list is one sequence

    labels : list-like (optional)
        Target labels to include in the confusion matrix. Values must correspond to the values used in Ytest and Ypred.

        You can use this to reorder the row and columns as well.

        If None, all occurring targets are used.

    target_names : list-like (optional)
        String representation of the target labels. Must be of same length as labels.

        If given, the strings are used for labeling the rows and columns.

    label_rotation_deg : int, float, default = 0
        Optional rotation of the column labels in degrees.

    cmap : str, matplotlib colormap object, default = 'jet'
        The colormap to paint the matrix cells.

    ax : matplotlib axis object (optional)
        If given, the confusion matrix is plotted inside this axis.

    colorbar : '', matplotlib axis object, or None, default = ''
        Specifies where to plot the colorbar. Set to None to not plot a colorbar.

        If '', a new colorbar axis is created that fits the heights of the confusion matrix.

        If an axis object is given, the colorbar is plotted inside this axis.

    norm : 'prec', 'recall', or None, default = None
        Specified how to normalize the confusion values. Set to None to not normalize at all and plot the absolute samples counts.

        If 'prec', the values in the matrix correspond to the precision measure: How many of my predictions are actually correct?.

        If 'recall', the value correspond to the recall measure: How many of the class samples did I predict correctly?

    Returns
    -------
    fig : matplotlib.Figure
        The figure in which the confusion matrix was plotted.

    ax : matplotlib.Axes or matplotlib.axes._subplots.AxesSubplot
        The axes in which the confusion matrix was plotted

    cmimg : matplotlib.image.AxesImage
        The image axes representing the matrix plot

    '''

    cm = np.zeros((len(labels), len(labels)))
    for (ytrue, ypred) in Y_true_pred:
        cm += confusion_matrix(ytrue, ypred, labels)
    fig, ax, cmimg = plot_matrix(cm, target_names, label_rotation_deg, cmap, ax, colorbar, norm, font)
    ax.set_xlabel('Predicted')
    ax.set_ylabel('True')
    return fig, ax, cmimg


def plot_confusion_matrix(Ytest, Ypred, labels=None, target_names=None, label_rotation_deg=0, cmap='jet', ax=None, colorbar='', norm=None, font=None):
    '''
    Plots the confusion matrix of a prediction result.

    Parameters
    ----------
    Ytest : array-like
        Ground truth data

    Ypred : array-like
        Predictions, same shape as Ytest

    labels : list-like (optional)
        Target labels to include in the confusion matrix. Values must correspond to the values used in Ytest and Ypred.

        You can use this to reorder the row and columns as well.

        If None, all occurring targets are used.

    target_names : list-like (optional)
        String representation of the target labels. Must be of same length as labels.

        If given, the strings are used for labeling the rows and columns.

    label_rotation_deg : int, float, default = 0
        Optional rotation of the column labels in degrees.

    cmap : str, matplotlib colormap object, default = 'jet'
        The colormap to paint the matrix cells.

    ax : matplotlib axis object (optional)
        If given, the confusion matrix is plotted inside this axis.

    colorbar : '', matplotlib axis object, or None, default = ''
        Specifies where to plot the colorbar. Set to None to not plot a colorbar.

        If '', a new colorbar axis is created that fits the heights of the confusion matrix.

        If an axis object is given, the colorbar is plotted inside this axis.

    norm : 'prec', 'recall', or None, default = None
        Specified how to normalize the confusion values. Set to None to not normalize at all and plot the absolute samples counts.

        If 'prec', the values in the matrix correspond to the precision measure: How many of my predictions are actually correct?.

        If 'recall', the value correspond to the recall measure: How many of the class samples did I predict correctly?

    Returns
    -------
    fig : matplotlib.Figure
        The figure in which the confusion matrix was plotted.

    ax : matplotlib.Axes or matplotlib.axes._subplots.AxesSubplot
        The axes in which the confusion matrix was plotted

    cmimg : matplotlib.image.AxesImage
        The image axes representing the matrix plot

    '''

    cm = confusion_matrix(Ytest, Ypred, labels)
    fig, ax, cmimg = plot_matrix(cm, target_names, label_rotation_deg, cmap, ax, colorbar, norm, font)
    ax.set_xlabel('Predicted class')
    ax.set_ylabel('True class')
    return fig, ax, cmimg


def plot_matrix(matrix_values, target_names=None, label_rotation_deg=0, cmap='jet', ax=None, colorbar='', norm=None, font=None, hide_threshold=0.005, thresh_operator=operator.lt, percent=False, annot_format=None):
    '''
    Plots values of symmetric 2D matrix in a 'confusion matrix' formatting style.

    Parameters
    ----------
    matrix_values : matrix-like
        matrix values to plot

    target_names : list-like (optional)
        String representation of the target labels. Must be of same length as number of columns/rows.

        If given, the strings are used for labeling the rows and columns.

    label_rotation_deg : int, float, default = 0
        Optional rotation of the column labels in degrees.

    cmap : str, matplotlib colormap object, default = 'jet'
        The colormap to paint the matrix cells.

    ax : matplotlib axis object (optional)
        If given, the matrix is plotted inside this axis.

    colorbar : '', matplotlib axis object, or None, default = ''
        Specifies where to plot the colorbar. Set to None to not plot a colorbar.

        If '', a new colorbar axis is created that fits the heights of the matrix.

        If an axis object is given, the colorbar is plotted inside this axis.

    norm : 'prec', 'recall', or None, default = None
        Specified how to normalize the confusion values. Set to None to not normalize at all and plot the absolute samples counts.

        If 'prec', the values in the matrix correspond to the precision measure: How many of my predictions are actually correct?.

        If 'recall', the value correspond to the recall measure: How many of the class samples did I predict correctly?

    font : dict or str, optional
        Matplotlib font-dict, or 'print'

    hide_threshold : float, optional
        threshold for hiding value (text) in cell

    thresh_operator : operator, optional
        operator.lt (less than), operator.gt (greater than)

    Returns
    -------
    fig : matplotlib.Figure
        The figure in which the matrix was plotted.

    ax : matplotlib.Axes or matplotlib.axes._subplots.AxesSubplot
        The axes in which the matrix was plotted

    cmimg : matplotlib.image.AxesImage
        The image axes representing the matrix plot

    '''

    if font is not None:
        if isinstance(font, dict):
            rc('font', **font)
        elif font == 'print':
            font = {'family': 'serif',
                    'weight': 'normal',
                    'size': 16}

            rc('font', **font)

    textsize = rcParams['font.size']

    # rename
    cm = matrix_values

    figtitle = 'Matrix'
    if annot_format is None:
        if cm.dtype == np.float or norm is not None:
            annot_fmt = '{0:.2f}'
        else:
            annot_fmt = '{0:}'
    else:
        annot_fmt = annot_format

    vmin = None
    vmax = None

    if norm is not None:
        if isinstance(norm, tuple):
            vmin, vmax = norm
        elif norm.lower() == 'recall' or norm == True:

            # normalized by number of True samples per class
            #  --> Recall
            #   How many of the class samples did I predict correctly?

            cm = cm / np.nansum(cm, axis=1, dtype=np.float, keepdims=True)
            cm = np.nan_to_num(cm)
            figtitle = 'Confusion matrix (recall)'
            vmin = 0.
            vmax = 1.

        elif norm.lower() == 'precision':

            # normalized by number of predicted samples per class
            #  --> Precision
            #   How many of my predictions are actually correct?

            cm = cm / np.nansum(cm, axis=0, dtype=np.float, keepdims=True)
            cm = np.nan_to_num(cm)
            figtitle = 'Confusion matrix (precision)'
            vmin = 0.
            vmax = 1.
        else:
            print('[WARN] Undefined normalization option: "{}".'.format(norm))

    if ax is None:
        fig = plt.figure(figsize=cm.shape)
        ax = fig.gca()
    else:
        fig = ax.get_figure()

    cmimg = ax.matshow(cm, cmap=cmap, vmin=vmin, vmax=vmax)
    ax.set_title(figtitle,
                 y=1.08 if label_rotation_deg == 0 else (1 + label_rotation_deg / 200.))
    ax.grid(False)

    ###
    # write values into matrix fields and make the text black/white depending on cell gray level

    colorvalues = np.mean(cmimg.to_rgba(cm)[:, :, 0:3], axis=-1)
    you_have_been_warned = False
    for i in xrange(cm.shape[0]):
        for j in xrange(cm.shape[1]):
            if thresh_operator is not None and thresh_operator(cm[i, j], hide_threshold):
                if not you_have_been_warned and cm[i, j] > 0:
                    you_have_been_warned = True
                    print('[WARN] Some values may be hidden because they are beyond the threshold {}.'.format(hide_threshold))
                continue
            ax.annotate(annot_fmt.format(100 * cm[i, j] if percent else cm[i, j]),
                        xy=(j, i),
                        horizontalalignment='center',
                        verticalalignment='center',
                        color='k' if colorvalues[i, j] > .5 else 'w',
                        size=textsize + 2)

    if colorbar is not None:

        if isinstance(colorbar, plt.Axes):
            divider = make_axes_locatable(colorbar)
        else:
            divider = make_axes_locatable(ax)

        cb_ax = divider.append_axes("right", size="5%", pad=0.075)
        colorbar = fig.colorbar(cmimg, cax=cb_ax,
                                ticks=None if norm is None else np.linspace(vmin, vmax, 6))
        colorbar.ax.tick_params(axis='y', color='k')

    if target_names is not None:

        print_target_names = [str(l).replace('_', ' ') for l in target_names]

        ax.set_xticks(range(len(print_target_names)))
        ax.set_yticks(range(len(print_target_names)))

        ax.set_xticklabels(print_target_names, rotation=label_rotation_deg,
                           rotation_mode="anchor", ha="left", size=textsize,
                           color='k')
        ax.set_yticklabels(print_target_names, size=textsize, color='k')

    ax.tick_params(axis="both", which="both", bottom=False, top=False,
                   labelbottom=False, left=False, right=False,
                   labelleft=True, labeltop=True)
    ax.set_xlabel('x axis', size=textsize, color='k')
    ax.set_ylabel('y axis', size=textsize, color='k')

    return fig, ax, cmimg
