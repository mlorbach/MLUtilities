# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-05-19 10:27:23
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-09 11:58:33


import os
import re
import logging
import numpy as np
import pandas as pd
from sklearn.preprocessing import MaxAbsScaler, MinMaxScaler

from MLUtilities.features.ethovision import load_ethovision_csv


# check for formatting options: https://docs.python.org/2/library/logging.html#logrecord-attributes
_LOGFORMAT = '%(asctime)-9s %(levelname)-6s %(message)s'
_LOGDATEFORMAT = '%H:%M:%S'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

COL_REGEX = re.compile('^(?P<axis>[xXyY])[\s_]*(?P<id>.+)')

def parse_column(column):

    m = COL_REGEX.match( column )

    out = ()
    if m is not None:
        out = (m.group('id'), m.group('axis'))

    return out

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_tracking_file(tracking_file, scale_rect=[0,0,1,1]):

    logging.basicConfig(level=logging.DEBUG, format=_LOGFORMAT, datefmt=_LOGDATEFORMAT)
    logger = logging.getLogger('Player')

    x0, y0, W, H = scale_rect

    # two formats possible: csv or h5
    fileformat = os.path.splitext(tracking_file)[1]

    if fileformat.lower() == '.csv':

        df = pd.read_csv(tracking_file, sep=';', header=[0,1,2], index_col=0)

    elif fileformat.lower() == '.txt':
        # ETHOVISION Export format
        # ------------------------

        # position information has to be in pixels
        df = load_ethovision_csv(tracking_file, return_recording_time=False)

        cols = map(parse_column, df.columns.tolist())

        df.columns = pd.MultiIndex.from_tuples(cols, names=['id', 'axis'])

        df = df.stack(level=0)

        logger.debug('minmax x = {},{}'.format( df.min(), df.max() ))

        # scaler = MinMaxScaler().fit(df)
        # df.loc[:, :] = scaler.transform(df)
        df.loc[:, 'X'] = (df.loc[:, 'X'] / W + x0).round(0).astype(np.int)
        df.loc[:, 'Y'] = (df.loc[:, 'Y'] / H + y0).round(0).astype(np.int)
        df = df.unstack(level=-1)
        df.columns = df.columns.swaplevel(0,1)
        df = df.sortlevel(axis=1)


    elif fileformat == '.h5':
        df = pd.read_hdf(tracking_file, 'tracks')

    else:
        raise IOError('Unrecognized tracking file format: {}'.format(fileformat))

    return df

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def scale_track_position(df_track, scale, column_names=['X', 'Y']):

    for n, s in zip(column_names, scale):
        scaled = df_track.xs(n, level='axis', axis=1, drop_level=False) * s
        df_track.update(scaled)

    return df_track


def get_tracking_frame_rate(df_track):

    return np.round(1./np.diff(df_track.index.values).mean(), 2)


def convert_tracking_frame_index_to_time_index(df_tracking, fps):
    df_tracking.index = df_tracking.index.values / float(fps)
    df_tracking.index.name = 'time'
    return df_tracking
