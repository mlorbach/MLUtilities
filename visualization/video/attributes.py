# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-08-25 17:21:08
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-08-25 17:24:13

import os
import numpy as np
import pandas as pd



def make_html5_video(source_video, fps=None, outputpath=r'output/dev/gifs'):

    outputpath = os.path.realpath(outputpath)
    atttemp = dfat.loc[indexer, :]
    tm0 = atttemp.first_valid_index()[1]
    tm1 = atttemp.last_valid_index()[1]
    del atttemp
    clipfilename = os.path.join(outputpath, 'seq_{}_{}-{}_ksim.mp4'.format(vid, tm0, tm1))
    t_start, t_end = (tm0/float(meta[dset]['fps']), tm1/float(meta[dset]['fps']))

    existing_movies = list(glob(os.path.join(outputpath, 'seq_*.mp4')))
    if len(existing_movies) > 10:
        logger.info('There are already {} other movie files in the output folder. Time to clean?'.format(len(existing_movies)))

    # load video
    clip_orig = VideoFileClip(source_video)
    clip1 = clip_orig.subclip(t_start, t_end).resize(.5)
    clips = [clip1]

    # make a text clip for every action class, whose color depends on whether the action is occurring or not
    for iact, action in enumerate(dfr.columns):
        def dealpha(get_frame, t, action=action):
            if dfr.loc[(vid, tm0+int(np.round(meta[dset]['fps']*t))), action]:
                return get_frame(t)
            else:
                return get_frame(t) * .5  # if action is not occurring, desaturate text

        clipannot = TextClip(txt=action.capitalize().replace('_', ' '),
                             color='rgba({},{},{},1.0)'.format(*(255*label_colors[iact, :]).astype(int).tolist()),
                             align='center',
                             fontsize=12)
        clipannot = clipannot.set_position((5,10 + iact*12))  # top left corner
        clipannot = clipannot.set_start(0)  # start right away
        clipannot = clipannot.set_duration(t_end - t_start)  # until end
        clipannot = clipannot.fl(dealpha, apply_to=['mask'])  # apply color filter
        clips.append(clipannot)  # add to composition

    # make colored border to indicate context:
    border = np.ones((clip1.size[1], clip1.size[0], 4), dtype=int)*255
    border[:, :, 1:3] = 0.
    border[3:-3, 3:-3, :] = 0.
    clipborder = ImageClip(border, duration=plotdata.duration/float(meta[dset]['fps'])).set_start(cntxt_end/float(meta[dset]['fps']))
    clips.append(clipborder)

    # compose final clip with text clips overlayed on top of video
    final_clip = CompositeVideoClip(clips)

    # write to file
    final_clip.write_videofile(clipfilename, audio=False, bitrate='200k', verbose=False)

    # and display here (random number prevents browser from showing an old cached version of the video)
    return HTML("""<video controls><source src="{}?t={}" type="video/mp4"></video>""".format(
                '../' + os.path.relpath(clipfilename),
                np.random.randint(0, 1e9, 1)))
