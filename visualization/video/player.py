# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-05-19 10:27:38
# @Last Modified by:   MAL
# @Last Modified time: 2016-07-05 14:17:35

import sys
import os
import argparse
import datetime
import numpy as np
import pandas as pd
import pygame
import cv2
import logging


from pygame import display, movie

from utils import load_tracking_file, get_tracking_frame_rate, convert_tracking_frame_index_to_time_index, scale_track_position
from drawing import TrackingMarkers, create_markers, DEEPBLUE, DEEPGREEN, DEEPRED
from MLUtilities.io_custom import mkdir_p


# check for formatting options: https://docs.python.org/2/library/logging.html#logrecord-attributes
_LOGFORMAT = '%(asctime)-9s %(levelname)-6s %(message)s'
_LOGDATEFORMAT = '%H:%M:%S'


# if sys.platform == 'win32' and sys.getwindowsversion()[0] >= 5: # condi. and
#     # On NT like Windows versions smpeg video needs windb.
#     os.environ['SDL_VIDEODRIVER'] = 'windib'




# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def play(video_file, tracking=None, video_start_offset=0, tracking_start_offset=0, scale=False, export=False):

    logger = logging.getLogger('Player')

    if not os.path.isfile(video_file):
        raise IOError('Could not find video file: {}.'.format(video_file))

    pygame.init()
    pygame.mixer.quit()

    clock = pygame.time.Clock()

    # INITIALIZE VIDEO STREAM
    cap = cv2.VideoCapture(video_file)
    W, H = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    fps = round(cap.get(cv2.CAP_PROP_FPS), 2)

    # default screen size
    if W <= 0 or H <= 0:
        W = 640
        H = 480

    cap.set(cv2.CAP_PROP_POS_MSEC, video_start_offset*1000.)

    logger.info('WxH = {}x{}'.format(W,H))
    logger.info('FPS = {}'.format(fps))

    # INITIALIZE TRACKING STREAM
    if tracking is not None:
        if not isinstance(tracking, pd.DataFrame):
            if tracking is not None and not os.path.isfile(tracking):
                raise IOError('Could not find tracking file: {}.'.format(tracking))
            df_tracking = load_tracking_file(tracking)
        else:
            df_tracking = tracking

        if df_tracking.index.name == 'frame':
            logger.debug('Found frame index in tracking; convert to time index using video fps: {}.'.format(fps))
            # convert frame index to time index
            # we assume that the tracking frame rate is the same as the video
            df_tracking = convert_tracking_frame_index_to_time_index(df_tracking, fps)

        if tracking_start_offset > 0:
            logger.debug('Start of tracking offset by {}.'.format(tracking_start_offset))
            df_tracking.index -= tracking_start_offset

        if scale:
            logger.debug('Scaling track positions by video resolution (W, W*1.125).')
            df_tracking = scale_track_position(df_tracking, [W, W*1.125]) # note we use W twice

        print df_tracking.head()

        # df_tracking.loc[:, (slice(None), 'Y')] = H - df_tracking.loc[:, (slice(None), 'Y')]
        fps_tracking = get_tracking_frame_rate(df_tracking)
        logger.debug('Detected tracking fps = {}.'.format(fps_tracking))

        if abs(fps - fps_tracking) > .1:
            logger.warning('The frame rates of the video and the tracking do not match (video: {}, tracking: {}).'.format(fps, fps_tracking))

            # TODO: interpolate/sample tracking to match video frame rate

        draw_tracking = True
    else:
        draw_tracking = False


    # INITIALIZE VIDEO CANVAS
    pygame.display.set_caption('Video Player')
    screen = pygame.display.set_mode((W,H))
    background = pygame.Surface(screen.get_size())
    background.fill((0, 0, 0))
    screen.blit(background, (0, 0))

    # INITIALIZE TEXT OVERLAYS
    # Initialize the font system and create the font and font renderer
    pygame.font.init()
    default_font = pygame.font.get_default_font()
    default_fps_size = 18
    fps_font_renderer = pygame.font.Font(default_font, default_fps_size)
    # create default speed label
    # fps_label_color = pygame.Color(76,  114,  176, 255)  # deep blue
    fps_label_color = pygame.Color(196,   78,   82, 255) # deep red
    fps_label = fps_font_renderer.render('{:.2f}x'.format(1), 1, fps_label_color)

    time_renderer = pygame.font.Font(default_font, default_fps_size)
    time_label_color = pygame.Color(255,255,255,255)
    time_label = time_renderer.render('{}'.format(0), 1, time_label_color)

    # INITIALIZE IMAGE EXPORT
    if export:
        try:
            export_folder = 'export_{}'.format(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
            mkdir_p(export_folder)
        except IOError:
            raise

    # INITIALIZE PLAYBACK CONTROL
    playing = True
    paused = False
    fps_multiplier = 1.

    if draw_tracking:
        track_iter = df_tracking.iterrows()
        t_track = -1.
        pos_track = None

        track_names = sorted(set([c[:-1] for c in df_tracking.columns.to_native_types()]))
        logger.debug('Found the following track names: {}'.format(track_names))
        colors = [DEEPRED, DEEPBLUE, DEEPRED, DEEPGREEN, DEEPBLUE, DEEPGREEN]
        tracking_draw_items = TrackingMarkers( create_markers(track_names, colors) )

    iframe = 0
    while playing and cap.isOpened():

        # EVENT HANDLING
        for event in pygame.event.get():
            if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and (event.unicode == 'q' or event.key == pygame.K_ESCAPE):
                playing = False
                break

            elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                paused = ~paused
                logger.debug('Paused' if paused else 'Unpaused')

            elif event.type == pygame.KEYDOWN and event.unicode == '-':
                fps_multiplier = max(.25, fps_multiplier - .25)
                logger.debug('Current FPS = {}'.format(fps*fps_multiplier))

                # create speed label
                fps_label = fps_font_renderer.render('{:.2f}x'.format(fps_multiplier), 1, fps_label_color)

            elif event.type == pygame.KEYDOWN and event.unicode == '+':
                fps_multiplier = min(2., fps_multiplier + .25)
                logger.debug('Current FPS = {}'.format(fps*fps_multiplier))

                # create speed label
                fps_label = fps_font_renderer.render('{:.2f}x'.format(fps_multiplier), 1, fps_label_color)

            # elif event.type == pygame.KEYDOWN:
            #     logger.debug('Pressed: {}'.format(pygame.key.name(event.key)))

        # READ NEXT FRAME AND SHOW
        if not paused:

            # read from capture
            flag, frame = cap.read()
            video_time = cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.

            if not flag:
                playing = False
                continue

            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            # convert to pygame
            pygframe = pygame.image.frombuffer(frame, (W, H), "RGB")

            # throw on canvas
            screen.blit(pygframe, (0, 0))


            if draw_tracking:
                try:
                    # synchronize tracking with video
                    while video_time - t_track > 1./(2.*fps):
                        # if track is behind, get next track frame
                        t_track, pos_track = track_iter.next()

                    tracking_draw_items.update(pd.DataFrame(pos_track).unstack('axis'))
                    tracking_draw_items.sprite_group.draw(screen)
                except StopIteration:
                    logger.debug('No further tracking information. Stop drawing tracking markers.')
                    draw_tracking = False

            # draw current playback speed
            screen.blit(fps_label, (W-75, 15))

            # draw current time
            time_label = time_renderer.render('{:.1f}s'.format(video_time), 1, time_label_color)
            screen.blit(time_label, (10, H-20))

        # update display and wait to ensure correct fps
        pygame.display.update()
        if export:
            # pygame.image.save(screen, os.path.join(export_folder, '{:06d}_{:.5f}.jpg'.format(iframe, video_time)))
            pygame.image.save(screen, os.path.join(export_folder, '{:06d}.jpg'.format(iframe)))

        iframe += 1
        clock.tick(fps * fps_multiplier)

    cap.release()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Video player with tracking overlay',
                                     epilog='Example: python player.py test_crim.avi test_crim.csv')
    parser.add_argument('video', help='Input video file')
    parser.add_argument('tracks', default='', help='Input tracking file')
    parser.add_argument('-ss', '--start_video', default=0, type=float, help='Start time of video (offset seconds)')
    parser.add_argument('-to', '--tracking_offset', default=0, type=float, help='Offset of tracking with respect to video (in seconds)')
    parser.add_argument('-s', '--scale', action='store_true', help='Set to multiply all tracking values by video frame size.')
    parser.add_argument('--exportimgs', action='store_true', help='Write all output frames to temporary folder.')
    parser.add_argument('-v', '--verbose', help='Set logging level to debug', action='store_true')
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format=_LOGFORMAT, datefmt=_LOGDATEFORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=_LOGFORMAT, datefmt=_LOGDATEFORMAT)

    play(args.video, args.tracks if len(args.tracks) > 0 else None, video_start_offset=args.start_video,
         tracking_start_offset=args.tracking_offset, scale=args.scale, export=args.exportimgs)
    pygame.quit()
