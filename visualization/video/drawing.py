# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-05-20 12:27:39
# @Last Modified by:   MAL
# @Last Modified time: 2016-06-09 11:39:18


import pygame
import pandas as pd
import logging
from itertools import cycle


TRANSPARENT = (255, 192, 203)
DEEPBLUE = pygame.Color(76,  114,  176, 128)  # deep blue
DEEPRED = pygame.Color(196,   78,   82, 128)  # deep red
DEEPGREEN = pygame.Color(85,  168,  104, 128)  # deep green

# check for formatting options: https://docs.python.org/2/library/logging.html#logrecord-attributes
_LOGFORMAT = '%(asctime)-9s %(levelname)-6s %(message)s'
_LOGDATEFORMAT = '%H:%M:%S'


class Marker(pygame.sprite.Sprite):
    def __init__(self, color, radius):
        pygame.sprite.Sprite.__init__(self)

        self.color = color
        self.radius = radius

        width = 2*radius
        height = width

        # Set the background color and set it to be transparent
        self.image = pygame.Surface([width, height])
        self.image.fill(TRANSPARENT)
        self.image.set_colorkey(TRANSPARENT)

        # Draw the ellipse
        pygame.draw.ellipse(self.image, color, [0, 0, width, height])

        # Fetch the rectangle object that has the dimensions of the image.
        # Update the position of this object by setting the values of rect.x and rect.y
        self.rect = self.image.get_rect()

    def update_pos(self, u, v):
        self.rect.x = u
        self.rect.y = v



class TrackingMarkers():

    def __init__(self, markers=None):
        logging.basicConfig(level=logging.DEBUG, format=_LOGFORMAT, datefmt=_LOGDATEFORMAT)
        self.logger = logging.getLogger('TrackingMarkers')

        self.markers = {}
        self.sprite_group = pygame.sprite.Group()

        for name, marker in markers.viewitems():
            self.add(name, marker)

    def add(self, name, marker):
        if isinstance(marker, Marker):
            self.markers[name] = marker
            self.sprite_group.add(marker)

    def add_tuple(self, marker):
        if isinstance(marker, tuple) and len(marker) > 1:
            self.add(marker[0], marker[1])

    def remove(self, name):
        self.sprite_group.remove(self.markers[name])
        del self.markers[name]

    def size(self):
        return len(self.markers)

    def update(self, df_pos):
        for name, uv in df_pos.iterrows():
            if not uv.isnull().all() and name in self.markers:
                self.markers[name].update_pos(*uv)

    def group(self):
        return self.sprite_group



def create_markers(names, colors=[DEEPBLUE, DEEPGREEN, DEEPRED], radius=5):

    markers = {}
    colors = cycle(colors)
    for i, n in enumerate(names):
        # if isinstance(colors, (list, tuple)):
        #     color = colors[i]
        # else:
        #     color = colors
        color = colors.next()
        markers[n] = Marker(color, radius)

    return markers
