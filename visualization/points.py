# -*- coding: utf-8 -*-
# @Author: MAL
# @Date:   2016-02-26 14:04:31
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-10-21 14:00:02

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D

from itertools import cycle

from sklearn.preprocessing import LabelEncoder


##############################################################################


def scatter2d(df, feature_names=None, label_col='label', label_colors=None, marker=None, ax=None, size_col=None, plot_means=False, force_colors=False,
              **kwargs):

    sns.set('talk', 'whitegrid', 'Set1', rc={'grid.linestyle':':'})

    return _scatter2d(df, feature_names, label_col, label_colors, marker,
                      ax, size_col, plot_means, force_colors, **kwargs)


def scatter2d_subplots(dfs, feature_names=None, label_col='label', label_colors=None, markers=None, col_wrap=2, size_col=None, plot_means=False, force_colors=False, **kwargs):

    if not isinstance(dfs, (list, tuple)):
        raise TypeError('dfs must be iterable of DataFrames/Series but is of type {}'.format(type(dfs)))

    N_plots = len(dfs)

    sns.set('talk', 'whitegrid', 'Set1', rc={'grid.linestyle':':'})

    n_cols = min(col_wrap, N_plots)
    n_rows = max(1, int(np.ceil(N_plots / float(n_cols))))
    fig, axs = plt.subplots(n_rows, n_cols, sharex=True, sharey=True,
                            figsize=(16,5*n_rows+1))

    for i, df in enumerate(dfs):
        ax = axs.flat[i]
        _scatter2d(df, feature_names,
                   label_col, label_colors,
                   markers[i], ax=ax, size_col=size_col,
                   plot_means=plot_means, force_colors=force_colors, **kwargs)

    return fig, axs


def _scatter2d(df, feature_names=None, label_col='label', label_colors=None,
               marker=None, ax=None, size_col=None, plot_means=False, force_colors=False, **kwargs):

    if feature_names is None:
        f0, f1 = [f for f in df.columns if f != label_col][:2]
    else:
        f0, f1 = feature_names[:2]

    if ax is None:
        fig = plt.figure(figsize=(8, 5))
        ax = fig.gca()
    else:
        fig = ax.get_figure()

    if not force_colors:
        le = LabelEncoder().fit(df[label_col])
        n_classes = len(le.classes_)
        labels = range(n_classes)
    else:
        labels = sorted(df[label_col].unique())
        n_classes = len(labels)

    if label_colors is None:
        label_colors = sns.color_palette('Set1', n_classes)
    label_colors = np.asarray(label_colors)

    if marker is None:
        marker = 'o'

    if size_col is not None and size_col not in df and not isinstance(size_col, (float, int)):
            size_col = None

    if isinstance(marker, (tuple, list)) and len(marker) > 1:

        markers = cycle(marker)
        scatters = []

        for l in labels:
            if force_colors:
                mask = df[label_col] == l
            else:
                mask = (le.transform(df[label_col]) == l)
            marker = markers.next()

            if size_col is None:
                marker_size = 50
            else:
                if size_col in df.columns:
                    marker_size = df.loc[mask, size_col].values
                else:
                    marker_size = size_col

            sc = ax.scatter(df.loc[mask, f0], df.loc[mask, f1],
                            c=label_colors[l], s=marker_size,
                            marker=marker, **kwargs)
            scatters.append(sc)

            if plot_means:
                ax.scatter(df.loc[mask, f0].mean(), df.loc[mask, f1].mean(),
                           c=label_colors[l], marker=marker,
                           s=mask.sum()*2, alpha=.7)

    else:
        if force_colors:
            colors = label_colors.take(df.loc[:, label_col], axis=0)
        else:
            colors = label_colors.take(le.transform(df.loc[:, label_col]),
                                       axis=0)
        if size_col is None:
            marker_size = 50
        else:
            if size_col in df.columns:
                marker_size = df.loc[:, size_col].values
            else:
                marker_size = size_col

        scatters = ax.scatter(df.loc[:, f0], df.loc[:, f1],
                              c=colors, s=marker_size,
                              marker=marker, **kwargs)
        if plot_means:
            ax.scatter(*df.groupby(label_col)[f0, f1].mean().values.T,
                       c=colors, marker=marker, alpha=.7, s=500)

    ax.set_xlabel(f0)
    ax.set_ylabel(f1)

    return fig, ax, scatters


##############################################################################


def joint2d(df, df2, feature_names=None, label_col='label', label_colors=None, marker=None, size_col=None):
    sns.set('talk', 'whitegrid', color_codes=True, rc={'grid.linestyle': ':'})

    return _joint2d(df, df2, feature_names, label_col, label_colors, marker, size_col)


def _joint2d(df, df2=None, feature_names=None, label_col='label', label_colors=None, marker=None,
             size_col=None):

    if feature_names is None:
        f0, f1 = [f for f in df.columns if f != label_col][:2]
    else:
        f0, f1 = feature_names[:2]

    markers = cycle(marker)

    le = LabelEncoder().fit(df[label_col])
    n_labels = len(le.classes_)
    labels = le.transform(df[label_col])

    g = sns.JointGrid(f0, f1, df.loc[labels==0], size=8)

    for l in range(n_labels):

        # SOURCE DATA
        g.x = df.loc[labels==l, f0]
        g.y = df.loc[labels==l, f1]

        if size_col is not None:
            swl = 10*df.loc[labels==l, size_col].values+10
        else:
            swl = 25
        g.plot_joint(plt.scatter, c=label_colors[l], s=swl, alpha=.35, marker=markers.next(), label='Source {}'.format(le.inverse_transform(l)))
        g.plot_marginals(sns.kdeplot, color=label_colors[l])


    le2 = LabelEncoder().fit(df2[label_col])
    n_labels2 = len(le2.classes_)
    labels2 = le2.transform(df2[label_col])


    for l in range(n_labels2):

        g.x = df2.loc[labels2==l, f0]
        g.y = df2.loc[labels2==l, f1]

        if size_col is not None:
            swl = 10*df2.loc[labels2==l, size_col].values+10
        else:
            swl = 25
        g.plot_joint(plt.scatter, c=label_colors[l], s=swl, alpha=.35, marker=markers.next(), label='Target {}'.format(le2.inverse_transform(l)))
        g.plot_marginals(sns.kdeplot, color=label_colors[l], linestyle='--')

    # FIX LEGENDS
    g.ax_marg_x.legend_.remove()
    g.ax_marg_y.legend_.remove()
    g.ax_joint.legend(loc='lower left'); #bbox_to_anchor=(1.2,1.2),

    return plt.gcf(), g.ax_joint


##############################################################################


def scatter3d(df, feature_names=None, label_col='label', label_colors=None, marker=None, ax=None):

    sns.set('talk', 'whitegrid', 'Set1', rc={'grid.linestyle':':'})

    return _scatter3d(df, feature_names, label_col, label_colors, marker, ax)


def _scatter3d(df, feature_names=None, label_col='label', label_colors=None, marker=None, ax=None):


    if feature_names is None:
        f0, f1, f2 = [f for f in df.columns if f != label_col][:3]
    else:
        f0, f1, f2 = feature_names[:3]

    if ax is None:
        fig = plt.figure(figsize=(8, 5))
        # ax = fig.gca()
        ax = plt.subplot(111, projection='3d')
    else:
        fig = ax.get_figure()

    le = LabelEncoder().fit(df[label_col])
    n_classes = len(le.classes_)

    if label_colors is None:
        label_colors = sns.color_palette('Set1', n_classes)
    label_colors = np.asarray(label_colors)

    if marker is None:
        marker = 'o'
    if not isinstance(marker, (tuple, list)):
        marker = [marker]

    markers = cycle(marker)

    for i in range(n_classes):
        mask = (le.transform(df[label_col]) == i)

        # ax.scatter(df.loc[mask, f0], df.loc[mask, f1], c=label_colors[i], marker=markers.next(), alpha=.6)

        ax.plot(df.loc[mask, f0], df.loc[mask, f1], df.loc[mask, f2], marker=markers.next(),
                color=label_colors[i], alpha=.6, linestyle='', markersize=3)

    # ax.set_xlabel(f0)
    # ax.set_ylabel(f1)

    return fig, ax


##############################################################################
##############################################################################
