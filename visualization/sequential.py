# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 11:00:26 2015

@author: MAL
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker
import seaborn as sns
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
from itertools import permutations

from utils import get_some_colors



def plot_transition_graph(transition_matrix, normed=True, hide_edge_threshold=None, state_names=None, ax=None):
    '''
    Plots a state transition matrix as a directed graph with edge labels corresponding to the states' transition value.

    Parameters
    ----------

    transition_matrix : numpy 2d-array
        Matrix containing the transition values from states i to states j: M[i,j].

    normed : bool, default: True
        Whether the transition matrix contains normed (True) or absolute (False) transition frequencies.

        If False, the size of the nodes will correspond to the absolute number of occurrences of that state.

    hide_edge_threshold : numeric, optional
        If set, edges with transition values below this threshold are not drawn.

    state_names : str, optional
        String representation of the states are written inside the nodes.

    ax : matplotlib axis object (optional)
        If given, the graph is plotted inside this axis.


    Returns
    -------
    '''

    if not isinstance(transition_matrix, np.ndarray):
        raise TypeError('Transition_matrix must be a 2D numpy array but is of type {}.'.format(type(transition_matrix)))

    if transition_matrix.ndim != 2 or transition_matrix.shape[0] != transition_matrix.shape[1]:
        raise AttributeError('Transition_matrix must be a square matrix with two dimensions but is of shape {}'.format(transition_matrix.shape))

    if hide_edge_threshold is None:
        hide_edge_threshold = np.min(transition_matrix)

    if state_names is None or len(state_names) < transition_matrix.shape[0]:
        state_names = ['%d'%d for d in xrange(transition_matrix.shape[0])]

    n_states = len(state_names)

    # Construct graph
    G = nx.DiGraph()

    if normed:
        G.add_nodes_from(state_names, size=10, labels=state_names)

        T = transition_matrix
        node_size = 1500
    else:
        node_size = np.fmin(10000, np.fmax(400 , np.sum(transition_matrix, axis=1)))

        for i in xrange(n_states):
            G.add_node(state_names[i], size=node_size[i], weight=node_size[i])

        T = transition_matrix / np.sum(transition_matrix, axis=1, keepdims=True, dtype=np.float)


    edge_labels = {}
    for i,j in permutations(xrange(n_states), r=2):
        if T[i,j] >= hide_edge_threshold:
            G.add_edge(state_names[i],state_names[j], {'weight':T[i,j], 'label':'{:.2f}'.format(T[i,j])})
            edge_labels[ (state_names[i],state_names[j]) ] = '{:.2f}'.format(T[i,j])


    # Plot graph
    if ax is None:
        fig = plt.figure(figsize=(8,8), frameon=False)
        ax = fig.gca()
    else:
        fig = ax.get_figure()

    # clrs = get_some_colors(n_states)
    clrs = sns.color_palette('hls', n_colors=n_states)

    # let graphviz do the layout
    pos = graphviz_layout(G, prog='neato')
    # pos = nx.spring_layout(G)

    # plot graph and edge labels (explicitely specify nodelist, so that order in node_size matches)
    drawn_nodes = nx.draw_networkx_nodes(G, pos=pos, nodelist=state_names, node_size=node_size, node_color=clrs, linewidths=0, node_shape='o', ax=ax)
    # uncomment to set node edge color:
    #drawn_nodes.set_edgecolor('k')
    nx.draw_networkx_labels(G, pos, ax=ax, font_color='w', font_size=12)

    # get order of edges to be drawn so that we can set width and colors accordingly:
    edge_list = edge_labels.keys()
    edge_widths = [40.*T[ state_names.index(i), state_names.index(j) ] for (i,j) in edge_list]
    edge_vals = [T[ state_names.index(i), state_names.index(j) ] for (i,j) in edge_list]

    nx.draw_networkx_edges(G, pos, ax=ax, arrows=False, edgelist=edge_list, width=edge_widths, alpha=.5, edge_color=edge_vals, edge_vmin=0, edge_vmax=1., edge_cmap=plt.cm.jet)
    nx.draw_networkx_edges(G, pos, ax=ax, arrows=True, width=1, alpha=.5)
    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, label_pos=0.4, font_size=8, ax=ax)

    # remove grid, axes ticks etc
    ax.grid(False)
    ax.set_xticklabels([])
    ax.set_yticklabels([])

    return fig, ax, G



def plot_event_plot(data, show_range=None, class_dict=None, colors=None, abs_dist=0.6, labels=None, ax=None, as_lines=False):
    '''
    Plots event plot with vertical lines as frame events.
    '''

    if isinstance(data, pd.Series):
        data = pd.DataFrame(data)

    if isinstance(data, pd.DataFrame):
        columns = data.columns
    else:
        columns = np.arange(data.shape[1])

    if colors is None:
        colors = get_some_colors(n=len(columns))

    if labels is None:
        labels = columns

    if ax is None:
        fig, ax = plt.subplots(1, 1, facecolor='white')
    else:
        fig = ax.get_figure()


    rel_dist = abs_dist/len(columns)
    pos_offset = ((np.arange( len(columns) ) - (len(columns)-1)/2.)*rel_dist)[::-1]

    plot_range = slice(None)
    if show_range is not None:
        plot_range = slice(*show_range)

    if isinstance(data, pd.DataFrame):
        for i, column in enumerate(columns):
            if as_lines:
                ax.step(data.loc[plot_range].index, data.loc[plot_range, column].values + pos_offset[i], '-', linewidth=10, alpha=.8, color=colors[i], markeredgecolor=colors[i], label=labels[i])
            else:
                ax.plot(data.loc[plot_range].index, data.loc[plot_range, column].values + pos_offset[i], '|', color=colors[i], markeredgecolor=colors[i], label=labels[i])
    else:
        for i, column in enumerate(columns):
            ax.plot( data[plot_range, column] + pos_offset[i], '|', color=colors[i], markeredgecolor=colors[i], label=labels[i])


    ax.legend(numpoints=1, fancybox=True, bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0.)

    if class_dict is not None and isinstance(class_dict, dict):
        ytickmajorlocator = matplotlib.ticker.FixedLocator(locs=sorted(class_dict.keys()))
        ax.yaxis.set_major_locator(ytickmajorlocator)
        ax.yaxis.set_ticklabels([ class_dict[k] for k in sorted(class_dict.keys()) ])

    plt.xlabel('frame')

    return fig, ax
