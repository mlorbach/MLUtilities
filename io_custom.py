# -*- coding: utf-8 -*-
"""
Created on Tue Aug 19 10:44:39 2014

@author: MAL
"""

import os, errno
import tempfile


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def dump_to_temp(string, prefix=''):

    with tempfile.NamedTemporaryFile(mode='w', prefix=prefix, delete=False) as fp:
        fname = fp.name
        fp.write(string)

    return fname


def read_and_delete_temp(fname, mode='r'):

    with open(fname, mode) as fp:
        string = fp.read()

    if os.path.realpath(fname).startswith(tempfile.gettempdir()):
        os.unlink(fname)

    return string
